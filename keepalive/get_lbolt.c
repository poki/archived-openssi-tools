/*
 *	time routines.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
 *
 */
#include <limits.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/times.h>

time_t get_date();


/*                  Function get_lbolt()
 *
 * Description
 *
 *              Retrieve the value of lbolt from the kernel.
 *              Use the times(buffer) function.
 *
 * Interface
 *
 * clock_t get_lbolt()
 *
 *
 * Returns
 *
 * The lbolt value.
 *
*/

clock_t
get_lbolt()
{
/*

	if( (t_lbolt = times(&t)) == -1){
		return(-1);
	}
	else
		return(t_lbolt / CLK_TCK);
*/

	return get_date();
}


/*                  Function get_date()
 *
 * Description
 *
 * Retrieve the date in seconds from the beginning of the epic.
 *
 * Returns
 *
 * Time in seconds from the beginning of the epic.
 *
*/

time_t
get_date()
{
	return time(NULL);
}
