/*
 *	print routines.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
 *
 */
/* $Id: printprocs.c,v 1.3 2003/04/22 00:01:29 jlbyrne Exp $ */

#include <sys/syslog.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "keepalive.h"
#include "spawndaemon.h"

#define gettxt(a, b)	(b)

extern process_t   *pprocs; 
extern kalive_ctl_t *pkctl;
extern int ka_com_succeeded;
extern char *msg_catalog;

char *state_names[ST_MAX] = {
"empty",
"start",
"ok",
"dead",
"down",
"respawn",
"shutdown",
"daemonize"
};


/*
 * This file can be compiled two ways:
 *     If SYSLOG_PRINT is defined, then printprocs() sends its output
 *      to the syslogd using syslog()
 *     If LOCAL_PRINT is defined, then printprocs() sends its output
 *      to stdout.
 *
 * If neither is defined, the code will not compile.
 */

#if defined( SYSLOG_PRINT )
#define PRINT  syslog( LOG_INFO,
#endif

#if defined( LOCAL_PRINT )
#define  PRINT  printf(
#endif

FILE  *out_file = NULL;
FILE  *dev_con = NULL;

void terse_output(int localprint);
void verbose_human_daemons_output();
void verbose_human_keepalive_output();
void verbose_machine_daemons_output();
void verbose_machine_keepalive_output();
void wait_for_lock(process_t *mmapped_proc, process_t *a_proc);

/*                        Function printprocs()                    
 *                                                                 
 * Description                                                     
 *                                                                 
 * The printprocs() routine prints the daemons. It sends its output
 * to syslogd using syslog() and to stdout.                        
 *                                                                 
 * Interface                                                       
 *                                                                
 * void printprocs()                                               
 *                                                                 
 * Returns                                                         
 *                                                                 
 * Nothing                                                         
*/

void
printprocs(int localprint, print_object_t print_object, verbosity_t verbosity)
{
  if (!localprint) {
    dev_con = fopen("/dev/console", "a");
    out_file = dev_con;
  }
  else {
    out_file = stdout;
  }

  if (verbosity == NO_VERBOSITY) {
    terse_output(localprint);
  }
  else if (verbosity == MACHINE) {
    if (print_object == PRINT_DAEMONS) {
      verbose_machine_daemons_output();
    }
    else {
      verbose_machine_keepalive_output(print_object);
    }
  }
  else if (verbosity == HUMAN) {
    if (print_object == PRINT_DAEMONS) {
      verbose_human_daemons_output();
    }
    else {
      verbose_human_keepalive_output(print_object);
    }
  }
}

void
terse_output(int localprint)
{
  int i;
  int  n_active;
  char  tbuf[30];
  clusternode_t node;
  char node_string[5] = "";
  char node_set_string[30];
  char buffer[5];
  int j;
  process_t a_proc;

  n_active = 0;
  for(i = 0; i < pkctl->registered_daemons; i++) {
    if (pprocs[i].state < ST_MAX) {
      if (0 == n_active++) {
        generic_msg = gettxt(":1", "Keepalive daemon status\n");
        fprintf(out_file, generic_msg);

        format_msg = gettxt(":2", "%-5s ");
        generic_msg = gettxt(":3", "slot");
        fprintf(out_file, format_msg, generic_msg);

        format_msg = gettxt(":4", "%-7s ");
        generic_msg = gettxt(":5", "state");
        fprintf(out_file, format_msg, generic_msg);

        format_msg = gettxt(":6", "%-7s ");
        generic_msg = gettxt(":7", "pid");
        fprintf(out_file, format_msg, generic_msg);

        format_msg = gettxt(":8", "%-5s ");
        generic_msg = gettxt(":9", "errs");
        fprintf(out_file, format_msg, generic_msg);

        format_msg = gettxt(":10", "%-6s ");
        generic_msg = gettxt(":11", "total");
        fprintf(out_file, format_msg, generic_msg);

        format_msg = gettxt(":12", "%-4s ");
        generic_msg = gettxt(":13", "max");
        fprintf(out_file, format_msg, generic_msg);

        format_msg = gettxt(":14", "%-25s ");
        generic_msg = gettxt(":15", "last_exec_time");
        fprintf(out_file, format_msg, generic_msg);

        format_msg = gettxt(":16", "%-5s ");
        generic_msg = gettxt(":17", "node");
        fprintf(out_file, format_msg, generic_msg);

        format_msg = gettxt(":18", "%-15s ");
        generic_msg = gettxt(":19", "node_set");
        fprintf(out_file, format_msg, generic_msg);

        format_msg = gettxt(":20", "%s\n");
        generic_msg = gettxt(":21", "name (args at startup)");
        fprintf(out_file, format_msg, generic_msg);
      } 

      /* kill trailing '\n' */
      safe_strncpy(tbuf, ctime(&pprocs[i].lastexeced_secs), sizeof(tbuf));
      if (strlen(tbuf)) {
         tbuf[strlen(tbuf)-1]='\0';
      }

      /* get current node */
      if ((node = node_pid(pprocs[i].pid)) == -1) {
        safe_strncpy(node_string, "-", sizeof(node_string));
      }
      else {
        sprintf(node_string, "%u", node);
      }

      /* convert node list to string */
      node_set_string[0] = '\0';
      if (pprocs[i].node != 0) {
        if (clusternode_avail(pprocs[i].node) == 1) {
          sprintf(buffer, "%u ", pprocs[i].node);
        }
        else {
          sprintf(buffer, "- ");
        }
        strcat(node_set_string, buffer); 
        for (j = 0; (j < MAX_NODES) && (pprocs[i].backup_nodes[j] != 0); j++) {
          if (clusternode_avail(pprocs[i].backup_nodes[j]) == 1) {
            sprintf(buffer, "%u ", pprocs[i].backup_nodes[j]);  
          }
          else {
            sprintf(buffer, "- ");
          }
          strcat(node_set_string, buffer);
        } 
      }

      wait_for_lock(&pprocs[i], &a_proc);
      format_msg = gettxt(":22", "%-5d %-7s %-7ld %-5d %-6d %-4d %-25s %-5s %-15s %s %s\n");
      fprintf(out_file, format_msg,
        i, state_names[a_proc.state], a_proc.pid, 
        a_proc.nerrors, a_proc.total_errors, a_proc.maxerrors,
        tbuf, node_string, node_set_string, a_proc.name, a_proc.args);
    } 
  }
  if (0 == n_active) {
    syslog_msg = gettxt(":344", "keepalive daemon status -- no active entries\n");

    fprintf(out_file, syslog_msg);
    if (!localprint) {
      syslog(LOG_INFO, syslog_msg);
    }
  }

  if (!localprint) {
    fclose(dev_con);
  }
}

void
verbose_machine_daemons_output()
{
  int i, j;
  char  tbuf[30];
  int node_num;
  process_t a_proc;

  for(i = 0; i < pkctl->registered_daemons; i++) {
    wait_for_lock(&pprocs[i], &a_proc);
    if (a_proc.state < ST_MAX) {
      generic_msg = gettxt(":23", "state=\"%s\";");
      fprintf(out_file, generic_msg, state_names[a_proc.state]);

      if (kill(a_proc.pid, 0) < 0) {
        generic_msg = gettxt(":24", "pid=\"None\";");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":25", "pid=\"%d\";");
        fprintf(out_file, generic_msg, a_proc.pid);
      }

      node_num=node_pid(a_proc.pid);
      if (node_num < 0) {
        generic_msg = gettxt(":26", "node_number=\"None\";");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":27", "node_number=\"%d\";");
        fprintf(out_file, generic_msg, node_num);
      }

      generic_msg = gettxt(":28", "full_path_to_process=\"%s\";");
      fprintf(out_file, generic_msg, a_proc.name);

      generic_msg = gettxt(":29", "arg_list=\"%s\";");
      fprintf(out_file, generic_msg, a_proc.args);

      if (a_proc.adoption_status == ADOPTED) {
        generic_msg = gettxt(":30", "child_of_keepalive=TRUE;");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":31", "child_of_keepalive=FALSE;");
        fprintf(out_file, generic_msg);
      }

      if (a_proc.daemonization_recovery) {
        generic_msg = gettxt(":32", "daemonization_recovery=TRUE;");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":33", "daemonization_recovery=FALSE;");
        fprintf(out_file, "daemonization_recovery=FALSE;");
      }

      if (a_proc.pin_anywhere || a_proc.node) {
        generic_msg = gettxt(":34", "pinned=TRUE;");
        fprintf(out_file, "pinned=TRUE;");
      }
      else {
        generic_msg = gettxt(":35", "pinned=FALSE;");
        fprintf(out_file, "pinned=FALSE;");
      }

      /* kill trailing '\n' */
      safe_strncpy(tbuf, ctime(&a_proc.lastexeced.date), sizeof(tbuf));
      if (strlen(tbuf)) {
         tbuf[strlen(tbuf)-1]='\0';
      }

      generic_msg = gettxt(":36", "lastexeced=\"%s\";");
      fprintf(out_file, generic_msg, tbuf);

      if (a_proc.daemon_first_died.date != 0) {
        /* kill trailing '\n' */
        safe_strncpy(tbuf, ctime(&a_proc.daemon_first_died.date), sizeof(tbuf));
        if (strlen(tbuf)) {
          tbuf[strlen(tbuf)-1]='\0';
        }

        generic_msg = gettxt(":37", "process_first_died=\"%s\";");
        fprintf(out_file, generic_msg, tbuf);
      }
      else {
        generic_msg = gettxt(":38", "process_first_died=\"Never\";");
        fprintf(out_file, generic_msg);
      }

      if (a_proc.daemon_last_died.date != 0) {
        /* kill trailing '\n' */
        safe_strncpy(tbuf, ctime(&a_proc.daemon_last_died.date), sizeof(tbuf));
        if (strlen(tbuf)) {
          tbuf[strlen(tbuf)-1]='\0';
        }

        generic_msg = gettxt(":39", "process_last_died=\"%s\";");
        fprintf(out_file, generic_msg, tbuf);
      }
      else {
        generic_msg = gettxt(":40", "process_last_died=\"Never\";");
        fprintf(out_file, generic_msg);
      }

      generic_msg = gettxt(":41", "minrespawn=%d;");
      fprintf(out_file, generic_msg, a_proc.minrespawn);

      generic_msg = gettxt(":42", "num_errors=%d;");
      fprintf(out_file, generic_msg, a_proc.nerrors);

      generic_msg = gettxt(":43", "total_errors=%d;");
      fprintf(out_file, generic_msg, a_proc.total_errors);

      generic_msg = gettxt(":44", "max_errors_during_probation=%d;");
      fprintf(out_file, generic_msg, a_proc.maxerrors);

      generic_msg = gettxt(":45", "probation_period=%d;");
      fprintf(out_file, generic_msg, a_proc.probation_period);

      if (a_proc.registration_policy == REGISTER_BY_PID) {
        generic_msg = gettxt(":47", "registration_policy=\"PID\";");
        fprintf(out_file, generic_msg);
      }
      else if (a_proc.registration_policy == REGISTER_BY_NAME) {
        generic_msg = gettxt(":48", "registration_policy=\"Name\";");
        fprintf(out_file, generic_msg);
      }
      else if (a_proc.registration_policy == REGISTER_BY_ARGS) {
        generic_msg = gettxt(":49", "registration_policy=\"Argument List\";");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":50", "registration_policy=\"Invalid\";");
        fprintf(out_file, generic_msg);
      }

      if (a_proc.restart_policy == LAST_NODE) {
        generic_msg = gettxt(":51", "node_selection_policy=\"LAST_NODE\";");
        fprintf(out_file, generic_msg);
      }
      else if (a_proc.restart_policy == F_NODE) {
        generic_msg = gettxt(":52", "node_selection_policy=\"FAVORED_NODE\";");
        fprintf(out_file, generic_msg);
      }
      else if (a_proc.restart_policy == ROUND_ROBIN) {
        generic_msg = gettxt(":53", "node_selection_policy=\"ROUND_ROBIN\";");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":54", "node_selection_policy=\"Invalid\";");
        fprintf(out_file, generic_msg);
      }

      if (a_proc.node == 0) {
        generic_msg = gettxt(":55", "favored_node=\"None\";");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":56", "favored_node=\"%d\";");
        fprintf(out_file, generic_msg, a_proc.node);
      }

      generic_msg = gettxt(":57", "backup_nodes=\"");
      fprintf(out_file, generic_msg);
      if (a_proc.backup_nodes[0] == 0) {
        generic_msg = gettxt(":58", "None");
        fprintf(out_file, "None");
      }
      else {
        for (j = 0; 
             (j < MAX_NODES) && (a_proc.backup_nodes[j] != 0); 
             j++) 
        {
          if (j == 0) {
            generic_msg = gettxt(":59", "%d");
            fprintf(out_file, generic_msg, a_proc.backup_nodes[j]);
          }
          else {
            generic_msg = gettxt(":60", ",%d");
            fprintf(out_file, generic_msg, a_proc.backup_nodes[j]);
          }
        }
      }
      generic_msg = gettxt(":61", "\";");
      fprintf(out_file, generic_msg);

      generic_msg = gettxt(":62", "rejected_nodes=\"");
      fprintf(out_file, generic_msg);
      if (a_proc.rejected_nodes[0].node_num == 0) {
        generic_msg = gettxt(":63", "None");
        fprintf(out_file, "None");
      }
      else {
        for (j = 0; 
             (j < MAX_NODES) && (a_proc.rejected_nodes[j].node_num != 0); 
             j++) 
        {
          if (j == 0) {
            generic_msg = gettxt(":64", "%d");
            fprintf(out_file, generic_msg, a_proc.rejected_nodes[j].node_num);
          }
          else {
            generic_msg = gettxt(":65", ",%d");
            fprintf(out_file, generic_msg, a_proc.rejected_nodes[j].node_num);
          }
        }
      }
      generic_msg = gettxt(":66", "\";");
      fprintf(out_file, generic_msg);

      generic_msg = gettxt(":67", "termwait=%d;");
      fprintf(out_file, generic_msg, a_proc.termwait);

      generic_msg = gettxt(":68", "euid=%d;");
      fprintf(out_file, generic_msg, a_proc.euid);

      generic_msg = gettxt(":69", "egid=%d;");
      fprintf(out_file, generic_msg, a_proc.egid);

      if (strcmp(a_proc.startup_script, "") == 0) {
        generic_msg = gettxt(":70", "startup_script=\"None\";");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":71", "startup_script=\"%s\";");
        fprintf(out_file, generic_msg, a_proc.startup_script);
      }

      if (strcmp(a_proc.shutdown_script, "") == 0) {
        generic_msg = gettxt(":72", "shutdown_script=\"None\";");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":73", "shutdown_script=\"%s\";");
        fprintf(out_file, generic_msg, a_proc.shutdown_script);
      }

      if (strcmp(a_proc.process_failure_script, "") == 0) {
        generic_msg = gettxt(":74", "process_failure_recovery_script=\"None\";");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":75", "process_failure_recovery_script=\"%s\";");
        fprintf(out_file, generic_msg, a_proc.process_failure_script);
      }

      if (strcmp(a_proc.node_failure_script, "") == 0) {
        generic_msg = gettxt(":76", "node_failure_recovery_script=\"None\";");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":77", "node_failure_recovery_script=\"%s\";");
        fprintf(out_file, generic_msg, a_proc.node_failure_script);
      }

      if (strcmp(a_proc.down_script, "") == 0) {
        generic_msg = gettxt(":78", "down_script=\"None\";");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":79", "down_script=\"%s\";");
        fprintf(out_file, generic_msg, a_proc.down_script);
      }

      if (strcmp(a_proc.group.name, "") == 0) {
        generic_msg = gettxt(":80", "group=\"None\";");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":81", "group=\"%s\";");
        fprintf(out_file, generic_msg, a_proc.group.name);
      }

      if (strcmp(a_proc.group.name, "") != 0) {
        if (a_proc.group.priority == 1) {
          generic_msg = gettxt(":82", "critical_group_process=\"TRUE\";");
          fprintf(out_file, generic_msg);
        }
        else {
          generic_msg = gettxt(":83", "critical_group_process=\"FALSE\";");
          fprintf(out_file, generic_msg);
        }
      }
      else {
        generic_msg = gettxt(":84", "critical_group_process=\"N/A\";");
        fprintf(out_file, generic_msg);
      }

      if (a_proc.reject_exit_code_used) {
        generic_msg = gettxt(":85", "reject_exit_code=\"%d\";");
        fprintf(out_file, generic_msg, a_proc.reject_exit_code);
      }
      else {
        generic_msg = gettxt(":86", "reject_exit_code=\"None\";");
        fprintf(out_file, generic_msg);
      }

      if (a_proc.down_exit_code_used) {
        generic_msg = gettxt(":87", "down_exit_code=\"%d\";");
        fprintf(out_file, generic_msg, a_proc.down_exit_code);
      }
      else {
        generic_msg = gettxt(":88", "down_exit_code=\"None\";");
        fprintf(out_file, generic_msg);
      }

      if ((a_proc.status_captured == ZOMBIE) && 
          WIFEXITED(a_proc.status)) 
      {
        generic_msg = gettxt(":89", "exit_status_returned=\"%d\";");
        fprintf(out_file, generic_msg, WEXITSTATUS(a_proc.status));
      }
      else {
        generic_msg = gettxt(":90", "exit_status_returned=\"None\";");
        fprintf(out_file, generic_msg);
      }

      if (a_proc.last_pid == -1) {
        generic_msg = gettxt(":91", "last_pid=\"None\";");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":92", "last_pid=\"%d\";");
        fprintf(out_file, generic_msg, a_proc.last_pid);
      }

      generic_msg = gettxt(":93", "slot=%d;");
      fprintf(out_file, generic_msg, i);

      /* Terminate record with a blank line. */
      generic_msg = gettxt(":94", "\n");
      fprintf(out_file, generic_msg);
    }
    else {
      break;
    }
  }
}

void
verbose_machine_keepalive_output()
{
  if (ka_com_succeeded) {
     generic_msg = gettxt(":95", "running=TRUE;");
     fprintf(out_file, generic_msg);
  }
  else {
    generic_msg = gettxt(":96", "running=FALSE;");
    fprintf(out_file, generic_msg);
  }

  if (pkctl->quiesce_flag) {
    generic_msg = gettxt(":97", "quiesce_flag=TRUE;");
    fprintf(out_file, "quiesce_flag=TRUE;");
  }
  else {
    generic_msg = gettxt(":98", "quiesce_flag=FALSE;");
    fprintf(out_file, "quiesce_flag=FALSE;");
  }

  if (ka_com_succeeded) {
    generic_msg = gettxt(":99", "pid=\"%d\";");
    fprintf(out_file, generic_msg, pkctl->primary_pid);

    generic_msg = gettxt(":100", "node_number=\"%d\";");
    fprintf(out_file, generic_msg, node_pid(pkctl->primary_pid));
  }
  else {
    generic_msg = gettxt(":101", "pid=\"None\";");
    fprintf(out_file, generic_msg);

    generic_msg = gettxt(":102", "node_number=\"None\";");
    fprintf(out_file, generic_msg);
  }

  generic_msg = gettxt(":103", "registered_processes=%d;");
  fprintf(out_file, generic_msg, pkctl->registered_daemons);

  generic_msg = gettxt(":104", "table_size=%d;");
  fprintf(out_file, generic_msg, pkctl->max_registered_daemons);

  generic_msg = gettxt(":105", "max_possible_processes=%d;");
  fprintf(out_file, generic_msg, pkctl->max_possible_daemons);

  if (pkctl->polling) {
    generic_msg = gettxt(":106", "polling=TRUE;");
    fprintf(out_file, generic_msg);
  }
  else {
    generic_msg = gettxt(":107", "polling=FALSE;");
    fprintf(out_file, generic_msg);
  }
  generic_msg = gettxt(":108", "polling interval=%d;");
  fprintf(out_file, generic_msg, pkctl->polling_interval);

  /* Terminate record with a blank line. */
  generic_msg = gettxt(":113", "\n");
  fprintf(out_file, generic_msg);
}

void
verbose_human_daemons_output()
{
  int i, j;
  char  tbuf[30];
  int node_num;
  process_t a_proc;

  for(i = 0; i < pkctl->registered_daemons; i++) {
    wait_for_lock(&pprocs[i], &a_proc);
    if (a_proc.state < ST_MAX) {
      generic_msg = gettxt(":114", "state: %s\n");
      fprintf(out_file, generic_msg, state_names[a_proc.state]);

      if (kill(a_proc.pid, 0) < 0) {
        generic_msg = gettxt(":115", "pid: None\n");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":116", "pid: %d\n");
        fprintf(out_file, generic_msg, a_proc.pid);
      }

      node_num=node_pid(a_proc.pid);
      if (node_num < 0) {
        generic_msg = gettxt(":117", "node number: None\n");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":118", "node number: %d\n");
        fprintf(out_file, generic_msg, node_num);
      }

      generic_msg = gettxt(":119", "full path to process: %s\n");
      fprintf(out_file, generic_msg, a_proc.name);

      generic_msg = gettxt(":120", "argument list: %s\n");
      fprintf(out_file, generic_msg, a_proc.args);

      if (a_proc.adoption_status == ADOPTED) {
        generic_msg = gettxt(":121", "child of keepalive: TRUE\n");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":122", "child of keepalive: FALSE\n");
        fprintf(out_file, generic_msg);
      }

      if (a_proc.daemonization_recovery) {
        generic_msg = gettxt(":123", "daemonization recovery: TRUE\n");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":124", "daemonization recovery: FALSE\n");
        fprintf(out_file, generic_msg);
      }

      /* kill trailing '\n' */
      safe_strncpy(tbuf, ctime(&a_proc.lastexeced.date), sizeof(tbuf));
      if (strlen(tbuf)) {
         tbuf[strlen(tbuf)-1]='\0';
      }

      generic_msg = gettxt(":125", "lastexeced: %s\n");
      fprintf(out_file, generic_msg, tbuf);

      if (a_proc.pin_anywhere || a_proc.node) {
        generic_msg = gettxt(":126", "pinned: TRUE\n");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":127", "pinned: FALSE\n");
        fprintf(out_file, generic_msg);
      }

      if (a_proc.daemon_first_died.date != 0) {
        /* kill trailing '\n' */
        safe_strncpy(tbuf, ctime(&a_proc.daemon_first_died.date), sizeof(tbuf));
        if (strlen(tbuf)) {
          tbuf[strlen(tbuf)-1]='\0';
        }

        generic_msg = gettxt(":128", "process first died: %s\n");
        fprintf(out_file, generic_msg, tbuf);
      }
      else {
        generic_msg = gettxt(":129", "process first died: Never\n");
        fprintf(out_file, generic_msg);
      }

      if (a_proc.daemon_last_died.date != 0) {
        /* kill trailing '\n' */
        safe_strncpy(tbuf, ctime(&a_proc.daemon_last_died.date), sizeof(tbuf));
        if (strlen(tbuf)) {
          tbuf[strlen(tbuf)-1]='\0';
        }

        generic_msg = gettxt(":130", "process last died: %s\n");
        fprintf(out_file, generic_msg, tbuf);
      }
      else {
        generic_msg = gettxt(":131", "process last died: Never\n");
        fprintf(out_file, generic_msg);
      }

      generic_msg = gettxt(":132", "min. respawn: %d\n");
      fprintf(out_file, generic_msg, a_proc.minrespawn);

      generic_msg = gettxt(":133", "num. errors: %d\n");
      fprintf(out_file, generic_msg, a_proc.nerrors);

      generic_msg = gettxt(":134", "total errors: %d\n");
      fprintf(out_file, generic_msg, a_proc.total_errors);

      generic_msg = gettxt(":135", "max. errors during probation: %d\n");
      fprintf(out_file, generic_msg, a_proc.maxerrors);

      generic_msg = gettxt(":136", "probation period: %d\n");
      fprintf(out_file, generic_msg, a_proc.probation_period);

      if (a_proc.registration_policy == REGISTER_BY_PID) {
        generic_msg = gettxt(":138", "registration policy: PID\n");
        fprintf(out_file, generic_msg);
      }
      else if (a_proc.registration_policy == REGISTER_BY_NAME) {
        generic_msg = gettxt(":139", "registration policy: Name\n");
        fprintf(out_file, generic_msg);
      }
      else if (a_proc.registration_policy == REGISTER_BY_ARGS) {
        generic_msg = gettxt(":140", "registration policy: Argument List\n");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":141", "registration policy: Invalid\n");
        fprintf(out_file, generic_msg);
      }

      if (a_proc.restart_policy == LAST_NODE) {
        generic_msg = gettxt(":142", "node selection policy: LAST_NODE\n");
        fprintf(out_file, generic_msg);
      }
      else if (a_proc.restart_policy == F_NODE) {
        generic_msg = gettxt(":143", "node selection policy: FAVORED_NODE\n");
        fprintf(out_file, generic_msg);
      }
      else if (a_proc.restart_policy == ROUND_ROBIN) {
        generic_msg = gettxt(":144", "node selection policy: ROUND_ROBIN\n");
        fprintf(out_file, generic_msg);
      }
      else if (a_proc.restart_policy == ROOT_NODE) {
        generic_msg = gettxt(":347", "node selection policy: ROOT_NODE\n");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":145", "node selection policy: Invalid\n");
        fprintf(out_file, generic_msg);
      }

      if (a_proc.node == 0) {
        generic_msg = gettxt(":146", "favored node: None\n");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":147", "favored node: %d\n");
        fprintf(out_file, generic_msg, a_proc.node);
      }

      generic_msg = gettxt(":148", "backup nodes: ");
      fprintf(out_file, generic_msg);
      if (a_proc.backup_nodes[0] == 0) {
        generic_msg = gettxt(":149", "None");
        fprintf(out_file, generic_msg);
      }
      else {
        for (j = 0; 
             (j < MAX_NODES) && (a_proc.backup_nodes[j] != 0); 
             j++) 
        {
          if (j == 0) {
            generic_msg = gettxt(":150", "%d");
            fprintf(out_file, generic_msg, a_proc.backup_nodes[j]);
          }
          else {
            generic_msg = gettxt(":151", ",%d");
            fprintf(out_file, generic_msg, a_proc.backup_nodes[j]);
          }
        }
      }
      generic_msg = gettxt(":152", "\n");
      fprintf(out_file, generic_msg);

      generic_msg = gettxt(":153", "rejected nodes: ");
      fprintf(out_file, generic_msg);
      if (a_proc.rejected_nodes[0].node_num == 0) {
        generic_msg = gettxt(":154", "None");
        fprintf(out_file, generic_msg);
      }
      else {
        for (j = 0; 
             (j < MAX_NODES) && (a_proc.rejected_nodes[j].node_num != 0); 
             j++) 
        {
          if (j == 0) {
            generic_msg = gettxt(":155", "%d");
            fprintf(out_file, "%u", a_proc.rejected_nodes[j].node_num);
          }
          else {
            generic_msg = gettxt(":156", ",%d");
            fprintf(out_file, ",%u", a_proc.rejected_nodes[j].node_num);
          }
        }
      }
      generic_msg = gettxt(":157", "\n");
      fprintf(out_file, "\n");

      generic_msg = gettxt(":158", "termwait: %d\n");
      fprintf(out_file, generic_msg, a_proc.termwait);

      generic_msg = gettxt(":159", "euid: %d\n");
      fprintf(out_file, generic_msg, a_proc.euid);

      generic_msg = gettxt(":160", "egid: %d\n");
      fprintf(out_file, generic_msg, a_proc.egid);

      if (strcmp(a_proc.startup_script, "") == 0) {
        generic_msg = gettxt(":161", "startup script: None\n");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":162", "startup script: %s\n");
        fprintf(out_file, generic_msg, a_proc.startup_script);
      }

      if (strcmp(a_proc.shutdown_script, "") == 0) {
        generic_msg = gettxt(":163", "shutdown script: None\n");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":164", "shutdown script: %s\n");
        fprintf(out_file, generic_msg, a_proc.shutdown_script);
      }

      if (strcmp(a_proc.process_failure_script, "") == 0) {
        generic_msg = gettxt(":165", "process failure recovery script: None\n");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":166", "process failure recovery script: %s\n");
        fprintf(out_file, generic_msg, a_proc.process_failure_script);
      }

      if (strcmp(a_proc.node_failure_script, "") == 0) {
        generic_msg = gettxt(":167", "node failure recovery script: None\n");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":168", "node failure recovery script: %s\n");
        fprintf(out_file, generic_msg, a_proc.node_failure_script);
      }

      if (strcmp(a_proc.down_script, "") == 0) {
        generic_msg = gettxt(":169", "down script: None\n");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":170", "down script: %s\n");
        fprintf(out_file, generic_msg, a_proc.down_script);
      }

      if (strcmp(a_proc.group.name, "") == 0) {
        generic_msg = gettxt(":171", "group: None\n");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":172", "group: %s\n");
        fprintf(out_file, generic_msg, a_proc.group.name);
      }

      if (strcmp(a_proc.group.name, "") != 0) {
        if (a_proc.group.priority == 1) {
          generic_msg = gettxt(":173", "critical group process: TRUE\n");
          fprintf(out_file, generic_msg);
        }
        else {
          generic_msg = gettxt(":174", "critical group process: FALSE\n");
          fprintf(out_file, generic_msg);
        }
      }
      else {
        generic_msg = gettxt(":175", "critical group process: N/A\n");
        fprintf(out_file, generic_msg);
      }

      if (a_proc.reject_exit_code_used) {
        generic_msg = gettxt(":176", "reject node exit code: %d\n");
        fprintf(out_file, generic_msg, a_proc.reject_exit_code);
      }
      else {
        generic_msg = gettxt(":177", "reject node exit code: None\n");
        fprintf(out_file, generic_msg);
      }

      if (a_proc.down_exit_code_used) {
        generic_msg = gettxt(":178", "down exit code: %d\n");
        fprintf(out_file, generic_msg, a_proc.down_exit_code);
      }
      else {
        generic_msg = gettxt(":179", "down exit code: None\n");
        fprintf(out_file, generic_msg);
      }

      if ((a_proc.status_captured == ZOMBIE) && 
          WIFEXITED(a_proc.status)) 
      {
        generic_msg = gettxt(":180", "exit status returned: %d\n");
        fprintf(out_file, generic_msg, WEXITSTATUS(a_proc.status));
      }
      else {
        generic_msg = gettxt(":181", "exit status returned: None\n");
        fprintf(out_file, generic_msg);
      }

      if (a_proc.last_pid == -1) {
        generic_msg = gettxt(":182", "last pid: None\n");
        fprintf(out_file, generic_msg);
      }
      else {
        generic_msg = gettxt(":183", "last pid: %d\n");
        fprintf(out_file, generic_msg, a_proc.last_pid);
      }

      generic_msg = gettxt(":184", "slot: %d\n");
      fprintf(out_file, generic_msg, i);

      /* Terminate record with a blank line. */
      generic_msg = gettxt(":185", "\n");
      fprintf(out_file, "\n");
    }
    else {
      break;
    }
  }
}

void
verbose_human_keepalive_output()
{
  if (ka_com_succeeded) {
    generic_msg = gettxt(":186", "running: TRUE\n");
    fprintf(out_file, generic_msg);
  }
  else {
    generic_msg = gettxt(":187", "running: FALSE\n");
    fprintf(out_file, generic_msg);
  }

  if (pkctl->quiesce_flag) {
    generic_msg = gettxt(":188", "quiesce flag: TRUE\n");
    fprintf(out_file, generic_msg);
  }
  else {
    generic_msg = gettxt(":189", "quiesce flag: FALSE\n");
    fprintf(out_file, generic_msg);
  }

  if (ka_com_succeeded) {
    generic_msg = gettxt(":190", "pid: %d\n");
    fprintf(out_file, "pid: %d\n", pkctl->primary_pid);

    generic_msg = gettxt(":191", "node number: %d\n");
    fprintf(out_file, generic_msg, node_pid(pkctl->primary_pid));
  }
  else {
    generic_msg = gettxt(":192", "pid: None\n");
    fprintf(out_file, generic_msg);

    generic_msg = gettxt(":193", "node number: None\n");
    fprintf(out_file, generic_msg);
  }

  generic_msg = gettxt(":194", "registered processes: %d\n");
  fprintf(out_file, generic_msg, pkctl->registered_daemons);

  generic_msg = gettxt(":195", "table size: %d\n");
  fprintf(out_file, generic_msg, pkctl->max_registered_daemons);

  generic_msg = gettxt(":196", "max. possible processes: %d\n");
  fprintf(out_file, generic_msg, pkctl->max_possible_daemons);

  if (pkctl->polling) {
    generic_msg = gettxt(":197", "polling: TRUE\n");
    fprintf(out_file, generic_msg);
  }
  else {
    generic_msg = gettxt(":198", "polling: FALSE\n");
    fprintf(out_file, generic_msg);
  }
  generic_msg = gettxt(":199", "polling interval: %d\n");
  fprintf(out_file, generic_msg, pkctl->polling_interval);
}

void
wait_for_lock(process_t *mmapped_proc, process_t *a_proc)
{
  clock_t unlock;
  struct timeval sleep_interval = { 1, 0 };

  do {
    /* Wait for Keepalive to release lock. */
    while (mmapped_proc->lock != 0) { 
      select(0, NULL, NULL, NULL, &sleep_interval);
    }

    unlock = mmapped_proc->unlock;

    *a_proc = *mmapped_proc;
  } 
  /* Guard against race condition where Keepalive modified the record
   * right after we copied it.
  */
  while ((mmapped_proc->lock != 0) || (unlock != mmapped_proc->unlock));
}

