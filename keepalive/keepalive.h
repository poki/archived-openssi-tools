/*
 *	keepalive header file.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
 *
 */
/* $Id: keepalive.h,v 1.5 2003/04/12 02:53:51 jlbyrne Exp $ */

#include <linux/limits.h>
#include <sys/types.h>
#include <sys/procfs.h>
#include <linux/cluster.h>

/* SSI_XXX: Hacks for signal.h not modified for SSI */
#ifndef SIGCLUSTER
#define SIGCLUSTER      (SIGUSR1)
#endif

/* Types of idempotency supported. */
typedef enum {
NO_IDEMPOTENCY,
CLUSTER_WIDE,
NODE_LIST
} idempotency_t;

typedef enum {
NOT_ADOPTED,
ADOPTED,
FAILED_ADOPT
} adopt_t;

typedef enum {
NO_FAILURE,
PROCESS_FAILURE,
NODE_FAILURE
} failure_t;

typedef enum {
NO_ZOMBIE_STATUS,
ZOMBIE,
ZOMBIE_NOT_AVAIL
} zombie_t;

#define MAXNAME	PATH_MAX

#define	KEEP_MAGIC	"Keep-Alive!"

/* use this until we know what the real number is */
#define MAX_NODES	64

/* process registration policies */
#define REGISTER_BY_PID		1
#define REGISTER_BY_NAME	2
#define REGISTER_BY_ARGS	3
#define DEFAULT_REGISTRATION 	REGISTER_BY_NAME

/* process restart policies */
#define	LAST_NODE	1
#define	F_NODE		2
#define	ROUND_ROBIN	3
#define	ROOT_NODE	4
#define DEFAULT_RESTART_POLICY	0

/* default down script policy is to execute down script when downed by
 * a node failure.
*/
#define DEFAULT_DOWN_POLICY 1

/* Returns TRUE if user is an administrator or FALSE otherwise. */
#define IS_ADMIN	((uid == 0) || (uid == 2) || (uid == 3) || (uid == 4))

/* The first block of data of the shared memory segment  is used to store
 * control information. Both the primary and the shadow keepalive use
 * this information to indicate to each other that they are still alive.
 * The following structure is used to hold the control information.
 */

typedef  struct keepalive_control {
	short	goodness_flag;		/* TRUE indicates the data has been */
					/* written atomically. FALSE indicates */ 
					/* that data has not been completely written */
					/* and is probably corrupted. */
	pid_t	primary_pid;		/* pid of primary keepalive */
	time_t	dm_hbeat_sent;		/* time keepalive node was read */
	long	max_possible_daemons;	/* Max possible size of table */
	long	max_registered_daemons;	/* Maximum number of daemons. */
	long	registered_daemons;	/* Number of daemons running. */
	int	quiesce_flag;		/* For the QUIESCE/RESTART commands. */
	int	polling;		/* TRUE if Keepalive is polling */
	int	polling_interval;	/* polling interval in seconds */

	/* Are we watching a process for which adoption has failed? */
	int	unadopted_process;
} kalive_ctl_t;

typedef struct groups {
        char name[MAXNAME];	/* name of group of processes */
        u_short priority;	/* 0 if non-critical or 1 if critical proc. */  
	int wait_time;		/* time to wait before spawning next member */
} group_t;

/* error codes */
typedef enum state_enum {
ST_EMPTY,
ST_START,
ST_OK,
ST_DEAD,
ST_DOWN,
ST_RESPAWNING,
ST_SHUTDOWN,
ST_DAEMONIZE,
ST_MAX  /* Must always be defined last. */
} state_t;

typedef struct times_s {
  time_t date;
  clock_t lbolt_in_sec;
} times_t;

typedef struct processes {
	group_t group;			/* group info. */
	pid_t	pid;			/* if pid = 0 then deleting by name */

        /* last pid before last failure or restart */
	pid_t	last_pid;

        /* PID used to create the daemon log file (!= pid if process 
         * daemonizes)
        */
	pid_t	original_pid;		

	char	name[MAXNAME];		/* name of daemon, null if using pid */
	char	args[MAXNAME];		/* arguments used with daemon, null if using pid */
	char	basename[MAXNAME];	/* basename of daemon, null if using pid */
	char	cfg_file[MAXNAME];	/* daemon config. file name */
	char	startup_script[MAXNAME];	/* Script to startup daemon. */
	char	shutdown_script[MAXNAME];	/* Script to shutdown daemon. */
	int	down_exit_code_used;	/* down exit code used? */
	int	down_exit_code;	        /* exit code to take daemon to down. */
	char	down_script[MAXNAME];	/* Script to down a daemon. */
        int     down_script_policy;   	/* down script policy for daemon */
	int	reject_exit_code_used;	/* down exit code used? */
	int	reject_exit_code;	/* exit code to take daemon to down. */
	char	process_failure_script[MAXNAME]; /* Script to respawn daemon */
						 /* after a proc. failure. */
	char	node_failure_script[MAXNAME];	/* Script to respawn daemon */
						/* after a node failure. */
	times_t	lastexeced;		/* time of last exec in jiffes*/
	time_t	lastexeced_secs;	/* time of last exec in secs */
	time_t	minrespawn;		/* min time between respawn attempts */
	time_t	spawnwait;		/* from respawn to new daemon check*/
	time_t	termwait;		/* time from SIGTERM to SIGKILL */
        time_t	probation_period;	/* from error to respawn */
        times_t	daemon_first_died;	/* first time a daemon fails */
	times_t daemon_last_died;	/* last time a daemon failed */
	ulong	total_errors;		/* total number of errors */
	int	euid;			/* effective user id */
	int	egid;			/* effective group id */
	clusternode_t node;		/* user specified node */
	clusternode_t backup_nodes[MAX_NODES]; /* user specified backup nodes */

        /* List of rejected nodes. */
	clusternode_info_t rejected_nodes[MAX_NODES]; 

	int	pin_anywhere;		/* flag to specify that process */
					/* should be pinned */
	int	respawning_group;	/* true if the process is part of a */
					/* group that is being respawned    */
	int	registration_policy;	/* registration policy for daemon */
	int	restart_policy;		/* restart policy for daemon */
	clusternode_info_t last_node;	/* last node process was started/restarted on */
        transid_t last_node_UP_transid; /* transid when last_node became UP */
	state_t	state;			/* Keepalive state of proc. */
        u_short	delete_group_flag; /* 0 if delete, 1 if kill */
	u_short	maxerrors;	/* 0 = infinity */
        u_short	nerrors;	/* number of errors */
	int	slot;			/* slot number this process occuppies */
	zombie_t status_captured;        /* zombie caputured status */
	int	status;			/* status returned by waitpid() */
	adopt_t adoption_status;	/* child adoption status */

        /* This field is only to indicate whether Keepalive has performed
         * daemonization recovery.  Keepalive cannot tell whether a process
         * has really daemonized.  This is important to note since some
         * daemons (e.g. syslogd) daemonize after spawning a number of
         * other processes which do their work and go away.  The daemonized
         * process is a descendent of the child of the original parent of
         * the daemon.  syslogd is the only daemon we know of that exhibits
         * this behavior.
        */
	int     daemonization_recovery;

	failure_t failure;		/* reason process died */

        /* 0 if unlocked, non-zero if Keepalive has record locked for
         * updating.
        */
	clock_t lock;

	/* Set to the value returned by times() when record was last
         * unlocked.
        */
	clock_t unlock;
} process_t;


/* The following structure contains the information needed for the commands
 * to communicate safely between spawndaemon and keepalive.
 */

typedef struct kalive_ctl {
        char         magic[12];   /* = KEEP_MAGIC, for sanity check */
        size_t       size;        /* size of structure for sanity check */
        u_long      cmd;         /* the commands: ADD, DELETE, etc. */
        process_t    daemon; 	  /* contents of request */
} kalive_cmd_t;


/* The following stucture contains information about an individual
 * process configuration file.
 */

typedef struct individual_file {
	char *daemon_name; /* full path to daemon */
	char *daemon_args; /* daemon arguments separated by a single space */
        char *gr;          /* group entry */
        time_t termwait;   /* termwait */
        char *uid;         /* user id */
        char *gid;         /* group id */
        int mer;           /* max errors */
        int prb;           /* probation period */
        int minr;          /* minrespawn    */
        char *startup_script;  /* startup script for daemon */
        char *shutdown_script;  /* shutdown script for daemon */
        char *scr_name;      /* process failure recovery script name */
        char *node_scr_name; /* node failure recovery script name */
        char *down_script;   /* down script for daemon */
        int down_script_policy;   /* down script policy for daemon */
} ind_conf_file;


/* The following stuctures contains information about a group of 
 * processes configuration file.
 */


typedef struct member_file {
        char	*mfname;    /* member file name */
	int	wait_time;  /* time to wait before spawning next daemon */
	int	priority;   /* priority */
} member;



typedef struct group_file {
        char        *gname;         /* name of the group */
        int         mems;           /* Number of member configuration files */
        member      *mbr;           /* member configuration file  */
} gfile;



/* commands */
#define		ADD		0x0000		/* code assumes this is zero */
#define		DELETE		0x0001
#define		QUIT		0x0002
#define		LIST		0x0004
#define		QUIET		0x0008
#define         DELETE_GROUP    0x10
#define         REGISTER        0x20
#define         RESET           0x40
#define         RESTART         0x100 
#define		CMD_MASK	(ADD|DELETE|QUIT|LIST|QUIET|DELETE_GROUP|REGISTER|RESET|RESTART)
/* flags */
#define		THEN_KILL	0x8000


#define KEEPCFG		"/cluster/dev/keepalivecfg"
#define KEEPDIR 	"/etc/keepalive.d"
#define KEEPDATA 	"/etc/keepalive.d/keepalive.data"
#define KEEPMSGDIR 	"/var/log/keepalive"
#define KEEPALIVE 	"/usr/sbin/keepalive"
#define SPAWNDIR 	"/etc/spawndaemon.d"

extern char			*program_name ;
extern char			*syslog_msg;
extern char			*generic_msg;
extern char			*format_msg;
int                             num_of_groupmembers;
int                             process_running;


extern clock_t		get_lbolt();
extern time_t		get_date();

/*
 *	functions in findproc.c
 */

extern	void		openproc();
extern	void		closeproc();
extern	void		rewindproc();
extern	process_t * 	nextproc(pid_t *ppid);
extern  process_t *	find_other_ka();
extern  process_t *	find_proc_info(pid_t pid);
extern	process_t * 	findproc(process_t *proc);
extern  int 		find_reg_proc(pid_t pid);
extern  process_t * 	find_process(pid_t pid);
extern  process_t * 	get_proc_name(pid_t pid);

extern	int			nmatching_procs(register char *name);


/*
 *      functions in mds_events.c
 */

 
extern   void            initialize_alarm_log();
extern   void            log_uidgid_event(char *msg, char *filename);
extern   void            log_fileperms_event(char *msg, char *filename);
extern   void            log_euidegidXperm_event(char *msg, char *process_name);
extern   void            log_stoprespawn_event(char *msg, char *daemon);
extern   void            log_noscript_event(char *msg, char *script);

/* maximum number of daemons that may be registered.  */
#define DEFAULT_MAX_DAEMONS 200  
#define REQUEST_DAEMONS 20 

#define	TERM_SECONDS	5

#define SHADOW_TERM_WAIT  5

#ifdef	DEBUG1
extern void myexit(int code);
/* #define exit myexit */
#endif

#ifdef	DEBUG
extern void mysyslog(int type, char *fmt, ...);
#define	syslog	mysyslog
#endif

/* extern	void		syslog(int level, char *fmt, ...); */
#ifndef	TRUE
#define	TRUE	1
#define	FALSE	0
#endif

extern kalive_ctl_t    *pkctl;

#ifdef DEBUG
extern char time_str[];
extern time_t sys_time;
#endif

extern void
safe_strncpy(char *s1, const char *s2, size_t s1_size);
