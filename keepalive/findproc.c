/*
 *	/proc interface routines.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
 *
 */
/* $Id: findproc.c,v 1.2 2003/01/23 12:40:51 kvaneesh Exp $ */

#include <errno.h>
#include <sys/types.h>
#include <nl_types.h>
#include <sys/ipc.h>
#include <sys/file.h>
#include <dirent.h>
#include <stdio.h>
#include <ctype.h>
#include <signal.h>
#include <fcntl.h>
#include <string.h>
/*
#include <sys/fault.h>
*/
#include <sys/syscall.h>
#include <sys/user.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/procfs.h>
#include <sys/syslog.h>
#include <time.h>

#ifdef	DEBUG
#include <assert.h>
#include <stdarg.h>
#endif

#include "keepalive.h"

#ifdef DEBUG
char time_str[80];
time_t sys_time;
#endif

static DIR	    *dirp = NULL;
extern kalive_ctl_t *pkctl;
extern process_t    *pprocs;
extern int stable_state;

extern void adopt_child(pid_t pid, char *name);

int slots_equivalent(const process_t *proc1, const process_t *proc2); 
int matches_slot_config(const process_t *proc, int pid);
void myexit(int code);
void mysyslog(int type, char *fmt, ...);

/*                                                                      
 *                      Function openproc()                             
 *                                                                      
 * Description                                                          
 *                                                                      
 * The openproc() routine opens the /proc directory.                    
 *                                                                      
 * Interface                                                            
 *                                                                      
 * void openproc()                                                      
 *                                                                      
 * Returns                                                              
 *                                                                      
 * Nothing                                                              
*/         
 
void
openproc()
{
	if (dirp != (DIR *)NULL) {
		return;
	}

	if((dirp = opendir("/proc")) == (DIR *) NULL){
		syslog(LOG_ERR, "keepalive.msg:96 %s: Cannot open /proc\n", program_name);
		fprintf(stderr, syslog_msg, program_name);
		exit(1);
	}
}


/*                                                                      
 *                      Function closeproc()                            
 *                                                                      
 * Description                                                          
 *                                                                      
 * The closeproc() routine closes the /proc directory.                  
 *                                                                      
 * Interface                                                            
 *                                                                      
 * void closeproc()                                                     
 *                                                                      
 * Returns                                                              
 *                                                                      
 * Nothing                                                              
*/


void
closeproc()
{
	if (dirp == (DIR *)NULL)
		return;
	closedir(dirp);
	dirp = NULL;
}


/*                                                                      
 *                      Function rewindproc()                           
 *                                                                      
 * Description                                                          
 *                                                                      
 * The rewindproc() routine rewinds the /proc directory.                
 *                                                                      
 * Interface                                                            
 *                                                                      
 * void rewindproc()                                                    
 *                                                                      
 * Returns                                                              
 *                                                                      
 * Nothing                                                              
*/


void
rewindproc()
{
	openproc();
	rewinddir(dirp);
}


/*                                                                      
 *                      Function nextproc()                             
 *                                                                      
 * Description                                                          
 *                                                                      
 * The nextproc() routine locates a process in the /proc directory.     
 *                                                                      
 * Interface                                                            
 *                                                                      
 * process_t *nextproc()                                                
 *                                                                      
 * Returns                                                              
 *                                                                      
 * The address of the process if the process was found                  
 * NULL otherwise                                                       
*/

process_t *
nextproc(pid_t *ppid)
{
 
	static process_t proc;
	register struct dirent *direntp;
	char fullprocname[MAXNAME];
	char sbuf[512];		/* buffer for stat data. */
	register char *c;
	int fd;
	int i;
	int arglen;
	time_t start_time;

	openproc();
	if (dirp == (DIR *)NULL) {
		return((process_t *) NULL);
	}

	/* CONSTANT CONDITION */
	for(;;){
		direntp = readdir(dirp);

		if (direntp == (struct dirent *) NULL)
			return((process_t *) NULL);
		
		if(!isdigit(direntp->d_name[0]))
			continue;

		/* Read in proc_name. */
		sprintf(fullprocname, "/proc/%s/cmdline", direntp->d_name);
		if ((fd = open(fullprocname, O_RDONLY)) < 0)
			continue; /* Error opening cmdline. */

		memset(proc.args, 0, sizeof(proc.args));
		memset(proc.name, 0, sizeof(proc.name));
		if ( (i = read(fd, sbuf, sizeof(sbuf)-1)) <= 0) {
			/* Error reading cmdline. */
			(void) close(fd);
			continue;
		}
		(void) close(fd);
		sbuf[i+1] = '\0';
		memcpy(proc.name, sbuf, strlen(sbuf));
		if (i > strlen(sbuf) + 1) {
			/* Read in arguments. */
			c = strchr(sbuf, '\0');
			arglen = i - strlen(sbuf) - 1;
			memcpy(proc.args, c+1, arglen);
			/* Put spaces in between args. */
			for (i = 0; i < arglen-1; i++) {
				if (proc.args[i] == '\0')
					proc.args[i] = ' ';
			}
		}
		
		/* Read in start_time. */
		sprintf(fullprocname, "/proc/%s/stat", direntp->d_name);
		if ((fd = open(fullprocname, O_RDONLY)) < 0) {
			/* Error opening stat. */
			memset(proc.args, 0, sizeof(proc.args));
			memset(proc.name, 0, sizeof(proc.name));
			continue;
		}

		if ( (i = read(fd, sbuf, sizeof(sbuf)-1)) <= 0) {
			/* Error reading stat. */
			memset(proc.args, 0, sizeof(proc.args));
			memset(proc.name, 0, sizeof(proc.name));
			(void) close(fd);
			continue;
		}
		(void) close(fd);
		
		c = strrchr(sbuf, ')');
		sscanf(c + 2,
		       "%c "
		       "%ld %d %d %d %d "
		       "%d %d %d %d %d %d %d "
		       "%d %d %d %d %d %d "
		       "%lu",
		       (char *)&i,
		       (long *)ppid, &i, &i, &i, &i,
		       &i, &i, &i, &i, &i, &i, &i,
		       &i, &i, &i, &i, &i, &i,
		       &start_time);

		proc.lastexeced.date = start_time;

		proc.pid = (pid_t) atoi(direntp->d_name);

		break;
	}
	(void)close(fd);

	return(&proc);
}

/*                                                                      
 *                      Function find_other_ka()                             
 *                                                                      
 * Description                                                          
 *                                                                      
 * The find_other_ka() routine calls the nextproc() routine to locate another 
 * keepalive process in the /proc directory and return that information to the
 * calling program. 
 *                                                                      
 * Interface                                                            
 *                                                                      
 * process_t * find_other_ka()        
 *                                                                      
 * Returns                                                              
 *                                                                      
 * The address of the process if the process is found                   
 * NULL otherwise                                                       
*/           

process_t *
find_other_ka()
{
	register process_t *p;
	int this_pid = getpid();
	pid_t ppid;

	rewindproc();
	while( (p = nextproc(&ppid))){
		if ( ((p->pid != this_pid) && (strncmp("/sbin/keepalive", p->name,15) == 0))){
			closeproc();
			return(p);
		}
	}   
	closeproc();

	return((process_t *) NULL);
}

/*                                                                      
 *                      Function find_proc_info()                             
 *                                                                      
 * Description                                                          
 *                                                                      
 * The find_proc_info() routine calls the nextproc() routine to locate a      
 * process in the /proc directory and return that information to the
 * calling program. 
 *                                                                      
 * Interface                                                            
 *                                                                      
 * process_t * find_proc_info(pid_t pid)        
 *                                                                      
 * Returns                                                              
 *                                                                      
 * The address of the process if the process is found                   
 * NULL otherwise                                                       
*/           

process_t *
find_proc_info(pid_t pid)
{
	register process_t *p;
	pid_t ppid;

	rewindproc();
	while( (p = nextproc(&ppid))){
		if (p->pid == pid){
			closeproc();
			return(p);
		}
	}   

	closeproc();
	return((process_t *) NULL);
}

/*                                                                      
 *                      Function findproc()                             
 *                                                                      
 * Description                                                          
 *                                                                      
 * The findproc() routine calls the nextproc() routine to locate a      
 * process in the /proc directory. 
 *                                                                      
 * Interface                                                            
 *                                                                      
 * process_t * findproc(register char *name)        
 *                                                                      
 * Returns                                                              
 *                                                                      
 * The address of the process if the process is found                   
 * NULL otherwise                                                       
*/           

process_t *
findproc(process_t *proc)
{
	register process_t *p;
	register char *s;
	register int 	i;
	pid_t ppid;

	if ( (s = strchr(proc->name, ' ')))
		*s = '\0';

	rewindproc();
	while( (p = nextproc(&ppid))){
		if ((strcmp(proc->name, p->name) == 0) &&
		    ((proc->registration_policy != REGISTER_BY_ARGS) || 
		     (strcmp(proc->args, p->args) == 0)))
		{
			for(i = 0; i < pkctl->registered_daemons; i++) {
				if((pprocs[i].pid == p->pid) 
				   && (kill(pprocs[i].pid, 0) >= 0)) 
					break;
			}
			if(pkctl->registered_daemons != i) {
				return(p);
			}
		}
	}   
	return((process_t *) NULL);
}

/*                                                                      
 *                      Function find_reg_proc()                             
 *                                                                      
 * Description                                                          
 *                                                                      
 * The find_reg_proc() routine locates to which slot a process is registered.
 *                                                                      
 * Interface                                                            
 *                                                                      
 * int find_reg_proc(pid_t pid)        
 *                                                                      
 * Returns                                                              
 *                                                                      
 * slot number or -1
*/           

int
find_reg_proc(pid_t pid)
{
	register int i;

	for(i = 0; i < pkctl->registered_daemons; i++) {
		if (pprocs[i].pid == pid) {
			return(i);
		}
	}   
	return(-1);
}


int
matches_slot_config(const process_t *proc, int pid) {
	/* If we have no pinning requirements, then we don't care. */
	if (proc->node == 0) {
		return TRUE;
	}

	/* Child of missing process must be pinned to same node as parent. */
	if (proc->last_node.node_num == node_pid(pid)) {
		return TRUE;
	}

	return FALSE;
}


/*       
 *                     Function get_proc_name()
 *
 * The get_proc_name() routine searches the /proc for the process name
 * given the process id. The routine is used by the find_process()
 * routine.
 *
 * Interface
 *
 * process_t * get_proc_name(pid_t pid)
 *
 * Returns
 *
 * The process name in the process structure.
*/


process_t *
get_proc_name(pid_t pid)
{

	register struct dirent *direntp;
	static process_t proc;
	char fullprocname[MAXNAME];
	char sbuf[512];
	int i;
	register char *c;
	int fd;
	int arglen;
	time_t start_time;

	openproc();
	if (dirp == (DIR *)NULL) {
		return((process_t *) NULL);
	}

	/* CONSTANT CONDITION */
	for(;;){
		direntp = readdir(dirp);
		if (direntp == (struct dirent *) NULL)
			return((process_t *) NULL);

		if(!isdigit(direntp->d_name[0]))
			continue;

		/* Read in proc_name. */
		sprintf(fullprocname, "/proc/%s/cmdline", direntp->d_name);
		if ((fd = open(fullprocname, O_RDONLY)) < 0)
			continue; /* Error opening cmdline. */

		memset(proc.name, 0, sizeof(proc.name));
		memset(proc.args, 0, sizeof(proc.args));
		if ( (i = read(fd, sbuf, sizeof(sbuf)-1)) <= 0) {
			/* Error reading cmdline. */
			(void) close(fd);
			continue;
		}
		(void) close(fd);
		sbuf[i+1] = '\0';
		memcpy(proc.name, sbuf, strlen(sbuf));
		if (i > strlen(sbuf) + 1) {
			/* Read in arguments. */
			c = strchr(sbuf, '\0');
			arglen = i - strlen(sbuf) - 1;
			memcpy(proc.args, c+1, arglen);
			/* Put spaces in between args. */
			for (i = 0; i < arglen; i++) {
				if (proc.args[i] == '\0')
					proc.args[i] = ' ';
			}
		}
			
		/* Read in start_time. */
		sprintf(fullprocname, "/proc/%s/stat", direntp->d_name);
		if ((fd = open(fullprocname, O_RDONLY)) < 0) {
			/* Error opening stat. */
			memset(proc.args, 0, sizeof(proc.args));
			memset(proc.name, 0, sizeof(proc.name));
			continue;
		}

		if ( (i = read(fd, sbuf, sizeof(sbuf)-1)) <= 0) {
			/* Error reading stat. */
			memset(proc.args, 0, sizeof(proc.args));
			memset(proc.name, 0, sizeof(proc.name));
			(void) close(fd);
			continue;
		}
		(void) close(fd);
		
		c = strrchr(sbuf, ')');
		sscanf(c + 2,
		       "%c "
		       "%d %d %d %d %d "
		       "%d %d %d %d %d %d %d "
		       "%d %d %d %d %d %d "
		       "%lu",
		       (char *)&i,
		       &i, &i, &i, &i, &i,
		       &i, &i, &i, &i, &i, &i, &i,
		       &i, &i, &i, &i, &i, &i,
		       &start_time);

		proc.lastexeced.date = start_time;

		proc.pid = (pid_t) atoi(direntp->d_name);

		break;
	}
	(void)close(fd);
	return(&proc);
}


/*
 *                    Function find_process()
 *
 * Description
 *
 * The find_process() routine is used to get the process name from
 * /proc given the process id.
 *
 * Interface
 *
 * process_t * find_process(pid_t pid)
 *
 * Returns
 *
 * The process if found.
*/

process_t *
find_process(pid_t pid)
{

    register process_t *p;

    rewindproc();
    p = get_proc_name(pid); 
	if(pid == p->pid) {
		return(p);
	}
    return( (process_t *) NULL);
}

#ifdef DEBUG2
void myexit(int code) {
	syslog(LOG_DEBUG, "%s: exit <%d> - %d\n", program_name, code, getpid());

	/* Force an exit() without calling exit() and generate a core file. */
	abort();
}
#endif

#ifdef DEBUG
void
mysyslog(int type, char *fmt, ...)
{
	va_list		va;
	char filename[255];
	FILE *fp;
	static FILE *fp2 = NULL;

#ifdef DEBUG3
	sprintf(filename, "/tmp/keepalive.mysyslog.time.%d", getpid());
	fp2=fopen(filename, "a+");
	time(&sys_time);
	ctime_r(&sys_time, time_str);
	fprintf(fp2, "/sbin/keepalive:  begin mysyslog() TIME: %s\n", time_str);
#endif

	sprintf(filename, "/tmp/keepalive.log.%d", getpid());
	fp=fopen(filename, "a+");
	fprintf(fp, "syslog <%d> ", type);
	va_start(va, fmt);
	vfprintf(fp, fmt, va);
	va_end(va);
	fflush(fp);
	fclose(fp);

#ifdef DEBUG3
	time(&sys_time);
	ctime_r(&sys_time, time_str);
	fprintf(fp2, "/sbin/keepalive:  end mysyslog() TIME: %s\n", time_str);
	fclose(fp2);
#endif
}
#endif /* DEBUG */

void
safe_strncpy(char *s1, const char *s2, size_t s1_size)
{
  /* Note: strncpy does not copy NULL character if length of s1 is too short. */
  strncpy(s1, s2, s1_size - 1);
  s1[s1_size - 1] = '\0';
}

/* 
 * Return the time when this process first started, in terms of seconds
 * since Jan. 1, 1970.
 */
time_t
get_pid_start(pid_t pid)
{
	char fullprocname[64];
	char sbuf[512];
	char *c;
	int i;
	int fd;
	time_t start_time; 	/* process start time, seconds since */

#ifdef DEBUG
	syslog(LOG_DEBUG, "get_pid_start: ENTER\n");
#endif /* DEBUG */

	sprintf(fullprocname, "/proc/%d/stat", pid);
	if ( (fd = open(fullprocname, O_RDONLY)) == -1) {
#ifdef DEBUG
		syslog(LOG_DEBUG, "get_pid_start: Cannot open %s\n", fullprocname);
#endif /* DEBUG */
		return 0;
	}

	if ( (i = read(fd, sbuf, sizeof(sbuf)-1)) <= 0) {
		/* Error reading stat. */
#ifdef DEBUG
		syslog(LOG_DEBUG, "get_pid_start: Error reading %s\n", fullprocname);
#endif /* DEBUG */
		(void) close(fd);
		return 0;
	}
	(void) close(fd);
	
	c = strrchr(sbuf, ')');
	sscanf(c + 2,
	       "%c "
	       "%d %d %d %d %d "
	       "%d %d %d %d %d %d %d "
	       "%d %d %d %d %d %d "
	       "%lu",
	       (char *)&i,
	       &i, &i, &i, &i, &i,
	       &i, &i, &i, &i, &i, &i, &i,
	       &i, &i, &i, &i, &i, &i,
	       &start_time);
#ifdef DEBUG
	syslog(LOG_DEBUG, "get_pid_start: pid_start: %ld\n", start_time);
#endif /* DEBUG */

	return start_time;
}
