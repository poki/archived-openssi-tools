/*
 *	spawndaemon command.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
 *
 */
#ident "$Id: spawndaemon.c,v 1.3 2003/05/22 18:49:43 dzafman Exp $" 

#include <errno.h>
#include <locale.h>
#include <sys/types.h>
#include <nl_types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <dirent.h>
#include <memory.h>
#include <stdio.h>
#include <ctype.h>
#include <signal.h>
#include <fcntl.h>
#include <string.h>
#include <assert.h>
#include <sys/syscall.h>
#include <sys/procfs.h>
#include <sys/syslog.h>
#include <pwd.h>
#include <unistd.h>
#include <stddef.h>
#include <grp.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/resource.h>
#include "keepalive.h"
#include "spawndaemon.h"

#define DEFAULT_TERMWAIT  2
#define DEFAULT_MINRESPAWN  0
#define DEFAULT_MAXERROR  10  
#define DEFAULT_PROBATION  300 
#define DEFAULT_REGISTER_WAIT  1

/* -c options */
#define REJECT 0
#define DOWN 1

#define KA_TIMEOUT    300  /* Give Keepalive time to startup. */

char *program_name = "spawndaemon";
char *msg_catalog = "spawndaemon.cat";
char *syslog_msg = NULL;  
char *generic_msg = NULL;  
char *format_msg = NULL;  
struct timeval sleep_one_second = { 1, 0 };  /* 1 second sleep interval */

process_t  msgproc;
kalive_cmd_t   kmsgproc = { { 0 } }; 
kalive_ctl_t   *pkctl = NULL;
kalive_ctl_t   *pkctl2 = NULL;
int    pipefd = -1;
int    data_fd;
int     ka_com_succeeded = FALSE;  /* Can we communicate with keepalive? */
uid_t     uid = -1;

/* Idempotency Feature */
idempotency_t idempotency_policy = NO_IDEMPOTENCY;

int num_registered_daemons; /* Number of registered daemons before adding
			       the new group */
process_t *pprocs = NULL;   
gfile     gf;
ind_conf_file ipf;

int  init(int n_procs, int needs_ka, int localprint);
void  usage(int argc, char **argv);
void  writemsg(kalive_cmd_t *m);
void printprocs(int localprint, print_object_t print_object, verbosity_t verbosity);
void   spawn_nprocs(int restart, int nprocs, kalive_cmd_t *m); 
int  spawn_proc(process_t *m);
void  process_conf_file(char *filename, char **argv);
void  register_proc(char *regname_file, pid_t pid);
char  *get_str(register char *string, const char *sepset);
static void  sig_parent(int i);
int  find_daemon(char *pathname);
int  check_conf_file_perms(char *filename);
int  file_group_or_individual(char *filename);
void  sync_data();
ind_conf_file *get_file_fields(char *filename);
process_t *find_watched_proc(char *name);
void  get_basename(char *basename, char *name);
void  tokens_to_str(char *args, char **token);
void   enforce_idempotency(process_t *new_proc, idempotency_t policy);
void check_script(char *cfg_file, char *script_basename, char *target, int target_size);

extern time_t get_pid_start(pid_t pid);

extern int  errno;
 
void  *pctl_procs = NULL;
int  needs_ka = TRUE;
int  nprocs = 1; 
int  killing = FALSE;
int  max_num_daemons = DEFAULT_MAX_DAEMONS;
int  sig_check = 0; /* Increment it if in signal handler. */

/* For checking configuration file. */
const char *nam_root = "root";
struct passwd *pwadm = NULL;
uid_t  sysuid;
gid_t  sysgid;

/* flags for checking options */
int  Fnode = 0;
int  Fflag = FALSE;
int  Bflag = FALSE;
int  Zflag = FALSE;
int  rflag = FALSE;
int  Rflag = FALSE;
int  Uflag = FALSE;
int  dflag = FALSE;
int  vflag = FALSE;      

#ifdef DEBUG
char time_str[80];
time_t sys_time;
#endif

int
main(int argc, char **argv)
{
  extern char *optarg;
  extern int optind;
  int c, i;
  char *conf_file = NULL;
  int cmdset = FALSE;
  int localprint = FALSE;
  print_object_t print_object = PRINT_DAEMONS;
  verbosity_t verbosity = NO_VERBOSITY;
  char *filename = NULL; 
  char *proc_file = NULL; 
  char *restart_policy = NULL;
  int backup_index = 0;
  int node, index;
  int token_index;
  char *c_opt_value;
  char *c_opt_tokens[3] = { "reject", "down" };
  process_running = FALSE;
  program_name = argv[0];

  /* Open log in case of usage errors. */
  openlog("spawndaemon", LOG_CONS | LOG_PID | LOG_NDELAY /*LOG_NOWAIT*/, LOG_DAEMON);

  /* Verify that Spawndaemon is running on an OpenSSI kernel. */
  if (
#ifndef DXX
!cluster_ssiconfig()
#else
!tnc_isconfig()
#endif
     ) 
  {
    syslog(LOG_ERR, "%s: Requires an OpenSSI kernel to run.\n", program_name);
    (void)fprintf(stderr, "%s: Requires an OpenSSI kernel to run.\n", program_name);
    exit(1);
  }

  if (argc < 2) {
    usage(argc, argv);
    /*NOTREACHED*/
  }
  /* Get the user id to check on user's validity. 
   * Only a super user can use spawndaemon to
   * administer processes. A simple user can only
   * list the processes, (-L) option.
  */
  uid = getuid();

  if( (pwadm = getpwnam(nam_root)) == NULL) {
    syslog(LOG_ERR, "%s: Cannot find entry adm in the /etc/passwd file.\n", program_name);
    (void) fprintf(stderr, "%s: Cannot find entry adm in the /etc/passwd file.\n", program_name);
    exit(1);
  }
  sysuid = pwadm->pw_uid;
  sysgid = pwadm->pw_gid;
  /* 
  * Establish signal handler.
  */
  signal(SIGUSR1, sig_parent);
        
  kmsgproc.cmd = ADD;
  kmsgproc.daemon.reject_exit_code_used = FALSE;
  kmsgproc.daemon.down_exit_code_used = FALSE;
  kmsgproc.daemon.termwait = DEFAULT_TERMWAIT;
  kmsgproc.daemon.minrespawn = DEFAULT_MINRESPAWN;
  kmsgproc.daemon.maxerrors = DEFAULT_MAXERROR;
  kmsgproc.daemon.probation_period = DEFAULT_PROBATION;
  kmsgproc.daemon.registration_policy = DEFAULT_REGISTRATION;
  kmsgproc.daemon.restart_policy = DEFAULT_RESTART_POLICY;
  kmsgproc.daemon.down_script_policy = DEFAULT_DOWN_POLICY;
  kmsgproc.daemon.last_node.node_num = 0;
  kmsgproc.daemon.slot = -1;
  safe_strncpy(kmsgproc.magic, KEEP_MAGIC, sizeof(kmsgproc.magic)); 
  for (i=0; i < MAX_NODES; i++) {
    kmsgproc.daemon.backup_nodes[i] = 0;
  }
  backup_index = 0;

#ifdef DEBUG
for (c = 0; c < argc; c++) {
  syslog(LOG_DEBUG, "%s: main(): argv[%d] = >%s<.\n", program_name, c, argv[c]);
}
#endif
  while(!rflag && !dflag && ((c = getopt(argc, argv, "B:d:D:F:g:i:kLn:aoP:p:QqR:r:S:s:Uv:XxZ:z:c:")) != EOF)) {
    switch(c){

      case 'a':
        if (kmsgproc.daemon.registration_policy == REGISTER_BY_PID) {
          syslog(LOG_ERR, "%s: Cannot use -a with the -o option.\n", program_name);
          (void) fprintf(stderr, "%s: Cannot use -a with the -o option.\n", program_name);
          exit(1);
			  }

        kmsgproc.daemon.registration_policy = REGISTER_BY_ARGS;
        break;

      case 'c':
        /* Verify that -c option is valid and that an exit code is given.
         * Take advantage of short circuit boolean OR.
        */
        if (((token_index=getsubopt(&optarg, c_opt_tokens, &c_opt_value)) < 0) ||
            (c_opt_value == NULL)) {
          syslog(LOG_ERR, "%s: Invalid -c usage.\n", program_name);
          (void) fprintf(stderr, "%s: Invalid -c usage.\n", program_name);
          exit(1);
        }

        /* Setup to check for invalid exit code. */
        errno = 0;

        /* Assign exit code to use for -c option. */
        if (token_index == REJECT) {
          kmsgproc.daemon.reject_exit_code_used = TRUE;
          kmsgproc.daemon.reject_exit_code = atoi(c_opt_value);
        }
        else if (token_index == DOWN) {
          kmsgproc.daemon.down_exit_code_used = TRUE;
          kmsgproc.daemon.down_exit_code = atoi(c_opt_value);
        }
        else {
          syslog(LOG_ERR, "%s: Invalid -c usage.\n", program_name);
          (void) fprintf(stderr, "%s: Invalid -c usage.\n", program_name);
          exit(1);
        }

        /* Check for invalid exit code. */
        if (errno == EINVAL) 
        {
          syslog(LOG_ERR, "%s: Invalid -c usage.\n", program_name);
          (void) fprintf(stderr, "%s: Invalid -c usage.\n", program_name);
          exit(1);
        }

        /* If same exit code has been given for both options, it's an error. */
        if (kmsgproc.daemon.reject_exit_code_used &&
            kmsgproc.daemon.down_exit_code_used &&
            (kmsgproc.daemon.reject_exit_code == 
             kmsgproc.daemon.down_exit_code))
        {
          syslog(LOG_ERR, "%s: Can't use same exit code for both -c options.\n", program_name);
          (void) fprintf(stderr, "%s: Can't use same exit code for both -c options.\n", program_name);
          exit(1);
        }

        break;

      case 'o':
        if (kmsgproc.daemon.registration_policy == REGISTER_BY_ARGS) {
          syslog(LOG_ERR, "%s: Cannot use -o with the -a option.\n", program_name);
          (void) fprintf(stderr, "%s: Cannot use -o with the -a option.\n", program_name);
          exit(1);
        }

        kmsgproc.daemon.registration_policy = REGISTER_BY_PID;
        break;

      case 'p':

        if (cmdset) {
          usage(argc, argv);
          /*NOTREACHED*/
        }
        if( uid == 0 || uid == 2 || uid == 3 || uid == 4 )
          ;
        else{
          syslog(LOG_ERR, "%s: Cannot use %s with the -p option if you are not a super user.\n", program_name, program_name);
          (void) fprintf(stderr, "%s: Cannot use %s with the -p option if you are not a super user.\n", program_name, program_name);
          exit(1);
        }
        cmdset = TRUE;
        kmsgproc.cmd |= DELETE;
        for(i = 0; optarg[i] != '\0'; i++) {
          if( (isalpha(optarg[i]) != 0) ) {
            syslog(LOG_ERR, "%s: Invalid process ID.\n", program_name);
            (void) fprintf(stderr, "%s: Invalid process ID.\n", program_name);
            usage(argc, argv);
            /* not reached */
          }
        }
        kmsgproc.daemon.pid = atoi(optarg);
        break;

      case 'd':

        if (cmdset) {
          usage(argc, argv);
          /*NOTREACHED*/
        }
        if( uid == 0 || uid == 2 || uid == 3 || uid == 4 )
          ;
        else{
          syslog(LOG_ERR, "%s: Cannot use %s with the -d option if you are not a super user.\n", program_name, program_name);
          (void) fprintf(stderr, "%s: Cannot use %s with the -d option if you are not a super user.\n", program_name, program_name);
          exit(1);
        }
        cmdset = TRUE;
        dflag = TRUE;
        kmsgproc.cmd |= DELETE;
        safe_strncpy(kmsgproc.daemon.name, optarg, sizeof(kmsgproc.daemon.name));
        break;

      case 'q':

        if (cmdset) {
          usage(argc, argv);
          /*NOTREACHED*/
        }
        if( uid == 0 || uid == 2 || uid == 3 || uid == 4 )
          ;
        else{
          syslog(LOG_ERR, "%s: Cannot use %s with the -q option if you are not a super user.\n", program_name, program_name);
          (void) fprintf(stderr, "%s: Cannot use %s with the -q option if you are not a super user.\n", program_name, program_name);
          exit(1);
        }
        cmdset = TRUE;
        kmsgproc.cmd |= QUIET;
        break;

      case 'Q':

        if (cmdset) {
          usage(argc, argv);
          /*NOTREACHED*/
        }
        if( uid == 0 || uid == 2 || uid == 3 || uid == 4 )
          ;
        else{
          syslog(LOG_ERR, "%s: Cannot use %s with the -Q option if you are not a super user.\n", program_name, program_name);
          (void) fprintf(stderr, "%s: Cannot use %s with the -Q option if you are not a super user.\n", program_name, program_name);
          exit(1);
        }
        cmdset = TRUE;
        kmsgproc.cmd |= QUIT;
        break;

      case 'L':

        if (cmdset) {
          usage(argc, argv);
          /*NOTREACHED*/
        }
        cmdset = TRUE;
        kmsgproc.cmd = LIST;
        localprint = TRUE;
        break;

    case 'k':

      if( uid == 0 || uid == 2 || uid == 3 || uid == 4 )
        ;
      else{
        syslog(LOG_ERR, "%s: Cannot use %s with the -k option if you are not a super user.\n", program_name, program_name);
        (void) fprintf(stderr, "%s: Cannot use %s with the -k option if you are not a super user.\n", program_name, program_name);
        exit(1);
      }
      kmsgproc.cmd |= THEN_KILL ;
      killing = TRUE;
      break;

    case 'i':
      if (strcmp(optarg, "cluster_wide") == 0) {
        idempotency_policy = CLUSTER_WIDE;
      }
      else if (strcmp(optarg, "node_list") == 0) {
        idempotency_policy = NODE_LIST;
      } 
      else
      {
        usage(argc, argv);
      }
      break;

    case 'r': 

      if (cmdset) {
        usage(argc, argv);
        /*NOTREACHED*/
      }
      if( uid == 0 || uid == 2 || uid == 3 || uid == 4 )
        ;
      else{
        syslog(LOG_ERR, "%s: Cannot use %s with the -r option if you are not a super user.\n", program_name, program_name);
        (void) fprintf(stderr, "%s: Cannot use %s with the -r option if you are not a super user.\n", program_name, program_name);
        exit(1);
      }
      cmdset = TRUE;
      rflag = TRUE;
      conf_file = optarg;
      if( (filename = strstr(optarg, "ka_")) == NULL){
        syslog(LOG_ERR, "%s: Incorrect usage of the -r option.\n", program_name);
        (void) fprintf(stderr, "%s: Incorrect usage of the -r option.\n", program_name);
        usage(argc, argv);
        /* not reached */
      }
      break;
        
    case 'n':

      nprocs = atoi(optarg);
      break;

    case 'R':

      if(cmdset) {
        usage(argc, argv);
      }
      if( uid == 0 || uid == 2 || uid == 3 || uid == 4 )
        ;
      else{
        syslog(LOG_ERR, "%s: Cannot use %s with the -R option if you are not a super user.\n", program_name, program_name);
        (void) fprintf(stderr, "%s: Cannot use %s with the -R option if you are not a super user.\n", program_name, program_name);
        exit(1);
      }
      cmdset = TRUE;
      Rflag = TRUE;
      conf_file = optarg;
      if( (filename = strstr(optarg, "ka_")) == NULL){
        syslog(LOG_ERR, "%s: Incorrect usage of the -R option.\n", program_name);
        (void) fprintf(stderr, "%s: Incorrect usage of the -R option.\n", program_name);
        usage(argc, argv);
        /* not reached */
      }

      kmsgproc.cmd |= REGISTER;
      break;

    case 'D':

      kmsgproc.cmd |= RESET;
      if( (strstr(optarg, "-")) != NULL) {
        usage(argc, argv);
        /* not reached */
      }
      safe_strncpy(kmsgproc.daemon.name, 
          optarg, sizeof(kmsgproc.daemon.name));
      break;         

    case 'P':

      if( (strstr(optarg, "-")) != NULL) {
        usage(argc, argv);
        /* not reached */
      }
      for(i = 0; optarg[i] != '\0'; i++) {
        if( (isalpha(optarg[i]) != 0) ) {
          syslog(LOG_ERR, "%s: Invalid process ID\n", program_name);
          (void) fprintf(stderr, "%s: Invalid process ID\n", program_name);
          usage(argc, argv);
          /* not reached */
        }
      }
      cmdset = TRUE;
      kmsgproc.daemon.pid = atoi(optarg);
      kmsgproc.cmd |= RESET;
      break;

    case 'S':

      if( (strstr(optarg, "-")) != NULL) {
        usage(argc, argv);
        /* not reached */
      }
      for(i = 0; optarg[i] != '\0'; i++) {
        if( (isalpha(optarg[i]) != 0) ) {
          syslog(LOG_ERR, "%s: Invalid slot ID\n", program_name);
          (void) fprintf(stderr, "%s: Invalid slot ID\n", program_name);
          usage(argc, argv);
          /* not reached */
        }
      }
      cmdset = TRUE;
      kmsgproc.daemon.slot = atoi(optarg);
      kmsgproc.cmd |= RESET;
      break;

    case 's':

        if (cmdset) {
          usage(argc, argv);
          /*NOTREACHED*/
        }
        if ( uid == 0 || uid == 2 || uid == 3 || uid == 4 )
          ;
        else{
          syslog(LOG_ERR, "%s: Cannot use %s with the -s option if you are not a super user.\n", program_name, program_name);
          (void) fprintf(stderr, "%s: Cannot use %s with the -s option if you are not a super user.\n", program_name, program_name);
          exit(1);
        }
        cmdset = TRUE;
        kmsgproc.cmd |= DELETE;
        for(i = 0; optarg[i] != '\0'; i++) {
          if( (isalpha(optarg[i]) != 0) ) {
            syslog(LOG_ERR, "%s: Invalid slot number.\n", program_name);
            (void) fprintf(stderr, "%s: Invalid slot number.\n", program_name);
            usage(argc, argv);
            /* not reached */
          }
        }
        kmsgproc.daemon.slot = atoi(optarg);
        break;

    case 'x':

      if(cmdset) {
        usage(argc, argv);
      }
      if( uid == 0 || uid == 2 || uid == 3 || uid == 4 )
        ;
      else{
        syslog(LOG_ERR, "%s: Cannot use %s with the -x option if you are not a super user.\n", program_name, program_name);
        (void) fprintf(stderr, "%s: Cannot use %s with the -x option if you are not a super user.\n", program_name, program_name);
        exit(1);
      }
      cmdset = TRUE;
      kmsgproc.cmd |= RESET;
      break;

    case 'g':

      if(cmdset) {
        usage(argc, argv);
      }
      if( uid == 0 || uid == 2 || uid == 3 || uid == 4 )
        ;
      else{
        syslog(LOG_ERR, "%s: Cannot use %s with the -g option if you are not a super user.\n", program_name, program_name);
        (void) fprintf(stderr, "%s: Cannot use %s with the -g option if you are not a super user.\n", program_name, program_name);
        exit(1);
      }
      cmdset = TRUE;
      kmsgproc.cmd = DELETE_GROUP;
      if(killing == TRUE) {
        kmsgproc.daemon.delete_group_flag = 1;
      }else
        kmsgproc.daemon.delete_group_flag = 0; 
        safe_strncpy(kmsgproc.daemon.group.name, optarg, sizeof(kmsgproc.daemon.group.name)); 
      break;

    case 'X':

      if(cmdset){
        usage(argc, argv);
      }
      if( uid == 0 || uid == 2 || uid == 3 || uid == 4 )
        ;
      else{
        syslog(LOG_ERR, "%s: Cannot use %s with the -X option if you are not a super user.\n", program_name, program_name);
        (void) fprintf(stderr, "%s: Cannot use %s with the -X option if you are not a super user.\n", program_name, program_name);
        exit(1);
      }
      cmdset = TRUE;
      kmsgproc.cmd = RESTART;
      break;
               
    case 'F':

      Fnode = atoi(optarg);
      if (Fnode <= 0) {
        syslog(LOG_ERR, "%s: Invalid node number\n", program_name);
        (void) fprintf(stderr, "%s: Invalid node number\n", program_name);
        usage(argc, argv);
      }
      Fflag = TRUE;
      break;

    case 'B':

      node = atoi(optarg);
      if (node <= 0) {
        syslog(LOG_ERR, "%s: Invalid node number\n", program_name);
        (void) fprintf(stderr, "%s: Invalid node number\n", program_name);
        usage(argc, argv);
      }

      if (backup_index == MAX_NODES) {
        syslog(LOG_ERR, "%s: No more than %d backup nodes may be specified\n", program_name, MAX_NODES);
        (void) fprintf(stderr, "%s: No more than %d backup nodes may be specified\n", program_name, MAX_NODES);
        usage(argc, argv);
      }

      /* Make sure this node is different than any other backup nodes */
      index = 0;
      while ((kmsgproc.daemon.backup_nodes[index] != 0) &&
          (index < MAX_NODES)) {
        if (node == kmsgproc.daemon.backup_nodes[index]) {
          syslog(LOG_ERR, "%s: Duplicated node number\n", program_name);
          (void) fprintf(stderr, "%s: Duplicated node number\n", program_name);
          usage(argc, argv);
        }
        index++;
      }

      /* Make sure this node is different than the primary node */
      if (node == Fnode) {
        syslog(LOG_ERR, "%s: Duplicated node number\n", program_name);
        (void) fprintf(stderr, "%s: Duplicated node number\n", program_name);
        usage(argc, argv);
      }
      kmsgproc.daemon.backup_nodes[backup_index] = node;
      backup_index++;
      Bflag = TRUE;
        
      break;

    case 'U':
      Uflag = TRUE;
      break;

    case 'v':
      vflag = TRUE;

      if (strncmp(optarg, "machine", 7) == 0) {
        verbosity=MACHINE;
      }
      else if (strncmp(optarg, "human", 5) == 0) {
        verbosity=HUMAN;
      }
      else {
        syslog(LOG_ERR, "%s: Invalid verbosity usage '%s'.\n", program_name, optarg);
        (void) fprintf(stderr, "%s: Invalid verbosity usage '%s'.\n", program_name, optarg);
        exit(1);
      }
      break;

    case 'Z':

      restart_policy = optarg;
      if (strcmp(restart_policy, "last_node") == 0) {
        kmsgproc.daemon.restart_policy = LAST_NODE;
      }
      else if (strcmp(restart_policy, "F_node") == 0) {
        kmsgproc.daemon.restart_policy = F_NODE;
      }
      else if (strcmp(restart_policy, "round_robin") == 0) {
        kmsgproc.daemon.restart_policy = ROUND_ROBIN;
      }
      else if (strcmp(restart_policy, "root_node") == 0) {
        kmsgproc.daemon.restart_policy = ROOT_NODE;
      }
      else {
        syslog(LOG_ERR, "%s: Invalid restart policy '%s'.\n", program_name, restart_policy);
        (void) fprintf(stderr, "%s: Invalid restart policy '%s'.\n", program_name, restart_policy);
        exit(1);
      }
      Zflag = TRUE;
      break;

    case '?':

    default:

      usage(argc, argv);

      /*NOTREACHED*/
    }
  }

  /* Check for '-L' optional parameter. */
  if (kmsgproc.cmd == LIST) {
    if (argv[optind]) {
      if (strcmp(argv[optind], "processes") == 0) {
        print_object = PRINT_DAEMONS;
      }
      else if (strcmp(argv[optind], "keepalive") == 0) {
        print_object = PRINT_KEEPALIVE;
      }
      else {
        syslog(LOG_ERR, "%s: List options invalid.  Expected \"processes\" or \"keepalive\".\n", program_name);
        (void)fprintf(stderr, "%s: List options invalid.  Expected \"processes\" or \"keepalive\".\n", program_name);
        exit(1);
      }
    }
  }

  /* If registering by PID, read the PID. */
  if (Rflag) {
    if (!(optind < argc)) {
      syslog(LOG_ERR, "%s: You must specify a numeric pid which is the pid of the running process you try to register (-R) with keepalive.\n", program_name);
      fprintf(stderr, "%s: You must specify a numeric pid which is the pid of the running process you try to register (-R) with keepalive.\n", program_name);
      usage(argc, argv);
    }

    for(i = 0; argv[optind][i] != '\0'; i++) {
      if( (isalpha(argv[optind][i]) != 0) ) {
        syslog(LOG_ERR, "%s: You must specify a numeric pid which is the pid of the running process you try to register (-R) with keepalive.\n", program_name);
        fprintf(stderr, "%s: You must specify a numeric pid which is the pid of the running process you try to register (-R) with keepalive.\n", program_name);
        usage(argc, argv);
      }
    }
    kmsgproc.daemon.pid = atoi(argv[optind]);
     
    /* Verify PID and capture node status. */ 
    if (kill(kmsgproc.daemon.pid, 0) < 0) {
      syslog(LOG_ERR, "%s: PID '%d' is not running.\n", program_name, kmsgproc.daemon.pid);
      (void) fprintf(stderr, "%s: PID '%d' is not running.\n", program_name, kmsgproc.daemon.pid);
      exit(1);
    }
    else {
      clusternode_info(node_pid(kmsgproc.daemon.pid), 
                       sizeof(clusternode_info_t), 
                       &kmsgproc.daemon.last_node);

      if (kmsgproc.daemon.last_node.node_state != CLUSTERNODE_UP) {
        /* The node has become down just as the daemon was started, so we
         * want a transid that is guaranteed to be invalid (to the extent
         * that we will never see it from a clusternode_info() call).
        */
        kmsgproc.daemon.last_node.node_lasttransid = (transid_t)0;
      }
    }
  }

  /*
   *   Used to verify permission in keepalive.
   */
  kmsgproc.daemon.euid = geteuid();
  kmsgproc.daemon.egid = getegid();

  if(!cmdset || ((kmsgproc.cmd&CMD_MASK) == ADD && kmsgproc.cmd != ADD)) {
    usage(argc, argv);
    /*NOTREACHED*/
  }

  if (ADD == kmsgproc.cmd) {
    if ( ((argc - optind) != 0) || (needs_ka && (NULL==conf_file)) || (!needs_ka && (NULL!=conf_file)) ){
      if(filename == NULL) {
        ;
      }else{
        usage(argc, argv);
        /*NOTREACHED*/
      }
    }
  }

  if ( ((ADD != kmsgproc.cmd) && 
    (((DELETE & kmsgproc.cmd) != 0) && 
                 (kmsgproc.daemon.registration_policy != REGISTER_BY_ARGS)) &&
    (REGISTER != kmsgproc.cmd) && 
    (RESET != kmsgproc.cmd) && 
    (RESTART != kmsgproc.cmd) && 
    (argc != optind)) ) {
    usage(argc, argv);
    /*NOTREACHED*/
  }

  /* Verify that the '-o' and '-a' options are used correctly. */
  if ((((kmsgproc.daemon.registration_policy == REGISTER_BY_PID) ||
        (kmsgproc.daemon.registration_policy == REGISTER_BY_ARGS)) &&
       (!rflag && !dflag && !Rflag)) ||
      ((kmsgproc.daemon.registration_policy == REGISTER_BY_PID) &&
       dflag))
  {
    usage(argc, argv);
    /*NOTREACHED*/
  }

  if (Bflag == TRUE) {
    if (Fnode == 0) {
      syslog(LOG_ERR, "%s: -B requires -F option too.\n", program_name);
      (void)fprintf(stderr, "%s: -B requires -F option too.\n", program_name);
      usage(argc, argv);
      /*NOTREACHED*/
    }
  }

  if ((Fflag == TRUE) && 
           (kmsgproc.daemon.restart_policy == ROUND_ROBIN)) {
    syslog(LOG_ERR, "%s: round_robin restart policy cannot be used with -F option.\n", program_name);
    (void)fprintf(stderr, "%s: round_robin restart policy cannot be used with -F option.\n", program_name);
    usage(argc, argv);
    /*NOTREACHED*/
  }

  /* -F can only be used with -r and -R */
  if ((Fnode != 0) && (rflag == FALSE) && (Rflag == FALSE)) {
    usage(argc, argv);
    /*NOTREACHED*/
  }

  /* -U can only be used with -r and -R */
  if ((Uflag == TRUE) && (rflag == FALSE) && (Rflag == FALSE)) {
    usage(argc, argv);
    /*NOTREACHED*/
  }

  /* -Z can only be used with -r and -R */
  if ((Zflag == TRUE) && (rflag == FALSE) && (Rflag == FALSE)) {
    usage(argc, argv);
    /*NOTREACHED*/
  }

  /* -Z cannot be used if -U is used */
  if ((Zflag == TRUE) && (Uflag == TRUE)) {
    usage(argc, argv);
    /*NOTREACHED*/
  }

  kmsgproc.daemon.node = Fnode;
  kmsgproc.daemon.pin_anywhere = !Uflag;

  ka_com_succeeded = init(nprocs, needs_ka, kmsgproc.cmd == LIST);


  /* Check for the case where we want to register, but Keepalive is not active.
   */
  if (!ka_com_succeeded && needs_ka) {
    /* 
     *  init will return 1 if we cannot communicate with keepalive.
     *  we cannot restart when keepalive is not around.
     */
    syslog(LOG_ERR, "%s: Cannot communicate with keepalive.  Keepalive is down.\n", program_name);
    (void)fprintf(stderr, "%s: Cannot communicate with keepalive.  Keepalive is down.\n", program_name);
    if (ADD & kmsgproc.cmd) { 
      syslog(LOG_ERR, "%s: Executable '%s' cannot be started and watched by Keepalive.\n", program_name, argv[optind]);
      (void)fprintf(stderr, "%s: Executable '%s' cannot be started and watched by Keepalive.\n", program_name, argv[optind]);
      exit(1);
    }
    else if (REGISTER & kmsgproc.cmd) {
      syslog(LOG_ERR, "%s: PID '%d' cannot be registered with Keepalive.\n", program_name, kmsgproc.daemon.pid);
      (void)fprintf(stderr, "%s: PID '%d' cannot be registered with Keepalive.\n", program_name, kmsgproc.daemon.pid);
      exit(1);
    }

    needs_ka = FALSE ;
  }

  /* Make sure that Keepalive's process table doesn't need to be resized, if we are registering a daemon . */
  if ((ADD & kmsgproc.cmd) || (REGISTER & kmsgproc.cmd)) {
    if (pkctl->registered_daemons >= pkctl->max_possible_daemons) {
      syslog(LOG_ERR, "%s: Only %ld slots are available for Keepalive registration.\n", program_name, pkctl->max_possible_daemons);
      (void)fprintf(stderr, "%s: Only %ld slots are available for Keepalive registration.\n", program_name, pkctl->max_possible_daemons);
      exit(1);
    }
  }

  if ( kmsgproc.cmd == LIST ) {
    printprocs(localprint, print_object, verbosity);

    if (!ka_com_succeeded) {
      syslog(LOG_ERR, "%s: Keepalive is not running.  The processes listed are not being monitored.\n", program_name);
      fprintf(stderr, "%s: Keepalive is not running.  The processes listed are not being monitored.\n", program_name);
      exit(1);
    }

    if (pkctl->quiesce_flag == TRUE) {
      syslog(LOG_WARNING, "%s: Keepalive is quiesced and is not monitoring processes.  Use '/sbin/spawndaemon -X' to get Keepalive to resume monitoring processes.\n", program_name);
      fprintf(stderr, "%s: Keepalive is quiesced and is not monitoring processes.  Use '/sbin/spawndaemon -X' to get Keepalive to resume monitoring processes.\n", program_name);
    }

    exit(0);
  }

  if (ADD != kmsgproc.cmd) {

    /* What if it is REGISTER  */
    if(REGISTER == (kmsgproc.cmd) ) {
      for(i = 0; i < argc; i++) {
        if( (proc_file = strstr(argv[i], "ka_")) != NULL) 
          break;
      }
      if((i == argc) && (proc_file == NULL)) {
        syslog(LOG_ERR, "%s: Usage: Trying to register the process with pid=%d you need to specify a configuration file.\n", program_name, kmsgproc.daemon.pid);
        (void) fprintf(stderr, "%s: Usage: Trying to register the process with pid=%d you need to specify a configuration file.\n", program_name, kmsgproc.daemon.pid);
        exit(1);
      }
      register_proc(proc_file, kmsgproc.daemon.pid);
      exit(0);
    }

    if(RESET == kmsgproc.cmd) {
      writemsg(&kmsgproc);
      exit(0);
    }

    if(RESTART == kmsgproc.cmd) {
      writemsg(&kmsgproc);
      exit(0);
    } 

    if( (DELETE != (kmsgproc.cmd & CMD_MASK)) && (DELETE_GROUP != kmsgproc.cmd)) {
      writemsg(&kmsgproc);
      exit(0);
    }else{ 
      if((needs_ka) && (DELETE_GROUP == kmsgproc.cmd)) {
        writemsg(&kmsgproc);
        exit(0);
      }else     
        if (needs_ka) {
          kmsgproc.daemon.maxerrors = nprocs;
          kmsgproc.daemon.total_errors = nprocs;
          /* adjust argv to represent part not consumed by getopt() */
          argv += optind;

          /* argv contains the argument list for the daemon */ 
          tokens_to_str(kmsgproc.daemon.args, argv);
          writemsg(&kmsgproc);
          exit(0);
        }
    }
    if ( (0 != (kmsgproc.cmd & THEN_KILL) ) ) {
      writemsg(&kmsgproc);
      exit(0);
    }                 

  }else{ /* The command is to ADD a daemon. */
#ifdef DEBUG
syslog_msg = "%s: main() needs_ka = %d.\n";
syslog(LOG_DEBUG, syslog_msg, program_name, needs_ka);
#endif

    /* We exec the daemon(s) and register it(them) */
    /* with keepalive.                             */ 
    argv += optind;
    process_conf_file(filename, argv);
  }

  exit(0);
}
 


/*
 * Signal handler
*/
static void
sig_parent(int i)
{

  /* 
  * Set sig_check to 1;
  * and return.
  */
  sig_check = 1;
  return;
}

void
process_registration(char *daemon_cfg_file, pid_t pid)
{
  ind_conf_file  *inf;
  struct stat buf;
  struct passwd *p;
  struct group *g;
  char *string;
  clock_t lbolt;

  inf = get_file_fields(daemon_cfg_file);

  safe_strncpy(kmsgproc.daemon.cfg_file, daemon_cfg_file, sizeof(kmsgproc.daemon.cfg_file));
  safe_strncpy(kmsgproc.daemon.name, inf->daemon_name, sizeof(kmsgproc.daemon.name));
  safe_strncpy(kmsgproc.daemon.args, inf->daemon_args, sizeof(kmsgproc.daemon.args));
  kmsgproc.daemon.pid = pid;
  kmsgproc.daemon.last_node.node_num = node_pid(kmsgproc.daemon.pid);

  if(stat(kmsgproc.daemon.name, &buf) < 0) {
    syslog(LOG_ERR, "%s: Executable '%s' cannot be found. Process is not spawned.  Check the process configuration file '/etc/spawndaemon.d/%s'.\n", program_name, kmsgproc.daemon.name, daemon_cfg_file);
    fprintf(stderr, "%s: Executable '%s' cannot be found. Process is not spawned.  Check the process configuration file '/etc/spawndaemon.d/%s'.\n", program_name, kmsgproc.daemon.name, daemon_cfg_file);
    exit(1);
  }

  /* Check if process belongs to a group. */
  if(inf->gr[0] != '\0'){
    safe_strncpy(kmsgproc.daemon.group.name, inf->gr, sizeof(kmsgproc.daemon.group.name));

    /* Set default restart policy, if one was not specified. */
    if ( kmsgproc.daemon.restart_policy == DEFAULT_RESTART_POLICY) {
      /* '-Z last_node' is the default for Keepalive process groups
       * as a kluge for keeping process groups together until
       * we can work on a more cohesive design. 
      */
      kmsgproc.daemon.restart_policy = LAST_NODE;
    }
  }
  else {
    /* We don't belong to a group. */
    kmsgproc.daemon.group.name[0] = '\0';

    /* Set default restart policy, if one was not specified. */
    if (kmsgproc.daemon.restart_policy == DEFAULT_RESTART_POLICY) {
      /* Choose '-Z round_robin' as the default restart policy. */
      kmsgproc.daemon.restart_policy = ROUND_ROBIN;
    }
  }

  if(inf->uid[0] == '\0') { 
    /* allocate some space for converting uid int to string. */
    if ((string = (char *)malloc(10)) == NULL) {
      syslog(LOG_ERR, "%s: Error in malloc|calloc\n", program_name);
      fprintf(stderr, "%s: Error in malloc|calloc\n", program_name);
      exit(1);
    } else {
      sprintf(string, "%u", buf.st_uid);
      inf->uid = string;
      syslog(LOG_INFO, "%s: User Id is obtained from the executable\n", program_name);
      fprintf(stderr, "%s: User Id is obtained from the executable\n", program_name);
    }
  }else{ 
    p = getpwuid(atoi(inf->uid));
    if(p == NULL || p->pw_uid != atoi(inf->uid) ){
      if ((string = (char *)malloc(10)) == NULL) {
        syslog(LOG_ERR, "%s: Error in malloc|calloc\n", program_name);
        fprintf(stderr, "%s: Error in malloc|calloc\n", program_name);
        exit(1);
      } else {
        sprintf(string, "%u", buf.st_uid);
        inf->uid = string;
        syslog(LOG_INFO, "%s: User Id is obtained from the executable\n", program_name);
        fprintf(stderr, "%s: User Id is obtained from the executable\n", program_name);
      }
    }
  }
  if(inf->gid[0] == '\0') { 
    if ((string = (char *)malloc(10)) == NULL) {
      syslog(LOG_ERR, "%s: Error in malloc|calloc\n", program_name);
      fprintf(stderr, "%s: Error in malloc|calloc\n", program_name);
      exit(1);
    } else {
      sprintf(string, "%u", buf.st_gid);
      inf->gid = string;
      syslog(LOG_INFO, "%s: Group Id is obtained from the executable\n", program_name);
      fprintf(stderr, "%s: Group Id is obtained from the executable\n", program_name);
    }
  }else { 
    g  = getgrgid(atoi(inf->gid));
    if(g == NULL || g->gr_gid != atoi(inf->gid) ){ 
      if ((string = (char *)malloc(10)) == NULL) {
        syslog(LOG_ERR, "%s: Error in malloc|calloc\n", program_name);
        fprintf(stderr, "%s: Error in malloc|calloc\n", program_name);
        exit(1);
      } else {
        sprintf(string, "%u", buf.st_gid);
        inf->gid = string;
        syslog(LOG_INFO, "%s: Group Id is obtained from the executable\n", program_name);
        fprintf(stderr, "%s: Group Id is obtained from the executable\n", program_name);
      }
    }
  }

  if (!inf->startup_script) {
      syslog(LOG_ERR, "%s: Startup script must be specified in process configuration file '%s'.  Executable '%s' will not be started nor watched by Keepalive.\n", program_name, daemon_cfg_file, kmsgproc.daemon.name);
      fprintf(stderr, "%s: Startup script must be specified in process configuration file '%s'.  Executable '%s' will not be started nor watched by Keepalive.\n", program_name, daemon_cfg_file, kmsgproc.daemon.name);
      exit(1);
  }

  check_script(daemon_cfg_file, inf->startup_script, kmsgproc.daemon.startup_script, sizeof(kmsgproc.daemon.startup_script));

  check_script(daemon_cfg_file, inf->shutdown_script, kmsgproc.daemon.shutdown_script, sizeof(kmsgproc.daemon.shutdown_script));

  check_script(daemon_cfg_file, inf->scr_name, kmsgproc.daemon.process_failure_script, sizeof(kmsgproc.daemon.process_failure_script));

  check_script(daemon_cfg_file, inf->node_scr_name, kmsgproc.daemon.node_failure_script, sizeof(kmsgproc.daemon.node_failure_script));

  check_script(daemon_cfg_file, inf->down_script, kmsgproc.daemon.down_script, sizeof(kmsgproc.daemon.down_script));

  if(inf->gr[0] == '\0') {
    memset(kmsgproc.daemon.group.name, 0, sizeof(kmsgproc.daemon.group.name) - 1);
  }
  if(inf->termwait != -1) {
    kmsgproc.daemon.termwait = inf->termwait;
  }else {
    kmsgproc.daemon.termwait = DEFAULT_TERMWAIT;
  }
  if(inf->mer != -1) {
    kmsgproc.daemon.maxerrors = inf->mer;
  }else{
    kmsgproc.daemon.maxerrors = DEFAULT_MAXERROR;
  }
  if(inf->prb != -1) {
    kmsgproc.daemon.probation_period = inf->prb;
  }else{
    kmsgproc.daemon.probation_period =  DEFAULT_PROBATION;
  }
  if(inf->minr != -1) {
    kmsgproc.daemon.minrespawn = inf->minr;
  }else{
    kmsgproc.daemon.minrespawn = DEFAULT_MINRESPAWN;
  }

  if (inf->down_script_policy != -1) {
    kmsgproc.daemon.down_script_policy = inf->down_script_policy;
  }
  else {
    kmsgproc.daemon.down_script_policy = DEFAULT_DOWN_POLICY;
  }

  lbolt = get_lbolt();
  kmsgproc.daemon.lastexeced.lbolt_in_sec = lbolt;
  kmsgproc.daemon.lastexeced.date = get_pid_start(pid);
#ifdef DEBUG
  syslog(LOG_DEBUG, "process_registration: lastexeced: %s\n", ctime(&kmsgproc.daemon.lastexeced.date));
#endif /* DEBUG */
  writemsg(&kmsgproc);
}

   
void 
register_proc(char *regname, pid_t pid)
{
  char *fname; 
  char buf[MAXNAME];

  sprintf(buf, "/etc/spawndaemon.d/%s", regname);
  fname = buf;
  if(pid == 0) {
    syslog(LOG_ERR, "%s: Usage: The process with the configuration file of '%s' should be already running to use the -R option.\n", program_name, fname);
    fprintf(stderr, "%s: Usage: The process with the configuration file of '%s' should be already running to use the -R option.\n", program_name, fname);
    exit(1);
  }
  if( check_conf_file_perms(fname) == 0) {
    syslog(LOG_INFO, "%s: Registration is aborted. The configuration file %s does not have the proper permissions\n", program_name, fname);
    fprintf(stderr, "%s: Registration is aborted. The configuration file %s does not have the proper permissions\n", program_name, fname);
  }else if( file_group_or_individual(fname) == 1 ) {
    syslog(LOG_INFO, "%s: The running process belongs to a group\n", program_name );
    fprintf(stderr, "%s: The running process belongs to a group\n", program_name);
  }else{
    process_registration(fname, pid);
  }
}



void
writemsg(kalive_cmd_t *m)
{
  int j, size;
  char *p;
 
#ifdef DEBUG
syslog_msg = "%s: ENTER writemsg().\n";
syslog(LOG_DEBUG, syslog_msg, program_name);
#endif

  /* If we can't communicate with keepalive, don't try to send a message. */
  if (!ka_com_succeeded) {
    /* Do nothing. */
    return;
  }

  safe_strncpy(m->magic, KEEP_MAGIC, sizeof(m->magic));
  m->size = sizeof(kalive_cmd_t);
  size = sizeof(*m);
  p = (char *)m;
#ifdef DEBUG	/* KS_DEBUG */
  syslog(LOG_DEBUG, "size: %d\nsizeof(kalive_cmd_t): %d\nsizeof(*m): %d", size, sizeof(kalive_cmd_t), sizeof(*m));
#endif /* DEBUG */
  j = write(pipefd, p, size);
  while (1) {
#ifdef DEBUG	/* KS_DEBUG */
  syslog(LOG_DEBUG, "write returned %d\n", j);
#endif /* DEBUG */
    if ( j < 0 ) {
      if (errno == EAGAIN)
	continue;
      syslog(LOG_ERR, "%s: Error in open|write pipe %s\n", program_name, KEEPCFG);
      fprintf(stderr, "%s: Error in open|write pipe %s\n", program_name, KEEPCFG);
#ifdef DEBUG	/* KS_DEBUG */
      syslog(LOG_DEBUG, "errno : %d\n", errno);
#endif /* DEBUG */
      exit(1);
    }
    else if (j == size) {
      /* We're done. */
      break;
    }
    else {
      /* Process a partial write. */
      p = p + j;
      size = size - j;
      j = write(pipefd, p, size);
    }
  }

#ifdef DEBUG
syslog_msg = "%s: EXIT writemsg().\n";
syslog(LOG_DEBUG, syslog_msg, program_name);
#endif
}



/*
 *                    Function usage()
 *
 * Description
 *
 * The usage() routine displays the correct usage and arguments
 * of spawndaemon to the user.
 *
 * Interface
 *
 * void usage()
 *
 * Returns
 *
 * Nothing
*/ 
void
usage(int argc, char **argv)
{
  char buf[MAXNAME];
  int i;

  /* Build command-line used by user. */
  buf[0] = '\0';
  for (i = 0; i < argc; i++) {
    if (buf[0] != '\0') {
      strcat(buf, " ");
    }

    strcat(buf, argv[i]);
  }

  (void)fprintf(stderr, "%s: ERROR: Invalid usage '%s'.\n\n", program_name, buf);
  syslog(LOG_ERR, "%s: ERROR: Invalid usage '%s'.\n\n", program_name, buf);

  (void)fprintf(stderr, "%s: ERROR: Usage:\n\n", program_name);
  syslog(LOG_ERR, "%s: ERROR: Usage:\n\n", program_name);

  fprintf(stderr, "%s [-i cluster_wide | node_list] [-n #] [-Z last_node | -Z round_robin | -Z root_node] [-o] [-a] [-c down=<exit_code> | reject=<exit_code>] -r process_cfg_file\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-i cluster_wide | node_list] [-n #] [-Z last_node | -Z round_robin | -Z root_node] [-o] [-a] [-c down=<exit_code> | reject=<exit_code>] -r process_cfg_file\n", program_name);

  fprintf(stderr, "%s [-i cluster_wide | node_list] [-Z last_node | -Z round_robin | -Z root_node] [-o] -R process_cfg_file pid\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-i cluster_wide | node_list] [-Z last_node | -Z round_robin | -Z root_node] [-o] -R process_cfg_file pid\n", program_name);

  fprintf(stderr, "%s [-i cluster_wide | node_list] [-n #] [-Z last_node | -Z round_robin | -Z root_node] [-o] [-a] -r group_cfg_file\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-i cluster_wide | node_list] [-n #] [-Z last_node | -Z round_robin | -Z root_node] [-o] [-a] -r group_cfg_file\n", program_name);

  fprintf(stderr, "%s [-i cluster_wide | node_list] [-n #] -F node [-B node [-B node ...]] [-Z F_node | -Z last_node | -Z round_robin | -Z root_node] [-o] [-a] [-c down=<exit_code> | reject=<exit_code>] -r process_cfg_file\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-i cluster_wide | node_list] [-n #] -F node [-B node [-B node ...]] [-Z F_node | -Z last_node | -Z round_robin | -Z root_node] [-o] [-a] [-c down=<exit_code> | reject=<exit_code>] -r process_cfg_file\n", program_name);

  fprintf(stderr, "%s [-i cluster_wide | node_list] -F node [-B node [-B node ...]] [-Z F_node | -Z last_node | -Z round_robin | -Z root_node] [-o] [-a] -R process_cfg_file pid\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-i cluster_wide | node_list] -F node [-B node [-B node ...]] [-Z F_node | -Z last_node | -Z round_robin | -Z root_node] [-o] [-a] -R process_cfg_file pid\n", program_name);

  fprintf(stderr, "%s [-i cluster_wide | node_list] [-n #] -F node [-B node [-B node ...]] [-Z F_node | -Z last_node | -Z round_robin | -Z root_node] [-o] [-a] -r group_cfg_file\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-i cluster_wide | node_list] [-n #] -F node [-B node [-B node ...]] [-Z F_node | -Z last_node | -Z round_robin | -Z root_node] [-o] [-a] -r group_cfg_file\n", program_name);

  fprintf(stderr, "%s [-i cluster_wide ] [-n #] -U [-o] [-a] [-c down=<exit_code>] -r process_cfg_file\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-i cluster_wide ] [-n #] -U [-o] [-a] [-c down=<exit_code>] -r process_cfg_file\n", program_name);

  fprintf(stderr, "%s [-i cluster_wide ] -U [-o] [-a] -R process_cfg_file pid\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-i cluster_wide ] -U [-o] [-a] -R process_cfg_file pid\n", program_name);

  fprintf(stderr, "%s [-i cluster_wide ] [-n #] -U [-o] [-a] -r group_cfg_file\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-i cluster_wide ] [-n #] -U [-o] [-a] -r group_cfg_file\n", program_name);

  fprintf(stderr, "%s [-k] -x -D full_pathname\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-k] -x -D full_pathname\n", program_name);

  fprintf(stderr, "%s [-k] -x -P pid\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-k] -x -P pid\n", program_name);

  fprintf(stderr, "%s [-k] -x -S slot\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-k] -x -S slot\n", program_name);

  fprintf(stderr, "%s [-k] [-a] -d full_pathname [arg_list]\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-k] [-a] -d full_pathname [arg_list]\n", program_name);

  fprintf(stderr, "%s [-k] -p pid\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-k] -p pid\n", program_name);

  fprintf(stderr, "%s [-k] -g group\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-k] -g group\n", program_name);

  fprintf(stderr, "%s [-k] -s slot\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-k] -s slot\n", program_name);

  fprintf(stderr, "%s -q\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s -q\n", program_name);

  fprintf(stderr, "%s [-k] -Q\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s [-k] -Q\n", program_name);

  fprintf(stderr, "%s -L [-v human [processes | keepalive] | -v machine [processes | keepalive]]\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s -L [-v human [processes | keepalive] | -v machine [processes | keepalive]]\n", program_name);

  fprintf(stderr, "%s -X\n", program_name);
  fprintf(stderr, "\n");
  syslog(LOG_ERR, "%s -X\n", program_name);

  exit(1);
}



/*
 *                     Function check_conf_file_perms()
 *
 * Description
 * The check_conf_file_perms() routine examines the permissions of the 
 * configuration file, the user id and the group id. 
 *
 * Interface
 *
 * int check_conf_file_perms(char *filename)
 *
 * Returns
 *
 * 1 when permissions and ids are the proper ones.
 * 0 when any of permissions and ids are not proper.
 * -1 on error.
*/

int
check_conf_file_perms(char *filename)
{

  struct stat buf;

  if(stat(filename, &buf) < 0) {
    syslog(LOG_ERR, "%s: Cannot stat %s\n", program_name, filename);
    fprintf(stderr, "%s: Cannot stat %s\n", program_name, filename);
    return(-1);
  }else{
    if(sysuid != buf.st_uid) {
      return(0);
    }
    if(sysgid != buf.st_gid) {
      return(0);
    }

    if(buf.st_mode & S_IWOTH) {
      return(0);
    }
  }
  return(1);
}

     

/*         
 *                     Function file_group_or_individual()
 *
 * Description
 *
 * The file_group_or_individual() routine examines the configuration file 
 * and it determines whether it is a group of processes or an individual
 * process.
 *
 * Interface
 *
 * int file_group_or_individual(char *filename)
 *
 * Returns
 *
 * 1 if it is a group of processes
 * 0 if it is an individual process.
 * -1 if error.
*/

int
file_group_or_individual(char *filename)
{

  int fd;
  char magic[MAXNAME];
  char *tmp;

  if( (fd = open(filename, 0)) < 0) {
    syslog(LOG_ERR, "%s: Error in open|read of file %s\n", program_name, filename);
    fprintf(stderr, "%s: Error in open|read of file %s\n", program_name, filename);
    exit(1);
  }
  if( read(fd, magic, MAXNAME) < 0) {
    syslog(LOG_ERR, "%s: Error in open|read of file %s\n", program_name, filename);
    fprintf(stderr, "%s: Error in open|read of file %s\n", program_name, filename);
    exit(1);
  }
     
  tmp = get_str(magic, ":");
  if (!tmp) {
    syslog(LOG_ERR, "%s: Invalid configuration file format.  1st field missing in configuration file %s\n", program_name, filename);
    fprintf(stderr, "%s: Invalid configuration file format.  1st field missing in configuration file %s\n", program_name, filename);
    exit(1);
  }
  else if(strcmp(tmp, "<keepalive_group>") != 0) {
    (void)close(fd);
    return(0);
  } else{
    (void)close(fd);
    return(1);
  }
}



/*
 *                     Function get_group_file_fields()
 *
 * Description
 *
 * The get_group_file_fields() routine parses the group configuration file 
 * and reads the file entries to a group file structure.
 *
 * Interface
 *
 * struct gfile *get_group_file_fields(char *filename)
 *
 * Returns
 *
 * The group file fields in a structure.
*/

gfile
*get_group_file_fields(char *filename)
{
  struct stat buf;
  char *ptr, *keep_str, *tmp;
  gfile *g;
  int line_count, i, fd;

  g = &gf;
  if(stat(filename, &buf) < 0) {
    syslog(LOG_ERR, "%s: Cannot stat %s\n", program_name, filename);
    fprintf(stderr, "%s: Cannot stat %s\n", program_name, filename);
    exit(1);
  }

  if( (fd = open(filename, O_RDONLY)) < 0) {
    syslog(LOG_ERR, "%s: Error in open|read of file %s\n", program_name, filename);
    fprintf(stderr, "%s: Error in open|read of file %s\n", program_name, filename);
    exit(1);
  }

  if( (ptr = (char *) malloc(buf.st_size + 1)) == NULL) {
    syslog(LOG_ERR, "%s: Error in malloc|calloc\n", program_name);
    fprintf(stderr, "%s: Error in malloc|calloc\n", program_name);
    exit(1);
  }

  if( read(fd, ptr, buf.st_size +1) < 0) {
    syslog(LOG_ERR, "%s: Error in open|read of file %s\n", program_name, filename);
    fprintf(stderr, "%s: Error in open|read of file %s\n", program_name, filename);
    exit(1);
  }

  line_count = 0;
  for(i = 0; i < buf.st_size; i++) {
    if(ptr[i] == '\n')
      line_count++;
  }

  keep_str = get_str(ptr, ":");
  if (!keep_str) {
    syslog(LOG_ERR, "%s:  Invalid configuration file format.  The string \"<keepalive_group>\" must be specified for Keepalive group configuration file \"%s\".\n", program_name, filename);
    fprintf(stderr, "%s:  Invalid configuration file format.  The string \"<keepalive_group>\" must be specified for Keepalive group configuration file \"%s\".\n", program_name, filename);
    exit(1);
  }
  else if(strcmp(keep_str, "<keepalive_group>") != 0) {
    syslog(LOG_ERR, "%s:  The string \"<keepalive_group>\" must be specified for Keepalive group configuration file \"%s\".\n", program_name, filename);
    fprintf(stderr, "%s:  The string \"<keepalive_group>\" must be specified for Keepalive group configuration file \"%s\".\n", program_name, filename);
    exit(1);
  } 
  
  g->gname = get_str(NULL, "\n");
  if((g->gname == NULL) || (g->gname[0] == '\0')) {
    syslog(LOG_ERR, "%s: Invalid group configuration file format.  You must provide a group name in Keepalive group configuration file \"%s\".\n", program_name, filename);
    fprintf(stderr, "%s: Invalid group configuration file format.  You must provide a group name in Keepalive group configuration file \"%s\".\n", program_name, filename);
    exit(1);
  }
  gf.gname = g->gname;

  /* If no lines are listed for group members, this is a configuration error. */
  if (line_count < 2) {
    syslog(LOG_ERR, "%s: Invalid group configuration file.  Expected 'member_file' field in \"%s\".\n", program_name, filename);
    fprintf(stderr, "%s: Invalid group configuration file.  Expected 'member_file' field in \"%s\".\n", program_name, filename);
    exit(1);
  }

  g->mbr = (member *) malloc (line_count * sizeof (member));
  for (i = 0; i < line_count -1; i++) {
    g->mbr[i].mfname = get_str(NULL, ":");
    if(g->mbr[i].mfname == NULL) {
      syslog(LOG_ERR, "%s: Invalid group configuration file.  Expected 'member_file' field in \"%s\".\n", program_name, filename);
      fprintf(stderr, "%s: Invalid group configuration file.  Expected 'member_file' field in \"%s\".\n", program_name, filename);
      exit(1);
    }
    gf.mbr[i].mfname = g->mbr[i].mfname;

    tmp = get_str(NULL, ":");
    if (tmp == NULL) {
      syslog(LOG_ERR, "%s: Invalid group configuration file.  Expected 'wait_time' field in \"%s\".\n", program_name, filename);
      fprintf(stderr, "%s: Invalid group configuration file.  Expected 'wait_time' field in \"%s\".\n", program_name, filename);
      exit(1);
    }
    else if ((isdigit(*tmp) == 0) && (*tmp != '\0')) {
      syslog(LOG_ERR, "%s: 'wait_time' field should be an integer in Keepalive group configuration file \"%s\".\n", program_name, filename);
      fprintf(stderr, "%s: 'wait_time' field should be an integer in Keepalive group configuration file \"%s\".\n", program_name, filename);
      exit(1);
    }
    g->mbr[i].wait_time = atoi(tmp);
    gf.mbr[i].wait_time = g->mbr[i].wait_time;

    tmp = get_str(NULL, "\n");
    if (tmp == NULL) {
      syslog(LOG_ERR, "%s: Invalid group configuration file.  Expected 'critical' field in %s.\n", program_name, filename);
      fprintf(stderr, "%s: Invalid group configuration file.  Expected 'critical' field in %s.\n", program_name, filename);
      exit(1);
    }
    else if((isdigit(*tmp) == 0) && (*tmp != '\0')) {
      syslog(LOG_ERR, "%s: 'critical' field should be an integer in Keepalive group configuration file \"%s\".\n", program_name, filename);
      fprintf(stderr, "%s: 'critical' field should be an integer in Keepalive group configuration file \"%s\".\n", program_name, filename);
      exit(1);
    }
    g->mbr[i].priority = atoi(tmp);
    gf.mbr[i].priority = g->mbr[i].priority;
  }
  g->mems = i;
  gf.mems = g->mems;
  (void)close(fd);
  return(g);
}



 
/*
 *                     Function find_daemon()
 *
 * Description
 *
 * The find_daemon() routine searches for the executable daemon.
 *
 * Interface
 * 
 * int find_daemon(char *pathname)
 *
 * Returns
 *
 * 1 if it finds the executable file.
 * -1 on error.
*/


int
find_daemon(char *pathname)
{

  struct stat buf;

  if (pathname[0] != '/' ) {
    syslog(LOG_ERR, "%s: Full pathname to executable \"%s\" must be given.\n", program_name, pathname);
    fprintf(stderr, "%s: Full pathname to executable \"%s\" must be given.\n", program_name, pathname);
    exit(1);
  }

  if(stat(pathname, &buf) < 0) {
    syslog(LOG_ERR, "%s: Cannot stat %s\n", program_name, pathname);
    fprintf(stderr, "%s: Cannot stat %s\n", program_name, pathname);
    return(-1);
  }else {
    return(1);
  }
}


/*
 *                     Function get_str()
 *
 * Description
 *
 * The get_str() function is the same as the strtok() function with
 * the difference of allowing the first string before the separator
 * to be a NULL string so that the sting can start with the separator.
 *
 * Interface
 * 
 * char * get_str(register char *string, const char *sepset)
 *
 * Returns
 *
 * The token string
*/

char *
get_str(register char *string, const char *sepset)
{
  register char   *delimiter;
  static char     *savept;

  /*first or subsequent call*/
  if (string == NULL)
    string = savept;

  if(string == 0)         /* return if no tokens remaining */
    return(NULL);
         
  if(*string == '\0')     /* return if no tokens remaining */
    return(NULL);

  /* move past token */
  if((delimiter = strpbrk(string, sepset)) == NULL)
    savept = 0;     /* indicate this is last token */
  else {
    *delimiter = '\0';
    savept = delimiter+1;
  }
  return(string);
}


/*
 *                     Function get_file_fields()
 *
 * Description
 *
 * The get_file_fields() routine parses the individual-process configuration
 * file and reads the file field in a structure.
 *
 * Interface
 *
 * ind_conf_file *get_file_fields(char *filename)
 *
 * Returns
 *
 * Structure with the file entries.
*/



ind_conf_file
*get_file_fields(char *filename)
{

  ind_conf_file *inf;
  struct stat buf;
  char *ptr;
  int fd;
  char *tmp;

  inf = &ipf;
  if(stat(filename, &buf) < 0) {
    syslog(LOG_ERR, "%s: Cannot stat %s\n", program_name, filename);
    fprintf(stderr, "%s: Cannot stat %s\n", program_name, filename);
    exit(1);
  }
  if( (fd = open(filename, O_RDONLY)) < 0) {
    syslog(LOG_ERR, "%s: Error in open|read of file %s\n", program_name, filename);
    fprintf(stderr, "%s: Error in open|read of file %s\n", program_name, filename);
    exit(1);
  }

  if( (ptr = (char *) calloc(1, buf.st_size + 1)) == NULL ) {
    syslog(LOG_ERR, "%s: Error in malloc|calloc\n", program_name);
    fprintf(stderr, "%s: Error in malloc|calloc\n", program_name);
    exit(1);
  }

  /* Read contents of file into buffer. */
  if (read(fd, ptr, buf.st_size) < 0 ) {
    syslog(LOG_ERR, "%s: Error in open|read of file %s\n", program_name, filename);
    fprintf(stderr, "%s: Error in open|read of file %s\n", program_name, filename);
    exit(1);
  }
  
  /* Get group name. */      
  inf->gr = get_str(ptr, ":");
  if (inf->gr == NULL) {
    syslog(LOG_ERR, "%s: Invalid process configuration file.  Expected 'group' field in '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: Invalid process configuration file.  Expected 'group' field in '%s'.\n", program_name, filename);
    exit(1);
  }

  /* Get path to daemon. */
  inf->daemon_name = get_str(NULL, ":");
  if (inf->daemon_name == NULL) {
    syslog(LOG_ERR, "%s: Invalid process configuration file.  Expected 'full_path_to_executable' field in '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: Invalid process configuration file.  Expected 'full_path_to_executable' field in '%s'.\n", program_name, filename);
    exit(1);
  }
  else if (inf->daemon_name[0] == '\0') {
    syslog(LOG_ERR, "%s: You must provide the full pathname to the executable in process configuration file '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: You must provide the full pathname to the executable in process configuration file '%s'.\n", program_name, filename);
    exit(1);
  }

  /* Get daemon argument list. */
  inf->daemon_args = get_str(NULL, ":");
  if (inf->daemon_args == NULL) {
    syslog(LOG_ERR, "%s: Invalid process configuration file.  Expected 'arg_list' field in '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: Invalid process configuration file.  Expected 'arg_list' field in '%s'.\n", program_name, filename);
    exit(1);
  }

  /* Get the process group name */
  if( read(fd, ptr, buf.st_size) < 0 ) {
    syslog(LOG_ERR, "%s: Error in open|read of file %s\n", program_name, filename);
    fprintf(stderr, "%s: Error in open|read of file %s\n", program_name, filename);
    exit(1);
  }

  /* Get termwait */
  tmp = get_str(NULL, ":");
  if (tmp == NULL) {
    syslog(LOG_ERR, "%s: Invalid process configuration file.  Expected 'termwait' field in '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: Invalid process configuration file.  Expected 'termwait' field in '%s'.\n", program_name, filename);
    exit(1);
  }
  else if( (isdigit(*tmp) == 0) && (*tmp != '\0') ) {
    syslog(LOG_ERR, "%s: 'termwait' field should be an integer in process configuration file '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: 'termwait' field should be an integer in process configuration file '%s'.\n", program_name, filename);
    exit(1);
  } 
  if (*tmp == '\0')
    inf->termwait = -1;
  else
    inf->termwait =  atoi(tmp);

  /* Get the uid */
  tmp = get_str(NULL, ":");
  if (tmp == NULL) {
    syslog(LOG_ERR, "%s: Invalid process configuration file.  Expected 'uid' field in '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: Invalid process configuration file.  Expected 'uid' field in '%s'.\n", program_name, filename);
    exit(1);
  }
  inf->uid = tmp;

  /* Get the gid */
  tmp = get_str(NULL, ":");
  if (tmp == NULL) {
    syslog(LOG_ERR, "%s: Invalid process configuration file.  Expected 'gid' field in '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: Invalid process configuration file.  Expected 'gid' field in '%s'.\n", program_name, filename);
    exit(1);
  }
  inf->gid = tmp;
        
  /* Get the maximum errors */
  tmp = get_str(NULL, ":");
  if (tmp == NULL) {
    syslog(LOG_ERR, "%s: Invalid process configuration file.  Expected 'max_errors' field in '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: Invalid process configuration file.  Expected 'max_errors' field in '%s'.\n", program_name, filename);
    exit(1);
  }
  else if( (isdigit(*tmp) == 0) && (*tmp != '\0') ) {
    syslog(LOG_ERR, "%s: 'max_errors' field should be an integer in process configuration file '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: 'max_errors' field should be an integer in process configuration file '%s'.\n", program_name, filename);
    exit(1);
  }
  if (*tmp == '\0') 
    inf->mer = -1;
  else
    inf->mer = atoi(tmp);

  /* Get the probation period */
  tmp = get_str(NULL, ":");
  if (tmp == NULL) {
    syslog(LOG_ERR, "%s: Invalid process configuration file.  Expected 'probation_period' field in '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: Invalid process configuration file.  Expected 'probation_period' field in '%s'.\n", program_name, filename);
    exit(1);
  }
  else if ((isdigit(*tmp) == 0) && (*tmp != '\0')) {
    syslog(LOG_ERR, "%s: 'probation_period' field should be an integer in process configuration file '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: 'probation_period' field should be an integer in process configuration file '%s'.\n", program_name, filename);
    exit(1);
  } 
  if ((tmp == NULL) || (*tmp == '\0')) 
    inf->prb = -1;
  else
    inf->prb = atoi(tmp);

  /* Get the minimum respawn time */
  tmp = get_str(NULL, ":");
  if (tmp == NULL) {
    syslog(LOG_ERR, "%s: Invalid process configuration file.  Expected 'minrespawn' field in '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: Invalid process configuration file.  Expected 'minrespawn' field in '%s'.\n", program_name, filename);
    exit(1);
  }
  else if ((isdigit(*tmp) == 0) && (*tmp != '\0')) {
    syslog(LOG_ERR, "%s: 'minrespawn' field should be an integer in process configuration file '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: 'minrespawn' field should be an integer in process configuration file '%s'.\n", program_name, filename);
    exit(1);
  }
  if ((tmp == NULL) || (*tmp == '\0')) 
    inf->minr = -1;
  else
    inf->minr = atoi(tmp);

  /* Get the start script name */
  inf->startup_script = get_str(NULL, ":");
  if (inf->startup_script == NULL) {
    syslog(LOG_ERR, "%s: Invalid process configuration file.  Expected 'startup_script' field in '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: Invalid process configuration file.  Expected 'startup_script' field in '%s'.\n", program_name, filename);
    exit(1);
  }
  else if(inf->startup_script[0] == '\0') {
    syslog(LOG_ERR, "%s: You must provide the start script file name in process configuration file '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: You must provide the start script file name in process configuration file '%s'.\n", program_name, filename);
    exit(1);
  }

  /* Get the stop script name */
  inf->shutdown_script = get_str(NULL, ":");
  if (inf->shutdown_script == NULL) {
    syslog(LOG_ERR, "%s: Invalid process configuration file.  Expected 'shutdown_script' field in '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: Invalid process configuration file.  Expected 'shutdown_script' field in '%s'.\n", program_name, filename);
    exit(1);
  }

  /* Get the process failure restart script name */
  inf->scr_name = get_str(NULL, ":");
  if (inf->scr_name == NULL) {
    syslog(LOG_ERR, "%s: Invalid process configuration file.  Expected 'process_failure_recovery_script' field in '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: Invalid process configuration file.  Expected 'process_failure_recovery_script' field in '%s'.\n", program_name, filename);
    exit(1);
  }

  /* Get the node failure restart script name */
  inf->node_scr_name = get_str(NULL, ":");
  if (inf->node_scr_name == NULL) {
    syslog(LOG_ERR, "%s: Invalid process configuration file.  Expected 'node_failure_recovery_script' field in '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: Invalid process configuration file.  Expected 'node_failure_recovery_script' field in '%s'.\n", program_name, filename);
    exit(1);
  }
  else if (!Fflag && Uflag && (inf->node_scr_name[0] != '\0')) {
    syslog(LOG_ERR, "%s: Node failure recovery script not supported unless either the '-F' or '-N' option is used.  Executable associated with process configuration file \"%s\" will not be started or registered with Keepalive.\n", program_name, filename);
    fprintf(stderr, "%s: Node failure recovery script not supported unless either the '-F' or '-N' option is used.  Executable associated with process configuration file \"%s\" will not be started or registered with Keepalive.\n", program_name, filename);
    exit(1);
  }

  /* Get the process failure restart script name */
  inf->down_script = get_str(NULL, ":");
  if (inf->down_script == NULL) {
    syslog(LOG_ERR, "%s: Invalid process configuration file.  Expected 'down_script' field in '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: Invalid process configuration file.  Expected 'down_script' field in '%s'.\n", program_name, filename);
    exit(1);
  }

  /* Get the down script policy. */
  tmp = get_str(NULL, "\n");
  if (tmp == NULL) {
    syslog(LOG_ERR, "%s: Invalid process configuration file.  Expected 'down_script_policy' field or missing newline character in '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: Invalid process configuration file.  Expected 'down_script_policy' field or missing newline character in '%s'.\n", program_name, filename);
    exit(1);
  }
  else if( (isdigit(*tmp) == 0) && (*tmp != '\0') ) {
    syslog(LOG_ERR, "%s: Down script policy should be an integer in process configuration file '%s'.\n", program_name, filename);
    fprintf(stderr, "%s: Down script policy should be an integer in process configuration file '%s'.\n", program_name, filename);
    exit(1);
  } 
  if (*tmp == '\0') 
    inf->down_script_policy = -1;
  else
    inf->down_script_policy = atoi(tmp);
  (void)close(fd);
  return(inf);
}

/* 
 *                     Function process_individual_proc()
 *
 * Description
 *
 * The process_individual_proc() routine processes the individual
 * configuration file.
 *
 * Interface
 *
 * void process_individual_proc(char *filename, char **argv, int index) 
 *
 * Returns
 *
 * Nothing
*/

void
process_individual_proc(char *daemon_cfg_file, char **argv, int index)
{
  ind_conf_file *inf;
  struct stat buf;
  struct passwd *p;
  struct group *g;
  char *string;

#ifdef DEBUG
syslog_msg = "%s: ENTER process_individual_proc().\n";
syslog(LOG_DEBUG, syslog_msg, program_name);
#endif

  inf = get_file_fields(daemon_cfg_file);

  safe_strncpy(kmsgproc.daemon.cfg_file, daemon_cfg_file, sizeof(kmsgproc.daemon.cfg_file));
  safe_strncpy(kmsgproc.daemon.name, inf->daemon_name, sizeof(kmsgproc.daemon.name));
  safe_strncpy(kmsgproc.daemon.args, inf->daemon_args, sizeof(kmsgproc.daemon.args));

  if(stat(kmsgproc.daemon.name, &buf) < 0) {
    syslog(LOG_ERR, "%s: Executable '%s' cannot be found. Process is not spawned.  Check the process configuration file '/etc/spawndaemon.d/%s'.\n", program_name, kmsgproc.daemon.name, daemon_cfg_file);
    fprintf(stderr, "%s: Executable '%s' cannot be found. Process is not spawned.  Check the process configuration file '/etc/spawndaemon.d/%s'.\n", program_name, kmsgproc.daemon.name, daemon_cfg_file);
    exit(1);
  }

  /* Check if process belongs to a group. */
  if(inf->gr[0] != '\0'){
    safe_strncpy(kmsgproc.daemon.group.name, inf->gr, sizeof(kmsgproc.daemon.group.name));

    /* Set default restart policy, if one was not specified. */
    if ( kmsgproc.daemon.restart_policy == DEFAULT_RESTART_POLICY) {
      /* '-Z last_node' is the default for Keepalive process groups
       * as a kluge for keeping process groups together until
       * we can work on a more cohesive design. 
      */
      kmsgproc.daemon.restart_policy = LAST_NODE;
    }
  }
  else {
    /* We don't belong to a group. */
    kmsgproc.daemon.group.name[0] = '\0';

    /* Set default restart policy, if one was not specified. */
    if (kmsgproc.daemon.restart_policy == DEFAULT_RESTART_POLICY) {
      /* If a favored node was specified, assume a '-Z F_node' default
       * restart policy.  If no favored node was specified, choose
       * '-Z round_robin' as the default restart policy.
      */
      if (Fflag) {
        kmsgproc.daemon.restart_policy = F_NODE;
      }
      else {
        kmsgproc.daemon.restart_policy = ROUND_ROBIN;
      }
    }
  }

  if(inf->uid[0] == '\0' ) { /* uid not specified in the file. */
    if ((string = (char *)malloc(10)) == NULL) {
      syslog(LOG_ERR, "%s: Error in malloc|calloc\n", program_name);
      fprintf(stderr, "%s: Error in malloc|calloc\n", program_name);
      exit(1);
    } else {
      sprintf(string, "%u", buf.st_uid);
      inf->uid = string;

      /* Set the euid to uid for keepalive. */
      kmsgproc.daemon.euid = buf.st_uid;
      syslog(LOG_INFO, "%s: User Id is obtained from the executable\n", program_name);
    }
  }else { /* uid is specified in the file. */
    /* 
    * PTS 30388 
    * The configuration file specifies user name (string), 
    * not uid (integer)
    */
    p = getpwnam(inf->uid);
    if(p == NULL) {                   /* if it is not valid uid. */
      if ((string = (char *)malloc(10)) == NULL) {
        syslog(LOG_ERR, "%s: Error in malloc|calloc\n", program_name);
        fprintf(stderr, "%s: Error in malloc|calloc\n", program_name);
        exit(1);
      } else {
        sprintf(string, "%u", buf.st_uid);
        inf->uid = string;
        kmsgproc.daemon.euid = buf.st_uid;
        syslog(LOG_INFO, "%s: User Id is obtained from the executable\n", program_name);
        fprintf(stderr, "%s: User Id is obtained from the executable\n", program_name);
      }
    }else{ /* If uid is valid get it from the file. */
      kmsgproc.daemon.euid = p->pw_uid;
    }  
  }
  if(inf->gid[0] == '\0') { /* gid not specified in the file */
    if ((string = (char *)malloc(10)) == NULL) {
      syslog(LOG_ERR, "%s: Error in malloc|calloc\n", program_name);
      fprintf(stderr, "%s: Error in malloc|calloc\n", program_name);
      exit(1);
    } else {
      sprintf(string, "%u", buf.st_gid);
      inf->gid = string;
      kmsgproc.daemon.egid = buf.st_gid;
      syslog(LOG_INFO, "%s: Group Id is obtained from the executable\n", program_name);
      fprintf(stderr, "%s: Group Id is obtained from the executable\n", program_name);
    }
  }else { /* gid specified in the file. */
    /* 
    * PTS 30388 
    * The configuration file specifies group name (string), 
    * not gid (integer)
    */
    g = getgrnam(inf->gid);
    if(g == NULL) {                  /* If not a valid gid */
      if ((string = (char *)malloc(10)) == NULL) {
        syslog(LOG_ERR, "%s: Error in malloc|calloc\n", program_name);
        fprintf(stderr, "%s: Error in malloc|calloc\n", program_name);
        exit(1);
      } else {
        sprintf(string, "%u", buf.st_gid);
        inf->gid = string;
        kmsgproc.daemon.egid = buf.st_gid;
        syslog(LOG_INFO, "%s: Group Id is obtained from the executable\n", program_name);
        fprintf(stderr, "%s: Group Id is obtained from the executable\n", program_name);
      }
    }else{ /* gid is valid */
      kmsgproc.daemon.egid = g->gr_gid;
    }
  }

  if (!inf->startup_script) {
      syslog(LOG_ERR, "%s: Startup script must be specified in process configuration file '%s'.  Executable '%s' will not be started nor watched by Keepalive.\n", program_name, daemon_cfg_file, kmsgproc.daemon.name);
      fprintf(stderr, "%s: Startup script must be specified in process configuration file '%s'.  Executable '%s' will not be started nor watched by Keepalive.\n", program_name, daemon_cfg_file, kmsgproc.daemon.name);
      exit(1);
  }

  check_script(daemon_cfg_file, inf->startup_script, kmsgproc.daemon.startup_script, sizeof(kmsgproc.daemon.startup_script));

  check_script(daemon_cfg_file, inf->shutdown_script, kmsgproc.daemon.shutdown_script, sizeof(kmsgproc.daemon.shutdown_script));

  check_script(daemon_cfg_file, inf->scr_name, kmsgproc.daemon.process_failure_script, sizeof(kmsgproc.daemon.process_failure_script));

  check_script(daemon_cfg_file, inf->node_scr_name, kmsgproc.daemon.node_failure_script, sizeof(kmsgproc.daemon.node_failure_script));

  check_script(daemon_cfg_file, inf->down_script, kmsgproc.daemon.down_script, sizeof(kmsgproc.daemon.down_script));

  if(inf->termwait != -1) {
    kmsgproc.daemon.termwait = inf->termwait;
  }else {
    kmsgproc.daemon.termwait = DEFAULT_TERMWAIT;
  }
  if(inf->mer != -1) {
    kmsgproc.daemon.maxerrors = inf->mer;
  }else{
    kmsgproc.daemon.maxerrors = DEFAULT_MAXERROR;
  }
  if(inf->prb != -1) {
    kmsgproc.daemon.probation_period = inf->prb;
  }else{
    kmsgproc.daemon.probation_period =  DEFAULT_PROBATION;
  }
  if(inf->minr != -1) {
    kmsgproc.daemon.minrespawn = inf->minr;
  }else{
    kmsgproc.daemon.minrespawn = DEFAULT_MINRESPAWN;
  }

  if (inf->down_script_policy != -1) {
    kmsgproc.daemon.down_script_policy = inf->down_script_policy;
  }
  else {
    kmsgproc.daemon.down_script_policy = DEFAULT_DOWN_POLICY;
  }

  if (inf->gr[0] != '\0') {
    kmsgproc.daemon.group.wait_time = gf.mbr[index].wait_time;
    kmsgproc.daemon.group.priority = gf.mbr[index].priority;
  }
  else {
    kmsgproc.daemon.group.wait_time = 0;
    kmsgproc.daemon.group.priority = 0;
  }

  kmsgproc.daemon.respawning_group = FALSE;

  spawn_nprocs(needs_ka, nprocs, &kmsgproc);

#ifdef DEBUG
syslog_msg = "%s: EXIT process_individual_proc().\n";
syslog(LOG_DEBUG, syslog_msg, program_name);
#endif
}

void
tokens_to_str(char *args, char **token) {
  args[0] = '\0';
  while (*token && (strlen(args) + strlen(*token) < MAXNAME)) {
#ifdef DEBUG
syslog_msg = "%s: tokens_to_str(): *token = >%s<.\n";
syslog(LOG_DEBUG, syslog_msg, program_name, *token);
#endif
    strcat(args, *token);
    strcat(args, " ");
    token++;
  }
  args[strlen(args)-1] = '\0';
#ifdef DEBUG
syslog_msg = "%s: tokens_to_str(): args = >%s<.\n";
syslog(LOG_DEBUG, syslog_msg, program_name, args);
#endif
}


/*                     Function process_conf_file()
 *
 * Description
 *
 * The process_conf_file() routine processes the configuration file. It
 * performs all necessary checking of permissions and ids. It determines
 * whether the file is a group configuration file or an individual process
 * configuration file and processes either accordingly.
 *
 * Interface
 *
 * void process_conf_file(char *filename, char **argv)
 *
 * Returns
 *
 * Nothing
*/

void
process_conf_file(char *filename, char **argv)
{
  gfile *group;
  int i;
  char buf[MAXNAME];
  char indfname[MAXNAME];
  char *fname;

#ifdef DEBUG
syslog_msg = "%s: ENTER process_conf_file().\n";
syslog(LOG_DEBUG, syslog_msg, program_name);
#endif

  fname = filename;
  if(fname != NULL) {
    sprintf(buf,"/etc/spawndaemon.d/%s", fname);
    fname = buf;

    if( check_conf_file_perms(fname) == 0 ) { 
      syslog(LOG_ERR, "%s: Registration will not take place because of the configuration file permissions or invalid uid, gid\n", program_name);
      fprintf(stderr, "%s: Registration will not take place because of the configuration file permissions or invalid uid, gid\n", program_name);

    /* If we have a group file, process it. */
    } else if( file_group_or_individual(fname) == 1) {
      group = get_group_file_fields(fname);
      num_of_groupmembers = group->mems;

      for(i = 0; i < num_of_groupmembers; i++) {
        sprintf(buf, "/etc/spawndaemon.d/%s", group->mbr[i].mfname);
        if( check_conf_file_perms(buf) == 0) {
          syslog(LOG_ERR, "%s: Registration will not take place because of the configuration file permissions or invalid uid, gid\n", program_name);
          fprintf(stderr, "%s: Registration will not take place because of the configuration file permissions or invalid uid, gid\n", program_name);
          exit(1);
        }
      }

      for(i = 0; i < num_of_groupmembers; i++) {
        sprintf(buf, "/etc/spawndaemon.d/%s", group->mbr[i].mfname);

        if(file_group_or_individual(buf) == 1) {
          syslog(LOG_ERR, "%s: Process is a group member. Cannot be a group of processes: Registration is aborted\n", program_name);
          fprintf(stderr, "%s: Process is a group member. Cannot be a group of processes: Registration is aborted\n", program_name);
          exit(1);
        }
      }
      kmsgproc.cmd = ADD;
			num_registered_daemons = pkctl->registered_daemons;
      if( (num_of_groupmembers + pkctl->registered_daemons) > pkctl->max_possible_daemons ) {
        exit(1);
      }else{
        for(i = 0; i < num_of_groupmembers; i++) {
          sprintf(indfname, "/etc/spawndaemon.d/%s", group->mbr[i].mfname);
          process_individual_proc(indfname, argv, i);
        }
      }

    /* Otherwise, assume we have a daemon configuration file and try to
     * process it.
    */
    }else {
      process_individual_proc(fname, argv, 0);
    }

  /* There is no configuration file. */
  } else {
    syslog(LOG_ERR, "%s: Cannot access process configuration file.  Executable \"%s\" will not be started.\n", program_name, kmsgproc.daemon.name);
    fprintf(stderr, "%s: Cannot access process configuration file.  Executable \"%s\" will not be started.\n", program_name, kmsgproc.daemon.name);
    spawn_nprocs(needs_ka, nprocs, &kmsgproc);
    exit(1);
  }
#ifdef DEBUG
syslog_msg = "%s: EXIT process_conf_file().\n";
syslog(LOG_DEBUG, syslog_msg, program_name);
#endif
}

   
                        

/*               
 *                     Function init()
 *
 * Description
 *
 * The init() routine determines whether we can communicate with keepalive or 
 * not. If there is a request to start a specified daemon it tries to
 * allocate enough memory for all the processes monitored.
 *
 * Interface
 *
 * int init(int n_procs, int needs_ka, int print)
 *
 * Returns
 *
 * TRUE if we can communicate with keepalive.
 * FALSE if we can't communicate.
*/


int
init(int n_procs, int needs_ka, int print)
{
  int  retval = TRUE;
  kalive_ctl_t *p_ctl;
  struct stat buf;
  int timeout;
 
  openproc();

  /* If the spawndaemon command needs keepalive to be there ... */
  if (needs_ka) {
    timeout = 0;
    while ((pipefd < 0) && (timeout < KA_TIMEOUT)) {
      if((pipefd = open(KEEPCFG, O_WRONLY | O_NONBLOCK, 0666)) < 0){
        if (timeout % 30 == 0) {
          syslog(LOG_WARNING, "%s: Could not open pipe %s.  Keepalive is not active.  Retrying ...\n", program_name, KEEPCFG);
        }

        retval = FALSE;
        timeout += 1;
        sleep(1);
      }
      else {
        retval = TRUE;
      }
    }

    if (fcntl(pipefd, F_SETFL, fcntl(pipefd, F_GETFL) & ~O_NONBLOCK) < 0){
      syslog(LOG_ERR, "%s: fcntl(%s) failed\n", program_name, KEEPCFG);
    }
 
    /* Open keepalive's data file for reading */
    if ((data_fd = open(KEEPDATA, O_RDONLY, 0664)) == -1) {
      if (errno == ENOENT) {
        syslog(LOG_ERR, "%s: Keepalive data file does not exist.\n", program_name);
        fprintf(stderr, "%s: Keepalive data file does not exist.\n", program_name);
        retval = FALSE;
      }
      else {
        syslog(LOG_ERR, "%s: Could not open keepalive data file.\n", program_name);
        fprintf(stderr, "%s: Could not open keepalive data file.\n", program_name);
        retval = FALSE;
      }
    }
    else {
      if (fstat(data_fd, &buf) == -1) {
        syslog(LOG_ERR, "%s: Could not stat keepalive data file.\n", program_name);
        fprintf(stderr, "%s: Could not stat keepalive data file.\n", program_name);
        retval = FALSE;
      }
      else {
        pctl_procs = (void *)mmap((caddr_t)0, buf.st_size - 1,
            PROT_READ, MAP_SHARED, data_fd, (off_t)0);
        if (pctl_procs == (void *)-1) {
          syslog(LOG_ERR, "%s: Could not map keepalive data file.\n", program_name);
          fprintf(stderr, "%s: Could not map keepalive data file.\n", program_name);
          retval = FALSE;
        }
        else {
          pkctl = (kalive_ctl_t *) pctl_procs;
          p_ctl = pkctl + 1;
          pprocs = (process_t *) p_ctl;
        }
      }
    }
    (void)close(data_fd);
  }

  if (((!retval) || (!needs_ka)) && !print) {
    pprocs = (process_t *)calloc((size_t)n_procs, sizeof(process_t));
    if( pprocs == (process_t *)NULL ) {
      syslog(LOG_ERR, "%s: Error in malloc|calloc\n", program_name);
      fprintf(stderr, "%s: Error in malloc|calloc\n", program_name);
      exit(1);
    }
  }
  return(retval);
}
            
   

/*   
 *                   Function spawn_proc()
 *
 * Description
 *
 * The spawn_proc() routine spawns the spaecified daemon.
 *
 * Interface
 *
 * void spawn_proc(process_t *m, char **argv)
 *
 * Returns
 *
 * Nothing
*/

int
spawn_proc(process_t *m)
{
  int ret = 0;

#ifdef DEBUG
syslog_msg = "%s: ENTER spawn_proc:  %s\n";
syslog(LOG_DEBUG, syslog_msg, program_name, m->name);
#endif

  /* Send message to Keepalive. */
  writemsg(&kmsgproc); 

#ifdef DEBUG
syslog_msg = "%s: EXIT spawn_proc.\n";
syslog(LOG_DEBUG, syslog_msg, program_name);
#endif

  return(ret);
}


/*
 *                   Function spawn_nprocs()
 *
 * Description
 *
 * The spawn_nprocs() routine spawns the specified number of daemons and
 * registers with keepalive the script to be used to restart the daemon.
 * It checks for the -i option and if the daemon is already running it
 * does not restart it. It builds a dummy process table to track multiple 
 * daemons.
 *
 * Returns
 *
 * Nothing
*/

void
spawn_nprocs(int restart, int nprocs, kalive_cmd_t *p)
{
  int    i, ret;
  process_t  proc, *q;

#ifdef DEBUG
syslog_msg = "%s: ENTER spawn_nprocs().\n";
syslog(LOG_DEBUG, syslog_msg, program_name);
#endif

  /* Enforce any idempotency requirements. */ 
  enforce_idempotency(&(p->daemon), idempotency_policy);

  /*
  * We're not idempotent.  If keepalive is already watching a daemon with 
  * this name and we want keepalive to watch this one, make sure that 
  * both daemons are pinned or both are not pinned.  Otherwise, keepalive 
  * will get confused.
  */
  if ( (q = find_watched_proc(p->daemon.name))) {

    /*
    * if -F or -N option was used and another daemon with the
    * same name is running without -F or -N, we can't start.
    */
    if ((p->daemon.node != 0) || (p->daemon.pin_anywhere != 0)) {
      if ((q->node == 0) && (q->pin_anywhere == 0)) {
        syslog(LOG_INFO, "%s: Process '%s' is already running unpinned.\n", program_name, p->daemon.name);
        fprintf(stderr, "%s: Process '%s' is already running unpinned.\n", program_name, p->daemon.name);
        exit(1);
      }
    }

    /*
    * if neither -F nor -N option was used and another daemon with the
    * same name is running with -F or -N, we can't start.
    */
    if ((p->daemon.node == 0) && (p->daemon.pin_anywhere == 0)) {
      if ((q->node != 0) || (q->pin_anywhere != 0)) {
        syslog(LOG_INFO, "%s: Process '%s' is already running pinned.\n", program_name, p->daemon.name);
        fprintf(stderr, "%s: Process '%s' is already running pinned.\n", program_name, p->daemon.name);
        exit(1);
      }
    }
  }

  /* Startup nprocs copies of the daemon. */
  for (i=0; i < nprocs; i++) {
    proc = p->daemon;
    ret = spawn_proc(&proc); 
  }

#ifdef DEBUG
syslog_msg = "%s: EXIT spawn_nprocs().\n";
syslog(LOG_DEBUG, syslog_msg, program_name);
#endif

}


/*
 *                 Function sync_data()
 *
 * Description
 *
 * The sync_data() routine writes changes from mapped memory to the data
 * file on disk.
 *
 *
 * Interface
 *
 * void sync_data()
 *
 * Returns
 *
 * Nothing
*/

void
sync_data()
{
  int ret;
  struct stat buf;

  if (fstat(data_fd, &buf) == -1) {
    syslog(LOG_ERR, "%s: Could not stat keepalive data file.\n", program_name);
    fprintf(stderr, "%s: Could not stat keepalive data file.\n", program_name);
  }
  else {
    ret = msync((caddr_t)pkctl, (size_t)buf.st_size, MS_SYNC);
    if (ret == -1) {
      syslog(LOG_ERR, "%s: Could not synchronize keepalive data file.\n", program_name);
      fprintf(stderr, "%s: Could not synchronize keepalive data file.\n", program_name);
    }
  }
}

/*
 *                 Function find_watched_proc()
 *
 * Description
 *
 * Find a daemon with a given name in keepalive's list of watched processes.
 *
 *
 * Interface
 *
 * process_t *find_watched_proc(char *name)
 *
 * Returns
 *
 * A pointer to keepalive's data structure for that process, if a process
 * is found.  NULL if no process is found.
*/

process_t *
find_watched_proc(char *name)
{
  int i;

  for (i=0; i<pkctl->registered_daemons; i++) {
    if (strcmp (name, pprocs[i].name) == 0) {
      return(&pprocs[i]);
    }
  }
  return((process_t *) NULL);
}

void
get_basename(char *basename, char *name) {
  int i, j;
  char filename[MAXNAME];

  /* Find basename of daemon. */
  j = 0;
  for (i=strlen(name) - 1; (i >= 0) && (name[i] != '/'); i--) {
    filename[j] = name[i];
    j++;
  }
  i = 0;
  for (j--; j >= 0; j--) {
    basename[i] = filename[j];
    i++;
  }
  basename[i] = '\0';

#ifdef DEBUG
syslog_msg = "%s: basename = %s.\n";
syslog(LOG_DEBUG, syslog_msg, program_name, basename);
#endif
}

/*
 *                 Function not_node_lists_intersect()
 *
 * Description
 *
 * Determines whether or not the node lists between two daemon instances
 * are mutually exclusive.
 *
 * Returns
 *
 * TRUE:  node lists intersect
 * FALSE:  node lists share no members
*/

int
node_lists_intersect(process_t *new_proc, process_t *reg_proc)
{
  int i, j;

  /* See if new_proc's favored node is the same as reg_proc's. */
  if (new_proc->node == reg_proc->node) {
    return TRUE;
  }

  /* Verify node lists. */
  for (i = 0; (i < MAX_NODES) && (reg_proc->backup_nodes[i] != 0); i++)
  {
    /* See if new_proc's favored node is a member of reg_proc's backup
     * node set.
    */
    if (reg_proc->backup_nodes[i] == new_proc->node) {
      return TRUE;
    }

    for (j = 0; (j < MAX_NODES) && (new_proc->backup_nodes[j] !=0); j++)
    {
      /* See if reg_proc's favored node is a member of new_proc's backup
       * node set.
      */
      if (new_proc->backup_nodes[j] == reg_proc->node) {
        return TRUE;
      }

      /* See if reg_proc's backup node is a member of new_proc's backup
       * node set.
      */
      if (reg_proc->backup_nodes[i] == new_proc->backup_nodes[j]) {
        return TRUE;
      } 
    }
  }

  return FALSE;
}

void
enforce_idempotency(process_t *new_proc, idempotency_t policy)
{
  int i;
  static int group_checked = FALSE; /* Group checked for idempotency? */

  /* Make sure that we need to check for idempotency. */
  if (policy == NO_IDEMPOTENCY) {
    /* Do nothing. */
    return;
  }

  /* If there are no registered daemons, then we've done our group
   * check for idempotency.
   */
  if (num_registered_daemons == 0) {
    group_checked = TRUE;
	}
  

  /* Search the list of registered daemons. */
  for (i = 0; i < pkctl->registered_daemons; i++) {
    /* If the process groups are the same and not group checked, ... */

    if (!group_checked &&
        (new_proc->group.name[0] != '\0') &&
        (strcmp(new_proc->group.name, pprocs[i].group.name) == 0))
    {
      /* Indicate that the group will no longer need to be checked. */
      group_checked = TRUE;

      /* Do checks based on policy used. */
      if (policy == CLUSTER_WIDE) {
        syslog(LOG_ERR, "%s: Process group '%s' is already registered.  Registration is aborted.\n", program_name, new_proc->group.name);
        fprintf(stderr, "%s: Process group '%s' is already registered.  Registration is aborted.\n", program_name, new_proc->group.name);
        exit(2);
      }
      else { /* policy == NODE_LIST */
        if (node_lists_intersect(new_proc, &pprocs[i]))
        {
          syslog(LOG_ERR, "%s: Process group '%s' is already registered.  Registration is aborted.\n", program_name, new_proc->group.name);
          fprintf(stderr, "%s: Process group '%s' is already registered.  Registration is aborted.\n", program_name, new_proc->group.name);
          exit(2);
        }
      }
    }
    /* If the new daemon is in no group and we found another instance, ... */
    else if ((new_proc->group.name[0] == '\0') &&
             (strcmp(new_proc->name, pprocs[i].name) == 0))
    {
      if (policy == CLUSTER_WIDE) {
        syslog(LOG_ERR, "%s: Process '%s' is already registered.  Registration is aborted.\n", program_name, new_proc->name);
        fprintf(stderr, "%s: Process '%s' is already registered.  Registration is aborted.\n", program_name, new_proc->name);
        exit(2);
      }
      else { /* policy == NODE_LIST */
        if (node_lists_intersect(new_proc, &pprocs[i]))
        {
          syslog(LOG_ERR, "%s: Process '%s' is already registered.  Registration is aborted.\n", program_name, new_proc->name);
          fprintf(stderr, "%s: Process '%s' is already registered.  Registration is aborted.\n", program_name, new_proc->name);
          exit(2);
        }
      }
    } 
    else {
      /* Do nothing. */
    }
  }
}

void
check_script(char *cfg_file, char *script_basename, char *target, int target_size)
{
  static char script[MAXNAME];  /* Don't push/pop on stack. Performance. */

  target[0] = '\0';
  if (script_basename[0] != '\0') {
    sprintf(script, "/etc/keepalive.d/%s", script_basename);
    if(access(script, F_OK) < 0) {
      syslog(LOG_ERR, "%s: Cannot access '%s'.  Executable '%s' will not be started nor watched by Keepalive.  Check process configuration file '%s'.\n", program_name, script, kmsgproc.daemon.name, cfg_file);
      fprintf(stderr, "%s: Cannot access '%s'.  Executable '%s' will not be started nor watched by Keepalive.  Check process configuration file '%s'.\n", program_name, script, kmsgproc.daemon.name, cfg_file);
      exit(1);
    } else {
      safe_strncpy(target, script, target_size);
    } 
  }
}

