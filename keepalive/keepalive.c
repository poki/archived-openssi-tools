/*
 *	keepalive daemon.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
 *
 */

#ident "$Id: keepalive.c,v 1.7 2003/05/22 18:49:43 dzafman Exp $" 

#ifdef DEBUG
#include <time.h>
#endif

#include <errno.h>
#include <locale.h>
#include <sys/types.h>
#include <sys/stropts.h>
#include <nl_types.h>
#include <sys/param.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/syslog.h>
#include <assert.h>
#include <sys/file.h>
#include <sys/wait.h>
#include <dirent.h>
#include <stdio.h>
#include <ctype.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/user.h>
#include <sys/stat.h>
#include <pwd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/times.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>
#include "keepalive.h"
#include <linux/kernel.h>
#include <sys/poll.h>

typedef struct zombie_struct {
  pid_t pid;
  int waitpid_info;
} zombie_info_t;

#define max(a, b)  ((a) > (b) ? (a) : (b))
#define min(a, b)  ((a) < (b) ? (a) : (b))
#define bzero(a, b)  memset((a), '\0', (b))

#define DEFAULT_POLLING_TIME	5	/* seconds */
#define ADOPT_RETRY		100	/* milliseconds */
#define MAX_ADOPT_RETRIES	5	/* max. number of attempts */

/* The following two functions are used to lock and unlock process_t
 * records for sections of code that make major changes to the records
 * that might confuse a user of "spawndaemon -L" were he to see these
 * records changed non-atomically.  To make small updates to process_t
 * records (e.g. where a small number of integer fields are updated,
 * the use of these locks is not recommended for performance reasons.
 * A user of "spawndaemon -L" should only expect major changes to
 * occur atomically.
*/
#define LOCK_PROC(proc) \
{ \
struct tms tms_buf; \
(proc).lock = times(&tms_buf); \
sync_data(); \
}

#define UNLOCK_PROC(proc) \
{ \
struct tms tms_buf; \
(proc).unlock = times(&tms_buf); \
(proc).lock = 0; \
sync_data(); \
}

sigset_t controlled_signals;       /* for signal handling */
sigset_t held_signals;            /* for signal handling */
sigset_t old_signals;            /* for signal handling */
sigset_t sigwait_signals;    /* for signal handling */
struct sigaction sa;

char *syslog_msg = NULL;
char *generic_msg = NULL;
char *format_msg = NULL;

process_t  *pprocs = NULL;
kalive_ctl_t    *pkctl = NULL;
kalive_ctl_t    *pkctl2 = NULL; 
kalive_cmd_t    *cmdp = NULL;
void            *pctl_procs = NULL;
int		pctl_procs_size = 0;
int    data_fd;

char  *program_name = "NSUX: keepalive";
char  *msg_catalog  = "keepalive.cat";

/* This is the shortest minrespawn interval that will be used to set the
 * alarm clock after making a pass through the watched process table.
*/
int next_alarm = 0;

struct timeval sleep_one_second = { 1, 0 };  /* 1 second sleep interval */
char *argvInterval = NULL;
int restart = FALSE;

/* Set to TRUE when keepalive receives command from spawndaemon to reset
 * the state of a process.
*/
int reset = FALSE;

/* True when Keepalive's process watching algorithm is in a stable state.
 * Keepalive continues to make passes through the watched process table
 * until a stable state is reach.  This variable is used to deal with race
 * conditions involving SIGCHLD and zombie processes, where processes died
 * within 1 second of each other we don't get a SIGCHLD for some of the
 * later deaths (i.e. we can process all the zombies for the first few that
 * died which caused the SIGCHLD but processes that die shortly after do
 * not produce another SIGCHLD unless at least 1 second has elapsed).
*/ 
int stable_state;

/* Is there a process adoption problem? */
int adoption_problem;

int interval = DEFAULT_POLLING_TIME;

#ifdef DEBUG
char time_str[80];
time_t sys_time;

#define MAX_SIGNAL 36
sigset_t DEBUG_pending_mask;
sigset_t DEBUG_current_mask;
int a_signal;
#endif

/* Following variables are used to extract the name uid and gid from the 
 * password file. Then they are used in check_file_perms() to check the
 * file permissions before a daemon is respawned by keepalive.
*/
 
const char   *nam_root = "root";
struct passwd  *pwadm = NULL;
uid_t    sysuid;
gid_t    sysgid;

extern int errno;

int pipefd = -1;
fd_set fdset;
int	child_pid ;

/* Maintains list of UP nodes and the transids at which they were declared UP. 
*/
int max_nodes = 0;

typedef struct node_list_struct {
  int size;
  clusternode_info_t *node;
} node_list_t;

node_list_t up_nodes = { 0, NULL };

#define MAX_ZOMBIES 100
typedef struct zombie_list_struct {
  int bottom;
  zombie_info_t zombie[MAX_ZOMBIES];
} zombie_list_t;

zombie_list_t zombie_list = { 0 };

#define ENV_BUFSIZE 64
char downbuf[ENV_BUFSIZE];
char rejectbuf[ENV_BUFSIZE];
char lastpidbuf[ENV_BUFSIZE];
char activebuf[ENV_BUFSIZE];

/* Function prototypes */

void usage();
void initialize();
void makedaemon();
int getnextprocslot();
void keepalive();
void proc_cmd();
void spawn(register process_t *proc);
void respawn(register process_t *proc);
void cleanup();
void clear_process_board();
void clear_control_board();
void terminate_daemon(process_t *proc, int state);
void kill_all_daemons(char *group, int state);
void delete_daemons(kalive_cmd_t *p);
int check_if_registered(process_t *proc);
int get_ts_parms();
int get_fp_parms();
void set_ts_prclass();
int wait_sem(int sem_id);
int signal_sem(int sem_id);
int map_ka_data();
void map_data();
void sync_data();
int choose_node();
pid_t fork_daemon_process(process_t *proc);
int open_ka_files(); 
void verify_ka_not_running();
void close_all_files();
void get_basename(char *basename, char *name);
void cleanup_daemon_logs();
void adopt_child(process_t *pid);
void find_missing_proc(int slot);
void exec_script(char *script, char *daemon, char *daemon_cfg_file); 
void loadNodeTable(void);
void declare_dead(process_t *proc, int may_fault_daemon);
void declare_down(process_t *proc);
pid_t rfork_anywhere(time_t *start_time); 
void setup_proc(process_t *daemon, int unpin);
int insert_zombie(const zombie_info_t * const zombie);
int serve_zombie(pid_t pid, zombie_info_t *zombie);
int is_zombie_list_full();
int is_zombie_list_empty();
int is_node_available(int node);
void make_zombie_list_empty();
clock_t min_alarm(clock_t time1, clock_t time2);
void dummy_handler(int sig);

extern time_t get_pid_start(pid_t pid);
extern int _cluster_reclaim_child(pid_t pid, time_t date);
extern int matches_slot_config(const process_t *proc, int pid);

void
delete_restart_log(void)
{
	struct stat buf;

	if (stat(KEEPDATA, &buf) == 0) {
	    if (unlink(KEEPDATA) == -1) {
		syslog(LOG_ERR, "%s: Could not delete keepalive data file '%s', errno %d\n", program_name, KEEPDATA, errno);
		fprintf(stderr, "%s: Could not delete keepalive data file '%s', errno %d\n", program_name, KEEPDATA, errno);
		exit(1);
	    }
	}

	cleanup_daemon_logs();
}

int
do_pin(int pinit)
{
	int error = 0;
	int fd = -1;
	int nin;
	int nout;
	const char pinstr[] = "1\n";
	const char unpinstr[] = "0\n";
	const char *cp;


	fd = open("/proc/self/pin", O_WRONLY);
	if (fd == -1) {
		syslog(LOG_ERR,
			"%s: error %d opening \"/proc/self/pin\"\n",
			program_name, errno);
		error = 1;
	}
	cp = (pinit ? pinstr : unpinstr);
	nin = strlen(cp);
	nout = write(fd, cp, nin);
	if (nout == -1) {
		syslog(LOG_ERR,
		       "%s: error %d writing %s to \"/proc/self/pin\"\n",
		       program_name, errno, cp);
		error = 1;
	}
	if (close(fd) == -1) {
		syslog(LOG_ERR,
		       "%s: error %d closing \"/proc/self/pin\"\n",
		       program_name, errno);
		error = 1;
	}

	return error;
}

/*                     Main Function                                    
 *                                                                      
 * Description                                                          
 *                                                                      
 * It gets the command line argument and validates it. It calls the     
 * initialize() routine and the keepalive() routine which performs the  
 * functionality of the keepalive system.                                  
 * 
*/

int
main(int argc, char **argv)
{
  int c;
  extern char *optarg;
  extern int optind;

  program_name = argv[0];

  /* Make Keepalive safe from signals.  Allow a small set of signals through to */
  /* allow us to detect any potential defects in Keepalive. */ 
  sigfillset(&held_signals);
  sigdelset(&held_signals, SIGABRT);
  sigdelset(&held_signals, SIGBUS);
/*  sigdelset(&held_signals, SIGEMT); */
  sigdelset(&held_signals, SIGFPE);
  sigdelset(&held_signals, SIGILL);
  sigdelset(&held_signals, SIGIOT);
  sigdelset(&held_signals, SIGSEGV);
  sigdelset(&held_signals, SIGSYS);
  sigdelset(&held_signals, SIGTRAP);
  sigdelset(&held_signals, SIGXCPU);
  sigdelset(&held_signals, SIGXFSZ);
  sigdelset(&held_signals, SIGXFSZ);
  sigdelset(&held_signals, SIGXFSZ);
  sigprocmask(SIG_SETMASK, &held_signals, &old_signals);

  setsid();
  chdir("/");
  umask(0);

  /* Close stdio. */
  fclose(stdin);
  fclose(stdout);
  fclose(stderr);

  openlog("keepalive", LOG_CONS | LOG_PID | LOG_NDELAY /*LOG_NOWAIT*/, LOG_DAEMON);

  /* Verify that Keepalive is running on an OpenSSI kernel. */
  while (
#ifndef DXX
!cluster_ssiconfig()
#else
!tnc_isconfig()
#endif
        ) 
  {
    syslog(LOG_ERR, "%s: Requires an OpenSSI kernel to run.  Keepalive is inactive.\n", program_name);
    (void)fprintf(stderr, "%s: Requires an OpenSSI kernel to run.  Keepalive is inactive.\n", program_name);
    pause();
  }

  while((c = getopt(argc, argv, "t:i")) != EOF) {
    switch(c){

      case 't':

        argvInterval = optarg;
        interval = atoi(optarg);
        if (interval <= 0) {
          syslog(LOG_ERR, "%s: Invalid number of seconds.\n", program_name);
          (void)fprintf(stderr, "%s: Invalid number of seconds.\n", program_name);
          usage();
        }
        break;

      case 'i':

	delete_restart_log();

	exit(0);
	/* NOTREACHED */

      case '?':
      default:

        usage();
    }
  }

  /* Get the uid, gid for root from the password file. */
  /* It will be used to check uid, gid of conf files. */ 
  pwadm = getpwnam(nam_root);
  sysuid = pwadm->pw_uid;
  sysgid = pwadm->pw_gid;
  initialize();

  keepalive();
  /* 
   * Something went wrong, return 1
   */
  return 1;
} 
  

/*                                                                        
 *                 Function usage()                                      
 *                                                                       
 * Description                                                           
 *                                                                       
 * The usage() routine logs the correct way of using the keepalive argument.
 *                                                                       
 * Interface                                                             
 *                                                                       
 * void usage()                                                          
 *                                                                       
 * Returns                                                               
 *                                                                       
 * Nothing                                                               
 */  

void
usage()
 {
  syslog(LOG_ERR, "Usage: keepalive [-i] [-t interval-sec]\n");
  exit(1);
} 


/*                                                                       
 *                 Function clear_process_board()                        
 *                                                                       
 * Description                                                           
 *                                                                       
 * The clear_process_board() routine zeroes out the part of the data   
 * file that is used for the process information.              
 *                                                                       
 * Interface                                                             
 *                                                                       
 * void clear_process_board()                                            
 *                                                                       
 * Returns                                                               
 *                                                                       
 * Nothing                                                               
 */

void
clear_process_board()
{

  int i;

  for (i = 0; i < DEFAULT_MAX_DAEMONS; i++) {
    memset(&pprocs[i], 0, sizeof(process_t));
    pprocs[i].pid = -1;
  }
} 

                                                                      
/*                 Function clear_control_board()                        
 *                                                                       
 * Description                                                           
 *                                                                       
 * The clear_control_board() routine zeroes out the part of the data   
 * file that is used for control information.                  
 *                                                                       
 * Interface                                                             
 *                                                                       
 * void clear_control_board()                                            
 *                                                                       
 * Returns                                                               
 *                                                                       
 * Nothing                                                               
*/ 
                                      
void
clear_control_board()
{
  pkctl->primary_pid = (pid_t) 0;
  pkctl->max_possible_daemons = DEFAULT_MAX_DAEMONS;
  pkctl->max_registered_daemons = pkctl->max_possible_daemons;
  pkctl->registered_daemons = (long) 0;
  pkctl->quiesce_flag = FALSE;
} 
    

/*                                                                       
 *                 Function clear_data_file()                        
 *                                                                       
 * Description                                                           
 *                                                                       
 * The clear_data_file() routine clears the keepalive data file,    
 * zeroing out the control information and the process information.      
 *                                                                       
 * Interface                                                             
 *                                                                       
 * void clear_data_file()                                            
 *                                                                       
 * Returns                                                               
 *                                                                      
 * Nothing                                                               
*/

void
clear_data_file()
{
  clear_control_board();
  clear_process_board();
}

void
unmap_ka_data() {
  if (pctl_procs != NULL) {
    munmap(pctl_procs, pctl_procs_size);
    pctl_procs=NULL;
  }
}

/*                 Function map_ka_data()
 *
 * Description
 *
 * The map_ka_data() routine unmaps and maps the keepalive data file 
 * when a process gets the semaphore. This is to make sure that the 
 * process has the current version of the file.  The file can be
 * expanded and we need to see all of it.
 *
 * Interface
 *
 * void map_ka_data(void)
 *
 * Returns
 *
 * 0  success
 * -1 failure
*/


int 
map_ka_data(void)
{

  kalive_ctl_t *p_ctl;
  struct stat  buf;

  unmap_ka_data();

  if (stat(KEEPDATA, &buf) == -1) {
    syslog(LOG_ERR, "%s: Could not stat keepalive data file '%s'.\n", program_name, KEEPDATA);
    return -1;
  }
  pctl_procs_size = buf.st_size;

  pctl_procs = (void *)mmap((caddr_t)0, pctl_procs_size, 
      PROT_READ|PROT_WRITE, MAP_SHARED, data_fd, (off_t)0);
  if (pctl_procs == (void *)-1) {
    syslog(LOG_ERR, "%s: Could not map keepalive data file '%s'.\n", program_name, KEEPDATA);
    return -1;
  }
  if (msync(pctl_procs, pctl_procs_size, MS_SYNC) == -1) {
    syslog(LOG_WARNING, "%s: Could not set keepalive data file '%s' for synchronous writes.\n", program_name, KEEPDATA);
  }

  pkctl = (kalive_ctl_t *) pctl_procs;
  p_ctl = pkctl + 1;
  pprocs = (process_t *) p_ctl;

  return 0;
}

/*                 Function map_data()
 *
 * Description
 *
 * The map_data() routine unmaps and maps the keepalive data file 
 * when a process gets the semaphore. This is to make sure that the 
 * process has the current version of the file.  The file can be
 * expanded and we need to see all of it.
 *
 * Interface
 *
 * void map_data(void)
 *
 * Returns
 *
 * Nothing
*/


void 
map_data(void)
{
  if (map_ka_data() != 0) {
    exit(1);
  }
}


/*
 *
 *                 Function create_primary()
 *
 * The create_primary() routine calls the fork() or rfork() to create the
 * primary keepalive.  Until object migration is supported (i.e. such that
 * semaphores and named pipes are migrated with processes), keepalive
 * primary must be pinned to a node.  Keepalive shadow watches keepalive
 * primary to determine the health of the semaphore and named pipe.  During
 * testing, doing a semop() on a "dead" semaphore did not always result in
 * an error condition.  Watching keepalive primary seems to be the only
 * reliable method.
 *
 * Note: While may be possible to allow keepalive shadow to migrate,
 *       support for that is not currently implemented.  So, once
 *       keepalive primary pins itself, by default keepalive shadow
 *       also becomes pinned to the extent that keepalive shadow is
 *       allowed to move to another node but cannot be told to migrate
 *       by an external process. 
 *
 * 
 * Interface
 *
 * void create_primary()
 *
 * Returns
 *
 * Nothing
*/  


void
create_primary()
{
  struct  stat  buf;

  /* pin ourselves */
  if (do_pin(1))
	  exit(1);

  setsid();
  chdir("/");
  umask(0);
  /* SSI_XXX: Since keepalive is only run as single instance and we always
   * run on the same node its called on, no need to reopen files or
   * remap data. */
  open_ka_files();
  map_data();

  if (chdir(KEEPDIR) < 0) {
    syslog(LOG_WARNING, "%s: Cannot chdir to %s.  Continuing . . .\n", program_name, KEEPDIR);
  }

  if (!restart) {
    /* Re-establish named pipe. */
    (void)unlink(KEEPCFG);
    errno = 0;      /* ignore errors from unlink */
    if(mkfifo(KEEPCFG, 0660) < 0) {
      syslog(LOG_ERR, "%s: mkfifo(%s) failed\n", program_name, KEEPCFG);
      cleanup();
      exit(1);
    }
  } 

  pkctl->primary_pid = getpid();

  sync_data();

  if( stat(KEEPCFG, &buf) < 0) {
    syslog(LOG_ERR, "FIFO pipe not found.\n");

    if(mkfifo(KEEPCFG, 0660) < 0){
      syslog(LOG_ERR, "%s: mkfifo(%s) failed\n", program_name, KEEPCFG);
      cleanup();
      exit(1);
    }
  }

  if ((pipefd = open(KEEPCFG, O_RDWR|O_NONBLOCK)) < 0){
    syslog(LOG_ERR, "%s: open(%s) failed\n", program_name, KEEPCFG);
    cleanup();
    exit(1);
  }

  if (fcntl(pipefd, F_SETFL, fcntl(pipefd, F_GETFL) & ~O_NONBLOCK) < 0){
    syslog(LOG_ERR, "%s: fcntl(%s) failed\n", program_name, KEEPCFG);
  }
}

void
verify_ka_not_running()
{
  if (find_other_ka()) {
    syslog(LOG_ERR, "%s: Keepalive is already running.\n", program_name);
    fprintf(stderr, "%s: Keepalive is already running.\n", program_name);
    exit(1);
  }
}

   
/*                                                                       
 *                 Function initialize()                                 
 *                                                                       
 * Description                                                           
 *                                                                       
 * The initialize routine sets up the environment for the appropriate    
 * keepalive functioning. It looks for a preexisting keepalive data file       
 * If there is none it creates one and attaches to it the       
 * necessary information. It makes sure that only one primary and one    
 * shadow keepalive exist. It spawns a shadow if the shadow is not       
 * active. It opens a FIFO file to accomodate for the communication     
 * between keepalive and spawndaemon.  It pins this process to the node it
 * is running on, so that the processes that it forks will be pinned
 * as well.
 * Interface                                                             
 *                                                                       
 * void initialize()                                                     
 *                                                                       
 * Returns                                                               
 *                                                                       
 * Nothing                                                               
*/
 
void
initialize()
{
  int    ret;

#ifdef DEBUG1
  syslog_msg = "%s: (%ld) ENTER initialize().\n";
  syslog(LOG_DEBUG, syslog_msg, program_name, getpid());
#endif


  /* Verify that another instance of Keepalive is not running. */
  verify_ka_not_running();

  restart=open_ka_files();

  /* If we're not restarting, the file we want to map has length 0. */
  if (!restart) {

    if (lseek(data_fd, (off_t)sizeof(kalive_ctl_t) + (sizeof(process_t) * DEFAULT_MAX_DAEMONS), SEEK_SET) == (off_t)-1) {
      syslog(LOG_ERR, "%s: Could not lseek on keepalive data file.\n", program_name);
      fprintf(stderr, "%s: Could not lseek on keepalive data file.\n", program_name);
      exit(1);
    }
    else {
      char endchar = '0';
      ret = write(data_fd, (void *)&endchar, sizeof(endchar));
      if (ret == -1) {
        syslog(LOG_ERR, "%s: Could not write to keepalive data file.\n", program_name);
        fprintf(stderr, "%s: Could not write to keepalive data file.\n", program_name);
        exit(1);
      }
    }
  }

  map_data();
  if (!restart) {
    /* if we're starting from scratch, fill in the file with defaults */
    clear_data_file();
  }


  /* Create primary Keepalive. */
  pkctl->polling_interval = interval;
  create_primary();

  /* SSI_XXX: Using poll instead.
  if (ioctl(pipefd, I_SETSIG, S_INPUT) < 0) {
    syslog(LOG_ERR, "%s: Could not perform ioctl for named pipe.\n", program_name);
  }
  */
}

/*                                                                       
 *                 Function cleanup()                                    
 *                                                                       
 * Description                                                           
 *                                                                       
 * The cleanup() routine unmaps keepalive's data file, removes it, and
 * unlinks the pipe with which keepalive and spawndaemon communicate. 
 *
 *                                                                       
 * Interface                                                             
 *                                                                       
 * void cleanup()                                                        
 *                                                                       
 * Returns                                                              
 *                                                                       
 * Nothing                                                               
*/
 
void
cleanup()
{
  int i;

  munmap(pctl_procs, sizeof(kalive_ctl_t) + (pkctl->max_registered_daemons * sizeof(process_t)));
  pctl_procs=NULL;
  errno = 0;


  /*
   * kill the child process that polls for 
   * command from spawndaemon
   */ 	 
	kill(child_pid,SIGTERM);

  /* Remove the data file.          */
  /* For some reason, sometimes one unlink() isn't enough. */
  for (i = 0; i < 10; i++) {
    if (unlink(KEEPDATA) == 0) {
      break;
    }
  }

  if (i == 10) {
    syslog(LOG_ERR, "%s: Could not delete keepalive data file '%s', errno %d\n", program_name, KEEPDATA, errno);
  }

  unlink(KEEPCFG);
}


/*                                                                       
 *                 Function sync_data()                                    
 *                                                                       
 * Description                                                           
 *                                                                       
 * The sync_data() routine writes changes from mapped memory to the data
 * file on disk.
 *
 *                                                                       
 * Interface                                                             
 *                                                                       
 * void sync_data()                                                        
 *                                                                       
 * Returns                                                              
 *                                                                       
 * Nothing                                                               
*/

void
sync_data()
{
  int ret;
  struct stat buf;

  if (stat(KEEPDATA, &buf) == -1) {
    syslog(LOG_ERR, "Could not stat keepalive data file\n");
  }
  else {
    ret = msync((caddr_t)pkctl, (size_t)buf.st_size, MS_SYNC);
    if (ret == -1) {
      syslog(LOG_ERR, "Could not sync keepalive data file\n");
    }
  }
}
 

/*                                                                      
 *              Function check_file_perms()                             
 *                                                                      
 * Description                                                          
 *                                                                      
 * The check_file_perms() routine locates the respawn script file of the
 * process in the /etc/keepalive.d directory. It checks the file for    
 * the correct access permissions for owner, group and other fields as  
 * well as the user id and the group id which both have to be set to     
 * "adm". If any of these tests fail the routine sends an MDS event     
 * notifying the problem.                                               
 *                                                                      
 * Interface                                                            
 *                                                                      
 * int check_file_perms(char *file_name)                                
 *                                                                      
 * Returns                                                             
 *                                                                      
 * 0 if any of the file access permissions is not the right one.        
 * 1 otherwise.           
 * -1 on error.                                              
*/         

int
check_file_perms(char *file_name)
{

  struct stat buf;
  
  if(stat(file_name, &buf) < 0){
    syslog(LOG_ERR, "File %s cannot be found\n", file_name); 
  }else{ 
    if(sysuid != buf.st_uid) {
      syslog(LOG_INFO, "%s: OWNER/GROUP ID: %s.\n", program_name, file_name);
      return(0);
    }
    if(sysgid != buf.st_gid) {
      syslog(LOG_INFO, "%s: OWNER/GROUP ID: IN GID %s.\n", program_name, file_name);
      return(0);
    }
    if(buf.st_mode & S_IWOTH) {
      return(0);
    }
  }
  return(1);
} 
 

/*                                                                      
 *              Function process_group()                                
 *                                                                      
 * Description                                                          
 *                                                                      
 * The process group routine restarts the processes of the group after  
 * a member process has terminated. The priority must be equal to 1 in
 * order to restart all the processes belonging to a group. 
 *                                                                      
 * Interface                                                            
 *                                                                      
 * void process_group(char *group)                                      
 *                                                                      
 * Returns                                                              
 *                                                                      
 * Nothing                                                             
*/  

void
process_group(char *group)
{
  int i;
  int group_members;
  struct timeval sleep_interval = { 0, 0 };

#ifdef DEBUG1
syslog_msg = "%s: ENTER process_group().\n";
syslog(LOG_ERR, syslog_msg, program_name);
#endif

  /* Mark all the processes in the group, so no one else will mess with 
   * them while we are working. Send a SIGTERM or execute the shutdown script
   * for each member.  Shutdown the daemons in the reverse order from which 
   * they were registered just in case there is some type of dependency 
   * between them. 
  */
  group_members = 0;
  for (i = (pkctl->registered_daemons - 1); i >= 0; i--) {
    if ((strcmp(pprocs[i].group.name, group) == 0)) {
      group_members++;
      pprocs[i].respawning_group = TRUE;
    }
  }

  kill_all_daemons(group, ST_SHUTDOWN);

  /* respawn each daemon in the group */
  for(i = 0; i < pkctl->registered_daemons; i++) {

    if ((strcmp(pprocs[i].group.name, group) == 0)) {
#ifdef DEBUG1
syslog_msg = "%s: respawning %s.\n";
syslog(LOG_DEBUG, syslog_msg, program_name, pprocs[i].name);
#endif

      if ((pprocs[i].state == ST_DEAD) || (pprocs[i].state == ST_SHUTDOWN)) {
        respawn(&pprocs[i]); 
        if (pprocs[i].group.wait_time != 0) {
          sleep_interval.tv_sec = pprocs[i].group.wait_time;
          select(0, NULL, NULL, NULL, &sleep_interval);
        }
      }  
    }
  }

  /* unmark the group so normal processing can resume */
  for(i = 0; i < pkctl->registered_daemons; i++) {

    if ((strcmp(pprocs[i].group.name, group) == 0)) {
      pprocs[i].respawning_group = FALSE;
    }
  }

#ifdef DEBUG1
syslog_msg = "%s: EXIT process_group().\n";
syslog(LOG_ERR, syslog_msg, program_name);
#endif
}
 


/*                                                                      
 *              Function respawn_process_or_group()                     
 *                                                                      
 * Description                                                          
 *                                                                      
 * The respawn_process_or_group() routine determines whether a process  
 * is to be respawned or a group of processes and it respawns either.   
 * Before the respawn a check on the file permissions is done and only  
 * if the permissions are the right ones the process or group of        
 * processes gets respawned.                                            
 *                                                                      
 * Interface                                                            
 *                                                                      
 * void respawn_process_or_group(process_t *proc)                       
 *                                                                      
 * Returns                                                              
 *                                                                      
 * Nothing                                                              
*/ 

void
respawn_process_or_group(process_t *proc)
{
  if(proc->group.name[0] == '\0') { 
    respawn(proc);
  } else if(proc->group.priority == 0) { /* group with priority 0 */
    respawn(proc);
  } else{ /* group with priority 1 */
    process_group(proc->group.name); 
  }
} 



/*                                                                       
 *                Function getnextprocslot()                             
 *                                                                       
 * Description                                                           
 *                                                                       
 * The getnextprocslot() routine examines the process table for a process
 * slot. If it doesn't find a slot it attaches space for 5 more processes
 * to the memory-mapped file.                                         
 *                                                                       
 * Interface                                                             
 *                                                                       
 * int getnextprocslot()                                                 
 *                                                                       
 * Returns                                                               
 *                                                                       
 * -1 on error 
 * The slot on the process table, otherwise. 
*/

int
getnextprocslot()
{
       
  short nextslot;
  int   ret;
  struct stat buf;
  off_t status;
  long temp;

  nextslot = 0;


  while ((ST_EMPTY != pprocs[nextslot].state) && 
         (nextslot < pkctl->max_registered_daemons)) 
  {
    nextslot++;

    /* Verify that we have not exceeded the number of slots available to Keepalive. */
    if (nextslot >= pkctl->max_possible_daemons) {
      syslog(LOG_ERR, "%s: Keepalive only has %ld slots available for process registration.  Use /sbin/spawndaemon '-s' option to increase the size of Keepalive's watch process table.\n", program_name, pkctl->max_possible_daemons);
      return(-1);
    }

    if (nextslot >= pkctl->max_registered_daemons) {

      /* save the data we need from the file */
      temp = pkctl->max_registered_daemons;

      /* unmap the data file */
      errno = 0;
      if (munmap(pctl_procs, sizeof(kalive_ctl_t) + (pkctl->max_registered_daemons * sizeof(process_t))) == -1) {
        syslog(LOG_ERR, "%s: Could not unmap data file '%s', errno %d\n", program_name, KEEPDATA,  errno);
        return(-1);
      }
      pctl_procs=NULL;

      /* grow the data file by REQUEST_DAEMON entries */
      errno = 0;

      status = lseek(data_fd, (off_t)(sizeof(kalive_ctl_t) + ((sizeof(process_t) * (temp + REQUEST_DAEMONS)))), SEEK_SET);
      if (status == -1) {
        syslog(LOG_ERR, "%s: Could not lseek on keepalive data file '%s', errno %d\n", program_name, KEEPDATA, errno);
        return(-1);
      }
      else {
        char endchar = '0';
        ret = write(data_fd, (void *)&endchar, sizeof(endchar));
        if (ret == -1) {
          syslog(LOG_ERR, "%s: Could not write to keepalive data file '%s'.\n", program_name, KEEPDATA);
          return(-1);
        }
      }

      if (stat(KEEPDATA, &buf) == -1) {
        syslog(LOG_ERR, "%s: Could not stat keepalive data file '%s'.\n", program_name, KEEPDATA);
        return(-1);
      }

      /* map the data file again */
      map_data();

      pkctl->max_registered_daemons += REQUEST_DAEMONS;
      sync_data();
    }
  }
  return(nextslot);
}



/*                                                                       
 *                 Function check_if_registered()                        
 *                                                                       
 * Description                                                           
 *                                                                       
 * The check_if_registered() routine examines whether a process is       
 * already registered with keepalive or not.                                 
 *                                                                       
 * Interface                                                             
 *                                                                       
 * int check_if_registered(process_t *proc)                     
 *                                                                       
 * Returns                                                               
 *                                                                       
 * 0 if process is not registered                                        
 * 1 if process is registered                                            
*/

int
check_if_registered(process_t *proc)
{
  int i, ret;

  ret = 0;
  for (i = 0; i < pkctl->registered_daemons; i++) {
    /*
      Compare the pid and the process name against the pid and
      name on the keepalive table. This is especially useful 
      when a daemon is registered with keepalive using the -R
      option and the system changes run level, say from 3 to s. 
    */  
    if((proc->pid == pprocs[i].pid) &&
      (strcmp(proc->name, pprocs[i].name) == 0) ) {
      ret = 1;
      return(ret);
    }
  }
  return(ret);
} 

/*                                                                       
 *                 Function proc_cmd()                                   
 *                                                                       
 * Description                                                           
 *                                                                       
 * The proc_cmd() routine processes the commands sent by spawndaemon     
 * through the named pipe. It reads the commands from the pipe and then  
 * it processes them accordingly.                                        
 *                                                                       
 * Interface                                                             
 *                                                                       
 * void proc_cmd()                                                       
 *                                                                       
 * Returns                                                               
 *                                                                       
 * Nothing                                                               
*/

void  
proc_cmd()
{
  kalive_cmd_t kcmd;
  char *p;
  int i;
  clock_t lbolt;
  time_t date;
  int size;
  unsigned old_alarm;

  /*
  while(1) {
  */
  /* Read message. */
  p = (char *) &kcmd;
  size = sizeof(kcmd);
  while(size) {
    i = read(pipefd, (void *) p, size);
#ifdef DEBUG
    syslog(LOG_DEBUG, "read %d of %d bytes\n", i, size);
#endif /* DEBUG */
    if (i == 0)
      return;
    if (i < 0) {
      /* An error has occurred. */
      if (errno == EAGAIN)
	 continue;
      syslog(LOG_ERR, "%s: Cannot read the pipe %s, errno is %d\n", program_name, KEEPCFG, errno);
      return;
    }
    /* Process partially read message. */
    p = p + i;
    size = size - i;
  }
  
  if (strcmp(kcmd.magic, KEEP_MAGIC)!=0) {
    syslog(LOG_ERR, "%s: invalid magic cookie, expected %s\n", program_name, KEEP_MAGIC);
    return;        
  }

  if (kcmd.size != sizeof(kcmd)) {
    syslog(LOG_ERR, "%s: invalid size, expected (%d)\n", program_name, sizeof(kcmd)); 
    return;
  }

#ifdef DEBUG
time(&sys_time);
ctime_r(&sys_time, time_str);
syslog(LOG_DEBUG, "/sbin/keepalive: TIME: time before processing spawndaemon command:  %s\n", time_str);
#endif

  switch(kcmd.cmd & CMD_MASK){

    case ADD:

      if( (i = getnextprocslot()) < 0) {
        syslog(LOG_ERR, "%s: Process '%s' failed to register with Keepalive.\n", program_name, kcmd.daemon.name);
        return;
      }
      LOCK_PROC(pprocs[i]);
      pprocs[i] = kcmd.daemon;
      get_basename(pprocs[i].basename, pprocs[i].name);
      pprocs[i].status_captured = NO_ZOMBIE_STATUS;
      pprocs[i].adoption_status = NOT_ADOPTED;
      pprocs[i].nerrors = 0;
      pprocs[i].slot = i;
      pprocs[i].pid = -1;
      pprocs[i].last_pid = -1;
      pprocs[i].failure = NO_FAILURE;
      pprocs[i].state = ST_START;
      UNLOCK_PROC(pprocs[i]);
      pkctl->registered_daemons += 1;  

      /* Force data out to disk. */
      sync_data();

      /* Start daemon. */
      spawn(&pprocs[i]);
      break;
  
    case DELETE_GROUP:
  
      delete_daemons(&kcmd);
      if(pkctl->registered_daemons < 0 ) {
        pkctl->registered_daemons = 0;
      }
      sync_data();
      break;
    
    case DELETE:

      delete_daemons(&kcmd);
      if(pkctl->registered_daemons < 0 ) {
        pkctl->registered_daemons = 0;
      }
      sync_data();
      break;
  
    case REGISTER:
  
      if (kcmd.daemon.euid) {
        syslog(LOG_ERR, "%s: Only root can administer keepalive.\n", program_name);
        break;
      }
      if(pkctl->registered_daemons >= pkctl->max_possible_daemons) {
        break;
      }
      if( check_if_registered(&kcmd.daemon) == TRUE ) {
        syslog(LOG_ERR, "%s: Process (%d) is already registered.\n", program_name, kcmd.daemon.pid);
        break;
      }else{
        if( (i = getnextprocslot()) < 0) {
          syslog(LOG_ERR, "%s: Process with PID '%d' failed to register with Keepalive.\n", program_name, kcmd.daemon.pid);
          return;
        }
        LOCK_PROC(pprocs[i]);
        pprocs[i] = kcmd.daemon;
        pprocs[i].last_pid = -1;
        pprocs[i].status_captured = NO_ZOMBIE_STATUS;
        pprocs[i].adoption_status = NOT_ADOPTED;
        pprocs[i].failure = NO_FAILURE;
        pprocs[i].state = ST_OK;
        pprocs[i].daemonization_recovery = FALSE;

        pprocs[i].slot = i;
        UNLOCK_PROC(pprocs[i]);

        pkctl->registered_daemons += 1;
        sync_data();

        /* Adopt daemon as child process. */
        adopt_child(&pprocs[i]);
        sync_data();

        /* If Keepalive is now polling due to an adoption failure, set
         * alarm clock but verify that it is not already set to go off
         * at an earlier time.
        */
        if (pkctl->polling) {
          old_alarm=alarm(pkctl->polling_interval);

          if ((old_alarm != 0) && (old_alarm < pkctl->polling_interval)) {
            alarm(old_alarm);
          }
        }
      }
      break;
  
    case RESET:
  
      if (kcmd.daemon.euid){
        syslog(LOG_ERR, "%s: Only root can administer keepalive.\n", program_name);
        break;
      }
      for(i = 0; i < pkctl->registered_daemons; i++) {
        if((strcmp(pprocs[i].name, kcmd.daemon.name) == 0) ||
           (pprocs[i].pid == kcmd.daemon.pid) ||
           (pprocs[i].slot == kcmd.daemon.slot)) {
          if ((pprocs[i].state == ST_DOWN) ||
              (pprocs[i].state == ST_DEAD) ||
              (pprocs[i].state == ST_OK))
          {
            /* Shutdown daemon if requested to do so. */
            if ((pprocs[i].state == ST_OK) && 
                ((kcmd.cmd & THEN_KILL) != 0)) 
            {
              terminate_daemon(&pprocs[i], ST_SHUTDOWN);
            }

            /* We want to force a respawn of the daemon. */
            pprocs[i].nerrors = 0;

            /* If the daemon is OK, don't mess with this state info. */
            if (pprocs[i].state != ST_OK) {
              lbolt = get_lbolt();
              date = get_date();

              LOCK_PROC(pprocs[i]);
              pprocs[i].daemon_last_died.lbolt_in_sec = lbolt;
              pprocs[i].daemon_last_died.date = date;
              pprocs[i].daemon_first_died.lbolt_in_sec = lbolt;
              pprocs[i].daemon_first_died.date = date;
              pprocs[i].status_captured = NO_ZOMBIE_STATUS;
              pprocs[i].state = ST_RESPAWNING;
              UNLOCK_PROC(pprocs[i]);
              reset = TRUE;
            }
          }
        }
      }
      if (reset == FALSE) {
        syslog(LOG_ERR, "%s: Could not find process to reset its state.\n", program_name);
      } 
      sync_data();
      break;
  
    case RESTART:
  
      if (kcmd.daemon.euid){
        syslog(LOG_ERR, "%s: Only root can administer keepalive.\n", program_name);
        break;
      }
      pkctl->quiesce_flag = FALSE;
      sync_data();

      /* Indicate that we need to inspect the process table.  We are losing
       * a SIGCHLD possibly due to a reclaim_children() problem that should be
       * fixed soon.
      */
      stable_state = FALSE;
      break;
  
    case QUIT:
  
      if (kcmd.daemon.euid){
        syslog(LOG_ERR, "%s: Only root can administer keepalive.\n", program_name);
        break;
      }
      if( (0 != (kcmd.cmd & THEN_KILL)) ){
        kill_all_daemons("", ST_EMPTY);
      }
      sync_data();
      syslog(LOG_INFO, "keepalive daemon is terminating\n");
      cleanup();
#ifdef DEBUG1
time(&sys_time);
ctime_r(&sys_time, time_str);
syslog(LOG_DEBUG, "/sbin/keepalive: TIME: time after processing spawndaemon command:  %s\n", time_str);
#endif
      exit(0);
      break;

    case QUIET:
  
      if (kcmd.daemon.euid){
        syslog(LOG_ERR, "%s: Only root can administer keepalive.\n", program_name);
        break;
      }
      pkctl->quiesce_flag = TRUE;
      sync_data();
      break;
  
    default:     
  
      syslog(LOG_ERR, "%s: Invalid keepalive command %ld.\n", program_name, kcmd.cmd & CMD_MASK);
      break;
  }

#ifdef DEBUG1
time(&sys_time);
ctime_r(&sys_time, time_str);
syslog(LOG_DEBUG, "/sbin/keepalive: TIME: time after processing spawndaemon command:  %s\n", time_str);
#endif
/*
    }
 */
} 
 
int
downed_slot(process_t *proc)
{
  clock_t lbolt;   
  time_t date;
  time_t last_die;

  lbolt = get_lbolt();
  date = get_date();

  /* Set beginning of probation period if not already set. */
  if (proc->daemon_first_died.lbolt_in_sec == 0) {
    proc->daemon_first_died.lbolt_in_sec = lbolt;
    proc->daemon_first_died.date = date;
  }

  /* Find time between when we first died and now.  Later we will use this
   * to determine if the current probation period has elapsed.
  */
  proc->daemon_last_died.lbolt_in_sec = lbolt;
  proc->daemon_last_died.date = date;
  last_die = proc->daemon_last_died.lbolt_in_sec - proc->daemon_first_died.lbolt_in_sec;

  /* Needed only while time() is used for lbolt. */
  if (last_die < 0)
    last_die = -last_die;

  /* Turn off probation period feature if time() is returning -1 which
   * indicates a problem with the system time.
  */
  if ((proc->state != ST_RESPAWNING) && (proc->nerrors > 0) && 
      (last_die > proc->probation_period) && (proc->probation_period != 0)) 
  {
    /* reset beginning of probation period */
    proc->daemon_first_died.lbolt_in_sec = lbolt;
    proc->daemon_first_died.date = date;
  }

  /* Check for exit code from daemon requesting to go down. */
  if (proc->down_exit_code_used && 
      (proc->status_captured == ZOMBIE) && 
      WIFEXITED(proc->status)) 
  {
    if (WEXITSTATUS(proc->status) == proc->down_exit_code) {
      syslog(LOG_ERR, "%s: Process '%s' (pid %d) has requested to go down.  It is now down.\n",  program_name, proc->name, proc->pid);
      declare_down(proc);
      return TRUE;
    }
  }

  /* See if max. errors has been exceeded within probation period. */
  if ((proc->nerrors >= proc->maxerrors ) &&
      ( proc->maxerrors != 0 ) &&
      ( proc->probation_period != 0 )) 
  {
    syslog(LOG_ERR, "%s: Process '%s' (pid %d) has exceeded max_errors.  It is now down.\n",  program_name, proc->name, proc->pid);
    declare_down(proc);
    return TRUE;
  }

  return FALSE;
}


/*                                                                       
 *                 Function keepalive()                                  
 *                                                                       
 * The keepalive() routine performs the major functionality of the       
 * keepalive system. It monitors processes when it is time to do so      
 * and it checks the shadow keepalive when it is time to check it. It    
 * processes commands received by spawndaemon. When there is no command  
 * to process it checks the daemons states and if necessary it will      
 * respawn the daemons.  
 * Description                                                           
 *                                                                       
 * Interface                                                             
 *                                                                       
 * void keepalive()                                                      
 *                                                                       
 * Returns                                                               
 *                                                                       
 * Nothing                                                               
*/


void
keepalive()
{
  int result;
  int i, j;
  struct stat buf;
  int alive;
  zombie_info_t zombie;
  struct pollfd pfd;
  pid_t pid;
  int signal_received = 0;
  pid_t ppid = getpid();
  	

  if ( (pid = fork()) < 0) {
	  syslog(LOG_INFO, "fork failed! %d\n", pid);
	  exit(1);
  }

  if (pid == 0) { /* Child */
	/* 
	 * SSI_XXX: Linux doesn't send SIGPOLL on FIFOs.
	 * Therefore, implement using poll().
	 */
	sigset_t sigset;

	sigemptyset(&sigset);
	sigaddset(&sigset, SIGUSR1);
	sigprocmask(SIG_SETMASK, &sigset, NULL);

	pfd.fd = pipefd;
	pfd.events = POLLIN;

	while (1) {
		result = poll(&pfd, 1, -1);

#ifdef DEBUG
		syslog(LOG_DEBUG, "poll() returned: %d.\n", result);
		syslog(LOG_DEBUG, "poll() revents: %x.\n", pfd.revents);
#endif /* DEBUG */
		if (result == 1) {
			/* SSI_XXX: Simulate a SIGPOLL signal */
			kill(ppid, SIGPOLL);
		}
		else if (result < 0 && errno == EINTR) {
			syslog(LOG_DEBUG, "poll() was interrupted by signal %d.\n", signal_received);
			continue;
		}
#ifdef DEBUG
		else {
			syslog(LOG_ERR, "poll() returned unexpected result %d.\n", result);
			exit(1);
		}
#endif /* DEBUG */
		sigwait(&sigset, &result);
	}
  }

	/*
	 *	Child pid saved in a global variable 
	 *	used during cleanup()
   */
  child_pid = pid; 
	
  /* If a keepalive data file exists restart is set to TRUE in initialize().
  * This indicates that keepalive gets restarted otherwise we start
  * up fresh.
  */ 
  if (restart) {
    syslog(LOG_INFO, "%s: keepalive daemon restarted\n", program_name);
  } else {
    syslog(LOG_INFO, "%s: keepalive daemon started\n", program_name);
  }

  /* Keepalive requires that the semaphore is held already 
  * and that the data file is already memory-mapped. 
  */

  map_data();

  /* set the signal handler */
  sa.sa_handler = dummy_handler;
  sa.sa_flags = SA_SIGINFO;
  sigaction(SIGCHLD, &sa, NULL);
  sigaction(SIGALRM, &sa, NULL);
  sigaction(SIGIO, &sa, NULL);

  if (cluster_events_register_signal(-1, SIGCLUSTER) == -1)
	syslog(LOG_ERR, "%s: Cannot set SIGCLUSTER, error %d\n",
        program_name, errno);
  /* CONSTANT CONDITION */
  while (TRUE) {

#ifdef DEBUG
time(&sys_time);
ctime_r(&sys_time, time_str);
syslog(LOG_DEBUG, "/sbin/keepalive:  TOP while() in keepalive()  TIME: %s\n", time_str);
syslog(LOG_DEBUG, "%s: pkctl->primary_pid = %d\n", program_name, pkctl->primary_pid);
#endif

    /* Verify that data file still exists, if not exit. 
       Note: fstat() doesn't seem to be reliable for this type
       of check. */
    
    if (stat(KEEPDATA, &buf) == -1) {
      syslog(LOG_ERR, "%s: Cannot stat keepalive data file '%s'.\n", program_name, KEEPDATA);
      exit(1);
    }

    /* If Keepalive wasn't just restarted or is quiesced, wait for a signal. */
    if (!restart || pkctl->quiesce_flag == TRUE) {
      /* Wait for SIGPOLL, SIGALARM, or SIGCHLD */
      sigemptyset(&sigwait_signals);
      sigaddset(&sigwait_signals, SIGPOLL);
      sigaddset(&sigwait_signals, SIGTERM);

      /* If Keepalive is not quiesced, then watch processes. */
      if (pkctl->quiesce_flag == FALSE ) {
        sigaddset(&sigwait_signals, SIGALRM);
        sigaddset(&sigwait_signals, SIGCHLD);
        sigaddset(&sigwait_signals, SIGCLUSTER);
      }

#ifdef DEBUG2
sigpending(&DEBUG_pending_mask);
for (a_signal = 1; a_signal <= MAX_SIGNAL; a_signal++) {
  if (sigismember(&DEBUG_pending_mask, a_signal)) {
    syslog(LOG_DEBUG, "/sbin/keepalive: before sigwait(): signal %d is pending.\n", a_signal);
  }
}


sigprocmask(SIG_SETMASK, NULL, &DEBUG_current_mask);
for (a_signal = 1; a_signal <= MAX_SIGNAL; a_signal++) {
  if (sigismember(&DEBUG_current_mask, a_signal)) {
    syslog(LOG_DEBUG, "/sbin/keepalive: before sigwait(): signal %d is held.\n", a_signal);
  }
}
#endif

 sigprocmask(SIG_BLOCK, &sigwait_signals, NULL);
 signal_received = sigwaitinfo(&sigwait_signals, NULL);
 if (signal_received == -1) {
	      syslog(LOG_ERR, "%s: sigwait failed.\n", program_name);
	      exit(1);
       }

#ifdef DEBUG
syslog(LOG_DEBUG, "/sbin/keepalive: after sigwait(): signal %d detected by sigwait().\n", signal_received);
#endif

#ifdef DEBUG2
sigpending(&DEBUG_pending_mask);
for (a_signal = 1; a_signal <= MAX_SIGNAL; a_signal++) {
  if (sigismember(&DEBUG_pending_mask, a_signal)) {
    syslog(LOG_DEBUG, "/sbin/keepalive: after sigwait(): signal %d is pending.\n", a_signal);
  }
}

sigprocmask(SIG_SETMASK, NULL, &DEBUG_current_mask);
for (a_signal = 1; a_signal <= MAX_SIGNAL; a_signal++) {
  if (sigismember(&DEBUG_current_mask, a_signal)) {
    syslog(LOG_DEBUG, "/sbin/keepalive: after sigwait(): signal %d is held.\n", a_signal);
  }
}
#endif

      /* If Keepalive's mmapped file is not present, exit. */
      if (stat(KEEPDATA, &buf) == -1) {
        syslog(LOG_ERR, "%s: Cannot stat keepalive data file '%s'.\n", program_name, KEEPDATA);
        exit(1);
      }

      switch (signal_received) {
        case SIGPOLL:
          /* A command has been received */
          result = 1;
          break;
        case SIGALRM:
        case SIGCHLD:
        case SIGCLUSTER:
          /* Check the processes */
          result = 0;
          break;
        case SIGTERM:
	  delete_restart_log();
	  /*
	   * kill the child process that polls for
	   * command from spawndaemon
	   */
	  kill(child_pid,SIGTERM);
          exit(0);
        default:
          syslog(LOG_ERR, "%s: sigwait() received unexpected signal %d.\n", program_name, signal_received);
          result = -1;    /* An error has occured */
          break;
      }
    }
    else {
      /* Walk through Keepalive's watched process table. */
      result = 0;
    }

#ifdef DEBUG
time(&sys_time);
ctime_r(&sys_time, time_str);
syslog(LOG_DEBUG, "/sbin/keepalive: TIME: time after sigwait(): %s\n", time_str);
#endif

    /* Take care of spawndaemon commands and monitor processes. */
    switch(result){

      case 1:
#ifdef DEBUG
time(&sys_time);
ctime_r(&sys_time, time_str);
syslog(LOG_DEBUG, "/sbin/keepalive: TIME: time before proc_cmd():  %s\n", time_str);
#endif

        /* Assume that we are in a stable state. */
        stable_state = TRUE;

        /* A command has come in from spawndaemon */
        proc_cmd();
	kill(pid, SIGUSR1);

#ifdef DEBUG
time(&sys_time);
ctime_r(&sys_time, time_str);
syslog(LOG_DEBUG, "/sbin/keepalive: TIME: time after proc_cmd():  %s\n", time_str);
#endif

        /* Unless we got a command to reset the error counters of a daemon
         * or Keepalive is no longer in a stable state, we're done.  Go back 
         * and listen for another command or wait for a process to die.  
        */
        if (!reset && stable_state) {
          break; 
        }

      case 0:
#ifdef DEBUG
time(&sys_time);
ctime_r(&sys_time, time_str);
syslog(LOG_DEBUG, "/sbin/keepalive: TIME: time ENTER monitor processes:  %s\n", time_str);
#endif
        /* Re-adopt processes if Keepalive has been restarted. */
        if (restart) {
          for(i=0; i < pkctl->registered_daemons; i++) {
            pprocs[i].adoption_status = NOT_ADOPTED;
            adopt_child(&pprocs[i]);

            /* Cleanup any locks that might have been left behind by a
             * previous instance of Keepalive.  We don't currently support
             * a robust method of recovering when Keepalive dies in the middle
             * of recovering a process.
            */
            UNLOCK_PROC(pprocs[i]);
          }
        }

        /* Assume that Keepalive doesn't need to poll.  Polling is done
         * by Keepalive in the following cases:
         *
         * 1) Keepalive is watching a process that has not been adopted.
         * 2) Keepalive experienced a fork()/rfork() failure.
         *
         * Whether or not more polling is needed is redetermined in
         * the main loop.  Once set to TRUE, polling remains true until
         * we re-enter the main loop from this point in the code.
        */
        pkctl->polling = FALSE;

        /* Compensate for SIGCHLD problem where sometimes SIGCHLD is not
         * received for outstanding zombies.  Keepalive is in an unstable
         * state while we suspect that there are more zombies (i.e. dead
         * processes) that need to be cleaned up for which we have not
         * received a SIGCHLD.
        */
        stable_state = FALSE;
        while (!stable_state) { 
          /* Assume that we are in a stable state and have not updated
           * any status information for any daemon.
          */
          stable_state = TRUE;

          /* Assume that there is no adoption problem. */
          adoption_problem = FALSE;
 
          /* Assume that there is no min. respawn interval.  The min. respawn
           * interval defines the minimum time that must elapse before a
           * specific daemon can be restarted.  The min. respawn interval
           * is specified for each daemon.
          */
          next_alarm = 0;

          /* Load zombie queue.  We don't care about any zombies left in the
           * list from the previous pass.  If there are any zombies left,
           * they belong to child processes that are not in Keepalive's
           * watched process table.  An example of such processes are:
           * processes started by Keepalive but later unregistered, down
           * scripts, and shutdown scripts.
          */ 
          make_zombie_list_empty();
          while ((zombie.pid = waitpid((pid_t)-1, 
                                       &zombie.waitpid_info, 
                                       WNOHANG)) > 0) 
          {
#ifdef DEBUG
		  syslog(LOG_DEBUG, " %s: found zombie pid %d with return status %d\n", program_name, zombie.pid, zombie.waitpid_info);
#endif /* DEBUG */
            /* Other processes may die very soon such that we will not get
             * a SIGCHLD for them--but they are not yet dead (i.e. zombies
             * are not available for them).  Keepalive's watched process 
             * table is now unstable.  We will have to loop again after
             * sleeping 1 second to check for more dead processes.
            */
            stable_state = FALSE;

            /* Insert zombies as long as there is room in the list. 
             * We are guaranteed another trip around in the loop, since
             * the watched process table has been declared unstable.
            */
            if (insert_zombie(&zombie) <= 0) {
              break;
            }
          }

          /* Start monitoring processes. */
          for(i=0; i < pkctl->registered_daemons; i++) {
#ifdef DEBUG1
time(&sys_time);
ctime_r(&sys_time, time_str);
syslog_msg = " %s: pprocs[%d].pid = %ld, pprocs[%d].state = %d  TIME: %s\n";
syslog(LOG_DEBUG, syslog_msg, program_name, i, pprocs[i].pid, i, pprocs[i].state, time_str);
#endif
            /* Capture state information for processes that just died. */
            if (ST_OK == pprocs[i].state) {
              alive = (kill(pprocs[i].pid, 0) == 0);

              /* See if Keepalive must poll due to unadopted child. */
              if (alive && (pprocs[i].adoption_status != ADOPTED)) {
                adopt_child(&pprocs[i]);
              }

              /* If process is not alive, then change state to ST_DEAD. 
               * Keepalive's state is now unstable, other processes may have
               * died within 1 second after we received the SIGCHLD.  For
               * these other processes we don't see a SIGCHLD even though
               * all the zombies were cleaned up for the first SIGCHLD.
              */
              if (!alive) {
                /* If is configured to allow daemonization, set the state
                 * to ST_DAEMONIZE to indicate the possibility of daemonization
                 * and the type of recovery needed.  This does not mean that
                 * a process has actually daemonized.  We only know that it's
                 * allowed to at this point.
                */
                if ((pprocs[i].registration_policy == REGISTER_BY_NAME) ||
                    (pprocs[i].registration_policy == REGISTER_BY_ARGS))
                {
                  /* Note:  Later we will try to determine what type of failure
                   * occurred (i.e. process or node failure).  For now assume
                   * that the process might be daemonizing until we prove
                   * differently.
                  */
                  pprocs[i].state = ST_DAEMONIZE;
                }

                /* Otherwise, process must be dead. */
                else {
                  declare_dead(&pprocs[i], TRUE);
                }

                /* Other processes may die very soon such that we will not get
                 * a SIGCHLD for them--but they are not yet dead (i.e. zombies
                 * are not available for them).  Keepalive's watched process 
                 * table is now unstable.  We will have to loop again after
                 * sleeping 1 second to check for more dead processes.
                */
                stable_state = FALSE;
              }
            } 

            /* Search for processes that require handling. */
            if ((ST_SHUTDOWN == pprocs[i].state) || 
                (ST_DEAD == pprocs[i].state) ||
                (ST_DAEMONIZE == pprocs[i].state) ||
                ((ST_RESPAWNING == pprocs[i].state) && reset))
            {
              /* Check validity of pid of the daemon.*/
              errno = 0;

              /* Save zombie info.  */
              if (pprocs[i].status_captured == NO_ZOMBIE_STATUS) {

                /* If process is a child of Keepalive, get zombie. */
                if (pprocs[i].adoption_status == ADOPTED) {
                  /* We make a blocking call to get zombie, so an error
                   * indicates that no zombie is available and we may have
                   * suffered a node failure.
                  */
                  if (serve_zombie(pprocs[i].pid, &zombie)) {
                    pprocs[i].status = zombie.waitpid_info;
                    pprocs[i].status_captured = ZOMBIE;
                  }
                  else {
                    pprocs[i].status_captured = ZOMBIE_NOT_AVAIL;
                  }
                }
                else {
                  pprocs[i].status_captured = ZOMBIE_NOT_AVAIL;
                }

                if ((pprocs[i].status_captured == ZOMBIE) && 
                    WIFEXITED(pprocs[i].status))
                {
                  syslog(LOG_INFO, "%s: Exit status of %d captured for '%s' in slot %d.\n", program_name, WEXITSTATUS(pprocs[i].status), pprocs[i].name, pprocs[i].slot);
                }
                else {
                  syslog(LOG_WARNING, "%s: Exit status for '%s' in slot %d could not be found.\n", program_name, pprocs[i].name, pprocs[i].slot);
                }
              }

              /* If process is suspected to have daemonized and the delay
               * for daemonization recovery has elapsed, do daemonization
               * recovery.
              */
              if (ST_DAEMONIZE == pprocs[i].state)
              {
                /* If process purposefully exited, it may have daemonized.
                 * Unfortunately, we have found strange cases with 
                 * daemonization (e.g. syslogd) where multiple processes
                 * are spawned before daemonization actually occurs, so
                 * to support daemons like this Keepalive cannot assume that
                 * the daemonization process is ever complete.
                */
                if ((pprocs[i].status_captured == ZOMBIE) && 
                    WIFEXITED(pprocs[i].status))
                {
#ifdef DEBUG
		    syslog(LOG_DEBUG, "finding missing proc..\n");
#endif /* DEBUG */
                    find_missing_proc(i);
                }

                /* Process has died unexpectedly. */
                else {
                  declare_dead(&pprocs[i], TRUE);
                }
              }

              /* Restart process only if this process isn't a part 
               * of a group that is being restarted.  At this point
               * the process may be ST_OK, so we must be careful to
               * only respawn for ST_DEAD, ST_SHUTDOWN, or ST_RESPAWNING.
              */
              if (((ST_DEAD == pprocs[i].state) || 
                   (ST_SHUTDOWN == pprocs[i].state) ||
                   (ST_RESPAWNING == pprocs[i].state)) &&
                  pprocs[i].respawning_group == FALSE) {
                /* Check for downed slot. */
                if (!downed_slot(&pprocs[i])) {
                  respawn_process_or_group(&pprocs[i]);
                }
              }
            }

            /* If a process is in ST_START, it has been waiting for a node
             * to be reported to be available by the Cluster API, so try
             * to start it.
            */
            if (ST_START == pprocs[i].state) 
            {
              spawn(&pprocs[i]);
            }

            /* If process has been reset, respawn it. */
            if (ST_RESPAWNING == pprocs[i].state) 
            {
              respawn_process_or_group(&pprocs[i]);
            } 

            /* Process has failed to register. */
            if (ST_EMPTY == pprocs[i].state) {
              syslog(LOG_ERR, "%s: The daemon, %s, is not running. You need to reexamine the daemon, its configuration files, and the way it is registered with keepalive.\n", program_name, pprocs[i].name);
#ifdef DEBUG1
syslog(LOG_DEBUG, "%s: ppprocs[%d].state = %d\n", program_name, i, pprocs[i].state);
#endif
              /* Clear the slot in the data file 
              *  occupied by that process.  Push cleared slot
              *  to the bottom of the list.
              */
              pprocs[i].state = ST_EMPTY;
              for(j = i; j < pkctl->registered_daemons; j++) {
                pprocs[j] = pprocs[j + 1];
                pprocs[j + 1].state = ST_EMPTY;
              }
              i--;
              pkctl->registered_daemons -= 1;
            }
          } /* End of for. */

          /* If a command was issued to reset the error counter and negate
           * the "down" state of a daemon or daemons, at this point we are 
           * done processing the command.
          */
          reset = FALSE;

         /* If we have reached a stable state, set the alarm clock for the 
          * closest minrespawn interval, if any. Otherwise, sleep for 1 second
          * and retry.  The delay is to cover for a race condition where
          * when processes die within 1 second of each other, we only get
          * one SIGCHLD
          * 
         */
         if (stable_state) {
           /* If Keepalive must poll due to unadopted children, set the
            * alarm clock.
           */
           if (pkctl->polling && 
               ((pkctl->polling_interval < next_alarm) ||
                (next_alarm == 0))) 
           {
             alarm(pkctl->polling_interval);
           }
           else {
             alarm(next_alarm);
           }
         }
         /* Otherwise, Keepalive's watch process table is unstable, so
          * we need to get ready to make another pass.
         */
         else {
           /* Sleep for 1 second to cover condition where SIGCHLD is not
            * received for processes that die within 1 second of a previously
            * issued SIGCHLD.
           */
           select(0, NULL, NULL, NULL, &sleep_one_second);
         }

         /* See if Keepalive is okay with adoption. */
         if (!adoption_problem) {
           pkctl->unadopted_process = FALSE;
         }

          /* Keepalive is finished with restart algorithm where Keepalive
           * has finished recovering from a Keepalive process restart.
          */
          restart = FALSE;

         /* End of monitoring processes. */ 
         sync_data();

       }
#ifdef DEBUG
time(&sys_time);
ctime_r(&sys_time, time_str);
syslog(LOG_DEBUG, "/sbin/keepalive: TIME: time EXIT monitor processes:  %s\n", time_str);
#endif
        break;

      default:
        /* Do nothing. */
        break;
                  
    }/* End of switch */

#ifdef DEBUG
time(&sys_time);
ctime_r(&sys_time, time_str);
syslog(LOG_DEBUG, "/sbin/keepalive: BOTTOM while() in keepalive()  TIME: %s\n", time_str);
#endif
    
  } /* End of the while */
} 

void
do_shutdown(process_t *proc)
{ 
  int script_pid = 0;
  process_t daemon;
  time_t start_time;

  /* Verify process isn't already dead. */
  if (kill(proc->pid, 0) < 0) {
    syslog(LOG_WARNING, "%s: Process '%s' with pid %d is not running.  No shutdown performed.\n", program_name, proc->name, proc->pid);
    return;
  }

  syslog(LOG_WARNING, "%s: Shutting down process '%s' with pid %d.\n", program_name, proc->name, proc->pid);

  if (proc->shutdown_script[0] != '\0') {
    /* create input record for child process so that it doesn't need */
    /*  the mmapped file. */
    daemon = *proc;

    /* Try to fork to last node where process was running. */
    if (daemon.last_node.node_num != 0) {
      daemon.lastexeced_secs = time(NULL); /* store start time */
      script_pid = rfork(daemon.last_node.node_num);
    }

    /* If we couldn't run on the last node, try to find another node 
     * to run the shutdown script on. 
    */
    if (script_pid < 0) {
      script_pid = rfork_anywhere(&start_time);
      daemon.lastexeced_secs = start_time;
    }

    /* Child process. */
    if (script_pid == 0) {
      sprintf(activebuf, "KEEPALIVE_ACTIVE_PID=%ld", (long)daemon.pid);
      putenv(activebuf);

      setup_proc(&daemon, FALSE);
      exec_script(daemon.shutdown_script, daemon.name, daemon.cfg_file);
    }
  }
  else {
    kill(proc->pid, SIGTERM);
  }
}

/*                                                                       
 *                 Function terminate_daemon()                           
 *                                                                       
 * Description                                                           
 *                                                                       
 * The terminate_daemon() routine is used to kill the daemon.            
 *                                                                       
 * Returns                                                                
 *                                                                       
 * Nothing                                                               
*/

void
terminate_daemon(process_t *proc, int state)
{
  int n;
  int zombie_info;
  struct timeval sleep_interval = { 1, 0 };

  do_shutdown(proc);

  n = 0;
  while ((kill(proc->pid, 0) == 0) && (n < proc->termwait)) {
    select(0, NULL, NULL, NULL, &sleep_interval);
    n++;
  }

  /* If process has not shutdown by termwait seconds, then SIGKILL it. */
  if (kill(proc->pid, 0) == 0) {
    (void)kill(proc->pid, SIGKILL);
  }

  /* Make sure that process has not hung on an I/O device and, thus can't be
   * SIGKILLed.  We don't want Keepalive to hang in waitpid().
  */
  waitpid(proc->pid, &zombie_info, WNOHANG);

  /* Other processes may die very soon such that we will not get
   * a SIGCHLD for them--but they are not yet dead (i.e. zombies
   * are not available for them).  Keepalive's watched process 
   * table is now unstable.  We will have to loop again after
   * sleeping 1 second to check for more dead processes.
   *
   * Note: We also want to cover ourselves in case we race waitpid().
  */
  stable_state = FALSE;

  proc->state = state;
} 


/*                                                                       
 *                 Function kill_all_daemons()                           
 *                                                                       
 * The kill_all_daemons() routine checks the process state and the       
 * process id of each daemon and shuts down the daemons.                                                              
 *                                                                       
 * Interface                                                             
 *                                                                       
 * void kill_all_daemons(void)              
 *                                                                       
 * Returns                                                               
 *                                                                       
 * Nothing                                                               
*/                                                               

void
kill_all_daemons(char *group, int state)
{
  int i;
  int sleep_time;
  int zombie_info;
  int dead_count;
  struct timeval sleep_interval = { 1, 0 };
  int group_count;

  /* Shutdown the daemons in the reverse order from which they were registered 
   * just in case there is some type of dependency between them. 
  */
  group_count = 0;
  for (i = (pkctl->registered_daemons - 1); i >= 0; i--) {
    if (pprocs[i].state != ST_EMPTY) {
      if (0 == pprocs[i].pid) {
        continue;
      }

      if ((group[0] == '\0') || (strcmp(pprocs[i].group.name, group) == 0)) 
      {
        group_count++;
        do_shutdown(&pprocs[i]);
      }
    }
  }

  /* If any group member is still alive, SIGKILL it after giving it 
   * the specified time to shutdown. 
  */
  sleep_time = 0;
  dead_count = 0;
  while (dead_count < group_count) {
    dead_count = 0;
    for (i = (pkctl->registered_daemons - 1); 
         (i >= 0) && (dead_count < group_count); 
         i--) 
    {
      if ((group[0] == '\0') || (strcmp(pprocs[i].group.name, group) == 0)) {
        if (kill(pprocs[i].pid, 0) == 0) {
          if (sleep_time == pprocs[i].termwait) {
            kill(pprocs[i].pid, SIGKILL);

            /* Make sure that process has not hung on an I/O device and, thus 
             * can't be SIGKILLed.  We don't want Keepalive to hang in 
             * waitpid().
            */
            waitpid(pprocs[i].pid, &zombie_info, WNOHANG);

            ++dead_count;
            pprocs[i].state = state;
          }
        }
        else {
          /* At this point, we're guaranteed that the process is dead. */
          waitpid(pprocs[i].pid, &zombie_info, 0);

          ++dead_count;
          pprocs[i].state = state;
        }
      }
    }

    if (dead_count < group_count) {
      select(0, NULL, NULL, NULL, &sleep_interval);
      ++sleep_time;
    }
  }

  /* Other processes may die very soon such that we will not get
   * a SIGCHLD for them--but they are not yet dead (i.e. zombies
   * are not available for them).  Keepalive's watched process 
   * table is now unstable.  We will have to loop again after
   * sleeping 1 second to check for more dead processes.
   *
   * Note: We also want to cover ourselves in case we race waitpid().
  */
  stable_state = FALSE;

  cleanup_daemon_logs();
} 


/*                                                                       
 *                 Function delete_daemons()                             
 *                                                                       
 * Description                                                           
 *                                                                       
 * The delete_daemons() routine deletes daemons requested to be deleted.   
 *                                                                       
 * Interface                                                             
 *                                                                       
 * void delete_daemons(kalive_cmd_t *p)                                     
 *                                                                       
 * Returns                                                               
 *                                                                       
 * Nothing                                                               
*/

void
delete_daemons(kalive_cmd_t *p)
{
  int i, j;
  int ndeleted;
  char log[MAXNAME];

  ndeleted = 0;
  i = 0;
  while(i < pkctl->registered_daemons) {
    if (((pprocs[i].pid == p->daemon.pid) ||
         (i == p->daemon.slot) || 
         (strcmp(p->daemon.name, pprocs[i].name) == 0)) &&
        ((p->daemon.registration_policy != REGISTER_BY_ARGS) || 
         (strcmp(pprocs[i].args, p->daemon.args) == 0)) && 
        ((p->cmd & DELETE_GROUP) == 0))
    {
      ndeleted++;
      if ( ((p->cmd & THEN_KILL) != 0) && (pprocs[i].pid != 0)) {
        syslog(LOG_INFO, "%s: killing process '%s' (pid %d)\n", program_name, pprocs[i].name, pprocs[i].pid); 
        terminate_daemon(&pprocs[i], ST_EMPTY);

        /* Remove slot's log. */
        sprintf(log, "%s/%s.%d", KEEPMSGDIR, pprocs[i].basename, pprocs[i].original_pid);
        unlink(log);

        if ( ST_EMPTY == pprocs[i].state ) {
          for(j = i; j < pkctl->registered_daemons; j++) {
            pprocs[j] = pprocs[j + 1];

            pprocs[j + 1].state = ST_EMPTY;
          }
          pkctl->registered_daemons -= 1;
          if(pkctl->registered_daemons == 0) {
            pprocs[0].state = ST_EMPTY;
          }
          sync_data();

          /* If we are removing by slot number, break out of this loop. */
          if (i == p->daemon.slot) {
            break;
          }
          else {
            i--;
          }
        }

      }else{
        syslog(LOG_INFO, "%s: unregistering process '%s' (pid %d)\n", program_name, pprocs[i].name, pprocs[i].pid);
                           
        for(j = i; j < pkctl->registered_daemons; j++) {
          pprocs[j] = pprocs[j + 1];
          pprocs[j + 1].state = ST_EMPTY;
        }
        pkctl->registered_daemons -= 1;
        sync_data();

        /* If we are removing by slot number, break out of this loop. */
        if (i == p->daemon.slot) {
          break;
        }
        else {
          i--;
        }
      }
    }else if(((DELETE_GROUP & p->cmd) != 0) && 
             (p->daemon.delete_group_flag == 0)) {
      syslog(LOG_INFO, "%s: Deleting process '%s' (pid %d)\n", program_name, pprocs[i].name, pprocs[i].pid);
      if (strcmp(p->daemon.group.name, pprocs[i].group.name) == 0) {
        ndeleted++;
        pprocs[i].state = ST_EMPTY;
        for(j = i; j < pkctl->registered_daemons; j++) {
          pprocs[j] = pprocs[j + 1];
          pprocs[j + 1].state = ST_EMPTY;
        }
        pkctl->registered_daemons = pkctl->registered_daemons - 1;
        sync_data();

        /* If we are removing by slot number, break out of this loop. */
        if (i == p->daemon.slot) {
          break;
        }
        else {
          i--;
        }
      }
    }else if( ((DELETE_GROUP & p->cmd) != 0) && 
          (p->daemon.delete_group_flag == 1) ) {

      if (strcmp(p->daemon.group.name, pprocs[i].group.name) == 0) {
        ndeleted++;
        terminate_daemon(&pprocs[i], ST_EMPTY);

        /* Remove slot's log. */
        sprintf(log, "%s/%s.%d", KEEPMSGDIR, pprocs[i].basename, pprocs[i].original_pid);
        unlink(log);

        for(j = i; j < pkctl->registered_daemons; j++) {
          pprocs[j] = pprocs[j + 1];
          pprocs[j + 1].state = ST_EMPTY;
        }
        pkctl->registered_daemons = pkctl->registered_daemons - 1;
        sync_data();

        /* If we are removing by slot number, break out of this loop. */
        if (i == p->daemon.slot) {
          break;
        }
        else {
          i--;
        }
      }
                       
    }

    i++;
  }    

  if (0 == ndeleted) {
    if (0 != (p->cmd & THEN_KILL)) {
      syslog(LOG_INFO, "%s: Could not find process '%s' (pid=%d) to kill\n", program_name, p->daemon.name, p->daemon.pid);
    } else {
  
      syslog(LOG_INFO, "%s: Could not find process '%s' (pid=%d) to delete\n", program_name, p->daemon.name, p->daemon.pid);
    }
  } else if ((u_short)ndeleted < p->daemon.maxerrors) {
    if (0 != (p->cmd & THEN_KILL)) {
      syslog(LOG_INFO, "%s: Could only find %d processes to kill.\n", program_name, ndeleted);
    } else {
      syslog(LOG_INFO, "%s: Could only find %d processes to delete.\n", program_name, ndeleted);
    }
  }
} 

int
nodes_available(process_t *proc)
{
  int i;

  /* If no nodes were specified, a node must be available. */
  if (proc->node == 0) {
    return TRUE;
  }
  else {
    /* If favored node (-F) is up, return TRUE. */
    if (clusternode_avail(proc->node) == 1) {
      return TRUE;
    }
    /* Else check for a backup node that is UP. */
    else {
      for (i = 0; 
           (i < MAX_NODES) && 
           (proc->backup_nodes[i] != 0); 
           i++) 
      {
        if (clusternode_avail(proc->backup_nodes[i]) == 1) {
          return TRUE;
        }
      }
    }
  }

  return FALSE;
} 

void
setup_proc(process_t *daemon, int unpin)
{
  char log[MAXNAME];

  /* This process has inherited ignoring pinning from */
  /* keepalive process.  If the new process shouldn't be */
  /* pinned, unpin it.   */
  if (unpin)
	  (void)do_pin(0);

  /* Compensate for SIGCHLD issue where SIGCHLD is not received if
   * process dies too quickly.
  */
  select(0, NULL, NULL, NULL, &sleep_one_second);

  (void)close(pipefd);
  closelog();
  closeproc();
  setsid();
  chdir("/");
  umask(0);

  close_all_files();

  /* Open stdin. */
  if (stdin != freopen("/", "r", stdin)) {
    syslog(LOG_WARNING, "%s: Cannot open \"/\" for stdin for starting executable '%s'.  Trying to open /dev/null.\n", program_name, daemon->name);

    if (stdin != freopen("/dev/null", "r", stdin)) {
      syslog(LOG_ERR, "%s: Cannot open file for stdin for starting executable '%s'.\n", program_name, daemon->name);
      exit(1);
    }
  }

  /* Open stdout */
  sprintf(log, "%s/%s.%d", KEEPMSGDIR, daemon->basename, getpid());
  if (stdout != freopen(log, "a+", stdout)) {
    syslog(LOG_WARNING, "%s: Cannot open a file for stdout for starting executable '%s'.  Trying to open /dev/null.\n", program_name, daemon->name);

    if (stdout != freopen("/dev/null", "w", stdout)) {
      syslog(LOG_ERR, "%s: Cannot open file for stdout for starting executable '%s'.\n", program_name, daemon->name);
      exit(1);
    }
  }

  /* Open stderr */
  if (stderr != freopen(log, "a+", stderr)) {
    syslog(LOG_WARNING, "%s: Cannot open a file for stderr for starting executable '%s'.  Trying to open /dev/null.\n", program_name, daemon->name);

    if (stderr != freopen("/dev/null", "w", stderr)) {
      syslog(LOG_ERR, "%s: Cannot open file for stderr for starting executable '%s'.\n", program_name, daemon->name);
      exit(1);

    }
  }

  printf("Log for %s:\n\n", daemon->name);
  fflush(stdout);

  errno = 0;
  setgid(daemon->egid);
  setuid(daemon->euid);

#ifdef DEBUG1
time(&sys_time);
ctime_r(&sys_time, time_str);
syslog(LOG_DEBUG, "%s: setup_proc() - before execl() TIME:  %s.\n", program_name, time_str);
#endif

  /* Restore old signal mask. */
  sigprocmask(SIG_SETMASK, &old_signals, NULL);
}

/*                                                                       
 *                 Function spawn()                                    
 *                                                                       
 * Description                                                           
 *                                                                       
 * The spawn() routine spawns a daemon for the first time.
 *                                                                       
 * Returns                                                               
 *                                                                       
 * Nothing                                                               
*/

void
spawn(process_t *proc)
{
  clock_t lbolt;   
  int pid;
  process_t daemon;
  struct timeval sleep_interval = { 0, 0 };
  clusternode_info_t node_info;

#ifdef DEBUG1
time(&sys_time);
ctime_r(&sys_time, time_str);
syslog(LOG_DEBUG, "%s: ENTER spawn()  TIME:  %s.\n", program_name, time_str);
syslog(LOG_DEBUG, "proc->state = %d\n", proc->state);
#endif

  /* If no nodes are available, just return. */
  if (!nodes_available(proc)) {
    syslog(LOG_WARNING, "%s: Process '%s' in slot %d cannot be spawned because all of its nodes are down.  Bring up one of its nodes and it should restart automatically.\n", program_name, proc->name, proc->slot);
    return;
  }

  syslog(LOG_INFO, "%s: Starting executable '%s'.\n", program_name, proc->name);

  /* create input record for child process so that it doesn't need */
  /*  the mmapped file. */
  daemon = *proc;

#ifdef DEBUG1
time(&sys_time);
ctime_r(&sys_time, time_str);
syslog(LOG_DEBUG, "%s: spawn() - before fork() TIME:  %s.\n", program_name, time_str);
#endif

  /* decide where to fork the process */
  pid = fork_daemon_process(proc);

  switch (pid) {
    case 0:  /* child process */
      /* Setup environment variables for special exit codes--if requested. */
      if (daemon.down_exit_code_used) {
        sprintf(downbuf, "KEEPALIVE_PROCESS_DOWN=%ld", (long)daemon.down_exit_code);
        putenv(downbuf);
      }

      if (daemon.reject_exit_code_used) {
        sprintf(rejectbuf,"KEEPALIVE_NODE_REJECT=%ld", (long)daemon.reject_exit_code);
        putenv(rejectbuf);
      }

      setup_proc(&daemon, 
                 (daemon.node == 0) && (daemon.pin_anywhere == FALSE));
      exec_script(daemon.startup_script, daemon.name, daemon.cfg_file);
      break;

    case -1:

      syslog(LOG_ERR, "%s: Trying to spawn %s, fork failed.\n", program_name, proc->name);
      declare_dead(proc, FALSE);

      /* Begin polling since fork()/rfork() has failed.  We will retry the
       * fork()/rfork() during the next pass through the main loop.
      */
      pkctl->polling = TRUE;
      break;

    default:   /* parent process */
#ifdef DEBUG1
  syslog(LOG_DEBUG, "%s: (%d) My child's PID = %d\n", program_name, getpid(), pid);
#endif

      lbolt = get_lbolt();

      LOCK_PROC(*proc);
      proc->last_pid = proc->pid;
      proc->pid = pid;
      proc->original_pid = pid;
      proc->state = ST_OK;
      proc->lastexeced.lbolt_in_sec = lbolt;
      proc->lastexeced.date = get_pid_start(proc->pid);
      proc->status_captured = NO_ZOMBIE_STATUS;
      proc->adoption_status = ADOPTED;
      proc->daemonization_recovery = FALSE;
      UNLOCK_PROC(*proc);
   
      /* Save transid of when the node became UP. */
      clusternode_info(proc->last_node.node_num, sizeof(clusternode_info_t), &node_info);
      if (node_info.node_state == CLUSTERNODE_UP) {
        proc->last_node = node_info;
      }
      else {
        /* The node has become down just as the daemon was started, so we
         * want a transid that is guaranteed to be invalid (to the extent
         * that we will never see it from a clusternode_info() call). 
        */
        proc->last_node.node_lasttransid = (transid_t)0;
      }

      /* REPLACE:  This is a temperary fix to support wait_time for Keepalive
       * process groups.  A better solution is to do something similar to
       * the minrespawn algorithm which does not block the rest of Keepalive
      */ 
      if ((proc->group.name[0] != '\0') && (proc->group.wait_time != 0)) {
        sleep_interval.tv_sec = proc->group.wait_time;
        select(0, NULL, NULL, NULL, &sleep_interval);
      }

      /* Force data to disk. */
      sync_data();

      break;
  }
}

/*                                                                       
 *                 Function respawn()                                    
 *                                                                       
 * Description                                                           
 *                                                                       
 * The respawn() routine checks the minimum time between respawn attempts
 * and the time of the last exec of the process. If it's time to respawn 
 * the daemon it respawns it.                                              
 *                                                                       
 * Interface                                                             
 *                                                                       
 * void respawn(register process_t *proc)                                
 *                                                                      
 * Returns                                                               
 *                                                                       
 * Nothing                                                               
*/

void
respawn(process_t *proc)
{
  char log[MAXNAME];
  clock_t lbolt;   
  time_t lastexec_time;
  int pid;
  process_t daemon;
  clusternode_info_t node_info;

#ifdef DEBUG1
time(&sys_time);
ctime_r(&sys_time, time_str);
syslog(LOG_DEBUG, "%s: ENTER respawn()  TIME:  %s.\n", program_name, time_str);
syslog(LOG_DEBUG, "proc->state = %d\n", proc->state);
#endif

  if ((ST_DEAD == proc->state) || 
      (ST_SHUTDOWN == proc->state) ||
      (ST_RESPAWNING == proc->state)) 
  { 
    /* Remove old log file. */
    sprintf(log, "%s/%s.%d", KEEPMSGDIR, proc->basename, proc->original_pid);
    unlink(log);
  }

  /* Get time since last exec. of daemon instance. */           
  lbolt = get_lbolt();
  lastexec_time = lbolt - proc->lastexeced.lbolt_in_sec;

  /* Needed only while time() is used for lbolt. */
  if (lastexec_time < 0)
    lastexec_time = -lastexec_time;

  /* Verify that minimum respawn time has elapsed. */
  if (lastexec_time < proc->minrespawn)
  {
    /* Find the closest minrespawn time so that we can set the alarm clock. */
    next_alarm = min_alarm(next_alarm, proc->minrespawn - lastexec_time);

    syslog(LOG_ERR, "%s: Process '%s' failed too quickly (pid=%d).  Minimum respawn time has not yet elapsed.  This parameter is specified in the \"minrespawn\" field of the process configuration file.\n", program_name, proc->name, proc->pid); 
    return;
  }
  /* Else if no nodes are available, just return. */
  else if (!nodes_available(proc)) {
    /* Process's node must have failed at this point. */
    proc->failure = NODE_FAILURE;

    syslog(LOG_WARNING, "%s: Process '%s' in slot %d cannot be respawned because all of its nodes are down.  Bring up one of its nodes and it should restart automatically.\n", program_name, proc->name, proc->slot); 
    return;
  }

  syslog(LOG_WARNING, "%s: Trying to invoke replacement for process '%s' (pid = %d).\n", program_name, proc->name, proc->pid);

  /* Try to catch race condition where the node has gone down after a process
   * failure had occurred.  Note:  Process group members shutdown by
   * Keepalive did not fail and so proc->failure will be NO_FAILURE.
  */
  if (proc->failure != NODE_FAILURE) 
  {
    clusternode_info(proc->last_node.node_num, sizeof(clusternode_info_t), 
                     &node_info);
   
    /* Guard against race condition where we declared a process failure, but
     * the node has since then gone down.
    */ 
    if (proc->last_node.node_lasttransid != node_info.node_lasttransid) {
      proc->failure = NODE_FAILURE;
    }
    else {
      proc->failure = PROCESS_FAILURE;
    }
  }

  /* create input record for child process so that it doesn't need */
  /*  the mmapped file. */
  daemon = *proc;

#ifdef DEBUG1
time(&sys_time);
ctime_r(&sys_time, time_str);
syslog(LOG_DEBUG, "%s: respawn() - before fork() TIME:  %s.\n", program_name, time_str);
#endif

  /* decide where to fork the process */
  pid = fork_daemon_process(proc);

  switch (pid) {
    case 0:  /* child process */
      /* Setup environment variables for special exit codes--if requested. */
      if (daemon.down_exit_code_used) {
        sprintf(downbuf, "KEEPALIVE_PROCESS_DOWN=%ld", (long)daemon.down_exit_code);
        putenv(downbuf);
      }

      if (daemon.reject_exit_code_used) {
        sprintf(rejectbuf,"KEEPALIVE_NODE_REJECT=%ld", (long)daemon.reject_exit_code);
        putenv(rejectbuf);
      }

      /* Make last PID available in case script that performs restart
       * operation needs to be able to cleanup things left behind by
       * previous instance.
      */
      sprintf(lastpidbuf, "KEEPALIVE_LAST_PID=%ld", (long)daemon.pid);
      putenv(lastpidbuf);

      setup_proc(&daemon, 
                 (daemon.node == 0) && (daemon.pin_anywhere == FALSE));

      /* If a node failure recovery script was defined and the node failed 
       * perform a node failure recovery, otherwise do a process failure
       * recovery.
      */
      if ((daemon.node_failure_script[0] != '\0') &&
          (daemon.failure == NODE_FAILURE)) 
      {
        /* do node failure recovery */
        exec_script(daemon.node_failure_script, daemon.name, daemon.cfg_file);
      }

      /* Else we don't distinguish between node and process failures. */
      else 
      {
        /* do process failure recovery */
        if (daemon.process_failure_script[0] != '\0') {
          exec_script(daemon.process_failure_script, daemon.name, daemon.cfg_file);
        }
        else {
          exec_script(daemon.startup_script, daemon.name, daemon.cfg_file);
        }
      }

    case -1:

      syslog(LOG_ERR, "%s: Trying to respawn %s, fork failed.\n", program_name, proc->name);
      declare_dead(proc, FALSE);

      /* Begin polling since fork()/rfork() has failed.  We will retry the
       * fork()/rfork() during the next pass through the main loop.
      */
      pkctl->polling = TRUE;
      break;

    default:   /* parent process */
#ifdef DEBUG1
  syslog(LOG_DEBUG, "%s: (%d) My child's PID = %d\n", program_name, getpid(), pid);
#endif

      lbolt = get_lbolt();

      LOCK_PROC(*proc);
      proc->last_pid = proc->pid;
      proc->pid = pid;
      proc->original_pid = pid;
      proc->state = ST_OK;
      proc->lastexeced.lbolt_in_sec = lbolt;
      proc->lastexeced.date = get_pid_start(proc->pid);
      proc->status_captured = NO_ZOMBIE_STATUS;
      proc->adoption_status = ADOPTED;
      proc->daemonization_recovery = FALSE;
      proc->failure = NO_FAILURE;
      UNLOCK_PROC(*proc);

      /* Save transid of when the node became UP. */
      clusternode_info(proc->last_node.node_num, sizeof(clusternode_info_t), 
                       &node_info);
      if (node_info.node_state == CLUSTERNODE_UP) {
        proc->last_node = node_info;
      }
      else {
        /* The node has become down just as the daemon was started. */
        proc->last_node.node_lasttransid = (transid_t)0;
      }

      /* Force data to disk. */
      sync_data();

      break;
  }
}

int
found_backup_node(process_t *proc) 
{ 
  int index;
  int pid = -1;

  /* if any backups were specified */
  if (proc->backup_nodes[0] != 0) {
    index = 0;
    while ((proc->backup_nodes[index] != 0) && (index < MAX_NODES)) {
      if (is_node_available(proc->backup_nodes[index])) {
        syslog(LOG_WARNING, "%s: Attempting to respawn '%s' on backup node %d.\n", program_name, proc->name, proc->backup_nodes[index]);

        proc->last_node.node_num = proc->backup_nodes[index];
        proc->lastexeced_secs = time(NULL);
        pid = rfork(proc->last_node.node_num);
      }

      if (pid == -1) {
        syslog(LOG_ERR, "%s: Backup node %d is down.\n", program_name, proc->backup_nodes[index]);
      }
      else {
        break;
      }
      index++;
    }
  }

  return pid;
}

int
valid_node(process_t *proc, int node)
{
  int i;

  /* UP node list may contain entries that were zeroed, so if we find one
   * return FALSE.
  */
  if (!node) {
    return FALSE;
  }

  /* If no node set was specified, any node is okay. */
  if ((proc->node == 0) && (proc->backup_nodes[0] == 0)) {
    return TRUE;
  }

  /* If favored node equals the target node, the node is okay. */
  if (proc->node == node) {
    return TRUE;
  }

  /* If the target node is in the backup node set, the node is okay. */
  for (i = 0; i < MAX_NODES; i++) {
    if (proc->backup_nodes[i] == node) {
      return TRUE;
    }
  }

  return FALSE;
}

void
reject_last_node(process_t *proc) {
  int i, j;
  clusternode_info_t node_info;

  /* Insert last node in list of rejected nodes. */
  for (i = 0; 
       (i < MAX_NODES) && 
       (proc->rejected_nodes[i].node_num != proc->last_node.node_num); 
       i++) 
  {
    if (proc->rejected_nodes[i].node_num == 0) {
      proc->rejected_nodes[i] = proc->last_node;
      break;
    }
  }

  /* If any of the rejected nodes have been rebooted, delete them from the
   * rejected node list.
  */
  for (i = 0; 
       (i < MAX_NODES) && 
       (proc->rejected_nodes[i].node_num != 0); 
       i++) 
  {
    clusternode_info(proc->rejected_nodes[i].node_num, 
                     sizeof(clusternode_info_t), &node_info);

    /* If node has made a transition from UP since it was rejected,
     * unreject the node.
    */
    if (proc->rejected_nodes[i].node_lasttransid != node_info.node_lasttransid) 
    {
      /* Delete node from rejected node list. */
      for (j = i+1; 
           (j < MAX_NODES) &&
           (proc->rejected_nodes[j].node_num != 0);
           j++)
      {
        proc->rejected_nodes[j - 1] = proc->rejected_nodes[j];
      }
      proc->rejected_nodes[j - 1].node_num = 0;
    }
  }
}

void
get_available_node_set(process_t *proc)
{
  int i, j;
  int remaining_up_nodes;
  int rejections;

RETRY_REJECT_get_available_node_set:

  /* If last node was rejected, put node in rejected node list. */
  if ((proc->restart_policy == ROUND_ROBIN) ||
      ((proc->reject_exit_code_used) && 
       (proc->status_captured == ZOMBIE) &&
       WIFEXITED(proc->status) &&
       (WEXITSTATUS(proc->status) == proc->reject_exit_code)))
  {
    /* Reject last node. */
    reject_last_node(proc);
  }

RETRY_NO_REJECT_get_available_node_set:

  /* Get list of UP nodes. */
  loadNodeTable();

  /* Remove invalid and rejected nodes from UP list. */
  rejections = 0;
  remaining_up_nodes = up_nodes.size;
  for (i = 0; i < up_nodes.size; i++) {
    /* If node is invalid for process, remove it from the UP node list. */
    if (!valid_node(proc, up_nodes.node[i].node_num)) {
      up_nodes.node[i].node_num = 0;
    }
    else {
      /* If node is rejected for process, remove it from the UP node list. */
      for (j = 0; 
           (j < MAX_NODES) && (proc->rejected_nodes[j].node_num != 0); 
           j++) 
      {
        /* If UP node is in our reject node list, remove UP node from list of 
         * available nodes.
        */
        if (up_nodes.node[i].node_num == proc->rejected_nodes[j].node_num)
        {
          up_nodes.node[i].node_num = 0;
          rejections++;
          break;
        }
      }
    }

    /* We have one less UP node that can be used. */
    if (up_nodes.node[i].node_num == 0) {
      remaining_up_nodes--;
    }
  }

  /* If no nodes are available and we have rejected nodes, clear the rejected
   * and visted node lists and try again to get a list of available nodes.
  */
  if ((remaining_up_nodes == 0) && (proc->rejected_nodes[0].node_num != 0)) {
    /* Clear rejected and visited node lists. */
    for (i = 0; i < MAX_NODES; i++) {
      proc->rejected_nodes[i].node_num = 0;
    }

    /* If more than 1 node was rejected, it's safe to reject the last node
     * again.  Otherwise, don't reject the last node since it is the only
     * one available.
    */
    if (rejections > 1) {
      syslog(LOG_WARNING, "%s: No nodes are available to restart executable '%s'.  Ignoring all previous node rejections except for last node rejection.  Use \"spawndaemon -L -v human\" to get detailed information about how the process was registered.\n", program_name, proc->name);

      goto RETRY_REJECT_get_available_node_set;
    }

    syslog(LOG_WARNING, "%s: No nodes are available to restart executable '%s'.  Ignoring last node rejection.  Use \"spawndaemon -L -v human\" to get detailed information about how the process was registered.\n", program_name, proc->name);

    goto RETRY_NO_REJECT_get_available_node_set;
  }

  if (remaining_up_nodes == 0) {
    syslog(LOG_ERR, "%s: No nodes are available to restart executable '%s'.  To recover, bootup more nodes.  Also, use \"spawndaemon -L -v human\" to get detailed information about how the process was registered.\n", program_name, proc->name);
  }
}

int
is_node_available(int node)
{
  int i;

  for (i = 0; i < up_nodes.size; i++) {
    if (up_nodes.node[i].node_num == node) {
      return TRUE;
    }
  }

  return FALSE;
}
                                       
/*
 *                 Function fork_daemon_process()
 *
 * Description
 *
 * This routine selects the right node on which to fork a daemon process
 * and forks a process there.
 *
 * Interface
 *
 * void fork_daemon_process(process_t *proc)
 *
 * Returns
 *
 * PID of new process
*/

pid_t
fork_daemon_process(process_t *proc)
{
  pid_t pid = -1;
  int i = 0;
  static int rrnode = -1;	/* The node on which a process was last
				   spawned by Round Robin policy */

#ifdef DEBUG
  syslog(LOG_DEBUG, "fork_daemon_process: restart_policy = %d\n", proc->restart_policy);
#endif /* DEBUG */

  /* Get list of UP nodes. */
  get_available_node_set(proc);

  /* if the process wants to be restarted on the last node he ran on  */
  /* and we know what the last node is, try to fork there.            */
  if ((proc->restart_policy == LAST_NODE) && (proc->last_node.node_num != 0)) {
    /* Try to restart on last node. */
    if (is_node_available(proc->last_node.node_num)) {
      proc->lastexeced_secs = time(NULL);
      pid = rfork(proc->last_node.node_num);

      if (pid == -1) {
        syslog(LOG_ERR, "%s: Last node %u is down.\n", program_name, proc->last_node.node_num);
      }
    }
  }

  /* Favored node selection. For last node selection, use favored */
  /* node selection if the last node is not available.            */
  if ((proc->restart_policy == F_NODE) ||
	((proc->restart_policy == LAST_NODE) && (pid == -1))) {
    /* if a primary node was specified, try to fork there */
    if (proc->node != 0) {
      if (is_node_available(proc->node)) {
        proc->last_node.node_num = proc->node;
        proc->lastexeced_secs = time(NULL);
        pid = rfork(proc->last_node.node_num);
      }

      /* if the primary isn't there */
      if (pid == -1) {
        syslog(LOG_ERR, "%s: Primary node %d is down or rejected.\n", program_name, proc->node);

        /* try to find a backup node */
        if ((pid=found_backup_node(proc)) == -1) {
          syslog(LOG_ERR, "%s: Backup node not available - cannot restart executable '%s'.\n", program_name, proc->name);
        }
      }
    }
    /* Else primary node was not specified, try any nodes in cluster. */
    else {
      for (i = 0; (i < up_nodes.size) && (pid == -1); i++) {
        if (up_nodes.node[i].node_num != 0) {
          proc->last_node.node_num = up_nodes.node[i].node_num;
          proc->lastexeced_secs = time(NULL);
          pid = rfork(proc->last_node.node_num);
        }
      }
      
      if (pid == -1) {
        syslog(LOG_ERR, "%s: Cannot rfork() to any node in the cluster.\n", program_name);
      }
    }
  }

  /* Round-robin node selection. */
  if (proc->restart_policy == ROUND_ROBIN) {
    if (rrnode != -1) {
      /* Find an UP node next to rrnode. */
      for (i = 0; (i < up_nodes.size) && (pid == -1); i++) {
        if (up_nodes.node[i].node_num > rrnode) {
           proc->last_node.node_num = up_nodes.node[i].node_num;
           proc->lastexeced_secs = time(NULL);
           pid = rfork(proc->last_node.node_num);
           rrnode = proc->last_node.node_num;
        }
      }
    }

    if ((rrnode == -1) || (i == up_nodes.size)) {
      for (i = 0; (i < up_nodes.size) && (pid == -1); i++) {
        /* fork on the first available UP node */
        if (up_nodes.node[i].node_num != 0) {
          proc->last_node.node_num = up_nodes.node[i].node_num;
          proc->lastexeced_secs = time(NULL);
          pid = rfork(proc->last_node.node_num);
          rrnode = proc->last_node.node_num;
        }
      }
    }

    if (pid == -1) {
      syslog(LOG_ERR, "%s: Cannot rfork() to any node in the cluster.\n", program_name);
    }

  }

  if (proc->restart_policy == ROOT_NODE) {
      proc->last_node.node_num = clusternode_num();
			proc->lastexeced_secs = time(NULL); /* store start time */
      pid = fork();
  }

  return(pid);
}

void
close_all_files() {
  struct  rlimit rlim ;
  int n;

  unmap_ka_data();

  /* Close all files. */
  n = getrlimit(RLIMIT_NOFILE, &rlim);
  assert (0 == n);
  for (n = 0; n < rlim.rlim_cur; n++) {
    close(n);
  }
}

int
open_ka_files() {
  int restart = FALSE;

  /* Close all files and reopen because OpenSSI does not yet support 
         * Kernel object migration.
   */
  closelog();
  close_all_files();
  openlog("keepalive", LOG_CONS | LOG_PID | LOG_NOWAIT, LOG_DAEMON);

  /* if there is no file, try to create one */
  if ((data_fd = open(KEEPDATA, O_RDWR, 0664)) == -1) {
    if (errno == ENOENT) {
      if ((data_fd = open(KEEPDATA, O_RDWR | O_CREAT, 0664)) == -1) {
        syslog(LOG_ERR, "%s: Could not create keepalive data file\n", program_name);
        exit(1);
      }
    }
    else {
      syslog(LOG_ERR, "%s: Could not open keepalive data file\n", program_name);
      exit(1);
    }
  }
  /* if there is a file, we're restarting */
  else {
    restart = TRUE;
  }

  return restart;
}

void
get_basename(char *basename, char *name) {
  int i, j;
  char filename[MAXNAME];

  /* Find basename of daemon. */
  j = 0;
  for (i=strlen(name) - 1; (i >= 0) && (name[i] != '/'); i--) {
    filename[j] = name[i];
    j++;
  }
  i = 0;
  for (j--; j >= 0; j--) {
    basename[i] = filename[j];
    i++;
  }
  basename[i] = '\0';

#ifdef DEBUG1
syslog_msg = "%s: basename = %s.\n";
syslog(LOG_DEBUG, syslog_msg, program_name, basename);
#endif
}

void
cleanup_daemon_logs() {
  DIR    *msg_dir;
  struct dirent  *log_file;
  char    cwd[PATH_MAX];

  /* Cleanup old logs. */
  msg_dir = opendir(KEEPMSGDIR);
  if (msg_dir) {
    getcwd(cwd, PATH_MAX);
    if (chdir(KEEPMSGDIR) == 0) {
      /* Throw away "." and "..".  We don't want to unlink them. */
      readdir(msg_dir);
      readdir(msg_dir);

      while ((log_file = readdir(msg_dir))) {
        unlink(log_file->d_name);
      }
      closedir(msg_dir);
      chdir(cwd);
    }
    else {
      syslog(LOG_WARNING, "%s: Can't chdir() to %s.\n", program_name, KEEPMSGDIR);
    }
  }
  else {
    syslog(LOG_WARNING, "%s: Can't read %s to clean up process log files.\n", program_name, KEEPMSGDIR);
  }
}

struct exit_status {
  pid_t pid;
  int status;
};

void
adopt_child(process_t *proc)
{
  int num_pids = 1;
  int is_alive;
  int ret;

  /* Return if process is already adopted. */
  if (proc->adoption_status == ADOPTED)
  {
    return;
  }

  /* Try to adopt child. */
  num_pids = 1;
#ifndef DXX
  ret = _cluster_reclaim_child(proc->pid, proc->lastexeced.date);
  if (ret == -1)
	num_pids = 0;
#else
  num_pids = 0;
#endif
  is_alive = (kill(proc->pid, 0) == 0);

  /* If adoption succeeded, ... */
  if (num_pids == 1) {
    proc->adoption_status = ADOPTED;

    syslog(LOG_INFO, "%s: Acquired '%s' with pid %d as child.\n", program_name, proc->name, proc->pid);
  }
  /* Else adoption failed, ... */
  else {
    /* Make sure message is displayed only once, so we don't fill up syslog. */
    if (proc->adoption_status != FAILED_ADOPT) {
      proc->adoption_status = FAILED_ADOPT;

      syslog(LOG_ERR, "%s: Failed to acquire '%s' with pid %d as child.  Keepalive must now keep polling to watch this process.  Use spawndaemon with options '-k -x -P %d' to correct this problem and discontinue Keepalive polling.\n", program_name, proc->name, proc->pid, proc->pid);
    }

    adoption_problem = TRUE;
    pkctl->unadopted_process = TRUE;
    pkctl->polling = TRUE;
  }

  /* If the process has died before it was adopted, we won't get a SIGCHLD.
   * If the process died this close to adopting it, we may not get a SIGCHLD;
   * child processes that exit immediately after a fork/exec have been
   * demonstrated to NOT send the parent a SIGCHLD, yet inserting a 1 sec.
   * delay before the exit produces a SIGCHLD.  We believe this is a defect
   * in the kernel.  We are trying to work around the problem.
  */
  if (!is_alive) {
    stable_state = FALSE;
  }
}

/*                                                                      
 *                      Function find_missing_proc()                    
 *                                                                      
 * Description                                                          
 *                                                                      
 * The find_missing_proc() routine locates the process according to its 
 * name and id in the /proc directory.                                  
 *                                                                      
 * Interface                                                            
 *                                                                      
 * process_t *find_missing_proc(register char *name, process_t *table,  
 *                                                   int max_table)     
 * Returns                                                              
 *                                                                      
 * The address of the process if the process is found                   
 * NULL otherwise                                                       
*/

void 
find_missing_proc(int slot)
{
  process_t *proc = NULL;
  register char *s;
  int i;
  clock_t lbolt;
  pid_t ppid;
  int found = FALSE;
  int registered;

  if ( (s = strchr(pprocs[slot].name, ' ')))
    *s = '\0';

  /* Determine the number of unregistered instances of daemon that
   * match slot's equivalence class.
  */
  rewindproc();
  while ( (proc = nextproc(&ppid))){
    /* Ignore all processes who do not have init as a parent. */
    if (ppid != 1) {
      continue;
    }
#ifdef DEBUG
    syslog(LOG_DEBUG, "pprocs[slot].name: %s\n", pprocs[slot].name);
    syslog(LOG_DEBUG, "proc->name: %s\n", proc->name);
    syslog(LOG_DEBUG, "pprocs[slot].args: %s\n", pprocs[slot].args);
    syslog(LOG_DEBUG, "proc->args: %s\n", proc->args);
#endif /* DEBUG */

    /* Search for named process. */
    if ((strcmp(pprocs[slot].name, proc->name) == 0) &&
        ((pprocs[slot].registration_policy != REGISTER_BY_ARGS) ||
         (strcmp(pprocs[slot].args, proc->args) == 0)))
    {
      /* If Keepalive is managing an unadopted process, we can't count on
       * the fact that the process has a parent of init to mean that it
       * isn't already registered, so we must check the hard way.
      */
      registered = FALSE;
      if (pkctl->unadopted_process) {
        for(i = 0; i < pkctl->registered_daemons; i++) {
          if (pprocs[i].pid == proc->pid) {
            registered = TRUE;
            break;
          }
        }
      }

      /* If process matches slot, take it.
       *
       * Note:  We might catch an orphan left behind by a dead daemon
       *        instance here.  We don't have a solid algorithm for defending
       *        against this possiblity at this point.  We are counting
       *        on the orphans to go away.  If we grab an orphan and it
       *        goes away a short time later, that's okay.  Keepalive will
       *        perform daemonization recovery, again, and discover that the
       *        daemon must be restarted.
      */
      if (!registered && matches_slot_config(&pprocs[slot], proc->pid)) {
        found = TRUE;
        break;
      }
    }
  }

  /* If there are slots equal to available daemon instances, 
   * then match slots with daemons.  Otherwise, an instance has died
   * or has had children. 
  */
  if (found) {
    lbolt = get_lbolt();

    pprocs[slot].last_pid = pprocs[slot].pid;
    pprocs[slot].pid = proc->pid;

    /* Can't convert exec time from /proc into lbolt value, so
     * start the minrespawn clock now.
    */
    pprocs[slot].lastexeced.lbolt_in_sec = lbolt;

    pprocs[slot].lastexeced.date = proc->lastexeced.date;
    pprocs[slot].state = ST_OK;
    pprocs[slot].adoption_status = NOT_ADOPTED;
    pprocs[slot].daemonization_recovery = TRUE;
    pprocs[slot].failure = NO_FAILURE;

    /* Adopt daemon as child of Keepalive. */
    adopt_child(&pprocs[slot]);
    pprocs[slot].status_captured = NO_ZOMBIE_STATUS;
  }

  /* If no instances of the daemon are available, the daemon has died and
   * needs to be restarted.
  */
  else {
    declare_dead(&pprocs[slot], TRUE);
  }
}

void
exec_script(char *script, char *daemon, char *daemon_cfg_file) {
  if( access(script, X_OK) < 0 ) {
    syslog(LOG_ERR, "%s: Script '%s' cannot be executed on behalf of process '%s'.  Check the process configuration file '%s'.\n", program_name, script, daemon, daemon_cfg_file);
    exit(1);
  }

  syslog(LOG_INFO, "%s: Executing script '%s' on behalf of process '%s'.\n", program_name, script, daemon);
  execl(script, script, NULL);

  syslog(LOG_ERR, "%s: '%s' exec failed, errno is %d\n", program_name, script, errno);
  exit(1); 
}

void
loadNodeTable(void)
{
  static clusternode_t *nodeTab = NULL;
  int new_max_nodes;
  transid_t transid;
  int i;

  /* Allocate memory only when needed. */
  new_max_nodes = cluster_maxnodes();
  if (max_nodes != new_max_nodes) {
    max_nodes = new_max_nodes;

    if (nodeTab != NULL) {
      free((void *)nodeTab);
    }
    nodeTab = (clusternode_t *)calloc(max_nodes, sizeof(clusternode_t));
 
    if (up_nodes.node != NULL) {
      free((void *)up_nodes.node);
    }
    up_nodes.node = (clusternode_info_t *)calloc(max_nodes, sizeof(clusternode_info_t));

    /* If we had a memory allocation problem, deallocate everything and return
     * -1 to the caller.
    */
    if ((nodeTab == NULL) || (up_nodes.node == NULL)) {
      goto loadNodeTable_deallocate;
    }
  }

  /* Load the node table into allocated array. */
  errno = 0;
  up_nodes.size = cluster_membership(&transid, max_nodes, nodeTab);
  if (up_nodes.size <= 0)
  {
    goto loadNodeTable_deallocate;
  }

  /* Load UP node table and guard against race condition where node has gone
   * down before we could get the node info.
  */
  for (i = 0; i < up_nodes.size; i++) {
    errno = 0;
    if (clusternode_info(nodeTab[i], 
                         sizeof(clusternode_info_t), 
                         &up_nodes.node[i]) < 0)
    {
      syslog(LOG_WARNING, "%s: Could not get node information for node %u.  errno = %d\n", program_name, nodeTab[i], errno);
      
      /* Set node id. to invalid value. */
      up_nodes.node[i].node_num = 0;
    }

    /* If node has gone down before we could get the node info., take it out
     * of the UP node list.
    */
    if (up_nodes.node[i].node_state != CLUSTERNODE_UP) {
      /* Set node id. to invalid value. */
      up_nodes.node[i].node_num = 0;
    }
  }

  return;         	

/* Handle exceptions. */
loadNodeTable_deallocate:

  if (nodeTab != NULL) {
    free((void *)nodeTab);
    nodeTab = NULL;
  }

  if (up_nodes.node != NULL) {
    free((void *)up_nodes.node);
    up_nodes.node = NULL;
    up_nodes.size = 0;
  }

  max_nodes = 0;
  return;
}

/*
 * rfork_anywhere() 
 * Modified to return start time of the process in the argument 
 * start_time
 */ 
pid_t
rfork_anywhere( time_t *start_time ) {
  int i;
  pid_t pid = -1;

  /* Load table of UP nodes. */
  loadNodeTable();

  /* Try to rfork() to an UP node. */
  for (i = 0; (i < up_nodes.size) && (pid < 0); i++) {
    *start_time = time(NULL);	
    pid = rfork(up_nodes.node[i].node_num);
  }

  /* If we couldn't rfork() to an UP node, just fork() to the local node. */
  if (pid < 0) {
    *start_time = time(NULL);	
    pid = fork();
  }

  /* Display warning message if we can't fork a process anywhere. */
  if (pid < 0) {
    syslog(LOG_WARNING, "%s: Cannot fork() or rfork() to any node in the cluster.\n", program_name);
  }

  /* If any errors have occurred, pid will still be -1. */
  return pid; 
}

void
declare_down(process_t *proc) {
  pid_t pid = -1;
  clusternode_info_t node_info;
  process_t daemon;

  proc->state = ST_DOWN;

  /* If this process was a group member, take down entire group. */
  if (proc->group.name[0] != '\0') {
    kill_all_daemons(proc->group.name, ST_DOWN);
  }

  /* Execute down script for downed process. */
  if (proc->down_script[0] != '\0') {
    clusternode_info(proc->last_node.node_num, sizeof(clusternode_info_t), 
                     &node_info);

    /* create input record for child process so that it doesn't need */
    /* the mmapped file. */
    daemon = *proc;
   
    /* Guard against race condition where node has gone down and come back up. 
    */ 
    if (daemon.last_node.node_lasttransid == node_info.node_lasttransid) {
			daemon.lastexeced_secs = time(NULL); /* store start time of process */
      pid = rfork(daemon.last_node.node_num);
    }

    /* If the node on which the process was running has failed and our
     * down script policy specifies that we should find another node to
     * run the down script on, then do so. 
    */
    if ((pid == -1) && (daemon.down_script_policy != 0)) {
			time_t start_time;	

      syslog(LOG_INFO, "%s: Last node %u is down.  Trying to run down script on another node.\n", program_name, daemon.last_node.node_num);

      /* Try to find another node to run the down script on. */
      pid = rfork_anywhere(&start_time);
		daemon.lastexeced_secs = start_time; /* store start time */
    }

    /* Child process. */
    if (pid == 0) {
      sprintf(lastpidbuf, "KEEPALIVE_LAST_PID=%ld", (long)daemon.pid);
      putenv(lastpidbuf);

      setup_proc(&daemon, FALSE);
      exec_script(daemon.down_script, daemon.name, daemon.cfg_file);
    }
  }
}

int
insert_zombie(const zombie_info_t * const zombie) {
  if (is_zombie_list_full()) {
    return -1;
  }

  zombie_list.zombie[zombie_list.bottom] = *zombie;
  zombie_list.bottom++;

  if (is_zombie_list_full()) {
    return 0;
  }
  else {
    return 1;
  }
}

int
serve_zombie(pid_t pid, zombie_info_t *zombie) {
  int i;

  if (!is_zombie_list_empty()) {
    for (i = 0; i < zombie_list.bottom; i++) 
    {
      if (pid == zombie_list.zombie[i].pid) {
        *zombie = zombie_list.zombie[i];
        zombie_list.zombie[i].pid = 0;
        zombie_list.zombie[i].waitpid_info = 0;
        return TRUE;
      }
    }
  }

  /* If zombie is not in zombie list, try to get zombie.  Note that this
   * blocking call to waitpid() is only safe because we verify that the
   * process is dead before making the call.
  */
  if (kill(pid, 0) != 0) {
    zombie->pid = waitpid(pid, &zombie->waitpid_info, 0);

    /* If waitpid() was successful at finding the zombie, ... */
    if (zombie->pid == pid) {
      return TRUE;
    }
  }

  return FALSE;
}

int
is_zombie_list_full() {
  if (zombie_list.bottom == MAX_ZOMBIES) {
    return TRUE;
  }

  return FALSE;
}

int
is_zombie_list_empty() {
  if (zombie_list.bottom == 0) {
    return TRUE;
  }

  return FALSE;
}

void
make_zombie_list_empty() {
  zombie_list.bottom = 0;
}

clock_t
min_alarm(clock_t time1, clock_t time2) {
  /* Zero is not a minimum time.  It means no alarm clock is to be used, so
   * if one of the values is zero, return the other.
  */
  if (time1 == 0) {
    return time2;
  }
  
  /* Zero is not a minimum time.  It means no alarm clock is to be used, so
   * if one of the values is zero, return the other.
  */
  if (time2 == 0) {
    return time1;
  }

  /* Find the closest minrespawn time so that we can set the alarm clock. */
  if (time1 < time2) {
    return time1;
  }
  else {
    return time2;
  }
}

void
declare_dead(process_t *proc, int may_fault_daemon)
{
  proc->state = ST_DEAD;

  /* If we are told process can't be at fault return */
  if (!may_fault_daemon)
	return;

  /* If restarting, don't fault process */
  if (restart)
	return;

  /* If process was signalled with SIGKILL, don't fault process.
   * Either the process failed due to node down, or someone sent it
   * the signal.
   */
  if ((proc->status_captured == ZOMBIE) &&
    WIFSIGNALED(proc->status) && (WTERMSIG(proc->status) == SIGKILL))
	return;

  if (proc->status_captured != ZOMBIE)
	syslog(LOG_ERR, "%s: Process '%s' (pid %d) no status %d\n",
		program_name, proc->name, proc->pid, proc->status_captured);

  syslog(LOG_ERR, "%s: Process '%s' (pid = %d) died.\n", program_name, proc->name, proc->pid);

  /* Process has encountered a fault. */
  proc->nerrors += 1;
  proc->total_errors += 1;
}

void dummy_handler(int sig)
{
	/* nothing at all */
}
