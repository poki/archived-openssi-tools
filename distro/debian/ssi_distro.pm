#
# Date   :     April 02 2003
# Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
#
#
#
#      This program is free software; you can redistribute it and/or
#      modify it under the terms of the GNU General Public License as
#      published by the Free Software Foundation; either version 2 of
#      the License, or (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
#      or NON INFRINGEMENT.  See the GNU General Public License for more
#      details.

package ssi_distro; 
use strict;

use Net::Ping;
use Socket;

# Do NOT use the ssi_mgmt module
use lib "/cluster/lib";
use Clustertab;
use ssi_arch;
use ssi_utils;

our $distro;
our $bootloaderconf;
our $varlogfile;

BEGIN {
	$distro = "debian";
	if ( $arch =~ /i386/ ) {
		$bootloaderconf = "/boot/grub/menu.lst"
	} elsif ( $arch =~ /alpha/ ) {
		$bootloaderconf = "/etc/aboot.conf"
	}
	$varlogfile = "/var/log/daemon.log";

	our @ISA = qw(Exporter);
	our @EXPORT = qw(
			$distro
			$bootloaderconf
			$varlogfile 

			prompt_hwaddr
			prompt_ipaddr

			distro_create_prompt
			distro_create_write
			distro_addnode_prompt
			distro_addnode_write

			rebuild_boot_materials
			);
}

sub get_interface {

	my $macaddr = shift;
	my $interfaces = read_ifconfig();
	my $interface;

	foreach $interface (@$interfaces) {
		if ($interface->{HWADDR} eq $macaddr) {
			return $interface->{NAME};
		}
	}
}
sub disable_interface {

	my $interface = shift;
	my $interface_conf = shift;
	my $tmp_file = "$interface_conf".".temp";
	my $line;
	my @line_entry;

	open INF_CONF, "< $interface_conf" or die "Cannot open $interface_conf";
	open TMPINF_CONF, "> $tmp_file" or die "Cannot open $tmp_file";
	while ( ! eof(INF_CONF)) {

		$line = <INF_CONF>;
		if ( $line =~ /^auto/ ) {
			# This line indicate all interfaces configured by
			# default. We don't want ICS interface to be configured
			# via distro specific network script
			if ($line =~ /$interface/ ) {
				@line_entry = split(/[\t ]+/ ,$line);
				if (@line_entry == 2 ) {
					#This line correspond to only this interface
					print TMPINF_CONF "#$line";
					next;
				} 
				$line =~ s/$interface//;
			} 
		}
		print TMPINF_CONF "$line";
	}
	close INF_CONF;
	close TMPINF_CONF;
	rename($tmp_file, $interface_conf);
}
sub reset_interface {
	my $interface_conf = shift;
	open INF_CONF, "> $interface_conf" or die "Can't open $interface_conf";
	print INF_CONF
'# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).
#
# This entry denotes the loopback (127.0.0.1) interface.
auto lo
iface lo inet loopback
';
	close INF_CONF;
}


sub create_node_cdsl {
	my $record = shift;
	my $nodenum = $record->{NODE};

	if ( ! -l "/etc/network" 	||
		! -l "/var/run"		||
		! -l "/var/lock"	||
		! -l "/var/log"		||
		! -l "/etc/nodename" 	||
		! -l "/var/lib/urandom" ||
		! -l "/etc/blkid.tab"

		) {

		print "You should run ssi-create first\n";
		exit(1);
	}

# devfs creates /cluster/node$nodenum/dev directory when a node joins
# the cluster.
# This search string should verify whether the node CDSLs have been created
# not just the directory.
	if ( -d "/cluster/node".$nodenum."/etc" ) {
		#Node specific directory already exist
		return
	}

	print "Creating /cluster/node".$nodenum."\n";
	`mkdir -p /cluster/node$nodenum`;
	`cp -a /cluster/nodetemplate/.  /cluster/node$nodenum`;


#	print "Creating /cluster/node".$nodenum."/tmp\n";
#	`mkdir  -p "/cluster/node"$nodenum"/tmp"`;
#	# set the sticky bit and 777 for /tmp
#	# perl function was not working correcly
#	`chmod 1777 "/cluster/node"$nodenum"/tmp"`;

}

sub create_init_node {

	my $record = shift;
	my $initnode = $record->{NODE};


	# Remove the ICS interface before we create init node configuration
	my $interface = get_interface($record->{HWADDR});
	disable_interface($interface, "/cluster/nodetemplate/etc/network/interfaces") if $interface;

	#Now create cdsl targets for INIT node
	create_node_cdsl($record);

	# Now remove the network inteface information from the 
	# template file We don't want other nodes to have the initnode
	# configuration
	reset_interface("/cluster/nodetemplate/etc/network/interfaces");

	print "Making symlink /cluster/node{nodenum} for compatibility\n";
	unlink('/cluster/node{nodenum}');
	symlink("/cluster/node$initnode", '/cluster/node{nodenum}');
}

#Also ran during package upgrade. 
#Should not take any arguments
sub create_cdsl {

	system("mkdir -p /cluster/dev/pts");
	system("mkdir -p /cluster/init");

	# -d follows links  using "! -l" instead of "-d"
	# SSI_XXX we can move some of these command to perl func
	#
	#Here i prepare the template if template is already 
	#installed by any other packages just move the base files
	#to the template
	

	if (! -d "/cluster/nodetemplate" ) {
		print "Making /cluster/nodetemplate for ssi-addnode\n";
			`mkdir -p /cluster/nodetemplate`;

	}
	# This is needed to make the mkgloabl works when
	# creating a fresh cluster
	unless (-l '/cluster/node{nodenum}') {
		symlink("/cluster/nodetemplate", '/cluster/node{nodenum}');
	}

	# Create /cluster/var/lock/subsys
	system("mkdir -p /cluster/var/lock/subsys");

	if (! -d "/cluster/nodetemplate/dev" ) {
		system("mkdir -p /cluster/nodetemplate/dev");
		system("cd /cluster/nodetemplate/dev; /sbin/MAKEDEV console");
	}

	#If /etc/nodename is not a CDSL copy /etc/hostname to /etc/nodename
	if ( ! -l "/etc/nodename" ) {
		system("cp -a /etc/hostname /etc/nodename");
	}

	if ( -l "/etc/network/run" ) {
		# make /etc/network/run a directory 
		# instead of clusterwide symlink
		system("rm /etc/network/run");
		system("mkdir /etc/network/run");
	}
	if ( ! -l "/etc/network" ) {
		mklocalfile  "/etc/network";
 	}
	if ( ! -l "/etc/nodename" ){
		mklocalfile  "/etc/nodename";
	}
	if ( ! -l  "/var/run" ) {
		mklocalfile  "/var/run";
	}
	if ( ! -l "/var/run/utmp" ) {
		mkglobalfile "/var/run/utmp";
	}
	if ( ! -l "/var/run/sudo" ) {
		mkglobalfile "/var/run/sudo";
	}
	if ( ! -l  "/var/log" ) {
		mklocalfile  "/var/log";
	}
	if ( ! -l "/var/log/wtmp" ) {
		mkglobalfile "/var/log/wtmp";
	}
	if ( ! -l "/var/log/lastlog" ) {
		mkglobalfile "/var/log/lastlog";
	}
	if ( ! -l  "/var/lock" ) {
		mklocalfile  "/var/lock";
	}
	if ( ! -l  "/var/lib/urandom" ) {
		mklocalfile  "/var/lib/urandom";
	}
	if ( ! -l  "/etc/blkid.tab" ) {
		if (! -f "/etc/blkid.tab") {
			system ("/sbin/blkid >/dev/null");
		}
		mklocalfile  "/etc/blkid.tab";
	}

}

sub install_mod_rlfiles {
	my $record = shift;
	my $nfs_conf = $record->{NFS};

}
sub install_dep_packages {
	my $record = shift;
	my $nfs_conf = $record->{NFS};

}

sub help_failover {
	print "
Do you want this node to be a root failover node? It _must_
have access to the root filesystem on a shared disk in order 
to answer yes. If you answer yes, then this node can boot 
first as a root node, so you should configure it with a local 
boot device. This is done after this node joins the cluster 
and is described in the installation instructions.
";
}

sub prompt_failover {
	my $nodenum = shift;
	my $verbose = shift;

	my ($rootdev, $type, $opts) = get_fstabentry("/etc/fstab","/");
	my $input_line;
	my @line_entry;

	die "Root filesystem cannot be found\n" unless $rootdev;

	# Force no failover question since root is GFS
	# This could default to 1, and later require boot device
	# Could check for all parallel types in cfstab
	return 0 if $type eq 'gfs';

	# Could add check that type is in cfs type in cfstab

	# Could add check that device is UUID=

	# If root failover not enabled don't ask
	# Could parse this better
	if (  $opts !~ /chard/ ) {
		return 0;
	}
	
	help_failover if $verbose;
	print "\n";
PROMPT: print "Enable root filesystem failover to this node (y/n/?) [n]: ";
	my $selection = uc trim <STDIN>;
	do { help_failover; print"\n"; goto PROMPT } if $selection eq '?';
	$selection = "N" if $selection eq '';
	goto PROMPT unless $selection =~ /^[NY]$/;
	if ( $selection =~ /^Y$/) {
		return 1;
	}
	return 0;
}

sub parselog {
	my $logfile = shift;
	my $records = shift;
	my @loghits;

	open LOG, "< $logfile" or die "Cannot open $logfile: $!\n";
LINE:	while (<LOG>) {
		my @line = split /\s+/, $_;
		next unless $line[9];
		next unless $line[4] eq 'dhcpd:' and $line[5] eq 'DHCPDISCOVER';

		my %hit;
		$hit{TIME} = join ' ', @line[0..2];
		$hit{HWADDR} = uc $line[7];
		next unless validate_hwaddr($hit{HWADDR}, $records);
		foreach (@loghits) {
			if ($hit{HWADDR} eq $_->{HWADDR}) {
				$_->{TIME} = $hit{TIME};
				next LINE;
			}
		}
		unshift @loghits, \%hit;
	}
	close LOG;
	return \@loghits;
}

sub help_hwaddr {
	print "
The list shows all unknown hardware addresses that have 
recently probed the cluster's DHCP server. Choose the one 
that is displayed on the console of the new node when it 
attempts to network boot. If the desired NIC is not listed, 
make sure the node has attempted to network boot on the 
cluster interconnect, then select (R)escan.
";
}

sub prompt_hwaddr {
	my $records = shift;
	my $logfile = shift;
	my $verbose = shift;

	print "
Select the hardware address of the new node's cluster 
interconnect NIC.
";

	help_hwaddr if $verbose;
RESCAN:	my $loghits = parselog $logfile, $records;

	print "\n";
	print "\tHardware address\tTime last probed\n";
	print "\t----------------\t----------------\n";

	unless (@$loghits) {
		print "\tnothing found\n";
		exit(1);
	}

	my $ctr = 0;
	foreach (@$loghits) {
		$ctr++;
		print "  $ctr)\t$_->{HWADDR}\t$_->{TIME}\n";
	}
	print "\n";

PROMPT:	print "Select (1";
	print "-$ctr" if $ctr > 1;
	print "), (r)escan, (q)uit or (?) [1]: ";

	my $selection = uc trim <STDIN>;
	do { help_hwaddr; goto RESCAN } if $selection eq '?';
	goto RESCAN if $selection eq 'R';
	exit(0) if $selection eq 'Q';
	$selection = 1 if $selection eq '';
	goto PROMPT unless $selection =~ /^\d+$/;
	goto PROMPT unless $selection > 0 && $selection <= $ctr;

	return $loghits->[$selection - 1]{HWADDR};
}

our $pingproto = 'icmp';

sub help_ipaddr_scan {
	print "
The IP address must be unique and it must be on the same 
subnet as the cluster interconnect NICs for the other 
nodes. If you want, this program can scan the subnet for 
IP addresses that seem to be available. It does this by 
pinging every address in the subnet, which can take awhile 
for larger subnets. You can safely skip this feature if you 
know one or more available IP addresses.
";
}

sub help_ipaddr {
	print "
The IP address must be unique and it must be on the same 
subnet as the cluster interconnect NICs for the other 
nodes.
";
}

sub prompt_ipaddr {
	my $records = shift;
	my $verbose = shift;

	my $data = $records->{DATA};
	my (@list, %taken);
	foreach (@$data) {
		next if $_->{IPADDR} eq '<DHCP>';
		my $value = ip2num $_->{IPADDR};
		push @list, $value;
		$taken{$value} = 1;
	}

	my $interface = match_ifconfig($records);
	my $net = ip2num $interface->{NETADDR};
	my $mask = ip2num $interface->{NETMASK};
	my $min = $net + 1;
	my $max = $net + ($mask ^ ip2num '255.255.255.255') - 1;

	print "\nEnter an IP address for the NIC.\n";
	help_ipaddr_scan if $verbose;
	print "\n";
SCAN:	print "Do you want to scan for available IP addresses (y/n/?) [n]: ";
	my $selection = uc trim <STDIN>;
	do { help_ipaddr_scan; print "\n"; goto SCAN } if $selection eq '?';
	$selection = "N" if $selection eq '';
	goto SCAN unless $selection =~ /^[NY]$/;
	my $default = ($selection eq 'Y') ? 1 : '';
	do { print "\n"; goto PROMPT } unless $default;

	$| = 1;
	my $setstring = "\r\t\t\t";
	my $ping = Net::Ping->new($pingproto, 0.005);
	print "Scanning network:$setstring";
	foreach (0..($max-$min)) {
		my $cur = $min + $_;
		my $addr = num2ip $cur;
		print "$addr        $setstring";
		next unless $ping->ping($addr);
		push @list, $cur;
		$taken{$cur} = 1;
	}
	print "\r";
	$| = 0;

	my $string;
	($string, $default) = make_ipstring(\@list, $min, $max);
	unless ($string) {
		print "Sorry, there are no available IP addresses to add a " .
			"new node.\n";
		exit(1);
	}
	print "The following IP addresses seem to be available:\n\n";
	print "$string\n\n";

PROMPT:	print "Enter an IP address or (?)";
	print " [$default]" if $default;
	print ": ";

	$selection = trim <STDIN>;
	do { help_ipaddr; print "\n"; goto PROMPT } if $selection eq '?';
	$selection = $default if $selection eq '';
	goto PROMPT unless validate_ipaddr($selection, $records);
	return $selection if $selection eq '<DHCP>';
	$selection = ip2num $selection;
	goto PROMPT unless $selection >= $min && $selection <= $max;
	goto PROMPT if $taken{$selection};

	return num2ip $selection;
}

sub check_node_interface {
	my $record = shift;
	my $nodenum = $record->{NODE};
	my $ipaddr  = $record->{IPADDR};
	my $input_line;

	# FIXME !!! Make sure we don't have cluster interconnect interface
	# being configured via /etc/network/interfaces
	print "
Please make sure that the interface with MAC address $record->{HWADDR}
is not configured via /etc/network/interfaces

Press Enter to acknowledge:";
        <STDIN>;

}

sub munge_inittab {
	open OLDTAB, "< /etc/inittab" 
		or die "Cannot read /etc/inittab: $!\n";
	open NEWTAB, "> /etc/inittab.ssinew" 
		or die "Cannot create /etc/inittab.ssinew: $!\n";

	print "Generating SSI-enhanced /etc/inittab\n";
	while (<OLDTAB>) {
		# skip comments
		if (/^\s*#/) {
			print NEWTAB $_;
			next;
		}

		# parse line into fields
		my @fields = split /:/, $_;
		foreach (0..3) {
			$fields[$_] = '' unless defined $fields[$_];
		}

		# skip SSI-specific lines
		next if ($fields[0] eq 'siR') or
			($fields[0] =~ /^l[1-5]U$/) or
			($fields[0] =~ /^l[1-5]D$/);

		# add sysrecover line
		if ( ($fields[2] eq 'sysinit') and 
				($fields[3] =~ /\/rcS/) ) {
			print NEWTAB $_;
			print NEWTAB 
'siR:R:sysinit:/etc/init.d/rc.sysrecover < /dev/console > /dev/console 2>&1
';
			next;
		}

		# add nodeup and nodedown lines
		if ( ($fields[1] eq '3') and
				($fields[3] =~ /\/rc\s+3\D/) ) {
			print NEWTAB $_;
			print NEWTAB 
'l3U:S12345U:once:/etc/init.d/rc.nodeup %n < /dev/console > /dev/console 2>&1
l3D:S12345D:once:/etc/init.d/rc.nodedown %n
';
			next;
		}

		# edit getty lines
		if ( ($fields[2] eq 'respawn') and ($fields[3] =~ /getty/) ) {
			# add U for nodeup
			$fields[1] .= 'U' unless $fields[1] =~ /U$/;
			print NEWTAB join(':', @fields);
			next;
		}

		# leave everything else alone
		print NEWTAB $_;
	}
	close OLDTAB;
	close NEWTAB;

	unless (-f '/etc/inittab.ssisave') {
		print "Saving base /etc/inittab as /etc/inittab.ssisave\n";
		rename '/etc/inittab', '/etc/inittab.ssisave';
	}
	rename '/etc/inittab.ssinew', '/etc/inittab';
}
sub distro_create_prompt {
	my $record = shift;
	my $node = $record->{NODE};
}

sub distro_create_write {
	my $record = shift;

	create_cdsl();
	create_init_node($record);
	install_mod_rlfiles($record);
	install_dep_packages($record);

	# Move some files aside
	if (-f '/etc/dhcpd.conf' and not -f '/etc/dhcpd.proto') {
		print "Moving /etc/dhcpd.conf to /etc/dhcpd.proto\n";
		rename '/etc/dhcpd.conf', '/etc/dhcpd.proto';
	}
	if (-f '/etc/dhcp3/dhcpd.conf' and not -f '/etc/dhcp3/dhcpd.proto') {
		print "Moving /etc/dhcp3/dhcpd.conf to /etc/dhcp3/dhcpd.proto\n";
		rename '/etc/dhcp3/dhcpd.conf', '/etc/dhcp3/dhcpd.proto';
	}
	if (-f '/etc/securetty') {
		print "Saving /etc/securetty as /etc/securetty.ssisave\n";
		rename '/etc/securetty', '/etc/securetty.ssisave';
	}

	# Generate SSI inittab
	munge_inittab();
}

sub distro_addnode_prompt {
	my $record = shift;
	my $verbose = shift;

	my $node = $record->{NODE};
	my $initnode = $record->{MASTER};


	$record->{MASTER} = prompt_failover($node, $verbose)
		unless defined $initnode and $initnode ne '';

	check_node_interface($record);

}

sub distro_addnode_write {
	my $record = shift;

	my $node = $record->{NODE};
	my $name  = $record->{NODENAME};

	create_node_cdsl($record);

}

sub rebuild_boot_materials {
	my $clustertab = shift;
	my $bootloader = shift;
	my $initrds    = shift;
	my $initrd_filename ;

	# Return if we don't find any initrds
	return 0 unless $initrds;  

	foreach $initrd_filename (@$initrds) {
		if( $initrd_filename eq "" ) {
			print '
		
No  initial ramdisk for  OpenSSI kernel specified

IF you already have the ramdisk installed. You can rebuild the ramdisk using
# mkinitrd   -t <initrd-name> 

Steps for  building  initial ramdisk for the first time.
# mkinitrd -r <root-device>   -o <initrd-name> <module-dir>
# cp <initrd-name> <destination>
';
			return 0;
		}

		system("/usr/sbin/mkinitrd  -t $initrd_filename ");
	} # for loop 
	system("/sbin/ssi-ksync -c $bootloader -t $clustertab") == 0 or
		print "The ssi-ksync command failed to sync your " .
			"local boot devices.\n";
	return 1;

}

1;
