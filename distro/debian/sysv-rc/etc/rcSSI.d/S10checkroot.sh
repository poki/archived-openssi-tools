#
# checkroot.sh	Check to root file system.
#
# Version:	@(#)checkroot.sh  2.85-7  22-Jul-2003  miquels@cistron.nl
#

SULOGIN=no
VERBOSE=yes
[ -f /etc/default/rcS ] && . /etc/default/rcS

PATH=/lib/init:/bin:/sbin

#
#	Helper: is a directory writable ?
#
dir_writable () {
	if [ -d "$1/" ] && [ -w "$1/" ] && touch -a "$1/" 2>/dev/null
	then
		return 0
	fi
	return 1
}

#
#	Set SULOGIN in /etc/default/rcS to yes if you want a sulogin to
#	be spawned from this script *before anything else* with a timeout,
#	like sysv does.
#
[ "$SULOGIN" = yes ] && sulogin -t 30 $CONSOLE

KERNEL=`uname -s`
RELEASE=`uname -r`
MACHINE=`uname -m`

#
#	Ensure that bdflush (update) is running before any major I/O is
#	performed (the following fsck is a good example of such activity :).
#	Only needed for kernels < 2.4.
#
if [ -x /sbin/update ] && [ "$KERNEL" = Linux ]
then
	case "$RELEASE" in
		0.*|1.*|2.[0123].*)
			update
		;;
	esac
fi

# SSI_XXXX we use this stage only to activate swap
#
# Activate the swap device(s) in /etc/fstab. This needs to be done
# before fsck, since fsck can be quite memory-hungry.
#
doswap=no
case "${KERNEL}:${RELEASE}" in
	Linux:2.[0123].*)
		if [ $swap_on_md = yes ] && grep -qs resync /proc/mdstat
		then
			[ "$VERBOSE" != no ] &&
			  echo "Not activating swap - RAID array resyncing"
		else
			doswap=yes
		fi
		;;
	*)
		doswap=yes
		;;
esac
if [ "$doswap" = yes ]
then
	[ "$VERBOSE" != no ] && echo "Activating swap."
	swapon -a 2> /dev/null
fi

: exit 0

