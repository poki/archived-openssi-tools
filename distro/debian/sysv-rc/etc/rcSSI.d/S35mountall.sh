#
# mountall.sh	Mount all filesystems.
#
# Version:	@(#)mountall.sh  2.85-23  29-Jul-2004  miquels@cistron.nl
#
VERBOSE=yes
TMPTIME=0
[ -f /etc/default/rcS ] && . /etc/default/rcS
. /etc/init.d/bootclean.sh

#SSI discover the mount points
cmount 

#SSI_XX create /dev/Nodenumber link. 
if [ -L /dev ]; then
	for N1 in `cluster` ; do
		for N2 in `cluster` ; do
			if [ ! -L /cluster/node$N1/dev/$N2 ]; then
				ln -s /cluster/node$N2/dev \
				/cluster/node$N1/dev/$N2
			fi
		done
	done
	[ -d /dev/pts ] && rmdir /dev/pts
	ln -s /cluster/dev/pts /dev/pts
else
	for N1 in `cluster` ; do
		if [ ! -L /dev/$N1 ]; then
			ln -s /dev /dev/$N1
		fi
	done
fi

#
# Mount local file systems in /etc/fstab.
#
[ "$VERBOSE" != no ] && echo "Mounting local filesystems..."
mount -av -t nonfs,nonfs4,nosmbfs,nocifs,noncp,noncpfs,nocoda -O nochard 2>&1 |
	egrep -v '(already|nothing was) mounted'

case `uname -s` in
	*FreeBSD)
		INITCTL=/etc/.initctl
		;;
	*)
		INITCTL=/dev/initctl
		;;
esac

#
# We might have mounted something over /dev, see if /dev/initctl is there.
#
if [ ! -p $INITCTL ]
then
	rm -f $INITCTL
	mknod -m 600 $INITCTL p
fi
kill -USR1 1

#
# Execute swapon command again, in case we want to swap to
# a file on a now mounted filesystem.
#
doswap=yes
case "`uname -sr`" in
	Linux\ 2.[0123].*)
		if grep -qs resync /proc/mdstat
		then
			doswap=no
		fi
		;;
esac
if [ $doswap = yes ]
then
	swapon -a 2> /dev/null
fi

: exit 0

