#!/bin/bash
# hwclockset.sh 
#		This is a modified version which sync with the initnode
#		in a OpenSSI cluster
#		Aneesh Kumar K.V <aneesh.kumar@digital.com>
#

PATH="/sbin:/bin"
[ ! -x /bin/date ] && exit 0
[ ! -x /bin/onnode ] && exit 0
[ ! -x /bin/where_pid ] && exit 0

[ -f /etc/default/rcS ] && . /etc/default/rcS

# Determine who the initnode is
INITNODE=$(where_pid 1)

case "$1" in
        start)
		# Set the system clock.
		date -s "$(onnode $INITNODE date)" >/dev/null
		echo "Syncing clock with node $INITNODE: `date`" date
                ;;
        stop|restart|reload|force-reload)
                # Does nothing
                exit 0
                ;;
        *)
                echo "Usage: hwclockset.sh {start|stop|reload|restart}" >&2
                echo "       start sync kernel (system) clock with init-node clock" >&2
                echo "       stop, restart, reload and force-reload do nothing." >&2
                exit 1
                ;;
esac

