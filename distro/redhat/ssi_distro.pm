#
# Date   :     April 02 2003
# Authors:	Brian Watson <Brian.J.Watson@hp.com>
# 		David Zafman <David.Zafman@hp.com>
# 		Aneesh Kumar K.V ( aneesh.kumar@digital.com )
# 		Scott Hinchley (Scott.Hinchley@hp.com)
#
#
#
#      This program is free software; you can redistribute it and/or
#      modify it under the terms of the GNU General Public License as
#      published by the Free Software Foundation; either version 2 of
#      the License, or (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
#      or NON INFRINGEMENT.  See the GNU General Public License for more
#      details.

package ssi_distro; 
use strict;

use Net::Ping;
use Socket;

# Do NOT use the ssi_mgmt module
use lib "/cluster/lib";
use Clustertab;
use ssi_arch;
use ssi_utils;

our $distro;
our $bootloaderconf;
our $varlogfile;

BEGIN {
	$distro = "redhat";
	if ( $arch =~ /i386/ ) {
		$bootloaderconf = "/etc/grub.conf"
	} elsif ( $arch =~ /alpha/ ) {
		$bootloaderconf = "/etc/aboot.conf"
	}
	$varlogfile = "/var/log/messages";

	our @ISA = qw(Exporter);
	our @EXPORT = qw(
			$distro
			$bootloaderconf
			$varlogfile

			prompt_hwaddr
			prompt_ipaddr

			distro_create_prompt
			distro_create_write
			distro_addnode_prompt
			distro_addnode_write

			rebuild_boot_materials 
			);
}

# SSI_XXX: Need a more generic infrastructure that preserves the complete
# subdirectory structure of a directory moved into $TEMPLATE_DIR by 
# mklocalfile. Then we could eliminate most of the instructions for 
# preserving individual subdirectories.
sub create_cdsl {
	my $record = shift;

	my $nodenum = $record->{NODE};
	my $ifname = $record->{IFNAME};

	my $TMP_DIR = trim `mktemp -d /tmp/ssi-create.XXXXXX`;
	die 'Failed to create temporary directory'
		unless $TMP_DIR;

	my $NODE_DIR = "$CLUSTER_DIR/node$nodenum";
	`mkdir -p $NODE_DIR/dev`;
	`mkdir -p $TEMPLATE_DIR/dev`;
	`mkdir -p $CLUSTER_DIR/var/lock/subsys`;
	`mkdir -p $CLUSTER_DIR/dev/pts`;
	`mkdir -p $CLUSTER_DIR/init`;
	symlink($NODE_DIR, "$CLUSTER_DIR/node{nodenum}")
		unless -e "$CLUSTER_DIR/node{nodenum}";

	my $target = '/etc/nodename';
	`touch $target`;
	mklocalfile $target;
	`rm -f $TEMPLATE_DIR/$target`;

	symlink('../../sbin', "$NODE_DIR/sbin") 
		unless -e "$NODE_DIR/sbin";
	symlink('../../sbin', "$TEMPLATE_DIR/sbin") 
		unless -e "$TEMPLATE_DIR/sbin";

	$target = '/etc/sysconfig/network-scripts';
	mklocalfile $target;
	`mv $TEMPLATE_DIR/$target/ifcfg-lo $TMP_DIR`;
	`rm -f $TEMPLATE_DIR/$target/ifcfg-*`;
	`mv $TMP_DIR/* $TEMPLATE_DIR/$target`;

	$target = '/etc/sysconfig/networking';
	mklocalfile $target;
	`rm -rf $TEMPLATE_DIR/$target/profiles/default/*`;
	`mv $TEMPLATE_DIR/$target/profiles/default $TMP_DIR`;
	`rm -rf $TEMPLATE_DIR/$target/profiles/*`;
	`mv $TEMPLATE_DIR/$target/profiles $TMP_DIR`;
	`mv $TMP_DIR/default $TMP_DIR/profiles`;
	`rm -rf $TEMPLATE_DIR/$target/devices/*`;
	`mv $TEMPLATE_DIR/$target/devices $TMP_DIR`;
	`mv $TEMPLATE_DIR/$target/ifcfg-lo $TMP_DIR`;
	`rm -rf $TEMPLATE_DIR/$target/*`;
	`mv $TMP_DIR/* $TEMPLATE_DIR/$target`;

	$target = '/etc/sysconfig/network';
	symlink("$target", "$NODE_DIR/$target") 
		unless -e "$NODE_DIR/$target";
	symlink("$target", "$TEMPLATE_DIR/$target") 
		unless -e "$TEMPLATE_DIR/$target";

#	# Disable redhat-config-network profiles involving ICS interface
#	my @ics_profiles = split /\n/,
#		`grep -R "DEVICE=$ifname" $target/profiles | cut -d : -f 1`;
#	foreach (@ics_profiles) {
#		print "Disabling $_ to protect $ifname from changes\n";
#             my $dest = $_;
#                $dest =~ s/\/profiles\//\/profiles.ssisave\//;
#		`mv $_ $dest`;
#	}

	$target = '/var/lib/nfs/statd';
	mklocalfile $target;
	`rm -rf $TEMPLATE_DIR/$target/*`;

	$target = '/var/lib/random-seed';
	mklocalfile $target;

	$target = '/var/lock';
	mklocalfile $target;
	`rm -rf $TEMPLATE_DIR/$target/subsys/*`;
	`mv $TEMPLATE_DIR/$target/subsys $TMP_DIR`;
	`rm -rf $TEMPLATE_DIR/$target/*`;
	`mv $TMP_DIR/* $TEMPLATE_DIR/$target`;

	$target = '/var/log';
	mklocalfile $target;
	# SSI_XXX: Not idempotent
	# SSI_XXX: This is chatty about making the .ssisave file, which we
	# 	   promptly delete
	mkglobalfile "$target/wtmp";
		`rm -rf $NODE_DIR/$target/wtmp.ssisave`;
	`rm -rf $TEMPLATE_DIR/$target/gdm/*`;
	`test -d $TEMPLATE_DIR/$target/gdm && mv $TEMPLATE_DIR/$target/gdm $TMP_DIR`;
	`rm -rf $TEMPLATE_DIR/$target/httpd/*`;
	`test -d $TEMPLATE_DIR/$target/httpd && mv $TEMPLATE_DIR/$target/httpd $TMP_DIR`;
	`rm -rf $TEMPLATE_DIR/$target/news/*`;
	`test -d $TEMPLATE_DIR/$target/news && mv $TEMPLATE_DIR/$target/news $TMP_DIR`;
	`mv $TEMPLATE_DIR/$target/wtmp $TMP_DIR`;
	`rm -rf $TEMPLATE_DIR/$target/*`;
	`mv $TMP_DIR/* $TEMPLATE_DIR/$target`;

	$target = '/var/run';
	mklocalfile $target;
	# SSI_XXX: Not idempotent
	# SSI_XXX: This is chatty about making the .ssisave file, which we
	# 	   promptly delete
	mkglobalfile "$target/utmp";
		`rm -rf $NODE_DIR/$target/utmp.ssisave`;
	mkglobalfile "$target/sudo";
		`rm -rf $NODE_DIR/$target/sudo.ssisave`;
	`rm -rf $TEMPLATE_DIR/$target/console/*`;
	`mv $TEMPLATE_DIR/$target/console $TMP_DIR`;
	`rm -rf $TEMPLATE_DIR/$target/netreport/*`;
	`mv $TEMPLATE_DIR/$target/netreport $TMP_DIR`;
	`mv $TEMPLATE_DIR/$target/utmp $TMP_DIR`;
	`mv $TEMPLATE_DIR/$target/sudo $TMP_DIR`;
	`rm -rf $TEMPLATE_DIR/$target/*`;
	`mv $TMP_DIR/* $TEMPLATE_DIR/$target`;

	$target = '/var/yp';
	mklocalfile $target;

	# Get node_name from HOSTNAME= line in /etc/sysconfig/network
	my @lines;
	my $node_name;
	open (NETWORK_FILE, "</etc/sysconfig/network") or
		die "Unable to open /etc/sysconfig/network for reading: $!\n";
	while (<NETWORK_FILE>) {
		chop;
		unless ($_ =~ /^HOSTNAME=['"]?([^'"]*)/ ) {
			push @lines, "$_\n";
			next
		}
		$node_name = $1;
	}
	push @lines, "HOSTNAME=\"`cat /etc/nodename`\"\n";
	close NETWORK_FILE;

	open (NETWORK_FILE, ">/etc/sysconfig/network") or
		die "Unable to open /etc/sysconfig/network for writing: $!\n";
	print NETWORK_FILE $_ foreach @lines;
	close NETWORK_FILE;
	
	open (NODE_NAME_FILE,">$NODE_DIR/etc/nodename") or
		die "Unable to open $NODE_DIR/etc/nodename " .
			"for writing: $!\n";
	print NODE_NAME_FILE "$node_name\n";
	close NODE_NAME_FILE;

	rmdir $TMP_DIR;
}

sub install_dep_packages {
	# Dependent packages are supposed to already be installed

	# Enable dhcpd in case it isn't already
	system("/sbin/chkconfig dhcpd on");
	# Enable tftpd in case it isn't already
	system("/sbin/chkconfig tftp on");
	# Kudzu doesn't work well with a global root on Redhat systems 
	system("/sbin/chkconfig --del kudzu");
}

sub create_node_cdsl {
	my $nodenum = shift;

	my $NODE_DIR = "$CLUSTER_DIR/node$nodenum";

	if ( ! -d $TEMPLATE_DIR ) {
		print "You should run ssi-create first\n";
		exit(1);
	}

	if ( -d $NODE_DIR ) {
		#Node specific directory already exists
		return;
	}

	print "Creating $NODE_DIR\n";
	`cp -a $TEMPLATE_DIR $NODE_DIR`;
}

sub help_failover {
	print "
Do you want this node to be a root failover node? It _must_
have access to the root filesystem on a shared disk in order 
to answer yes. If you answer yes, then this node can boot 
first as a root node, so you should configure it with a local 
boot device. This is done after this node joins the cluster 
and is described in the installation instructions.
";
}

sub prompt_failover {
	my $nodenum = shift;
	my $verbose = shift;

	my ($rootdev, $type, $opts);

	open FSTAB, "< /etc/fstab" or die;
	while (<FSTAB>) {
		(/^(\S+)[\s]*\/[\s]*(\S+)[\s]*(\S+)/) or next;
		($rootdev, $type, $opts) = ($1, $2, $3);
		last
	}
	close FSTAB;

	die "Root filesystem cannot be found\n" unless $rootdev;

	# Force no failover question since root is GFS
	# This could default to 1, and later require boot device
	# Could check for all parallel types in cfstab
	return 0 if $type eq 'gfs';

	# Could add check that type is in cfs type in cfstab

	# Could add check that device is UUID=

	# If root failover not enabled don't ask
	# SSI_XXX: Should parse this better
	if ( $opts !~ /chard/ ) {
		return 0;
	}

	help_failover if $verbose;
	print "\n";
PROMPT: print "Enable root filesystem failover to this node (y/n/?) [n]: ";
	my $selection = uc trim <STDIN>;
	do { help_failover; print"\n"; goto PROMPT } if $selection eq '?';
	$selection = "N" if $selection eq '';
	goto PROMPT unless $selection =~ /^[NY]$/;
	if ( $selection =~ /^Y$/) {
		return 1;
	}
	return 0;
}

sub parselog {
	my $logfile = shift;
	my $records = shift;
	my @loghits;

	open LOG, "< $logfile" or die "Cannot open $logfile: $!\n";
LINE:	while (<LOG>) {
		my @line = split /\s+/, $_;
		next unless $line[9];
		next unless $line[4] eq 'dhcpd:' and $line[5] eq 'DHCPDISCOVER';

		my %hit;
		$hit{TIME} = join ' ', @line[0..2];
		$hit{HWADDR} = uc $line[7];
		next unless validate_hwaddr($hit{HWADDR}, $records);
		foreach (@loghits) {
			if ($hit{HWADDR} eq $_->{HWADDR}) {
				$_->{TIME} = $hit{TIME};
				next LINE;
			}
		}
		unshift @loghits, \%hit;
	}
	close LOG;
	return \@loghits;
}

sub help_hwaddr {
	print "
The list shows all unknown hardware addresses that have 
recently probed the cluster's DHCP server. Choose the one 
that is displayed on the console of the new node when it 
attempts to network boot. If the desired NIC is not listed, 
make sure the node has attempted to network boot on the 
cluster interconnect, then select (R)escan.
";
}

sub prompt_hwaddr {
	my $records = shift;
	my $logfile = shift;
	my $verbose = shift;

	print "
Select the hardware address of the new node's cluster 
interconnect NIC.
";

	help_hwaddr if $verbose;
RESCAN:	my $loghits = parselog $logfile, $records;

	print "\n";
	print "\tHardware address\tTime last probed\n";
	print "\t----------------\t----------------\n";

	unless (@$loghits) {
		print "\tnothing found\n";
		exit(1);
	}

	my $ctr = 0;
	foreach (@$loghits) {
		$ctr++;
		print "  $ctr)\t$_->{HWADDR}\t$_->{TIME}\n";
	}
	print "\n";

PROMPT:	print "Select (1";
	print "-$ctr" if $ctr > 1;
	print "), (r)escan, (q)uit or (?) [1]: ";

	my $selection = uc trim <STDIN>;
	do { help_hwaddr; goto RESCAN } if $selection eq '?';
	goto RESCAN if $selection eq 'R';
	exit(0) if $selection eq 'Q';
	$selection = 1 if $selection eq '';
	goto PROMPT unless $selection =~ /^\d+$/;
	goto PROMPT unless $selection > 0 && $selection <= $ctr;

	return $loghits->[$selection - 1]{HWADDR};
}

our $pingproto = 'icmp';

sub help_ipaddr_scan {
	print "
The IP address must be unique and it must be on the same 
subnet as the cluster interconnect NICs for the other 
nodes. If you want, this program can scan the subnet for 
IP addresses that seem to be available. It does this by 
pinging every address in the subnet, which can take awhile 
for larger subnets. You can safely skip this feature if you 
know one or more available IP addresses.
";
}

sub help_ipaddr {
	print "
The IP address must be unique and it must be on the same 
subnet as the cluster interconnect NICs for the other 
nodes.
";
}

sub prompt_ipaddr {
	my $records = shift;
	my $verbose = shift;

	my $data = $records->{DATA};
	my (@list, %taken);
	foreach (@$data) {
		next if $_->{IPADDR} eq '<DHCP>';
		my $value = ip2num $_->{IPADDR};
		push @list, $value;
		$taken{$value} = 1;
	}

	my $interface = match_ifconfig($records);
	my $net = ip2num $interface->{NETADDR};
	my $mask = ip2num $interface->{NETMASK};
	my $min = $net + 1;
	my $max = $net + ($mask ^ ip2num '255.255.255.255') - 1;

	print "\nEnter an IP address for the NIC.\n";
	help_ipaddr_scan if $verbose;
	print "\n";
SCAN:	print "Do you want to scan for available IP addresses (y/n/?) [n]: ";
	my $selection = uc trim <STDIN>;
	do { help_ipaddr_scan; print "\n"; goto SCAN } if $selection eq '?';
	$selection = "N" if $selection eq '';
	goto SCAN unless $selection =~ /^[NY]$/;
	my $default = ($selection eq 'Y') ? 1 : '';
	do { print "\n"; goto PROMPT } unless $default;

	$| = 1;
	my $setstring = "\r\t\t\t";
	my $ping = Net::Ping->new($pingproto, 0.005);
	print "Scanning network:$setstring";
	foreach (0..($max-$min)) {
		my $cur = $min + $_;
		my $addr = num2ip $cur;
		print "$addr        $setstring";
		next unless $ping->ping($addr);
		push @list, $cur;
		$taken{$cur} = 1;
	}
	print "\r";
	$| = 0;

	my $string;
	($string, $default) = make_ipstring(\@list, $min, $max);
	unless ($string) {
		print "Sorry, there are no available IP addresses to add a " .
			"new node.\n";
		exit(1);
	}
	print "The following IP addresses seem to be available:\n\n";
	print "$string\n\n";

PROMPT:	print "Enter an IP address or (?)";
	print " [$default]" if $default;
	print ": ";

	$selection = trim <STDIN>;
	do { help_ipaddr; print "\n"; goto PROMPT } if $selection eq '?';
	$selection = $default if $selection eq '';
	goto PROMPT unless validate_ipaddr($selection, $records);
	return $selection if $selection eq '<DHCP>';
	$selection = ip2num $selection;
	goto PROMPT unless $selection >= $min && $selection <= $max;
	goto PROMPT if $taken{$selection};

	return num2ip $selection;
}

sub munge_inittab {
	open OLDTAB, "< /etc/inittab" 
		or die "Cannot read /etc/inittab: $!\n";
	open NEWTAB, "> /etc/inittab.ssinew" 
		or die "Cannot create /etc/inittab.ssinew: $!\n";

	print "Generating SSI-enhanced /etc/inittab\n";
	while (<OLDTAB>) {
		# skip comments
		if (/^\s*#/) {
			print NEWTAB $_;
			next;
		}

		# parse line into fields
		my @fields = split /:/, $_;
		foreach (0..3) {
			$fields[$_] = '' unless defined $fields[$_];
		}

		# skip SSI-specific lines
		next if ($fields[0] eq 'siR') or
			($fields[0] =~ /^l[1-5]U$/) or
			($fields[0] =~ /^l[1-5]D$/);

		# add sysrecover line
		if ( ($fields[2] eq 'sysinit') and 
				($fields[3] =~ /\/rc\.sysinit/) ) {
			print NEWTAB $_;
			print NEWTAB 
'siR:R:sysinit:/etc/rc.d/rc.sysrecover < /dev/console > /dev/console 2>&1
';
			next;
		}

		# add nodeup and nodedown lines
		if ( ($fields[1] eq '3') and
				($fields[3] =~ /\/rc\s+3\D/) ) {
			print NEWTAB $_;
			print NEWTAB 
'l3U:S12345U:once:/etc/rc.d/rc.nodeup %n < /dev/console > /dev/console 2>&1
l3D:S12345D:once:/etc/rc.d/rc.nodedown %n
';
			next;
		}

		# edit getty lines
		if ( ($fields[2] eq 'respawn') and ($fields[3] =~ /getty/) ) {
			# add U for nodeup
			$fields[1] .= 'U' unless $fields[1] =~ /U$/;
			print NEWTAB join(':', @fields);
			next;
		}

		# leave everything else alone
		print NEWTAB $_;
	}
	close OLDTAB;
	close NEWTAB;

	unless (-f '/etc/inittab.ssisave') {
		print "Saving base /etc/inittab as /etc/inittab.ssisave\n";
		rename '/etc/inittab', '/etc/inittab.ssisave';
	}
	rename '/etc/inittab.ssinew', '/etc/inittab';
}

sub distro_create_prompt {
	my $record = shift;
	my $verbose = shift;

	die "\nNFS support must be enabled for OpenSSI on Red Hat\n"
		unless $record->{NFS} eq 'yes';
}

sub distro_create_write {
	my $record = shift;

	create_cdsl($record);
	install_dep_packages();

	# Move some files aside
	if (-f '/etc/dhcpd.conf' and not -f '/etc/dhcpd.proto') {
		print "Moving /etc/dhcpd.conf to /etc/dhcpd.proto\n";
		rename '/etc/dhcpd.conf', '/etc/dhcpd.proto';
	}
	if (-f '/etc/securetty') {
		print "Saving /etc/securetty as /etc/securetty.ssisave\n";
		rename '/etc/securetty', '/etc/securetty.ssisave';
	}

	# Generate SSI inittab
	munge_inittab();
}

sub distro_addnode_prompt {
	my $record = shift;
	my $verbose = shift;

	my $node = $record->{NODE};
	my $initnode = $record->{MASTER};

	$record->{MASTER} = prompt_failover($node, $verbose)
		unless defined $initnode and $initnode ne '';
	$record->{FAILOVER} = $record->{MASTER} ? 'Y' : 'N';
}

sub distro_addnode_write {
	my $record = shift;

	my $node = $record->{NODE};

	create_node_cdsl($node);

	# SSI_XXX: Either user must run netconfig after joining cluster or
	# we must do the right thing here by create ifcfg-* (i.e. ifcfg-eth0)
}

sub rebuild_boot_materials {
	my $clustertab = shift;
	my $bootloader = shift;
	my $initrds = shift;

	# SSI_XXX: what if a node with a new driver type for its ICS interface
	#      is added to the cluster? --tabonly doesn't handle this

	# Return if we don't find any initrds
	return 0 unless $initrds;

	foreach (@$initrds) {
		print "Updating $_:\t";
		my $error = system("/sbin/mkinitrd --tabonly $_") >> 8;
		if ($error == 0) {
			print "success\n";
		} elsif ($error == 2) {
			print "ignored (not SSI)\n";
		} else {
			print "FAILED\n";
		}
	}
	system("/sbin/ssi-ksync -c $bootloader -t $clustertab") == 0 or
		print "The ssi-ksync command failed to sync your " .
			"local boot devices.\n";
	return 1;
}

1;
