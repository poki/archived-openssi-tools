#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/cluster.h>

void
print_usage(void)
{
	fprintf(stderr, "Usage...\n\twhere_pid  pid\n");
	exit(-1);
}

int
main(int ac, char *av[])
{
	long pidnum = -1;
	char *p;

	if ((ac < 2) || (ac > 2)) 
		print_usage();

	pidnum = strtol((av[1]), &p, 0);
	/* check for failure from strtol */
	if ((p == av[1]) || (*p != '\0')) {
		fprintf(stderr, "invalid pid\n");
		print_usage();
	}
	printf("%d\n", node_pid(pidnum));
	return 0;
}
