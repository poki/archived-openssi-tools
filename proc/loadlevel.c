/*
 * 	loadllevel command
 *	Copyright 2002 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
 *
 */
/*
 *	loadlevel.c :
 *
 *
 *	syntax :
 *		loadlevel <prog>		
 *		loadlevel -p <pid>
 *		loadlevel -a <on|off>	print detailed node state for each node
 *		loadlevel -n <node #> <on|off>	 the number of fully-up nodes
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <libgen.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/cluster.h>


extern char *optarg;
extern int optind, opterr, optopt;

extern int cluster_ssiconfig(void);

#define LOADSTATUS_FILE	"/.loadlevel_status"

clusternode_t *node_info;
int node_info_cnt;

void do_loadlevel_write(clusternode_t, char *);
void do_update_status_file(clusternode_t, char *);
int do_check_loadlevel(clusternode_t, int);

void
pr_usage(
	char	*pname)
{
	fprintf(stderr,/* MM_ERROR, */
	     "Usage...\n"
	     "\t%s [-p pid] [-a on|off] [-n # on|off] [prog]>\n"
	     "\t\tprog		Loadlevel executable prog\n"
	     "\t\t-p,--pid pid		Loadlevel pid \n"
	     "\t\t-n,--node # on|off	Toggle loadleveling on node #\n"
	     "\t\t-a,--all on|off	Toggle loadlevel on all nodes in cluster\n"
	     "\t\t-V,--version 		Loadleveler Version \n"
	     "\t\tNote that these are all mutually exclusive\n",
	     basename(pname));;
}

static struct option longopts[] = {
	{"all", 1, 0, 'a'},
	{"node", 1, 0, 'n'},
	{"pid", 1, 0, 'p'},
	{"version", 0, 0, 'V'},
	{0, 0, 0, 0}
};


int main(
	int		argc,
	char		*argv[])
{
	char	opt;
	int	i;
	pid_t 	pid = 0;
	clusternode_t node = 0;
	int 	allnodes = 0;
	char 	action[8];
	char 	buf[8];
	char 	cmd_buf[1024];
	char	*todo = NULL;
	int 	version = 0;

	/*
	 * Make sure we are running on a OpenSSI kernel
	 */
	if (cluster_ssiconfig() != 1) {
		fprintf(stderr,/* MM_HALT  ,  */
		     "This command requires an OpenSSI kernel.\n");
		exit(1);
	}

	if (argc == 1) {
		pr_usage(argv[0]);
		exit(1);
	}

	while ((opt = getopt_long(argc, argv, "+a:n:p:V",longopts, NULL )) != EOF) {
		switch(opt) {
		case 'p':
			pid = atoi(optarg);
			break;
		case 'n':
			node = atoi(optarg);
			if (argv[optind] && (!strcmp(argv[optind], "on") ||
					 	!strcmp(argv[optind],"off"))) {
				strcpy(action, argv[optind++]);
				todo = action;
			}
			else {
				pr_usage(argv[0]);
				exit(1);
			}
			break;
		case 'a':
			allnodes = 1;
			strcpy(action, optarg);
			todo = action;
			break;
		case 'V':
			version = 1;
			do_check_loadlevel(clusternode_num(), version);
			exit(0);
		default:
			pr_usage(argv[0]);
			exit(1);
		}
	}

	if (todo) {
		if (strcmp(action, "on") && strcmp(action, "off")) {
			pr_usage(argv[0]);
			exit(1);
		}
		if (!strcmp(action, "on"))
			sprintf(buf, "1");
		else
			sprintf(buf, "0");
	}

	if (optind < argc) {
		int ret;

		strcpy(cmd_buf, argv[optind]);

		if (do_check_loadlevel(clusternode_num(), 0)) {
			printf("A loadleveler has not been registered,"
					" %s will run locally \n", cmd_buf);
		}
		sprintf(cmd_buf,
				"/bin/bash -c 'echo 1 > /proc/%d/loadlevel'",
				getpid());
		system(cmd_buf);
		ret = rexecvp(argv[optind], &argv[optind], CLUSTERNODE_BEST);
		if (ret != 0) 
			execvp(cmd_buf, argv+optind);
		exit(127);
	}

	if (pid) {
		if (do_check_loadlevel(clusternode_num(), 0)) {
			printf("A loadleveler has not been registered,"
			" cannot turn loadleveling %s for pid %d\n", 
			"on", pid);
			exit(1);
		}
		sprintf(cmd_buf,
			"/bin/bash -c 'echo 1 > /proc/%d/loadlevel'",
			 pid);
		system(cmd_buf);
		exit(0);
	}

	/*  Only superuser can turn on/off loadleveling on a per node basis */
	if (geteuid() != 0) {
		printf("Only superuser can turn loadleveling on/off on nodes\n");
		exit(1);
	}

	if (allnodes) {
		/* Get UP nodes */
		node_info = calloc(cluster_maxnodes(), sizeof(clusternode_t));

		node_info_cnt = cluster_membership(0, cluster_maxnodes(), 
								node_info);
		
		/* Cycle through UP nodes */
		for(i = 0; i < node_info_cnt; i++) {
			/* if this node is down, skip it */
			if (!node_info[i])
				continue;
			if (do_check_loadlevel(node_info[i], 0)) {
				printf("A loadleveler has not been registered,"
				" cannot turn loadleveling %s for node %u\n", 
				action, node_info[i]);
				continue;
			}

			do_loadlevel_write(node_info[i], buf);
		}
	}
	if (node) {
		if (do_check_loadlevel(node, 0)) {
			printf("A loadleveler has not been registered,"
			" cannot turn loadleveling %s for node %u\n",
			action, node);
			exit(1);
			
		}
		do_loadlevel_write(node, buf);
	}
	exit(0);
}

void
do_loadlevel_write(clusternode_t node, char *buf)
{
	char filename[256];
	int fd, l, r;

	sprintf(filename, "/proc/cluster/node%u/loadlevel",node);
	fd = open(filename, 1);
	if (fd == -1) {
		printf("failed to open %s \n", filename);
		return;
	}
	r = write(fd, buf, l = strlen(buf));
	close(fd);

	/* update load status file */
	do_update_status_file(node, buf);
}

void
do_update_status_file(clusternode_t node, char *buf)
{
	FILE *fp;
	FILE *fp2;
	int num;
	char on[8];
	long found = 0;

	fp = fopen(LOADSTATUS_FILE, "a+");
	fp2 = fopen("/.load.tmp", "w+");
	if (fp == NULL)
		printf("failed to open %s \n", LOADSTATUS_FILE);
	rewind(fp);
	while (fscanf(fp, "%d %s\n", &num, on) == 2) {
		if (num == node) {
			found = 1;
			fprintf(fp2,"%u %s\n",node,buf);
			continue;
		}
		fprintf(fp2,"%d %s\n",num,on);
	}
	
	if (!found)
		fprintf(fp2,"%u %s\n",node,buf);
	fclose(fp);
	fclose(fp2);
	rename("/.load.tmp", LOADSTATUS_FILE);
}

int
do_check_loadlevel(clusternode_t node, int version) 
{
	int fd, r;
	char filename[256];
	char buf[1024];
	int value;
	int ret;
	int userlevel;
	char name[16];
	char versbuf[16];
	int len;
	int i;


	sprintf(filename, "/proc/cluster/node%u/loadlevel",node);
	fd = open(filename, O_RDONLY);
	if (fd == -1)
		printf("failed to open %s \n", filename);
	r = read(fd, buf, 1024);
	close(fd);
	buf[r]='\0';
	ret = sscanf(buf, "%d\n%s %s %d\n", &value, name, versbuf, &userlevel);
	if (!version) 
		return ((value < 0) ? 1 : 0);
	else {
		if (ret == 1) {
			printf("A loadleveler has not been registered\n");
			return (1);
		}
		len = strlen(versbuf);
		for(i=0; i < len; i++) {
			if (versbuf[i] == '0' && versbuf[i+1] == '0') {
				versbuf[i+1] = '.';
				continue;
			}
			if (versbuf[i] == '0')
				versbuf[i] = '.';
		}
		printf("%s %s\n", name, versbuf);
		return (0);
	}
}
