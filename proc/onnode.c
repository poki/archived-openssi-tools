#ident "%Z%$RCSfile: onnode.c,v $	Revision: $Revision: 1.3 $	$Date: 2003/11/07 21:48:41 $ "

#include <dirent.h>
#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <linux/cluster.h>

#ifndef FALSE
#define	FALSE	0
#define	TRUE	1
#endif

/*
 *	onnode.c : (also includes localview, fast and fastnode.)
 *
 *	Purpose:  Allow cross-node process transparency and
 * 		  access to remote processing.
 *
 *	N.B. This source file produces onnode, fast, and fastnode. -- jp
 *
 *		onnode:	 Execute command on a specified node.
 *		fast:	 Execute command on the fastest (least loaded) node.
 *		fastnode:Return the fastest node
 *		localview:Execute command setting localview mode
 *
 *		Use rexecvp() call after skipping first two
 *		entries in argv list and utilizing the PATH
 *		environment variable to find the target file, if the
 * 		complete filename is not given.  (i.e. '/' not
 * 		first character of filename).
 *
 *
 *
 *	onnode:
 *		Execute command on a remote node.
 *	syntax : onnode nodenum command [args...]
 *	  	nodenum is an integer >= 0.
 * 	  	command is a valid executable command, along with optional
 * 	  	arguments.
 *
 *
 *	fast :
 *		Execute command on the fastest node in the cluster.
 *	syntax : fast [-v] command [args...]
 *
 *
 *	fastnode :
 *		Report the fastest node in the cluster.
 *	syntax : fastnode
 *
 *
 *	localview :
 *		Execute command in localview mode pinned to current node.
 *	syntax : localview command [args...]
 */

/* #define DEBUG=1 */

#ifdef DEBUG
#define DBG(a) printf a /* for debugging, obviously */
#else
#define DBG(a)
#endif
#define PREPOSTEROUS_LOAD INT_MAX

extern int _cluster_clms_ignore_halfup(void);

int do_onnode(int, char **, char **);
int do_fast(int, char **, char **, char *);
int usage_onnode(void);
int usage_fastnode(void);
int usage_fast(void);
int onnode(int, char **, char **, int, int);
int fast_node(void);
int do_localview(int, char **, char **);

char myname[MAXNAMLEN];

enum pin_states {
	PIN_NOCHANGE,		/* no change pin status - default */
	PIN_YES,		/* pin to node */
	PIN_NO			/* unpin from node */
};
typedef enum pin_states pin_states_t;

int
main(int argc,
     char *argv[],
     char *envp[])
{
	char *command;

        if (!cluster_ssiconfig()) {
                fprintf(stderr, /* MM_HALT ,  */
			"This command requires a SSI kernel.\n");
                exit(1);
        }

	/*
	 * Are we doing fast or onnode?  Obtain the
	 * command first.
	 */
	command = (char *) strrchr(argv[0], '/');	/* find last slash */
	if (command == NULL)
		command = argv[0];
	else
		++command;
	strncpy(myname, command, MAXNAMLEN);		/* save for others */
	strcat(myname, "\0");
	DBG(("argv[0] is %s\n", argv[0]));
	DBG(("command is %s\n", command));

	if (strcmp(command, "onnode") == 0) {
		DBG(("doing onnode\n"));
		do_onnode(argc, argv, envp);
	}
	else if (strcmp(command, "fast") == 0 || strcmp(command, "fastnode") == 0) {
		DBG(("doing fast/{node}\n"));
		do_fast(argc, argv, envp, command);
	} else if (strcmp(command, "localview") == 0) {
		DBG(("doing localview\n"));
		do_localview(argc, argv, envp);
	} else {
		fprintf(stderr, "onnode: Error linked to command '%s'\n",
			 command);
		exit(1);
	}
	exit(-1);  /* NOTREACHED  */
}

void
do_pin(int pinit)
{
	int fd = -1;
	int nin;
	int nout;
	const char pinstr[] = "1\n";
	const char unpinstr[] = "0\n";
	const char *cp;


	fd = open("/proc/self/pin", O_WRONLY);
	if (fd == -1) {
		fprintf(stderr,
			"onnode: error %d opening \"/proc/self/pin\"\n",
			errno);
		exit(-1);
	}
	cp = (pinit ? pinstr : unpinstr);
	nin = strlen(cp);
	nout = write(fd, cp, nin);
	if (nout == -1) {
		fprintf(stderr,
			"onnode: error %d writing \"/proc/self/pin\"\n",
			errno);
		exit(-1);
	}
	if (close(fd) == -1) {
		fprintf(stderr,
			"onnode: error %d closing \"/proc/self/pin\"\n",
			errno);
		exit(-1);
	}
}

void
do_local()
{
	int fd, ret;
	const char localviewstr[] = "localview";

	fd = open("/proc/self/localview", O_WRONLY);
	if (fd == -1) {
		fprintf(stderr,
			"onnode: error %d opening \"/proc/self/localview\"\n",
			errno);
		exit(-1);
	}

	ret = write(fd, localviewstr, strlen(localviewstr));
	if (ret == -1) {
		fprintf(stderr,
			"onnode: error %d writing \"/proc/self/localview\"\n",
			errno);
		exit(-1);
	}
	if (close(fd) == -1) {
		fprintf(stderr,
			"onnode: error %d closing \"/proc/self/localview\"\n",
			errno);
		exit(-1);
	}
	return;
}

/*
 * this module checks the argument count and calls the
 * onnode() routine.  If there is an argument problem,
 * or the onnode() call fails, an error message is printed
 * depending on the returncode, rc
 */
int
do_onnode(int argc,
     	char *argv[],
     	char *envp[])
{

	char *p;
	int rc;
	int node;
	int opt;
	pin_states_t	pinflag = PIN_NOCHANGE;
	int setcontext = 1;
	int localview = FALSE;

	if (argc < 3) {  /* not enough args  */
		usage_onnode();
		exit(-1);
	}

	while ((opt = getopt(argc, argv, "+cfpPl")) != EOF) {
		switch (opt) {
		case 'c':
			/* don't set node context */
			setcontext = 0;
			break;
		case 'p':	/* pin to the node */
			if (pinflag == PIN_NO) {
				usage_onnode();
				exit(-1);
			}
			pinflag = PIN_YES;
			break;
		case 'P':	/* do NOT pin to node, unpin */
			if (pinflag == PIN_YES) {
				usage_onnode();
				exit(-1);
			}
			pinflag = PIN_NO;
			break;
		case 'f':
			if (_cluster_clms_ignore_halfup() == -1) {
				fprintf(stderr, "onnode: -f argument: Permission Denied\n");
				exit(1);
			}
			break;
		case 'l':
			localview = TRUE;
			break;
		case '?':
			usage_onnode();
			exit(-1);
		default:
			break;
		}
	}

	/* Pin or unpin a process to/from a node */
	if (pinflag == PIN_YES)
		do_pin(1);
	else if (pinflag == PIN_NO)
		do_pin(0);
	if (localview)
		do_local();
	/* Get the number of the target node */
	node = strtol((argv[optind]), &p, 0);
	DBG(("node number is %d and my name is %s.\n", node, argv[0]));
	/* check for failure from strtol */
	if ((p == argv[optind]) || (*p != '\0')) { 	/* failure cases  */
           fprintf(stderr,/* MM_ERROR, */"%s is not a valid node number.\n",
			  argv[optind]);
		exit(-1);
	}

	/* increment past node number */
	optind++;

	/* Sufficient parameters for onnode() invocation.  Call with
	 * modified argv and argc, skipping the first two argv params
	 * ('onnode' and nodenum).  The former is not needed, the latter
	 * already saved in the nodenum variable. (and verified above)
	 * Eliminate the first 2 argv[] elements, they are
	 * the nodenum gleaned above and the 'onnode' string
	 * itself.  Not needed.
	 */
	rc = onnode((argc - optind), &argv[optind], envp, node, setcontext);
	/* Should never be reached.  */
	exit(-1);
}



/*
 * do_fast() performs either the fast or fastnode command.
 */
int
do_fast(int argc,
     char *argv[],
     char *envp[],
     char *command)
{

	int rc;
	int nodenum;
	int vflag = 0;
	int kflag = 0;
	int errflag = 0;
	int argcoffset = 0; 	/* offsets into argc and argv for calls */
	int argvoffset = 0;
	int setcontext = 1;

	int c;

	extern int optind;
	extern int opterr;  /* disable getopt error messages */
	extern char *optarg;

	opterr = 0;
	while ((c = getopt(argc, argv, "+cv")) != EOF) {
		DBG(("getopt got %c\n", c));
		switch (c) {
			case 'c':
				/* don't set node context */
				setcontext = 0;
				break;
			case 'v': {
				vflag++;
				argcoffset++; /* get past flag */
				argvoffset++;
				break;
			}
			default: {
				errflag++;
				break;
			}
		}
	}

	/*
	 * Check to see if we are doing fastnode or fast.
	 */
	if (strcmp(myname, "fastnode") == 0) {
		if ((errflag) || ((argc > 2) && (!kflag))) {
			usage_fastnode();
			exit(-1); 
		}
		else {
			printf("%d\n", fast_node());
			return 0;
		}
	}

	/* 
	 * fall through, we must be doing fast. 
	 * Check for its error case.  
	 */
	if (errflag || (argc - optind < 1)) { /* need at least one to execute */
		usage_fast();
		exit(-1);
	}

	DBG(("do_fast, calling fast_node...argv=%s, argc=%d\n", *argv, argc));
	nodenum = fast_node();  /* get the fastest node. */
	DBG(("fast node returned %d\n", nodenum));

	/*
	 * Use the onnode subroutine to perform the operation
	 * after printing out the node, if verbose is enabled.
	 */
	if (vflag) {
		printf ("fast node %d\n", nodenum);
	}
	fflush(stdout);  /* buffer flush required in non-tty case.  */

	/* get past calling command */
	argcoffset++; 
	argvoffset++;

	DBG(("argcoffset=%d, argvoffset=%d\n", argcoffset, argvoffset));
	DBG(("argc-argcoffset=%d\n", argc-argcoffset));
	
	rc = onnode((argc - argcoffset), &argv[argvoffset], envp, nodenum,
		    setcontext);
	/* Should never be reached.  */
	DBG(("onnode call failed, rc=%d\n", rc));
	exit(-1);
}

int
fast_node(void)
{
	int	new_node, fast_node = 0;/* new node, fast node this iteration */
	long do_loads_get(clusternode_t);
	long	new_load, fast_load; 	/* new load, fast load this iteration */
	clusternode_t maxnode, node;

	maxnode = cluster_maxnodes();
	DBG(("maxnode: %d\n", maxnode));

	fast_load = PREPOSTEROUS_LOAD; /* init so normal load will replace */
	for (node = 1 ; node < maxnode;  node++) { 

		new_node = (int)node;
		new_load = do_loads_get(node);
		/* Skip non-UP nodes */
		if (new_load <= 0)
			continue;

		DBG(("read new node= %d, lm=%lf\n", new_node, new_load));
		DBG(("fastnode=%d, fastload= %lf\n", fast_node, fast_load));
		DBG(("fast_load:%lf; new_load:%lf\n", fast_load, new_load));
	
		if (new_load < fast_load) {
			fast_node = (int)new_node;
			fast_load = new_load;
			DBG(("found new fast node %d %g\n",
				fast_node, fast_load));
		}  
	DBG(("\n"));
	} 
	return(fast_node);
} /* end fast_node */

/*
 * 	onnode() :
 *		execute command on a specified node.  get info (nodenum and
 * 		command) from the argv[] array above, which are discarded and
 *		passed to the call as paramaters.
 */
int
onnode(	int argc,	/* arg count */
	char *argv[],	/* this argv is offset by 2, argv[0] is	*/
	char *envp[],	/* really argv[2] from the onnode invocation.	*/
	int nodenum,	/* see above for discussion.			*/
	int setcontext)
			/* argv[0] has the command name.  Any additional
			 * entries in the array are arguments.
			 */
{
	int rc;
	char *command;

	command = argv[0];

	DBG(("before rexecvp.  command='%s' argv=%s  nodenum=%d\n",
		command, *argv, nodenum));
	if (setcontext)
		(void)cluster_set_node_context(nodenum);
 	rc = rexecvp(command, argv, nodenum);
	/* The rexecvp failed.  Tell user why, using errno.
	 * This should not be reached.
	 */
	switch (errno) {
		case ENOENT:
			fprintf(stderr,/* MM_ERROR,*/ " %s not found.\n",
				command);
			break;
		case EACCES:
			fprintf(stderr,/* MM_ERROR,*/ 
				" can't execute %s, errno=%d.\n",
				command, errno);
			break;
		case EINVAL:
			fprintf(stderr, /* MM_ERROR, */
				" %d is not a valid node number.\n",
				nodenum);
			break;
		case ENOEXEC:
			fprintf(stderr,/* MM_ERROR,  */
				" is not valid executable file.\n");
			break;
		case EREMOTE:
			fprintf(stderr, /* MM_ERROR, */ 
				"Can't execute '%s', Node %d is not available.\n",
				command, nodenum);
			break;
		default:
			fprintf(stderr,/* MM_ERROR, */
				" can't execute %s, errno=%d.\n",
				command,errno);
			break;
	}  /*  end switch  */
	DBG(("returncode from rexecvp:%d\n", rc));
	return(rc);
} /* end onnode  */

/*  usage messages  */

/*
 * 	usage_onnode():
 * 		print usage message and return.
 */
int
usage_onnode(void)
{
	fprintf(stderr,/* MM_ACTION, */ 
		"Usage...\n\tonnode [-cpPfl] node command [ arg ] ...\n");
	return 0;
}

/*
 * 	usage_fast():
 * 		print usage message and return.
 */
int
usage_fast(void)
{
	fprintf(stderr,/* MM_ACTION, */ 
		"Usage....\n\tfast [ -cv ] command [ arg ] ...\n");
	return 0;
}

/*
 * 	usage_fastnode():
 * 		print usage message and return.
 */
int
usage_fastnode(void)
{
	fprintf(stderr,/* MM_ACTION, */"Usage...\n\tfastnode\n");
	return 0;
}

int
do_localview(int argc,
     	char *argv[],
     	char *envp[])
{

	int rc;
	int node;
	int setcontext = 1;

	if (argc < 2) {  /* not enough args  */
		fprintf(stderr, "usage: localview command ...\n");
		exit(-1);
	}

	/* Always pin a localview process? */
	do_pin(1);
	do_local();

	node = clusternode_num();

	rc = onnode(argc - 1, &argv[1], envp, node, setcontext);
	/* Should never be reached.  */
	exit(-1);
}

long
do_loads_get(clusternode_t node)
{
	char filename[256];
	char buf[1024];
	int fd, r;

	sprintf(filename, "/proc/cluster/node%u/load", (int)node);
	fd = open(filename, O_RDONLY);
	if (fd == -1) {
		return(-1);
	}
	r = read(fd, buf, 1024);
	close(fd);

	buf[r-1]='\0';
	return atol(buf);
}
