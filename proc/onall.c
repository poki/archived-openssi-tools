/*
 * Copyright (c) 1997 Tandem Computers, Inc.
 *        All Rights Reserved
 */
#ident "@(#)$Header: /cvsroot/ssic-linux/openssi/openssi-tools/proc/onall.c,v 1.9 2003/11/03 01:20:39 dzafman Exp $"

/*
 *  onall - Execute a command string on all nodes
 */

#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <sys/time.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <linux/cluster.h>

#define	max(a, b)	((a) > (b) ? (a) : (b))
#define min(a, b)	((a) < (b) ? (a) : (b))

#define	CALLOC(n, t)	(t *)calloc(n, sizeof(t))

#define MAXNODES	cluster_maxnodes()

#define SVC_LOCK_FILE	"/cluster/node%u/var/lock/subsys/%s"

#ifndef	TRUE
#define TRUE 1
#define FALSE 0
#endif

char cmd_buf[1024];

int	debug = FALSE;
int	dependent_nodes_flag = FALSE;
int	primary_node_flag = FALSE;
int	secondary_node_flag = FALSE;
int	all_nodes_flag = TRUE;	/* run on every node unless we get an option */
int	run_in_background = FALSE;
int	exclude_flag = FALSE;
int	list_node_numbers = TRUE;
int	local_node_only = FALSE;
int	*exclude_nodes;
int	force;
int	localview;

/*----------------------------------------------*/

void
quit(int sig)
{
	exit(1);
}

/* FIXME!!! need to add the long options */
void
usage_onall(int status)
{
	fprintf(stderr, "Usage...\n\tonall [-#bql] [-x node[,node[...]]]  "
		"<command...%%n...>\n");
	exit(status);
}

void
usage_onsvc(int status)
{
	fprintf(stderr, "Usage...\n\tonsvc [-#bqLl] "
		"<svc_name> <command...%%n...>\n");
	exit(status);
}

void
usage_onclass(int status)
{
	fprintf(stderr, "Usage...\n\tonclass [-#bqLl] "
		"<class> <command...%%n...>\n");
	exit(status);
}

int my_system (char *command) {
	int pid, status;

	if (command == 0)
		return 1;
	pid = fork();
	if (pid == -1)
		return -1;
	if (pid == 0) {
		char *argv[4];
		argv[0] = "sh";
		argv[1] = "-c";
		argv[2] = command;
		argv[3] = 0;
		execve("/bin/sh", argv, environ);
		exit(127);
	}
	do {
		if (waitpid(pid, &status, 0) == -1) {
			if (errno != EINTR)
				return -1;
		} else
			return status;
	} while(1);
}

/*
 *  Concatenate s2 onto s1 while replacing
 *  an occurence of %n with the specified integer.
 */
void
strcat_subts(char * s1, char * s2, int i)
{
	/* find end of s1 */
	while (*s1) s1++;

	*s1++ = '\'';

	while (*s2) {
		if (*s2 == '%') {
			switch (*(s2+1)) {
			case '%':		/* double % becomes single */
				*s1++ = '%';
				s2 += 2;
				break;
			case 'n':
				sprintf(s1, "%d", i);
				while (*s1) s1++;
				s2 += 2;
				break;
			default:
				*s1++ = *s2++;
				*s1++ = *s2++;
				break;
			}
		} else {
			*s1++ = *s2++;
		}
	}

	*s1++ = '\'';
	*s1 = '\0';
}

/*
* Run the given command on the given node
*/
int
run_on_node(int cmd_argc, char ** cmd_argv, int node)
{
	int j;
	int error = 0;

	/* construct the command */
	sprintf(cmd_buf, "exec %s/onnode %s %s %d", ONNODE_LOC,
		force ? "-f" : "",
		localview ? "-l" : "", node);
	for (j = 0; j < cmd_argc; j++) {
		strcat(cmd_buf, " ");
		strcat_subts(cmd_buf, cmd_argv[j], node);
	}

	if (run_in_background)
		strcat(cmd_buf, " &");

	if (debug) {
		fprintf(stderr, "%s\n", cmd_buf);
	} else {
		if (list_node_numbers  &&  !run_in_background) {
			printf("(node %d)\n", node);
			fflush(stdout);
		}
		error = my_system(cmd_buf);
	}

	if (error != 0)
		return(1);
	else
		return(0);
}

/* 
* Run the given command on the primary node
*/
int
on_primary_node(int cmd_argc, char ** cmd_argv)
{
	int root_node;

#ifdef TRUE
	root_node = -1;
#else
	/* figure out which is the root node */
	root_node = get_primary(CLUSTER_ROOT_SERVICE);	
#endif
	if (root_node == -1) {
		fprintf(stderr, "Can't determine root node: %s\n",
			strerror(errno));
		exit(1);
	}

	return(run_on_node(cmd_argc, cmd_argv, root_node));
}

/* 
* Run the given command on the backup node 
*/
int
on_secondary_node(int cmd_argc, char ** cmd_argv)
{
	int backup_node;

#ifdef TRUE
	/* SSI_XXX: get_primary(), get_secondary() are not available,
	 * so return -1
	 */
	backup_node = -1;
#else
	/* figure out which is the backup root node */
	backup_node = get_secondary(CLUSTER_ROOT_SERVICE);	
#endif
	if (backup_node == -1) {
		fprintf(stderr, "Can't determine backup node: %s\n",
			strerror(errno));
		exit(1);
	}

	return(run_on_node(cmd_argc, cmd_argv, backup_node));
}

/* Execute the given command on every specified node */
int
on_all_nodes(int cmd_argc, char ** cmd_argv)
{
	int	i, k, num_read;
	int error = 0;
	int match = FALSE;
	int root_node = -1;
	clusternode_t 	*node_info;		/* SSI clms per-node info */


	node_info = CALLOC(MAXNODES, clusternode_t);

	num_read = cluster_membership(0, MAXNODES, node_info);
	if (num_read <= 0) {
		fprintf(stderr, "Can't get node information: %s\n",
				strerror(errno));
		free(node_info);
		exit(1);
	}

	if (dependent_nodes_flag) {
#ifdef NOTYET
		root_node = get_primary(CLUSTER_ROOT_SERVICE);	
#endif
		if (root_node == -1) {
			fprintf(stderr, "Can't determine root node: %s\n",
				strerror(errno));
			free(node_info);
			exit(1);
		}
	}

	for (i = 0; i < num_read; i++) {

		/* if this node is down, skip it */
		if (!node_info[i])
			continue;

		/* If this node has been excluded, skip it */
		if (exclude_flag) {

			for (k = 0; (k < num_read) && (exclude_nodes[k] > 0); k++) {
				if (node_info[i] == exclude_nodes[k])
					match = TRUE;
			}
			if (match == TRUE) {
				match = FALSE;
				continue;
			}
		}

		/*
		* If only dependent nodes were specified and this is the 
		* root - skip it.
		*/
		if ((dependent_nodes_flag) && 
				(node_info[i] == root_node)) {
			continue;
		}

		error |= run_on_node(cmd_argc, cmd_argv, node_info[i]);
	}
	free(node_info);
	return(error);
}

/*
 * Collect the nodes belonging to a class
 */
int
get_class_nodes(char *class, clusternode_t *class_nodes)
{
	int		i;
	int		num_read, index = 0;
	clusternode_t 	*node_info;		/* SSI clms per-node info */
	clusternode_t 	node;
	char		*list, *string;
	char *token;

	/* if the class is "none", then just return */
	if (strcmp(class, "none") == 0) {
		return(0);
	}

	/* if the class is "initnode" read /proc/1/where to get node number */
	if (strcmp(class, "initnode") == 0) {
		FILE *wfile;
		/* Make sure we're in defaultview so we can find init */
		if ((wfile = fopen("/proc/self/localview", "w"))) {
			fputs ("defaultview\n", wfile);
			fclose (wfile);
		}
		if (!(wfile = fopen("/proc/1/where", "r"))) {
			fprintf(stderr, "Can't open /proc/1/where: %s\n",
					strerror(errno));
			return(-1);
		}
		if (fscanf(wfile, "%u", &(class_nodes[0])) != 1) {
			fprintf(stderr, "Can't read /proc/1/where: %s\n",
					strerror(errno));
			fclose (wfile);
			return(-1);
		}
		fclose(wfile);
		return(1);
	}

	/* get list of up nodes */
	node_info = CALLOC(MAXNODES, clusternode_t);

	num_read = cluster_membership(0, MAXNODES, node_info);
	if (num_read <= 0) {
		fprintf(stderr, "Can't get node information: %s\n",
				strerror(errno));
		free(node_info);
		return(num_read);
	}

	/* if the class is "all", copy node_info to class_nodes */
	if (strcmp(class, "all") == 0) {
		for (i = 0; i < num_read; i++)
			class_nodes[i] = node_info[i];
		free(node_info);
		return(num_read);
	}

	/* if the list of nodes is specified, parse the list */
	if (strncmp(class, "node", 4) == 0) {
		/* parse the list */
		list = strchr(class, '=');
		if (list == NULL)
			return(0);
		list++;
		/*
		 * SSI_XXX ',' is supported for backward compatibility
		 * will go away after some time
		 */
		if (strchr(list, ',') != NULL )
			token = ",";
		else
			token = ":";

		if ((string = strtok(list, token)) != NULL) {
			do {
				/* Check for valid number */
				for (i = 0; string[i] != '\0'; i++) {
					if (isdigit(string[i]) == 0) {
						fprintf(stderr, "Invalid node number\n");
						free(node_info);
						return(-1);
					}
				}
				/* Only add to list if node is UP */
				node = atoi(string);
				for (i = 0; i < num_read; i++) {
					if (node_info[i] && node_info[i] == node) {
						class_nodes[index++] = node;
						break;
					}
				}
			} while ((string = strtok((char *)NULL, token)) != NULL);
		}
		free(node_info);
		return(index);
	}

	fprintf(stderr, "Invalid class\n");
	free(node_info);
	return(-1);
}
		

static struct option longonallopts[] = {
	{"noaction", 0, 0, '#'}, 
	{"background", 0, 0, 'b'},
	{"primary", 0, 0, 'p'},
	{"secondary", 0, 0, 's'},
	{"dependent", 0, 0, 'd'},
	{"exclude", 1, 0, 'x'},
	{"quiet", 0, 0, 'q'},
	{"force", 0, 0, 'f'},
	{"procview", 1, 0, 'v'},
	{"help", 0, 0, 'h'},
	{0, 0, 0, 0}
};

int
do_onall(int ac, char **av)
{
	int		i;
	extern int	optind;
	int		status = 0;
	int		cmd_argc = 0;
	char		*cmd_argv[256];
	char		c;
	char		*string;
	int		index;

	while ((c = getopt_long(ac, av, "+#bpsdx:qflv:h",longonallopts,NULL)) != EOF) {
		switch (c) {
			case '#':
				debug = TRUE;
				break;

			case 'b':
				run_in_background = TRUE;
				break;
			case 'q':
				list_node_numbers = FALSE;
				break;

			/* Run command on primary (root) node */
			/* Undocumented option */
			case 'p':

				all_nodes_flag = FALSE;
				primary_node_flag = TRUE;
				break;

			/* Run on secondary (backup root) node */
			/* Undocumented option */
			case 's':

				all_nodes_flag = FALSE;
				secondary_node_flag = TRUE;
				break;
			/* Run on the dependent (non-root) nodes. */
			/* Undocumented option */
			case 'd':

				dependent_nodes_flag = TRUE;
				break;
			/* Force: ignore half up. */
			/* Undocumented option. */
			case 'f':
				force = TRUE;
				break;
			/* Run on every node except this node. */
			case 'x':

				/* don't allow more than one -x flag */
				if (exclude_flag) {
					usage_onall(1);
					/* not reached */
				}
				exclude_flag = TRUE;

				/* Initialize the list of excluded nodes */
				exclude_nodes = CALLOC(MAXNODES, int);

				for (i = 0; i<MAXNODES; i++)
					exclude_nodes[i] = 0;
				
				index = 0;
				if ((string = strtok(optarg, ",")) != NULL) {
					do {

						/* Make sure the node number is valid number */
						for(i = 0; string[i] != '\0'; i++) {
							if( (isdigit(string[i]) == 0) ) {
								fprintf(stderr,
								  "Invalid node number\n");
								exit(1);
							}
						}
						exclude_nodes[index++] = atoi(string);
					} while ((string = strtok((char *)NULL, ",")) != NULL);
				}
				break;
			case 'l':
				localview = TRUE;
				break;
			case 'v':
				if(!strcmp(optarg,"local"))  {
					localview = TRUE;
				}else {
					localview = FALSE;
				}
				break;

			case 'h':
				usage_onall(0);

			default:
				usage_onall(1);
				/* not reached */
		}
	}

	/* remaining args go to cmd string */
	for (i = optind; i < ac; i++)
		cmd_argv[cmd_argc++] = av[i];

	/* if no command was supplied, do nothing */
	if (cmd_argc == 0)
		exit(0);

	if (exclude_flag && 
		(primary_node_flag || secondary_node_flag || dependent_nodes_flag)) {
		fprintf(stderr, 
			"-x flag may not be used with -p, -s or -d\n");
		usage_onall(1);	
		/* not reached */
	}

	if (dependent_nodes_flag && (primary_node_flag || secondary_node_flag)) {
		fprintf(stderr,
			"-d flag may not be used with -p or -s\n");
		usage_onall(1);	
		/* not reached */
	}

	signal(SIGINT, quit);

	if (!all_nodes_flag) {
		if (primary_node_flag)
			status |= on_primary_node(cmd_argc, cmd_argv);
		if (secondary_node_flag)
			status |= on_secondary_node(cmd_argc, cmd_argv);
	}
	else {
		status = on_all_nodes(cmd_argc, cmd_argv);
	}

	if (run_in_background) {
		while(wait(NULL) != -1);
	}

	if (status)
		exit(1);
	else
		exit(0);

	return 0;
}


static struct option longonsvcopts[] = {
	{"noaction", 0, 0, '#'}, 
	{"background", 0, 0, 'b'},
	{"quiet", 0, 0, 'q'},
	{"force", 0, 0, 'f'},
	{"localnode", 0, 0, 'L'},
	{"procview", 1, 0, 'v'},
	{"help", 0, 0, 'h'},
	{0, 0, 0, 0}
};
int
do_onsvc(int ac, char **av)
{
	int		i, num_read, svc_index;
	extern int	optind;
	int		cmd_argc = 0;
	char		*cmd_argv[256];
	char		svc_lock_file[256];
	char		c;
	struct stat	buf;
	clusternode_t	local_node = 0;
	int		error = 0;
	clusternode_t 	*node_info;

	while ((c = getopt_long(ac, av, "+#bqfLl",longonsvcopts,NULL)) != EOF) {
		switch (c) {
			case '#':
				debug = TRUE;
				break;

			case 'b':
				run_in_background = TRUE;
				break;
			case 'q':
				list_node_numbers = FALSE;
				break;
			case 'L':
				local_node_only = TRUE;
				break;
			/* Force: ignore half up. */
			/* Undocumented option. */
			case 'f':
				force = TRUE;
				break;
			case 'l':
				localview = TRUE;
				break;
			case 'v':
				if(!strcmp(optarg,"local"))  {
					localview = TRUE;
				}else {
					localview = FALSE;
				}
				break;
			case 'h':
				usage_onsvc(0);
			default:
				usage_onsvc(1);
				/* not reached */
		}
	}

	/* next argument should be the service name */
	if (optind == ac)
		usage_onsvc(1);
	svc_index = optind;

	/* increment past service name */
	optind++;

	/* remaining args go to cmd string */
	for (i = optind; i < ac; i++)
		cmd_argv[cmd_argc++] = av[i];

	/* if no command was supplied, do nothing */
	if (cmd_argc == 0)
		exit(0);

	if (local_node_only)
		local_node = clusternode_num();

	node_info = CALLOC(MAXNODES, clusternode_t);

	num_read = cluster_membership(0, MAXNODES, node_info);
	if (num_read <= 0) {
		fprintf(stderr, "Can't get node information: %s\n",
			strerror(errno));
		free(node_info);
		exit(1);
	}

	sprintf(svc_lock_file, "/etc/init.d/%s", av[svc_index]);
	if (stat(svc_lock_file, &buf) < 0) {
		fprintf(stderr, "warning: service \"%s\" not in /etc/init.d\n",
			av[svc_index]);
	}

	/* run only on nodes where the lock file for the service exists */
	for (i = 0; i < num_read; i++) {
		if (!node_info[i])
			continue;
		if (local_node_only && node_info[i] != local_node)
			continue;
		sprintf(svc_lock_file, SVC_LOCK_FILE, node_info[i], av[svc_index]);
		if (stat(svc_lock_file, &buf) < 0) {
			continue;
		} else {
			error |= run_on_node(cmd_argc, cmd_argv, node_info[i]);
		}
	}
	return(error);
}


static struct option longonclassopts[] = {
	{"noaction", 0, 0, '#'}, 
	{"background", 0, 0, 'b'},
	{"quiet", 0, 0, 'q'},
	{"localnode", 0, 0, 'L'},
	{"force", 0, 0, 'f'},
	{"procview", 1, 0, 'v'},
	{"help", 0, 0, 'h'},
	{0, 0, 0, 0}
};
int
do_onclass(int ac, char **av)
{
	int		i, num_nodes;
	extern int	optind;
	int		cmd_argc = 0;
	char		*cmd_argv[256];
	char		c;
	clusternode_t	*class_nodes;
	clusternode_t	local_node = 0;
	int		error = 0;

	while ((c = getopt_long(ac, av, "+#bqLflv:h",longonclassopts,NULL)) != EOF) {
		switch (c) {
			case '#':
				debug = TRUE;
				break;

			case 'b':
				run_in_background = TRUE;
				break;
			case 'q':
				list_node_numbers = FALSE;
				break;
			case 'L':
				local_node_only = TRUE;
				break;
			/* Force: ignore half up. */
			/* Undocumented option. */
			case 'f':
				force = TRUE;
				break;
			case 'l':
				localview = TRUE;
				break;
			case 'v':
				if(!strcmp(optarg,"local"))  {
					localview = TRUE;
				}else {
					localview = FALSE;
				}
				break;
			case 'h':
				usage_onclass(0);
			default:
				usage_onclass(1);
				/* not reached */
		}
	}
	/* next argument should be the class name */
	if (optind == ac)
		usage_onclass(1);

	if (local_node_only)
		local_node = clusternode_num();

	/* get the list of nodes beloging to this class */
	class_nodes = CALLOC(MAXNODES, clusternode_t);
	num_nodes = get_class_nodes(av[optind], class_nodes);

	if (num_nodes <= 0) {
		free(class_nodes);
		return(num_nodes);
	}

	/* increment past class name */
	optind++;

	/* remaining args go to cmd string */
	for (i = optind; i < ac; i++)
		cmd_argv[cmd_argc++] = av[i];

	for (i = 0; i < num_nodes; i++) {
		if (local_node == 0 || local_node == class_nodes[i])
			error |= run_on_node(cmd_argc, cmd_argv, class_nodes[i]);
	}

	free(class_nodes);
	return(error);
}

/*----------------------------------------------*/

int
main(int ac, char **av)
{
	char	*command;


	/*
	* Make sure we are running on a SSI kernel
	*/
	if (cluster_ssiconfig() != 1) {
		fprintf(stderr, "This command requires an OpenSSI kernel.\n");
		exit(1);
	}

	command = (char *) strrchr(av[0], '/');
	if (command == NULL)
		command = av[0];
	else
		++command;

	if (strcmp(command, "onsvc") == 0) {
		return do_onsvc(ac, av);
	} else if (strcmp(command, "onclass") == 0) {
		return do_onclass(ac, av);
	} else {
		return do_onall(ac, av);
	}
	/* NOTREACHED */
	exit(-1);
}
