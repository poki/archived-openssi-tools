/*
 * 	loads command
 *	Copyright 2002 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
 *
 */
/*
 *	loads.c :
 *
 *
 *	syntax :
 *		loads 
 *		loads -n <node #>
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <libgen.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/cluster.h>


extern char *optarg;
extern int optind, opterr, optopt;

extern int cluster_ssiconfig(void);

#define LOADSTATUS_FILE	"/.loadlevel_status"

clusternode_t *node_info;
int node_info_cnt;

void do_loads_read(clusternode_t);


static struct option longopts[] = {
	{"format", 0, 0, 'w'},
	{"node", 1, 0, 'n'},
	{0, 0, 0, 0}
};

void
pr_usage(
	char	*pname)
{
	fprintf(stderr,/* MM_ERROR, */
	     "Usage...\n"
	     "\t%s [-w] [-n #]	Display loads for all nodes \n"
	     "\t\t-w,--format 		Display loads in 5 columns for all nodes\n"
	     "\t\t-n,--node #		Display load for a specific node \n",
	     basename(pname));
}

int main(
	int		argc,
	char		*argv[])
{
	char	opt;
	int	i,wide=0;
	clusternode_t node = 0;
	int 	allnodes = 1;

	/*
	 * Make sure we are running on a OpenSSI kernel
	 */
	if (cluster_ssiconfig() != 1) {
		fprintf(stderr,/* MM_HALT  ,  */
		     "This command requires an OpenSSI kernel.\n");
		exit(1);
	}

	while ((opt = getopt_long(argc, argv, "wn:",longopts,NULL)) != EOF) {
		switch(opt) {
		case 'n':
			node = atoi(optarg);
			allnodes = 0;
			break;
		case 'w':
			wide = 1;
			break;
		default:
			pr_usage(argv[0]);
			exit(1);
		}
	}

	if (optind < argc) {
		pr_usage(argv[0]);
		exit(1);
	}

	if (allnodes) {
		/* Get UP nodes */
		node_info = calloc(cluster_maxnodes(), sizeof(clusternode_t));

		node_info_cnt = cluster_membership(0, cluster_maxnodes(), 
								node_info);
		
		/* Cycle through UP nodes */
		for(i = 0; i < node_info_cnt; i++) {
			/* if this node is down, skip it */
			if (!node_info[i])
				continue;
			do_loads_read(node_info[i]);
			if (wide)
				printf("%c", (((i+1) % 5) ? ' ' : '\n'));
			else
				printf("\n");
		}
		
	}
	if (node) { 
		int ret;

		ret = clusternode_avail(node);
		if( ret != 1 ) {
			printf("error: %u invalid node number or node not up\n",node);
			exit(1);
		}

		do_loads_read(node);
		printf("\n");
	}

	if (wide && allnodes && (node_info_cnt % 5))
		printf("\n");

	exit(0);
}

void
do_loads_read(clusternode_t node)
{
	char filename[256];
	char buf[1024];
	int fd, r;

	sprintf(filename, "/proc/cluster/node%u/load",node);
	fd = open(filename, O_RDONLY);
	if (fd == -1) {
		printf("failed to open %s \n", filename);
		exit(1);
	}
	r = read(fd, buf, 1024);
	close(fd);

	buf[r-1]='\0';
	printf("%u:	%s ", node, buf);
	sprintf(filename, "/proc/cluster/node%u/loadlevel",node);
	fd = open(filename, O_RDONLY);
	if (fd == -1) {
		printf("failed to open %s \n", filename);
		exit(1);
	}
	r = read(fd, buf, 1024);
	close(fd);
	buf[1]='\0';
	printf("%c\t", (!strcmp(buf, "1") ? ' ' : '*'));

}

