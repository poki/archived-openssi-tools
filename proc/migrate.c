/*
 *	Copyright 2003 Hewlett-Packard Company
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
 *
 */

#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <linux/cluster.h>

static char *myname;

void
usage_exit(int exitstat)
{
	fprintf(stderr, "usage: %s node id ...\n", myname);
	exit(exitstat);
}

int
main(argc, argv)
	int argc;
	char *argv[];
{
	int errflag = 0;
	long nodearg;
	char gotostr[80];
	char nodestr[80];
	int nin;
	int nout;
	int fd = -1;

	int pid;
	int idx;			/* indexes args on cmd line */
	int error;
	char *p;

        if (!cluster_ssiconfig()) {
                fprintf(stderr, /* MM_HALT ,  */
			"This command requires an OpenSSI kernel.\n");
                exit(1);
        }

	myname = strrchr(argv[0], '/');
	if (myname)		/* point 1 past the slash we found */
		++myname;
	else
		myname = argv[0];

	idx = 1;			/* we start with argv[1] */
	if (argc < 3)
		usage_exit(1);
	/* now deal with the nodearg */
	nodearg = strtol(argv[idx], &p, 10);
	if (p == argv[idx] ||  *p != '\0') {
		fprintf(stderr, "%s:%s is not a numeric arg.\n",
			myname, argv[idx]);
		exit(1);
	}
	idx++;
	if (clusternode_avail( nodearg ) != 1) {
		fprintf(stderr, "%s:Node %ld is not in the cluster\n",
			myname, nodearg);
		exit(2);
	}
	for ( ; idx < argc; ++idx ) {
		pid = strtol(argv[idx], &p, 10);
		if (pid < 0 && !errno) {
			nodearg = -nodearg;
			pid = -pid;
		}
		
		if (p == argv[idx] || *p != '\0') {
			fprintf(stderr,
				"%s: non-numeric process_ID %s skipped.\n",
				myname, argv[idx]);
			continue;
		}
		(void)snprintf(gotostr, sizeof(gotostr), "/proc/%d/goto", pid);
		nin = snprintf(nodestr, sizeof(nodestr), "%ld\n", nodearg);

		error = 0;
		fd = open(gotostr, O_WRONLY);
		if (fd == -1)
			error = errno;
		if (!error) {
			nout = write(fd, nodestr, nin);
			if (nout == -1)
				error = errno;
		}
		if (fd != -1 && close(fd) == -1)
			error = errno;
		if (error) {
			if (error == EPERM) {
				fprintf(stderr,
					"%s: %s Not owner\n",
					myname, argv[idx]);
			} else if (error == ENOENT || error == ESRCH) {
				fprintf(stderr,
					"%s:%s: No such process\n",
					myname, argv[idx]);
			} else if (error == EINVAL) {
				fprintf(stderr, 
					"%s: process %s has exited"
					"/is exiting\n",
					 myname, argv[idx]);
			} else
				fprintf(stderr,
				        "%s: migrating %s returned error %d\n",
					myname, argv[idx], error);
			++errflag;
		}
	}

	if (errflag)
		exit(1); 
	exit(0);
}

