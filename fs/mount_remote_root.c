#include <stdio.h>
#include <stdlib.h>
#include <linux/cluster.h>

int
main(int argc, char *argv[])
{
	if (argc != 2) {
		fprintf(stderr, "usage: %s path\n", argv[0]);
		exit(1);
	}
	if (mount_remote_root(argv[1]) == -1)
		exit(1);
	exit(0);
}
