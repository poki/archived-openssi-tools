#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/mount.h>
#include <linux/cluster.h>
#include <cluster/ssisys.h>

int cfs_setroot(char *type, char *dev_name)
{
	ssisys_iovec_t  iovec;
	cfs_setroot_args_t inargs;

	inargs.type = type;
	inargs.dev_name = dev_name;
	
	iovec.tio_id.id_cmd = SSISYS_CFS_SETROOT;
	iovec.tio_id.id_ver = SSISYS_CURVER; 
	iovec.tio_udatain = (caddr_t)&inargs;
	iovec.tio_udatainlen = sizeof(inargs);
	iovec.tio_udataout = (caddr_t)NULL;
	iovec.tio_udataoutlen = 0;

	return ssisys((char *)&iovec, sizeof(iovec));
}

int
main(int argc, char *argv[])
{
	int ret;

	if (argc != 3)
		goto usage;

	ret = cfs_setroot(argv[1], argv[2]);
	if (ret == -1) {
		perror("cfs_setroot");
		exit(1);
	}
	exit(0);

usage:
	fprintf(stderr, "usage: %s type device\n", argv[0]);
	exit(1);
	
}
