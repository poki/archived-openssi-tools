#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/cluster.h>

#define	FALSE	(0)
#define	TRUE	(!(FALSE))

int
main(int argc, char *argv[])
{
	int donfs = FALSE;

	if (argc == 2 && strcmp(argv[1], "nfs") == 0)
		donfs = TRUE;
		
	discover_mounts(donfs);

	return 0; 
}
