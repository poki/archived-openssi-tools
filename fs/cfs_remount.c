#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/mount.h>
#include <linux/cluster.h>

int
main(int argc, char *argv[])
{
	int ret;
	int flags = 0;
	char buffer[1024];
	int ssidev;

	if (argc != 4)
		goto usage;

	(void)strcpy(buffer, "0x");
	(void)strcat(buffer, argv[3]);
	ssidev = (int)strtol(buffer, NULL, 16);

	ret = cfs_remount(argv[2], ssidev, argv[1], flags, NULL);
	if (ret == -1) {
		perror("cfs_remount");
		exit(1);
	}
	exit(0);

usage:
	fprintf(stderr, "usage: %s type device ssidev\n", argv[0]);
	exit(1);
	
}
