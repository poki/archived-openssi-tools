#	CVIP.pm -- routines for reading lvs configuration file /etc/cvip.conf
#
#	Date:		April 26 2004
#	Authors:	Roopa Prabhu <roopap@india.hp.com>
#			keerthi Bhushan <kbhushan@india.hp.com>
#
#	Copyright (c) 2001-3 Hewlett-Packard Company
#
#	This program is free software; you can redistribute it and/or
#	modify it under the terms of the GNU General Public License as 
#	published by the Free Software Foundation; either version 2 of 
#	the License, or (at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
#	or NON INFRINGEMENT.  See the GNU General Public License for more
#	details.
#
# 	You should have received a copy of the GNU General Public License
# 	along with this program; if not, write to the Free Software
# 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
#	Questions/Comments/Bugfixes to ssic-linux-devel@lists.sf.net


package CVIP;
use XML::Simple;
use Data::Dumper;

our @ISA = qw(Exporter);
our @EXPORT = qw(
		get_cvips
		get_master_director
		get_directors
		get_realservers
		get_backup_directors
		is_node_master_director
		node_role
		get_garp_interface
		is_director
);

my $cvipconf = '/etc/cvip.conf';
my $config = XMLin($cvipconf, forcearray => ["cvip","director_node","real_server_node"]);

my $cvips = $config->{cvip};

#
#	Returns all cvips 
#
sub get_cvips {

	my @cvips_arr;

	foreach(@$cvips) {
		push @cvips_arr,$_->{ip_addr};
	}
	return \@cvips_arr;
}

#
# Get master director for a given cvip 
#
sub get_master_director($)  {

	my $cvip_arg = shift; 

	foreach (@$cvips) {
		if ( $_->{ip_addr} eq $cvip_arg ) {
			return ($_->{director_node}[0]->{node_num});
		}
	}
	return 0;
}

#
# Return all directors for a given cvip 
#
sub get_directors($) {

	my $cvip_arg = shift; 
	my @ret_directors;

	foreach (@$cvips) {

		if ( $_->{ip_addr} eq $cvip_arg ) {

			my $directors = $_->{director_node};

			foreach(@$directors) {
				push @ret_directors,$_->{node_num};
			}
			break;
		}
	}
	return \@ret_directors;	
}

#
# Return all real servers  for a given cvip 
#
sub get_realservers($) {

	my $cvip_arg = shift; 
	my @ret_realservers;

	foreach (@$cvips) {

		if ( $_->{ip_addr} eq $cvip_arg ) {

			my $realservers = $_->{real_server_node};

			foreach(@$realservers) {

				if ($_->{node_num} =~ /([^@])+-(.+)/) {
					my $lo = $1;
					my $hi = $2;
					my $i;

					for ($i=$lo; $i <= $hi; $i++) {
						push @ret_realservers,$i;
					}
				} else {
					push @ret_realservers,$_->{node_num};
				}
			}
			break;
		}
	}
	return \@ret_realservers;	
}


#
# Return backup directors for a given cvip 
#
sub get_backup_directors($)  {

	my $cvip_arg = shift; 
	my $backup_directors ;

	foreach (@$cvips) {
		if ( $_->{ip_addr} eq $cvip_arg ) {

			my $directors = $_->{director_node};
			my $i = 0;
			foreach(@$directors) {
				if( $i eq 0 ) { 
					# Skip the first director, since its 
					the master director
					$i = 1;
				}
				else {
					push @backup_directors,$_->{node_num};
				}
			}
		}
	}
	return \@backup_directors;
}


# 
# Check if a given node is a master director for any of the cvips 
#
sub is_node_master_director($) {

	my $node = shift; 

	foreach (@$cvips) {
		if( $_->{director_node}[0]->{node_num} == $node ) {
			return 1;
		}
	}
	return 0;
}

# 
# This routine returns role of a node 
# for a given cvip
# Arguments :
#	cluster virtual ip and 	node num
# Returns 
#	0 if node not found 
#	1 if director node 
#	2 if real server node
#
sub node_role($$) {

	my $cvip = shift; 
	my $node = shift; 

	foreach (@$cvips) {
		if ( $_->{ip_addr} eq $cvip ) {

			my $directors = $_->{director_node};

			foreach(@$directors) {
				if( $_->{node_num} == $node ) {
					return 1; # director
				}
			}

			my $realservers = $_->{real_server_node};

			foreach(@$realservers) {
				if ($_->{node_num} =~ /([^@])+-(.+)/) {
					my $lo = $1;
					my $hi = $2;
					my $i;

					for ($i=$lo; $i <= $hi; $i++) {
						if ($i == $node) {
							return 3;	
						}
					}
				} elsif ($_->{node_num} == $node) {
					return 2; #  real server 
				}
			}
			break;
		}
	}
	return 0;
}

#
#	Returns arp interface for a given cvip and director node
#	
sub get_garp_interface($$) {

	my $cvip = shift; 
	my $node = shift; 
	my $garp_interface ;

	foreach (@$cvips) {
		if ( $_->{ip_addr} eq $cvip ) {

			my $directors = $_->{director_node};

			foreach(@$directors) {
				if( $_->{node_num} == $node ) {
					return ($_->{garp_interface});
				}
			}
			die "ERROR: Could not get garp_interface,Node ",$node," not a director node for cvip ",$cvip,"\n"; 
			return 0;
		}
	}

	die "ERROR : Could not get garp_interface, cvip ",$cvip," not found \n";
	return 0;
}

#
# Is node a director node 
#
sub is_director($) {

	my $node = shift;
	my @ret_directors;

	foreach (@$cvips) {

		my $directors = $_->{director_node};

		foreach(@$directors) {
			if ( $_->{node_num} == $node ) {
				return 1;
			}
		}
	}
	return 0;
}



