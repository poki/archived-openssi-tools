/*
 * Date   :	April 11 2003
 * Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
 *
 *
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License as
 *      published by the Free Software Foundation; either version 2 of
 *      the License, or (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *      or NON INFRINGEMENT.  See the GNU General Public License for more
 *      details.
 *
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <errno.h>
#include <linux/cluster.h>
#include <netinet/in.h>
#include "ip_vs.h"


static void usage()
{
	fprintf(stderr,"setport_weight	OPTIONS\n");
	fprintf(stderr,"\t--start-port=value ( The start port )\n");
	fprintf(stderr,"\t--end-port=value   ( The end port )\n");
	fprintf(stderr,"\t--weight=value     ( The loadbalancing weight)\n");
	fprintf(stderr,"\t--proto=value      ( The protocol)\n");
	fprintf(stderr,"\t--sched=value      ( The scheduler)\n");

	exit(1);
}
int
main(int argc ,char *argv[])
{
	static struct option long_options[] = {
		{"start-port",1,0,'s'},
		{"end-port",1,0,'e'},
		{"weight",1,0,'w'},
		{"proto",1,0,'p'},
		{"sched",1,0,'c'},
		{0,0,0,0}
	};
	short start_port = -1,end_port=-1;
	int weight=-1,ret;
	unsigned short protocol = IPPROTO_TCP;
	char lvs_sched [IP_VS_SCHEDNAME_MAXLEN] = "wlc";

	while(1) {
		ret = getopt_long(argc,argv,"",long_options,NULL);
		if (ret == -1)
			break;
		switch(ret) {
			case 's':
				start_port = atoi(optarg);
				break;
			case 'e':
				end_port = atoi(optarg);
				break;
			case 'w':
				weight = atoi(optarg);
				break;
			case 'p':
				if (strcmp(optarg, "tcp") == 0)
					protocol=IPPROTO_TCP;
				else 
					protocol=IPPROTO_UDP;

				break;
			case 'c':
				strncpy(lvs_sched, optarg, IP_VS_SCHEDNAME_MAXLEN);
				lvs_sched[IP_VS_SCHEDNAME_MAXLEN-1] = '\0';
				break;
		}
	}
	if (start_port == -1 || end_port == -1 || weight == -1 )
		usage();
	if( set_ipvsportweight(start_port, end_port, weight, protocol, lvs_sched) < 0 ) {
		fprintf(stderr, "%s\n",strerror(errno));
		return 1;
	}
	return 0;
}
