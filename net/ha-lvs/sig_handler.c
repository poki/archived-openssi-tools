/*
 * Date   :	Jun 19 2002
 * Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
 *
 *
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License as
 *      published by the Free Software Foundation; either version 2 of
 *      the License, or (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *      or NON INFRINGEMENT.  See the GNU General Public License for more
 *      details.
 *
 *
 */

#include <signal.h>
#include <linux/cluster.h>
#include <syslog.h>
#include <sys/wait.h>
#include "ipvswrap.h"
#include "garp.h"

#include  "lvs_ha.h"
#include  "interface.h"
#include  "ci-linux.h"


static void handle_masternode_down(void)
{
	/*
	 * The node went down is a master director
	 */
	FILE *fp;
	clusternode_t nodenum;
	struct cvip_list *clist;

	fp = get_cvip_filelock();
	if (fp == NULL) {
		syslog(LOG_ERR, "Error in getting cvip file lock\n");
		exit(1);
	}

	clist = get_cvip(this_node);

	do {
		if ((nodenum = select_master_director(clist)) == 0) {
			syslog(LOG_ERR,
			       "This is not supposed to happen. Possibly wrong clist  Exiting.....\n");
			exit(1);
		}
		/*
		 * before calling select_master_director again
		 * set  master
		 */
		set_as_master(clist, nodenum);
	} while (nodestatus(nodenum) <= 0);

	if (nodenum == this_node) {

		/* 
		 * I am the one who got selected as the master 
		 * hurray!!!! 
		 */
		lvs_sync_daemon("stop", "backup",
				get_multicast_interface(this_node));
		lvs_sync_daemon("start", "master",
				get_multicast_interface(this_node));
		announce_as_master(fp, clist);
		/* 
		 * send GARP
		 */
		send_clist_garp(clist, get_garp_interface(this_node));

	}
	if (rel_cvip_filelock(fp) < 0) {
		syslog(LOG_ERR, "Error in releasing cvip file lock\n");
		exit(1);
	}
	return;
}

void node_event_handler(void)
{
	cluster_transinfo_t transinfo;

	syslog(LOG_INFO, "Node event\n");

	if (!is_director(this_node)) {
		/* 
		 * Node down event need to be handled only by
		 * master and back director nodes
		 */
		return;
	}

	while (cluster_detailedtransition(CLUSTER_TRANSWHICH_NEXT,
					  &transid, sizeof(cluster_transinfo_t),
					  &transinfo)) {
		if (transinfo.tostate == CLUSTERNODE_KCLEANUP) {

			/* 
			 * check for the down node is master director
			 * If yes one of the other director should
			 * become master
			 */

			if (is_masterdirector(transinfo.transnode)) {
				handle_masternode_down();
			}
			if (is_masterdirector(this_node)) {
				if (is_realserver(transinfo.transnode)) {
					char *addr =
					    node_to_address(transinfo.
							    transnode);
					ipvs_cleanup_realserver(addr);
				}

			}

		} 
	}
	return;
}

void child_event_handler(int signum)
{
	/* NOTREACHED */
}

static void master_director_terminate(struct cvip_conf *cvip_confp)
{
	struct real_server_node *trnode;
	struct real_server_node *rnode;

	rnode = cvip_confp->rnode;

	/*
	 * cleanup all services registered with respect to the 
	 * real servers
	 */
	for (trnode = rnode; trnode != NULL; trnode = trnode->next) {

		char *addr;
		addr = node_to_address(trnode->node);
		ipvs_cleanup_realserver(addr);
	}

	/*
	 *      Shutdown my configured interfaces
	 */
	unconfigure_masterdirector_inf(cvip_confp);

	del_cvip(cvip_confp->cvip);
	lvs_sync_daemon("stop", "master", get_multicast_interface(this_node));

}
static void backup_director_terminate(struct cvip_conf *cvip_confp)
{
	unconfigure_backupdirector_inf(cvip_confp);
	lvs_sync_daemon("stop", "backup", get_multicast_interface(this_node));
	return;

}
static void director_node_terminate(void)
{
	FILE *fp;
	struct cvip_conf *cvip_confp;
	struct director_node *tdnode;
	struct director_node *dnode;
	clusternode_t nodenum;

	fp = get_cvip_filelock();
	if (fp == NULL) {
		syslog(LOG_ERR, "Error in getting cvip file lock\n");
		exit(1);
	}

	/*
	 * Move through the list of cvips 
	 */
	for (cvip_confp = cvipheadp; cvip_confp != NULL;
	     cvip_confp = cvip_confp->next) {

		/*
		 * Get pointer to all director nodes for this cvip
		 */
		dnode = cvip_confp->dnode;
		for (tdnode = dnode; tdnode != NULL; tdnode = tdnode->next) {

			/*
			 * If this_node is a director for this cvip 
			 */
			if (tdnode->node == this_node) {

				/*
				 * Get current master director 
				 */
				nodenum = get_master_from_file(fp,
						cvip_confp-> cvip);
				/*      
				 * If this node is the current master director 
				 */
				if (nodenum == this_node) {
					master_director_terminate(cvip_confp);
					remove_cvip_entry(fp, cvip_confp->cvip);
				} else {	/* I am a director-backup */
					backup_director_terminate(cvip_confp);
				}
				lvs_flush_virtual_table();
				break;
			}
		}	/*end  for loop */
	}
	if (rel_cvip_filelock(fp) < 0) {
		syslog(LOG_ERR, "Error in releasing cvip file lock\n");
		exit(1);
	}

	return;
}

static void realserver_terminate(void)
{
	FILE *fp;
	struct cvip_conf *cvip_confp;
	struct real_server_node *trnode;
	struct real_server_node *rnode;

	fp = get_cvip_filelock();
	if (fp == NULL) {
		syslog(LOG_ERR, "Error in getting cvip file lock\n");
		exit(1);
	}

	/*
	 * Move through the list of cvips 
	 */
	for (cvip_confp = cvipheadp; cvip_confp != NULL;
	     cvip_confp = cvip_confp->next) {

		rnode = cvip_confp->rnode;

		for (trnode = rnode; trnode != NULL; trnode = trnode->next) {
			/*
			 * If this is the real server node 
			 */
			if (trnode->node == this_node) {
				unconfigure_realserver_inf(cvip_confp);
			}
		}
	}

	if (rel_cvip_filelock(fp) < 0) {
		syslog(LOG_ERR, "Error in releasing cvip file lock\n");
		exit(1);
	}
	return;
}
void sigterm_handler(int signum)
{

	if (is_director(this_node)) {
		director_node_terminate();
	} else {
		realserver_terminate();
	}


	/*
	 * Set all services port weight to zero
	 */
	unconfigure_services(cvipheadp);

	remove_pidfile();
	exit(0);
}

void signal_init()
{
	struct sigaction saction;
	sigset_t set;

	sigemptyset(&set);

	/* 
	 * Don't handle SIGCLUSTER and SIGCHLD
	 * in this thread
	 */
	sigaddset(&set, SIGCLUSTER);
	sigaddset(&set, SIGCHLD);
	sigaddset(&set, SIGTERM);

	/* Block SIGCLUSTER / SIGCHLD
	 * leave everything else alone so that "root" can terminate
	 * using regular SIGTERM
	 */
	if (sigprocmask(SIG_BLOCK, &set, NULL) < 0) {
		syslog(LOG_ERR, "Error in  masking the signal\n");
		exit(1);
	}

	/*
	 * Geting SIGCHLD out of ignore for sigwait() in main()
	 */
	saction.sa_handler = child_event_handler;
	saction.sa_mask = set;
	saction.sa_flags = SA_NOMASK;

	saction.sa_handler = child_event_handler;
	sigaction(SIGCHLD, &saction, NULL);
}
