/*
 * Date   :	Jun 19 2002
 * Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
 *
 *
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License as
 *      published by the Free Software Foundation; either version 2 of
 *      the License, or (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *      or NON INFRINGEMENT.  See the GNU General Public License for more
 *      details.
 *
 *
 */
#include <unistd.h>
#include <sys/types.h>

#include "ci-linux.h"
#include "interface.h"

extern int init_directorhead(struct cvip_conf **);

#define SKIP_LINE(buffer,size,filep) do { \
				fgets(buffer,size,filep); \
			}while(buffer[(strlen(buffer)) -1] != '\n' )

/*
 * head pointer to the cvip_conf list
 * This contain information regarding all CVIP and nodes
 */
struct cvip_conf *cvipheadp;

static nodenum_ip_map_t *nodemap;

/*
 * You should pass an allocated nodenum_ip_map_t
 * nodenum_ip_map_t nodemap[max_node_number];
 */
static int initialize_nodemap(nodenum_ip_map_t * nodemap)
{
	FILE *fp;
	char buff[BUFF_SIZE];
	int node_number;

	if ((fp = fopen(CLUSTERTAB, "r")) == NULL)
		return -1;

	while (fscanf(fp, "%s", buff) != EOF) {
		if (buff[0] == '#') {
			SKIP_LINE(buff, BUFF_SIZE, fp);
			memset(buff, 0, BUFF_SIZE);
			continue;
		}
		node_number = atoi(buff);
		if (node_number > cluster_maxnodes()) {
			syslog(LOG_ERR,
			       " Node number greater than MAX node num\n");
			fclose(fp);
			return -1;
		}
		if (fscanf(fp, "%s", buff) == EOF) {
			syslog(LOG_ERR, " %s File Format Error", CLUSTERTAB);
			fclose(fp);
			return -1;
		}
		strncpy(nodemap[node_number].addr_ip, buff, DOTIPLEN);;
		SKIP_LINE(buff, BUFF_SIZE, fp);
		memset(buff, 0, BUFF_SIZE);
	}
	fclose(fp);
	return 1;
}

/*
 * return value 1 indicate the node is COMINGUP/UP
 * return value 0 indicate the node is DOWN
 * return value -1 indicate error
 */
int nodestatus(clusternode_t node_num)
{
	clusternode_info_t ni;

	if (clusternode_info(node_num, sizeof(ni), &ni) >= 0) {

		if (ni.node_state == CLUSTERNODE_COMINGUP ||
		    ni.node_state == CLUSTERNODE_UP) {
			return 1;
		} else {
			return 0;
		}
	} else {
		syslog(LOG_ERR, " Error in getting the cluster information");
	}

	return -1;
}

/*
 * Convert the node number to IP address 
 */
char *node_to_address(clusternode_t node_num)
{
	if (node_num > cluster_maxnodes()) {
		syslog(LOG_ERR, "Node number greater than Max node num \n");
		return NULL;
	}

	return (nodemap[node_num].addr_ip);

}

int init_director()
{
	if (init_directorhead(&cvipheadp) < 0)
		return -1;
	nodemap =
	    (nodenum_ip_map_t *) malloc(sizeof(nodenum_ip_map_t) *
					cluster_maxnodes() + 1);
	if (nodemap == NULL) {
		syslog(LOG_ERR, "Error in initializing node map from %s\
					( out of memory)\n", CLUSTERTAB);
		return -1;
	}
	if (initialize_nodemap(nodemap) < 0)
		return -1;
	return 1;

}

/* 
 * 0 if the nodenum is not a director node
 * 1 if the nodenum is  a director node
 */
int is_director(clusternode_t nodenum)
{
	struct cvip_conf *cvip_confp;
	struct director_node *dnode;
	struct director_node *tdnode;

	for (cvip_confp = cvipheadp; cvip_confp != NULL;
	     cvip_confp = cvip_confp->next) {

		dnode = cvip_confp->dnode;
		for (tdnode = dnode; tdnode != NULL; tdnode = tdnode->next) {

			if (tdnode->node == nodenum)
				return 1;
		}
	}
	return 0;

}

/*
 * 0 if the nodenum is not a realserver node
 * 1 if the nodenum is  a realserver node
 */
int is_realserver(clusternode_t nodenum)
{
	struct cvip_conf *cvip_confp;
	struct real_server_node *rnode;
	struct real_server_node *trnode;

	for (cvip_confp = cvipheadp; cvip_confp != NULL;
	     cvip_confp = cvip_confp->next) {

		rnode = cvip_confp->rnode;
		for (trnode = rnode; trnode != NULL; trnode = trnode->next) {

			if (trnode->node == nodenum)
				return 1;
		}
	}
	return 0;

}

/*
 * return 1 is this node is the master director for any CVIP
 * return 0 if it is not
 */
int is_masterdirector(clusternode_t nodenum)
{

/* 
 * When adding a node as a director for more than one CVIP
 *  make sure the same set of directors are
 * used for failover.  A node can't be the master director for one
 * CVIP and backup for another CVIP.That is if i find a node as the
 * director under any CVIP and if it is  not the master he can't be 
 * the master for any other CVIP that he could possibly handle.
 */
	struct cvip_conf *cvip_confp;

	for (cvip_confp = cvipheadp; cvip_confp != NULL;
	     cvip_confp = cvip_confp->next) {

		if (cvip_confp->master_node == nodenum)
			return 1;
	}

	return 0;

}

/*
 * IF a node is the director for more than one CVIP 
 * for all the CVIP it is assumed that it uses the same
 *  mulitcast interface.
 */
char *get_multicast_interface(clusternode_t nodenum)
{
	struct cvip_conf *cvip_confp;
	struct director_node *dnode;
	struct director_node *tdnode;

	for (cvip_confp = cvipheadp; cvip_confp != NULL;
	     cvip_confp = cvip_confp->next) {

		dnode = cvip_confp->dnode;
		for (tdnode = dnode; tdnode != NULL; tdnode = tdnode->next) {

			if (tdnode->node == nodenum)
				return tdnode->multicast_interface;
		}
	}
	return NULL;

}

/*
 * IF a node is the director for more than one CVIP 
 * for all the CVIP it is assumed that it uses the same
 * gratious  interface.
 */
char *get_garp_interface(clusternode_t nodenum)
{

	struct cvip_conf *cvip_confp;
	struct director_node *dnode;
	struct director_node *tdnode;

	for (cvip_confp = cvipheadp; cvip_confp != NULL;
	     cvip_confp = cvip_confp->next) {

		dnode = cvip_confp->dnode;
		for (tdnode = dnode; tdnode != NULL; tdnode = tdnode->next) {

			if (tdnode->node == nodenum)
				return tdnode->garp_interface;
		}
	}
	return NULL;

}
static int get_cvip_no(clusternode_t nodenum)
{
	struct cvip_conf *cvip_confp;
	struct director_node *dnode;
	struct director_node *tdnode;
	int cvip_no = 0;

	for (cvip_confp = cvipheadp; cvip_confp != NULL;
	     cvip_confp = cvip_confp->next) {

		dnode = cvip_confp->dnode;
		for (tdnode = dnode; tdnode != NULL; tdnode = tdnode->next) {

			if (tdnode->node == nodenum) {
				cvip_no += 1;
				break;
			}
		}
	}
	return cvip_no;

}

struct cvip_list *get_cvip(clusternode_t nodenum)
{
	struct cvip_conf *cvip_confp;
	struct director_node *dnode;
	struct director_node *tdnode;
	static struct cvip_list *clist = NULL;
	int no_of_entries, i, j;
	
	/*
	 * Make multiple calls returns the previous allocated list
	 */
	if (clist != NULL )
		return clist;

	no_of_entries = get_cvip_no(nodenum);
	if (no_of_entries == 0)
		return NULL;

	clist = (struct cvip_list *)malloc(sizeof(struct cvip_list));
	if (clist == NULL)
		return NULL;

	clist->cvip = (char **)malloc(no_of_entries * sizeof(char *));
	for (i = 0; i < no_of_entries; i++) {
		clist->cvip[i] = (char *)malloc(DOTIPLEN);

		if (clist->cvip[i] == NULL) {
			for (j = 0; j < i; j++)
				free(clist->cvip[j]);
			free(clist->cvip);
			free(clist);
			clist=NULL;
			return NULL;
		}
	}

	clist->cvip_no = 0;
	for (cvip_confp = cvipheadp; cvip_confp != NULL;
	     cvip_confp = cvip_confp->next) {

		dnode = cvip_confp->dnode;
		for (tdnode = dnode; tdnode != NULL; tdnode = tdnode->next) {

			if (tdnode->node == nodenum) {
				strncpy(clist->cvip[(clist->cvip_no)],
					cvip_confp->cvip, DOTIPLEN);
				clist->cvip_no += 1;
				break;
			}
		}
	}

	if (clist->cvip_no != no_of_entries) {
		/*
		 * This will never happen but still !!!!
		 */
		for (i = 0; i < no_of_entries; i++)
			free(clist->cvip[i]);
		free(clist->cvip);
		free(clist);
		clist=NULL;
		return NULL;
	}

	return clist;

}

/*
 * struct cvip_list is obtained at any node by
 * get_cvip(clusternode_num());
 * This function JUST select the master director node
 * for cvip. It doesn't mark it as master . 
 * use set_as_master() for setting 
 */
clusternode_t select_master_director(struct cvip_list * clist)
{

	struct cvip_conf *cvip_confp;
	struct director_node *dnode;
	struct director_node *tdnode;

	/* 
	 * The cvip_list doesn't contain any entry
	 */
	if (clist->cvip_no == 0)
		return 0;

	for (cvip_confp = cvipheadp; cvip_confp != NULL;
	     cvip_confp = cvip_confp->next) {

		/* 
		 * This function just select the possible 
		 * master director . It doesn't mark it as 
		 * master. So we need to just try it with only
		 * one cvip that this node belong. No need to 
		 * loop through all the IP.
		 * see is_masterdirector() comments
		 */
		if (strncmp(cvip_confp->cvip, clist->cvip[0], DOTIPLEN) != 0)
			continue;

		/*
		 * Got a matching CVIP
		 */
		if (cvip_confp->master_node == 0) {
			/* 
			 * You should not call this without already 
			 * having a master 
			 * Use get_master_from_file(CVIP) 
			 * to get the master node
			 */
			return 0;
		}

		dnode = cvip_confp->dnode;
		for (tdnode = dnode; tdnode != NULL; tdnode = tdnode->next) {

			if (tdnode->node == cvip_confp->master_node) {
				/*
				 * Found the director node which is 
				 * marked as master now
				 */
				/*
				 * put the next director node
				 * as master node for this cvip
				 */
				struct director_node *tmpdnode;
				tmpdnode = tdnode->next;
				if (tmpdnode == NULL) {
					/*
					 * loop back to the first
					 * director node for this
					 * cvip
					 */
					tmpdnode = cvip_confp->dnode;
				}
				return tmpdnode->node;
			}
		}
	}
	return 0;		/* Failed */
}

/*
 * This function need to be called by taking a get_cvip_filelock(..)
 * The return value of get_cvip_filelock(..) is passed
 * as the first argument
 * return 0 if there in no entry in the file pertaining to the address
 * otherwise return the node number 
 * File format is <CVIP>	<node_num>
 */
clusternode_t get_master_from_file(FILE * fp, char *addr)
{
	char buff[BUFF_SIZE];

	if (fp == NULL) {
		syslog(LOG_ERR, "Error in opening the %s file \n", ACTIVE_FILE);
		return 0;
	}

	memset(buff, 0, BUFF_SIZE);

	/*
	 * Get to the beginning of the file
	 */
	rewind(fp);

	while (fscanf(fp, "%s", buff) != EOF) {

		if (!strncmp(buff, addr, DOTIPLEN)) {
			memset(buff, 0, BUFF_SIZE);
			/*
			 * Get the node number
			 */
			fscanf(fp, "%s", buff);
			return (atoi(buff));

		}

		memset(buff, 0, BUFF_SIZE);
	}
	return 0;
}

/* 
 * This function need to be called by taking a get_cvip_filelock(..)
 * The return value of get_cvip_filelock(..) is passed
 * as the first argument
 */
int announce_as_master(FILE * fp, struct cvip_list *clist)
{
	FILE *tmp_file;
	char buff[BUFF_SIZE];
	int match = 0, i;
	int no_file_entry = 1;

	if (fp == NULL) {
		syslog(LOG_ERR, "Error in opening the %s file \n", ACTIVE_FILE);
		return -1;
	}
	if ((tmp_file = tmpfile()) == NULL) {
		syslog(LOG_ERR, "Error creating tmeporary file \n");
		return -1;
	}

	/* 
	 * Before announcing as master we should make sure 
	 * the interfaces are configured properly
	 */
	configure_masterdirector(fp, clist);

	/*
	 * Inform the kernel about new director for this
	 * CVIP
	 */
	for (i = 0; i < clist->cvip_no; i++) {
		set_mdirector(this_node, clist->cvip[i]);
	}

	memset(buff, 0, BUFF_SIZE);

	/*
	 * Go to the beginning of the file
	 */
	rewind(fp);

	while (fscanf(fp, "%s", buff) != EOF) {

		for (i = 0; i < clist->cvip_no; i++) {
			if (!strncmp(buff, clist->cvip[i], DOTIPLEN)) {
				match = 1;
				/* 
				 * there is an entry for the CVIP
				 */
				no_file_entry = 0;
				fprintf(tmp_file, "%s\t\t%u\n",
					clist->cvip[i], this_node);
				/* 
				 * Skip the node number
				 */
				fscanf(fp, "%s", buff);
				break;
			}
		}

		if (match == 1) {
			match = 0;
			memset(buff, 0, BUFF_SIZE);
			continue;
		}
		fprintf(tmp_file, "%s", buff);
		/* 
		 * Read node and write with \n
		 */
		memset(buff, 0, BUFF_SIZE);
		fscanf(fp, "%s", buff);
		fprintf(tmp_file, "\t\t%s\n", buff);

		memset(buff, 0, BUFF_SIZE);
	}

	/* 
	 * If there is no entry in  the file add the entry in 
	 * the tmp_file
	 */
	if (no_file_entry == 1) {
		for (i = 0; i < clist->cvip_no; i++)
			fprintf(tmp_file, "%s\t\t%u\n",
				clist->cvip[i], this_node);

	}
	rewind(fp);
	rewind(tmp_file);
	memset(buff, 0, BUFF_SIZE);
	while (fscanf(tmp_file, "%s", buff) != EOF) {

		fprintf(fp, "%s", buff);
		/* 
		 *  Reading the node number
		 */
		memset(buff, 0, BUFF_SIZE);
		fscanf(tmp_file, "%s", buff);
		fprintf(fp, "\t\t%s\n", buff);
		memset(buff, 0, BUFF_SIZE);
	}
	fclose(tmp_file);
	fflush(fp);
	return 1;

}

void set_as_master(struct cvip_list *clist, clusternode_t node_num)
{

	struct cvip_conf *cvip_confp;
	int i;
	for (i = 0; i < clist->cvip_no; i++) {
		for (cvip_confp = cvipheadp; cvip_confp != NULL;
		     cvip_confp = cvip_confp->next) {
			if (!strncmp
			    (cvip_confp->cvip, clist->cvip[i], DOTIPLEN)) {
				cvip_confp->master_node = node_num;
				break;
			}
		}
	}
}
void set_possible_director(struct cvip_list *clist)
{

	struct cvip_conf *cvip_confp;
	struct director_node *dnode;
	struct director_node *tdnode;
	int i;
	for (i = 0; i < clist->cvip_no; i++) {
		for (cvip_confp = cvipheadp; cvip_confp != NULL;
		     cvip_confp = cvip_confp->next) {
			if (!strncmp(cvip_confp->cvip,
				     clist->cvip[i], DOTIPLEN)) {
				/*
				 * Now go through all the 
				 * director list
				 */

				/*
				 * this is needed to intialize 
				 * array inside kernel 
				 */
				set_pdirector(0, cvip_confp->cvip);
				dnode = cvip_confp->dnode;
				for (tdnode = dnode; tdnode != NULL;
				     tdnode = tdnode->next) {
					/*
					 * Inform the kernel regarding 
					 * this director 
					 */
					set_pdirector(tdnode->node,
						      cvip_confp->cvip);
				}
				break;
			}
		}
	}
}

/* Function related to real server */
static int get_rcvip_no(clusternode_t nodenum)
{
	struct cvip_conf *cvip_confp;
	struct real_server_node *rnode;
	struct real_server_node *trnode;
	int cvip_no = 0;

	for (cvip_confp = cvipheadp; cvip_confp != NULL;
	     cvip_confp = cvip_confp->next) {

		rnode = cvip_confp->rnode;
		for (trnode = rnode; trnode != NULL; trnode = trnode->next) {

			if (trnode->node == nodenum) {
				cvip_no += 1;
				break;
			}
		}
	}
	return cvip_no;

}

/* Function related to real server */
struct cvip_list *get_rcvip(clusternode_t nodenum)
{
	struct cvip_conf *cvip_confp;
	struct real_server_node *rnode;
	struct real_server_node *trnode;
	static struct cvip_list *clist = NULL;
	int no_of_entries, i, j;

	/*
	 * Make multiple calls returns the previous allocated list
	 */
	if (clist != NULL)
		return clist;

	no_of_entries = get_rcvip_no(nodenum);
	if (no_of_entries == 0)
		return NULL;

	clist = (struct cvip_list *)malloc(sizeof(struct cvip_list));
	if (clist == NULL)
		return NULL;

	clist->cvip = (char **)malloc(no_of_entries * sizeof(char *));
	for (i = 0; i < no_of_entries; i++) {
		clist->cvip[i] = (char *)malloc(DOTIPLEN);

		if (clist->cvip[i] == NULL) {
			for (j = 0; j < i; j++)
				free(clist->cvip[j]);
			free(clist->cvip);
			free(clist);
			clist=NULL;
			return NULL;
		}

	}

	clist->cvip_no = 0;
	for (cvip_confp = cvipheadp; cvip_confp != NULL;
	     cvip_confp = cvip_confp->next) {

		rnode = cvip_confp->rnode;
		for (trnode = rnode; trnode != NULL; trnode = trnode->next) {

			if (trnode->node == nodenum) {
				strncpy(clist->cvip[(clist->cvip_no)],
					cvip_confp->cvip, DOTIPLEN);
				clist->cvip_no += 1;
				break;
			}
		}
	}

	if (clist->cvip_no != no_of_entries) {

		/* 
		 * This will never happen but still !!!!
		 */
		for (i = 0; i < no_of_entries; i++)
			free(clist->cvip[i]);
		free(clist->cvip);
		free(clist);
		clist=NULL;
		return NULL;
	}

	return clist;

}

FILE *get_cvip_filelock()
{
	int ret, fd;
	struct flock flockarg;
	FILE *fp;

#ifdef DEBUG
	syslog(LOG_ERR, " To take the lock \n");
#endif
	fp = fopen(ACTIVE_FILE, "r+");

	if (fp == NULL)
		return fp;

	fd = fileno(fp);
	flockarg.l_type = F_WRLCK;
	flockarg.l_whence = SEEK_SET;
	flockarg.l_start = 0;
	flockarg.l_len = 0;	/* till EOF */
	ret = fcntl(fd, F_SETLKW, &flockarg);
	if (ret < 0) {
		fclose(fp);
		return NULL;
	}
#ifdef DEBUG
	syslog(LOG_ERR, " Taken the lock \n");
#endif
	return fp;
}

int rel_cvip_filelock(FILE * fp)
{
	int ret, fd;
	struct flock flockarg;

#ifdef DEBUG
	syslog(LOG_ERR, " To release lock\n");
#endif
	if (fp == NULL)
		return -1;

	fd = fileno(fp);
	flockarg.l_type = F_UNLCK;
	flockarg.l_whence = SEEK_SET;
	flockarg.l_start = 0;
	flockarg.l_len = 0;	/* till EOF */

	ret = fcntl(fd, F_SETLKW, &flockarg);

#ifdef DEBUG
	syslog(LOG_ERR, " Released Lock\n");
#endif
	fclose(fp);
	return ret;

}

/*
 * This function need to be called by taking a get_cvip_filelock(..)
 * The return value of get_cvip_filelock(..) is passed
 * as the first argument
 */
int remove_cvip_entry(FILE * fp, char *cvip)
{
	FILE *tmp_file;
	char buff[BUFF_SIZE];
	int no_of_entries = 0;

	if (fp == NULL) {
		syslog(LOG_ERR, "Error in opening the %s file \n", ACTIVE_FILE);
		return -1;
	}

	if ((tmp_file = tmpfile()) == NULL) {
		syslog(LOG_ERR, "Error creating tmeporary file \n");
		return -1;
	}

	/*
	 * Go to the beginning of the file
	 */
	rewind(fp);
	while (fscanf(fp, "%s", buff) != EOF) {

		if (!strncmp(buff, cvip, DOTIPLEN)) {
			fscanf(fp, "%s", buff);
			memset(buff, 0, BUFF_SIZE);
			continue;
		}
		fprintf(tmp_file, "%s", buff);
		/* 
		 * Read node and write with \n
		 */
		memset(buff, 0, BUFF_SIZE);
		fscanf(fp, "%s", buff);
		fprintf(tmp_file, "\t\t%s\n", buff);
		memset(buff, 0, BUFF_SIZE);
		no_of_entries++;
	}

	rewind(fp);
	rewind(tmp_file);

	/*
	 *      If lvs.VIP.active is going to be an empty file
	 *      after the removal of this cvip entry then
	 *      truncate the file to zero length
	 */

	if (no_of_entries == 0) {
		int ret;
		ret = truncate(ACTIVE_FILE, 0);
		if (ret == -1) {
			perror("truncate failed");
		}
		fclose(tmp_file);
		fflush(fp);
		return 1;
	}

	memset(buff, 0, BUFF_SIZE);
	while (fscanf(tmp_file, "%s", buff) != EOF) {
		fprintf(fp, "%s", buff);
		/* 
		 *  Reading the node number
		 */
		memset(buff, 0, BUFF_SIZE);
		fscanf(tmp_file, "%s", buff);
		fprintf(fp, "\t\t%s\n", buff);
		memset(buff, 0, BUFF_SIZE);
	}
	fclose(tmp_file);
	fflush(fp);
	return 1;
}
