/*
 * Date   :	Jun 19 2002
 * Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
 *
 *
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License as
 *      published by the Free Software Foundation; either version 2 of
 *      the License, or (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *      or NON INFRINGEMENT.  See the GNU General Public License for more
 *      details.
 *
 *
 */

#ifndef LVS_HA_H
#define LVS_HA_H

#include <syslog.h>
#include <unistd.h>
#include <linux/cluster.h>	/* This should be changed to cluster.h */

extern transid_t transid;
/* List of CVIP  for which this node act as director */
extern struct cvip_list *gclist;
extern clusternode_t this_node;

extern void daemonize(void);
extern void redirect_output(void);
extern void signal_init(void);
extern void node_event_handler(void);
extern void sigterm_handler(int);
extern void remove_pidfile(void);

#define SIGCLUSTER (SIGIO)

#endif				/* LVS_HA_H */
