/*
 * Date   :	Jun 19 2002
 * Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
 *
 *
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License as
 *      published by the Free Software Foundation; either version 2 of
 *      the License, or (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *      or NON INFRINGEMENT.  See the GNU General Public License for more
 *      details.
 *
 *
 */

#ifndef _CI_LINUX_H_
#define _CI_LINUX_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <syslog.h>
#include <linux/cluster.h>
#include <net/ip_vs.h>

#define CLUSTER_CONF_FILE "/etc/cvip.conf"
#define CLUSTERTAB "/etc/clustertab"
#define ACTIVE_FILE "/etc/lvs.VIP.active"
#define BUFF_SIZE 100
#define NODE_NUMBER_ORDER 5
/* See ipvswrap.c::lvs_sync_daemon() */
#define IFNAME_LEN IP_VS_IFNAME_MAXLEN
#define DOTIPLEN 16
#define DEF_SCHED "wlc"

struct director_node {
	clusternode_t node;
	char multicast_interface[IFNAME_LEN];
	char garp_interface[IFNAME_LEN];
	struct director_node *next;
};
struct real_server_node {
	clusternode_t node;
	struct service *svc;
	struct real_server_node *next;
};
struct service {
	unsigned short type;
	unsigned short sport;
	unsigned short eport;
	int weight;
	char scheduler[IP_VS_SCHEDNAME_MAXLEN];
	struct service *snext;
};

typedef struct nodenum_ip_map {
	char addr_ip[DOTIPLEN];
} nodenum_ip_map_t;

struct cvip_conf {
	char cvip[DOTIPLEN];
	/* gateway used in case of NAT or DR without external interface */
	char gateway[DOTIPLEN];
	clusternode_t master_node;
	struct director_node *dnode;
	struct real_server_node *rnode;
	struct service *svc;
	struct cvip_conf *next;
};
enum lvsroute_type { 
	LVS_DR,
	LVS_NAT, 
};

extern enum lvsroute_type lvs_routing;

struct cvip_list {
/* number  of cvip in the array */
	int cvip_no;
/* cvip array */
	char **cvip;
};

extern struct cvip_conf *cvipheadp;

extern int nodestatus(clusternode_t);
extern char *node_to_address(clusternode_t);
extern int init_director(void);
extern int is_director(clusternode_t);
extern int is_realserver(clusternode_t);
extern int is_masterdirector(clusternode_t);
extern char *get_multicast_interface(clusternode_t);
extern char *get_garp_interface(clusternode_t);
extern struct cvip_list *get_cvip(clusternode_t);
/* Function related to real server */
extern struct cvip_list *get_rcvip(clusternode_t);
extern clusternode_t select_master_director(struct cvip_list *);
extern clusternode_t get_master_from_file(FILE *, char *);
extern int announce_as_master(FILE *, struct cvip_list *);
extern void set_as_master(struct cvip_list *, clusternode_t);
extern void set_possible_director(struct cvip_list *);
extern FILE *get_cvip_filelock(void);
extern int rel_cvip_filelock(FILE *);
extern int remove_cvip_entry(FILE *, char *);

#endif				/* _CI_LINUX_H_ */
