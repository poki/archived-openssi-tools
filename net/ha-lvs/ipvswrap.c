/*
 * Date   :	Jun 19 2002
 * Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
 *
 *
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License as
 *      published by the Free Software Foundation; either version 2 of
 *      the License, or (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *      or NON INFRINGEMENT.  See the GNU General Public License for more
 *      details.
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <errno.h>
#include <syslog.h>
#include <string.h>
#include <arpa/inet.h>
#include "libipvs/libipvs.h"

/*
 * addr the IPV4 address in 127.0.0.1 format
 */
int ipvs_cleanup_realserver(char *addr)
{
	struct ip_vs_get_services *serv;
	struct ip_vs_get_dests *dest;
	struct ip_vs_dest_entry *real;
	struct ip_vs_service_user  usvc;
	struct ip_vs_dest_user udst;
	int i, j;
	if (ipvs_init()) {

		syslog(LOG_WARNING, "Failed to initialize IPVS service\n");
	}
	if (!(serv = ipvs_get_services())) {
		syslog(LOG_WARNING, "ipvs_get_services error :%s\n",
		       ipvs_strerror(errno));
		ipvs_close();
		return -1;
	}

	for (i = 0; i < serv->num_services; i++) {
		dest = ipvs_get_dests(&serv->entrytable[i]);
		for (j = 0; j < dest->num_dests; j++) {
			int result;
			real = &dest->entrytable[j];
			if (real->addr != inet_addr(addr))
				continue;
			syslog(LOG_INFO,
			       "Deleting the service pertaining to the node  %s\n",
			       addr);

			usvc.protocol 	= dest->protocol;
			usvc.addr 	= dest->addr;
			usvc.port 	= dest->port;
			usvc.fwmark 	= dest->fwmark;

			udst.addr 	= real->addr;
			udst.port 	= real->port;
			udst.conn_flags = real->conn_flags;
			udst.weight 	= real->weight;

			result = ipvs_del_dest(&usvc, &udst);

			if (result) {
				syslog(LOG_WARNING, "%s\n",
				       ipvs_strerror(errno));
				ipvs_close();
				return -1;
			}

		}
		free(dest);
	}
	free(serv);
	ipvs_close();

	return 1;

}

/*
 * cmd   =  "start" to start the daemon 
 * cmd   = "stop"  to stop the daemon
 * state = "master" to start in master state
 * state = "backup to start in backup state 
 * interface is the multicast interface
 */
int lvs_sync_daemon(char *cmd, char *state, char *interface)
{
	struct ip_vs_daemon_user udaemon;
	int result;
	if (ipvs_init()) {

		syslog(LOG_WARNING, "Failed to initialize IPVS service\n");
	}

	if (!strncmp(state, "master", 6)) {
		udaemon.state = IP_VS_STATE_MASTER;
	} else {
		udaemon.state = IP_VS_STATE_BACKUP;
	}

	strncpy(udaemon.mcast_ifn, interface, IP_VS_IFNAME_MAXLEN);

	/* Don't forget to initialize syncID */
	udaemon.syncid = 255;

	if (!strncmp(cmd, "start", 5)) {
		result = ipvs_start_daemon(&udaemon);
	} else {
		result = ipvs_stop_daemon(&udaemon);
	}
	if (result) {
		syslog(LOG_WARNING, "%s\n", ipvs_strerror(errno));
		ipvs_close();
		return -1;
	}

	ipvs_close();
	return 1;

}

int lvs_flush_virtual_table(void)
{
	int result;

	if (ipvs_init()) {
		syslog(LOG_WARNING, "Failed to initialize IPVS service\n");
		return -1;
	}

	result = ipvs_flush();
	if (result) {
		syslog(LOG_WARNING, "%s\n", ipvs_strerror(errno));
		ipvs_close();
		return -1;
	}

	ipvs_close();
	return 1;
}
