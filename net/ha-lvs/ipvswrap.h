/*
 * Date   :	Jun 19 2002
 * Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
 *
 *
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License as
 *      published by the Free Software Foundation; either version 2 of
 *      the License, or (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *      or NON INFRINGEMENT.  See the GNU General Public License for more
 *      details.
 *
 *
 */

#ifndef  _LIBIPVS_IPVSWRAP_H_
#define  _LIBIPVS_IPVSWRAP_H_

extern int ipvs_cleanup_realserver(char *);
extern int lvs_sync_daemon(char *, char *, char *);
extern int lvs_flush_virtual_table(void);

#endif				/* _LIBIPVS_IPVSWRAP_H_ */
