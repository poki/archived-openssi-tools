/*
 * Date   :	August 26 2002
 * Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
 *
 *
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License as
 *      published by the Free Software Foundation; either version 2 of
 *      the License, or (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *      or NON INFRINGEMENT.  See the GNU General Public License for more
 *      details.
 *
 *
 */

#include <netdb.h>
#include <libxml/parser.h>
#include "ci-linux.h"




static int validate_ip(const char *ipaddr, char *dest)
{
	struct hostent *hp;
	char *s;

	hp = gethostbyname(ipaddr);
	if (!hp || hp->h_addr_list[1]) {
		syslog(LOG_ERR, "Not a valid IP Address: %s %d\n",
		       ipaddr, h_errno);
		return -1;
	}
	s = inet_ntoa(*((struct in_addr *)hp->h_addr_list[0]));
	if (strlen(s) >= DOTIPLEN) {
		syslog(LOG_ERR, "Not a valid IP Address: %s\n", s);
		return -1;
	}
	strcpy(dest, s);
	return 1;
}
static struct service *parse_service(xmlDocPtr doc, xmlNodePtr mnode)
{
	xmlChar *tmp;
	struct service *svc;
	xmlNodePtr node = mnode->xmlChildrenNode;
	svc = (struct service *)malloc(sizeof(struct service));
	if (svc == NULL ) {
		syslog(LOG_ERR, "Failed to allocate memory\n");
		return NULL;
	}
	memset(svc, 0, sizeof(struct service));
	strcpy(svc->scheduler, DEF_SCHED);

	while (node != NULL) {
		if (!xmlStrncmp(node->name, (const xmlChar *)"tcp_port", 8)) {
			char *tmp2;
			tmp = xmlNodeListGetString(doc,
					node->xmlChildrenNode, 1);
			svc->type = IPPROTO_TCP;
			if ((tmp2 = strchr(tmp, '-')) == NULL) {
				svc->sport = atoi(tmp);
				svc->eport = atoi(tmp);
			} else {
				*tmp2 = '\0';
				svc->sport = atoi(tmp);
				svc->eport = atoi(tmp2+1);
				*tmp2 = '-';
			}
			free(tmp);
		} else if (!xmlStrncmp(node->name, (const xmlChar *)"udp_port", 8)) {
			char *tmp2;
			tmp = xmlNodeListGetString(doc,
						   node->xmlChildrenNode, 1);
			svc->type = IPPROTO_UDP;
			if ((tmp2 = strchr(tmp, '-')) == NULL) {
				svc->sport = atoi(tmp);
				svc->eport = atoi(tmp);
			} else {
				*tmp2 = '\0';
				svc->sport = atoi(tmp);
				svc->eport = atoi(tmp2+1);
				*tmp2 = '-';
			}
			free(tmp);
		} else if (!xmlStrncmp(node->name, (const xmlChar *)"scheduler", 9)) {
			tmp = xmlNodeListGetString(doc,
						   node->xmlChildrenNode, 1);
			strncpy(svc->scheduler, tmp, IP_VS_SCHEDNAME_MAXLEN);
			svc->scheduler[IP_VS_SCHEDNAME_MAXLEN-1] = '\0';
			free(tmp);
		} else if (!xmlStrncmp(node->name, (const xmlChar *)"weight", 6)) {
			tmp = xmlNodeListGetString(doc,
						   node->xmlChildrenNode, 1);
			svc->weight = atoi(tmp);
			free(tmp);
		}
		node = node->next;
	}

	if (svc->sport == 0 && svc->eport == 0) {
		free(svc);
		syslog(LOG_ERR, "File Format Error %s\n", CLUSTER_CONF_FILE);
		exit(1);
	}

	if (svc->scheduler[0] == '\0')
		strcpy(svc->scheduler, DEF_SCHED);

	if (svc->weight == 0)
		svc->weight=1;

	return svc;
		
}
static struct real_server_node *get_realserver_chunk(int start_node,
		int end_node, struct service *svrc)
{
	struct real_server_node *rnode = NULL;
	int i;
	for (i = start_node; i <= end_node; i++) {
		struct real_server_node *r_tmpnode;
		struct service *svc = NULL;
		r_tmpnode = (struct real_server_node *) 
				malloc(sizeof(struct real_server_node));
		if (r_tmpnode == NULL) {
			syslog(LOG_ERR, "Failed to allocate memory\n");
			return NULL;
		}
		memset(r_tmpnode, 0, sizeof(struct real_server_node));
		r_tmpnode->node = i;
		if (svrc != NULL ) {
			svc = (struct service *)malloc(sizeof(struct service));
			if (svc == NULL ) {
				syslog(LOG_ERR, "Failed to allocate memory\n");
				return NULL;
			}
			memcpy(svc, svrc, sizeof(struct service));
			r_tmpnode->svc = svc;
		}
		if (rnode == NULL )
			rnode = r_tmpnode;
		else {
			r_tmpnode->next = rnode;
			rnode = r_tmpnode;
		}
	}
	return rnode;
}

static struct real_server_node *parse_realserver(xmlDocPtr doc, xmlNodePtr mnode)
{
	xmlChar *tmp;
	struct real_server_node *rnode;
	struct service *svc;
	int start_node = 0, end_node = 0;
	xmlNodePtr node = mnode->xmlChildrenNode;
	rnode = (struct real_server_node *) malloc(sizeof(struct real_server_node));
	if (rnode == NULL) {
		syslog(LOG_ERR, "Failed to allocate memory\n");
		return NULL;
	}
	memset(rnode, 0, sizeof(struct real_server_node));
	while (node != NULL) {
		if (!xmlStrncmp(node->name, (const xmlChar *)"service", 7)) {
			svc =  parse_service(doc, node);
			svc->snext = rnode->svc;
			rnode->svc = svc;
		} else if (!xmlStrncmp(node->name, (const xmlChar *)"node_num", 8)) {
			char *tmp2;
			tmp = xmlNodeListGetString(doc,
						   node->xmlChildrenNode, 1);
			/* 
			 * node numbers can be specified in a range. Used with 
			 * large cluster
			 */
			if ((tmp2 =strchr(tmp, '-')) == NULL) {
				rnode->node = atoi(tmp);
			} else {
				*tmp2 = '\0';
				start_node = atoi(tmp);
				end_node   = atoi(tmp2+1);
				rnode->node =  start_node;
				*tmp2 = '-';
			}
			free(tmp);
		}
		node = node->next;
	}
	/* 
	 * IF node is specified in the range. Build the list of nodes
	 */
	if (start_node != end_node ) {
		if ( (rnode->next = get_realserver_chunk(start_node+1,
					end_node, rnode->svc)) == NULL ) {
			syslog(LOG_ERR, "Failed to allocate memory\n");
			/*
			 * SSI_XXX Handle the memory allocation failures 
			 */
		}
	}
	return rnode;
}

static struct director_node *parse_director(xmlDocPtr doc, xmlNodePtr mnode)
{
	xmlChar *tmp;
	struct director_node *director;
	xmlNodePtr node = mnode->xmlChildrenNode;
	director = (struct director_node *) malloc(sizeof(struct director_node));
	if (director == NULL) {
		syslog(LOG_ERR, "Failed to allocate memory\n");
		return NULL;
	}
	memset(director, 0, sizeof(struct director_node));
	while (node != NULL) {
		if (!xmlStrncmp(node->name, (const xmlChar *)"node_num", 8)) {
			tmp = xmlNodeListGetString(doc,
						   node->xmlChildrenNode, 1);
			director->node = atoi(tmp);
			free(tmp);

		} else if (!xmlStrncmp(node->name,
				       (const xmlChar *)"garp_interface", 14)) {
			tmp = xmlNodeListGetString(doc,
						   node->xmlChildrenNode, 1);
			strncpy(director->garp_interface, tmp, IFNAME_LEN);
			free(tmp);

		} else if (!xmlStrncmp(node->name,
				       (const xmlChar *)"sync_interface", 14)) {
			tmp = xmlNodeListGetString(doc,
						   node->xmlChildrenNode, 1);
			strncpy(director->multicast_interface, tmp, IFNAME_LEN);
			free(tmp);

		}
		node = node->next;
	}

	return director;

}

static inline struct real_server_node *get_last_rnode(struct real_server_node *rnode)
{
	struct real_server_node *r_tmpnode = rnode;;
	while (rnode->next) {
		rnode = rnode->next;
		r_tmpnode = rnode;
	}
	return r_tmpnode;
}

static struct cvip_conf *parse_cvip(xmlDocPtr doc, xmlNodePtr mnode)
{
	xmlChar *tmp;
	struct cvip_conf *cvip;
	struct director_node *director = NULL, *dnext = NULL;
	struct real_server_node *rnode = NULL, *rnext = NULL;
	struct service *svc;
	int ret;
	xmlNodePtr node = mnode->xmlChildrenNode;
	cvip = (struct cvip_conf *) malloc(sizeof(struct cvip_conf));
	if (cvip == NULL) {
		syslog(LOG_ERR, "Failed to allocate memory\n");
		return NULL;
	}
	memset(cvip, 0, sizeof(struct cvip_conf));
	while (node != NULL) {
		if (!xmlStrncmp(node->name, (const xmlChar *)"service", 7)) {

			svc =  parse_service(doc, node);
			svc->snext = cvip->svc;
			cvip->svc = svc;

		} else if (!xmlStrncmp(node->name, (const xmlChar *)"gateway", 7)) {

			tmp = xmlNodeListGetString(doc,
						   node->xmlChildrenNode, 1);
			ret = validate_ip(tmp, cvip->gateway);
			free(tmp);
			if (ret < 0)
				goto error_out;

		} else if (!xmlStrncmp(node->name, (const xmlChar *)"ip_addr", 7)) {

			tmp = xmlNodeListGetString(doc,
						   node->xmlChildrenNode, 1);
			ret = validate_ip(tmp, cvip->cvip);
			free(tmp);
			if (ret < 0)
				goto error_out;

		} else if (!xmlStrncmp(node->name,
				       (const xmlChar *)"director_node", 13)) {

			if ((director=parse_director(doc, node)) == NULL) 
				goto error_out;

			if (cvip->dnode == NULL) {
				cvip->dnode = director;
				dnext = director;
			} else {
				dnext->next = director;
				dnext = director;

			}

		} else if (!xmlStrncmp(node->name,
				       (const xmlChar *)"real_server_node",
				       16)) {

			if ((rnode = parse_realserver(doc, node)) == NULL )
				goto error_out;

			if (cvip->rnode == NULL) {
				cvip->rnode = rnode;
				rnext = get_last_rnode(rnode);
			} else {
				rnext->next = rnode;
				rnext = get_last_rnode(rnode);
			}

		}

		node = node->next;
	}

	return cvip;
error_out:
	if (cvip->svc)
		free(cvip->svc);

	 /* SSI_XXX free real server and director node list */

	free(cvip);
	return NULL;


}
static int parse_lvsrouting(xmlChar *route_string)
{
	int ret = 0;
	if (!strncmp(route_string, "DR", 2)) {
		lvs_routing = LVS_DR;
	} else if (!strncmp(route_string, "NAT", 3)) {
		lvs_routing = LVS_NAT;
	} else {
		 /* error */
		ret = -1;
	}

	return ret;
}


enum lvsroute_type lvs_routing;
int init_directorhead(struct cvip_conf **cvipfile_p)
{

	xmlDocPtr doc;
	xmlNodePtr node;
	struct cvip_conf *cvipnext = NULL, *cvip = NULL;
	doc = xmlParseFile(CLUSTER_CONF_FILE);
	if (doc == NULL) {
		syslog(LOG_ERR, "Error in Parsing the file %s\n",
		       CLUSTER_CONF_FILE);
		return -1;
	}

	node = xmlDocGetRootElement(doc);

	if (node == NULL) {
		syslog(LOG_ERR, "Empty document %s\n", CLUSTER_CONF_FILE);
		xmlFreeDoc(doc);
		return -1;
	}

	if (xmlStrncmp(node->name, (const xmlChar *)"cvips", 5)) {
		syslog(LOG_ERR, "File Format Error %s\n", CLUSTER_CONF_FILE);
		exit(1);
	}
	node = node->xmlChildrenNode;
	while (node != NULL) {
		if (!xmlStrncmp(node->name, (const xmlChar *)"routing", 7)) {
			xmlChar *tmp;
			tmp = xmlNodeListGetString(doc,
						   node->xmlChildrenNode, 1);
			if (parse_lvsrouting(tmp) < 0) {
				free(tmp);
				return -1;
			}
			free(tmp);

		} else if (!xmlStrncmp(node->name, (const xmlChar *)"cvip", 4)) {

			/* Got CVIP */
			if ((cvip=parse_cvip(doc, node)) == NULL )
				return -1;

			if (*cvipfile_p == NULL) {
				*cvipfile_p = cvip;
				cvipnext = cvip;
			} else {
				cvipnext->next = cvip;
				cvipnext = cvip;
			}
		}
		node = node->next;
	}

	xmlFreeDoc(doc);
	return 1;

}
