/*
 * Date   :	Jun 19 2002
 * Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
 *
 *
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License as
 *      published by the Free Software Foundation; either version 2 of
 *      the License, or (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *      or NON INFRINGEMENT.  See the GNU General Public License for more
 *      details.
 *
 *
 */

#ifndef _GARP_H_
#define _GARP_H_
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <linux/if_arp.h>
#include <net/ethernet.h>
#include <syslog.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "ci-linux.h"
struct m_arphdr {
	unsigned short int ar_hrd;	/* Format of hardware address.  */
	unsigned short int ar_pro;	/* Format of protocol address.  */
	unsigned char ar_hln;	/* Length of hardware address.  */
	unsigned char ar_pln;	/* Length of protocol address.  */
	unsigned short int ar_op;	/* ARP opcode (command).  */
	/* Ethernet looks like this : This bit is variable sized however...  */
	unsigned char __ar_sha[ETH_ALEN];	/* Sender hardware address.  */
	unsigned char __ar_sip[4];	/* Sender IP address.  */
	unsigned char __ar_tha[ETH_ALEN];	/* Target hardware address.  */
	unsigned char __ar_tip[4];	/* Target IP address.  */
};

extern void send_clist_garp(struct cvip_list *, char *);

#endif				/* _GARP_H_ */
