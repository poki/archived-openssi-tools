/*
 * Date   :	Jun 19 2002
 * Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
 *
 *
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License as
 *      published by the Free Software Foundation; either version 2 of
 *      the License, or (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *      or NON INFRINGEMENT.  See the GNU General Public License for more
 *      details.
 *
 *
 */

#include <signal.h>
#include <linux/cluster.h>
#include <sys/wait.h>
#include  "lvs_ha.h"
#include "ipvswrap.h"
#include "ci-linux.h"
#include "garp.h"
#include "interface.h"

transid_t transid;

clusternode_t this_node;

int main(int argc, char *argv[])
{
	cluster_transinfo_t transinfo;
	clusternode_t nodenum;
	sigset_t set;
	int sig;
	FILE *fp;

	openlog("LVS_HA", LOG_CONS | LOG_PID, LOG_DAEMON);

	/* 
	 * Check whether running on SSI cluster
	 */
	if (!cluster_ssiconfig()) {
		syslog(LOG_ERR, "This need to be run on a SSI cluster\n");
		exit(1);
	}

	/* 
	 * Initialize signal handlers
	 * This blocks SIGCLUSTER before the cluster_detailedtransition()
	 * below.
	 */
#ifndef NO_DAEMON
	signal_init();
#endif

	/* redirect the ouput to /dev/null */
	redirect_output();

	/* 
	 * Getting the last transaction ID
	 */
	cluster_detailedtransition(CLUSTER_TRANSWHICH_LAST, &transid,
				   sizeof(cluster_transinfo_t), &transinfo);

	this_node = clusternode_num();
	if (this_node == 0) {
		syslog(LOG_ERR, "Error in getting the node number\n");
		exit(1);

	}
	if (init_director() < 0 ) {
		syslog(LOG_ERR, "Error in initializing director node\n");
		exit(1);
	}

	if (!is_director(this_node) && !is_realserver(this_node)) {
		/*
		 * If we are not a director node or a real server node
		 * exit
		 */
		exit(0);
	}

	fp = get_cvip_filelock();
	if (fp == NULL) {
		syslog(LOG_ERR, "Error in getting cvip file lock\n");
		exit(1);
	}
	if (is_director(this_node)) {
		struct cvip_list *clist;

		clist = get_cvip(this_node);

		if (clist == NULL) {
			syslog(LOG_ERR,
			       "This node is not in the director list \n");
			exit(1);
		}

		/* 
		 * I need to check only for 0 [ see the other source
		 * files for why ?? ]
		 */
		if ((nodenum = get_master_from_file(fp, clist->cvip[0])) == 0) {
			/* 
			 * all the address that this director
			 * belong will not have an entry in the
			 * file if this happens. So announce
			 * as master with this node 
			 */
			set_as_master(clist, this_node);
			lvs_sync_daemon("start", "master",
					get_multicast_interface(this_node));
			announce_as_master(fp, clist);
			/* 
			 * Send the GARP packet
			 */
			send_clist_garp(clist, get_garp_interface(this_node));
		} else if (nodestatus(nodenum) <= 0 || nodenum == this_node ||
			   !is_director(nodenum)) {

			set_as_master(clist, this_node);
			lvs_sync_daemon("start", "master",
					get_multicast_interface(this_node));
			announce_as_master(fp, clist);
			/* 
			 * Send the GARP packet
			 */
			send_clist_garp(clist, get_garp_interface(this_node));
		} else {
			set_as_master(clist, nodenum);
			lvs_sync_daemon("start", "backup",
					get_multicast_interface(this_node));
			/* 
			 * We consider backup director node also as a real server 
			 * We need to create an interface for handling the incoming 
			 * request. We don't reply for arp on this interface(lo:xx).
			 * During failover  this interface will be deleted and eth0:xx
			 * will be created 
			 *
			 * For master_director_node interfaces are created using
			 * announce_as_master(..)
			 */
			configure_backupdirector(fp, clist);
		}
		/* Inform the kernel about the possible director nodes 
		 * So that the automatic service registration happen on
		 * the director node. This is need for the fail-over of
		 * master director node to work correctly
		 */
		set_possible_director(clist);

	} 

	/* 
	 * Real server configuration: 
	 * Director node can be real server for some other CVIP
	 * Check if it is . Yes configure the interface for that
	 */
	if (is_realserver(this_node)) {
		configure_realserver(fp, get_rcvip(this_node));
	}

	if (rel_cvip_filelock(fp) < 0) {
		syslog(LOG_ERR, "Error in releasing cvip file lock\n");
		exit(1);
	}

	configure_services(cvipheadp);


	/* Make sure we have no remaining children from initialization */
	while (wait(NULL) > 0) ;

	/* 
	 * Pushing the process to background 
	 */
#ifndef NO_DAEMON
	daemonize();
#endif

	if (cluster_events_register_signal(-1, SIGCLUSTER) == -1) {
		syslog(LOG_ERR, "Cannot register SIGCLUSTER\n");
		exit(1);
	}

	/* SSI_XXX: SIGCLUSTER hasn't been dealt with until now, we should
	 * look for any transitions that have happened since we started.
	 */

	while (1) {
		sigemptyset(&set);

		/* These signals are blocked in signal_init().
		 * That makes sure none of the signals are lost.
		 * SIGCHLD has a dummy handler, so it is not in SIG_IGN.
		 */
		sigaddset(&set, SIGCLUSTER);
		sigaddset(&set, SIGCHLD);
		sigaddset(&set, SIGTERM);
		sigwait(&set, &sig);
		if (sig == SIGCLUSTER) {

			node_event_handler();
			continue;
		} else if (sig == SIGTERM) {
			sigterm_handler(SIGTERM);
			continue;
		} else if (sig == SIGCHLD) {

			/*
			 * Removing the zombie(s)
			 */
			while (wait3(NULL, WNOHANG, NULL) > 0) ;
			continue;
		} else {
			syslog(LOG_ERR,
			       "Received a signal on which i was not doing a sigwait \n");
			exit(1);
		}
	}
}
