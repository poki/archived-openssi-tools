/*
 * Date   :	Jun 24 2002
 * Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
 *
 *
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License as
 *      published by the Free Software Foundation; either version 2 of
 *      the License, or (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *      or NON INFRINGEMENT.  See the GNU General Public License for more
 *      details.
 *
 *
 */

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <syslog.h>
#include "ci-linux.h"
#include "lvs_ha.h"
#include "interface.h"

/*
 * Use fully qualified path name since system() is dep on PATH env variable
 */
#if defined(redhat)
#define IPCMD		"/sbin/ip"
#define ROUTECMD	"/sbin/route"
#define ONNODECMD	"/bin/onnode"
#define IPVSADMCMD	"/sbin/ipvsadm"
#define IPTABLESCMD	"/sbin/iptables"
#elif defined(debian)
#define IPCMD		"/bin/ip"
#define ROUTECMD	"/sbin/route"
#define ONNODECMD	"/bin/onnode"
#define IPVSADMCMD	"/sbin/ipvsadm"
#define IPTABLESCMD	"/sbin/iptables"
#elif defined(suse)
#define IPCMD		"/sbin/ip"
#define ROUTECMD	"/sbin/route"
#define ONNODECMD	"/bin/onnode"
#define IPVSADMCMD	"/sbin/ipvsadm"
#define IPTABLESCMD	"/usr/sbin/iptables"
#endif


/**
 * return the cvip_conf structure corresponding to the
 * cvip ipaddr 
 * @ipaddr Dotted ip address 
 */
static struct cvip_conf * get_cvipconf(char *ipaddr)
{
	struct cvip_conf *cvip_confp;
	for (cvip_confp = cvipheadp; cvip_confp != NULL;
			cvip_confp = cvip_confp->next) {

		if (!strncmp(ipaddr, cvip_confp->cvip, DOTIPLEN)) {
			return cvip_confp;
		}
	}
	return NULL;

}

/**
 * write to proc file the value 
 * @parameter file name in format a.b.c indicate /proc/a/b/c
 */
static int write_proc(const char *parameter, char *value)
{

	char *file = (char *)malloc(strlen(parameter) + 7);	/* /proc/ = 7 */
	char *index;
	int fd;

	/* SSI_XXX what is proc is not mounted on /proc: read from mtab */
	strcpy(file, "/proc/");

	strcat(file, parameter);
	while ((index = strchr(file, '.'))) {
		*index = '/';
	}

	fd = open(file, O_RDWR);
	if (fd == -1) {
		syslog(LOG_ERR, "Failed to open %s\n", file);
		free(file);
		return -1;
	}
	write(fd, value, strlen(value)+1);

	free(file);
	close(fd);

	return 0;
}
static void configure_interface( char *ip, char *dev)
{
	char cmd[60];
	sprintf(cmd, "%s addr add %s/%d dev %s broadcast +", IPCMD, ip, 32, dev);
	system(cmd);
	
}
static void unconfigure_interface( char *ip, char *dev)
{
	char cmd[60];
	sprintf(cmd, "%s addr del %s/%d dev %s broadcast +", IPCMD, ip, 32, dev);
	system(cmd);
}
/**
 * a NULL dev indicate default gateway
 */
static void configure_route( char *ip, char *dev)
{
	char cmd[60];
	if (dev == NULL) {
		sprintf(cmd, "%s add default gw %s", ROUTECMD, ip);
	} else {
		sprintf(cmd, "%s add -host %s dev %s", ROUTECMD, ip, dev);
	}
	system(cmd);
}
/**
 * a NULL dev indicate default gateway
 */
static void unconfigure_route( char *ip, char *dev)
{
	char cmd[60];
	if (dev == NULL) {
		sprintf(cmd, "%s del default gw %s", ROUTECMD, ip);
	} else {
		sprintf(cmd, "%s del -host %s dev %s", ROUTECMD, ip, dev);
	}
	system(cmd);
}

/*
 * sync the already existing lvs rules from the current
 * master director node
 * This function is called with get_cvip_filelock(..)
 * so we can trust the data in cvipheadp
 */
static void sync_lvs_rules(void)
{
	int i;
	struct cvip_conf *cvip_confp;
	char cmd[60];
	struct cvip_list *clist = get_cvip(this_node);
	for (i = 0; i < clist->cvip_no; i++) {
		cvip_confp = get_cvipconf(clist->cvip[i]);
		sprintf(cmd, "%s %d   %s -S | %s -R", ONNODECMD,
				cvip_confp->master_node, IPVSADMCMD, IPVSADMCMD);
		system(cmd);
	}

}

static void configure_dr_masterdirector_inf(struct cvip_conf *cvip_confp)
{
	if (cvip_confp->gateway[0] != '\0') {
		/*
		 * IF gateway is specified configure the gateway on the 
		 * sync interface 
		 */
		configure_interface(cvip_confp->gateway,
				    get_multicast_interface(this_node));

		/* set ip_forward ON for director */
		write_proc("sys.net.ipv4.ip_forward", "1");

		/* director is the gw for realservers: set icmp redirects off */
		write_proc("sys.net.ipv4.conf.all.send_redirects", "0");
		write_proc("sys.net.ipv4.conf.default.send_redirects", "0");

	} else {
		/* set ip_forward OFF for  director  */
		write_proc("sys.net.ipv4.ip_forward", "0");

		/* director is not gw for realservers: leave icmp redirects on */
		write_proc("sys.net.ipv4.conf.all.send_redirects", "1");
		write_proc("sys.net.ipv4.conf.default.send_redirects", "1");
	}
	/*
	 * Do the arp settings for director node also.
	 * This should takes care of unnecessary surprises during fail over.
	 */
	write_proc("sys.net.ipv4.conf.all.arp_ignore", "1");
	write_proc("sys.net.ipv4.conf.all.arp_announce", "0");
	{
		char tmp[40];
		sprintf(tmp, "sys.net.ipv4.conf.%s.send_redirects",
				get_garp_interface(this_node));
		write_proc(tmp, "1");
	}

	/* remove the already configure loop back interface */
	unconfigure_interface(cvip_confp->cvip, "lo");

	configure_interface(cvip_confp->cvip, get_garp_interface(this_node));
	configure_route(cvip_confp->cvip, get_garp_interface(this_node));

	/* Enable load balancing */
	write_proc("sys.net.ipv4.vs.loadbalance", "1");

	/* set TCP timeout to 10Hrs */
	system(IPVSADMCMD "  --set 36000 0 0");
}

static void unconfigure_dr_masterdirector_inf(struct cvip_conf *cvip_confp)
{
	if (cvip_confp->gateway[0] != '\0') {

		unconfigure_interface(cvip_confp->gateway,
				    get_multicast_interface(this_node));

		/* clear ip_forward for  director */
		write_proc("sys.net.ipv4.ip_forward", "0");
	} else {
		/* Leave icmp redirects off */
		write_proc("sys.net.ipv4.conf.all.send_redirects", "0");
		write_proc("sys.net.ipv4.conf.default.send_redirects", "0");
	}
	{
		char tmp[40];
		sprintf(tmp, "sys.net.ipv4.conf.%s.send_redirects",
				get_garp_interface(this_node));
		write_proc(tmp, "0");
	}
	unconfigure_route(cvip_confp->cvip, get_garp_interface(this_node));
	unconfigure_interface(cvip_confp->cvip, get_garp_interface(this_node));

	/* Disable load balancing */
	write_proc("sys.net.ipv4.vs.loadbalance", "0");
}

static void configure_dr_backupdirector_inf(struct cvip_conf *cvip_confp)
{
	/* Making backup director  NOT reply for ARP on CVIP */
	write_proc("sys.net.ipv4.conf.all.arp_ignore", "1");
	write_proc("sys.net.ipv4.conf.all.arp_announce", "2");

	/* clear ip_forward for backup director */
	write_proc("sys.net.ipv4.ip_forward", "0");

	configure_interface(cvip_confp->cvip, "lo");
	configure_route(cvip_confp->cvip, "lo");

	/* Disable load balancing */
	write_proc("sys.net.ipv4.vs.loadbalance", "0");

	/* Disable rp_filter on all interface*/
	write_proc("sys.net.ipv4.conf.all.rp_filter", "0");


	/* sync lvs rules from master director node */
	sync_lvs_rules();
}

static void unconfigure_dr_backupdirector_inf(struct cvip_conf *cvip_confp)
{
	unconfigure_route(cvip_confp->cvip, "lo");
	unconfigure_interface(cvip_confp->cvip, "lo");
}

static void configure_dr_realserver_inf(struct cvip_conf *cvip_confp)
{
	/* Making real server   NOT reply for ARP on CVIP */
	write_proc("sys.net.ipv4.conf.all.arp_ignore", "1");
	write_proc("sys.net.ipv4.conf.all.arp_announce", "2");

	/* clear ip_forward for real server */
	write_proc("sys.net.ipv4.ip_forward", "0");

	/* Disable rp_filter on all interface*/
	write_proc("sys.net.ipv4.conf.all.rp_filter", "0");

	configure_interface(cvip_confp->cvip, "lo");
	//configure_route(cvip_confp->cvip, "lo");
	

	if (cvip_confp->gateway[0] != '\0')
		configure_route(cvip_confp->gateway, NULL);
}

static void unconfigure_dr_realserver_inf(struct cvip_conf *cvip_confp)
{
	unconfigure_route(cvip_confp->cvip, "lo");
	unconfigure_interface(cvip_confp->cvip, "lo");

	if (cvip_confp->gateway[0] != '\0')
		unconfigure_route(cvip_confp->gateway, NULL);
}
static void configure_nat_masterdirector_inf(struct cvip_conf *cvip_confp)
{
	if (cvip_confp->gateway[0] == '\0') {
		syslog(LOG_ERR, "gateway not specified in cvip.conf for NAT\n");
		exit(1);
	}
	configure_interface(cvip_confp->gateway,
				    get_multicast_interface(this_node));

	configure_interface(cvip_confp->cvip,
			get_garp_interface(this_node));

	/* set ip_forward ON for NAT  director */
	write_proc("sys.net.ipv4.ip_forward", "1");

	/* director is the gw for realservers: set icmp redirects off */
	write_proc("sys.net.ipv4.conf.all.send_redirects", "0");
	write_proc("sys.net.ipv4.conf.default.send_redirects", "0");
		/* /bin/echo "0" >/proc/sys/net/ipv4/conf/$DEV/send_redirects */

	/* Enable load balancing */
	write_proc("sys.net.ipv4.vs.loadbalance", "1");

	/* set TCP timeout to 10Hrs */
	system(IPVSADMCMD "  --set 36000 0 0");

	{
		/*
		 * set the director node to forward connection originating 
		 * from real server 
		 */
		char tmp[90];
		sprintf(tmp, "%s --table nat --append POSTROUTING --out-interface %s --jump MASQUERADE", IPTABLESCMD, get_garp_interface(this_node));
		system(tmp);
	}
}
static void unconfigure_nat_masterdirector_inf(struct cvip_conf *cvip_confp)
{
	unconfigure_interface(cvip_confp->gateway,
			get_multicast_interface(this_node));

	unconfigure_interface(cvip_confp->cvip,
			get_garp_interface(this_node));

	/* clear ip_forward for  director */
	write_proc("sys.net.ipv4.ip_forward", "0");

	/* Disable load balancing */
	write_proc("sys.net.ipv4.vs.loadbalance", "0");

	{
		/* Flush iptables rules */
		char tmp[90];
		sprintf(tmp, "%s --table nat --delete POSTROUTING --out-interface %s --jump MASQUERADE", IPTABLESCMD, get_garp_interface(this_node));
		system(tmp);
	}
}
static void configure_nat_backupdirector_inf(struct cvip_conf *cvip_confp)
{
	if (cvip_confp->gateway[0] == '\0') {
		syslog(LOG_ERR, "gateway not specified in cvip.conf for NAT\n");
		exit(1);
	}
	configure_route(cvip_confp->gateway, NULL);

	/* clear ip_forward for  backup director */
	write_proc("sys.net.ipv4.ip_forward", "0");

	/* sync lvs rules from master director node */
	sync_lvs_rules();
}
static void unconfigure_nat_backupdirector_inf(struct cvip_conf *cvip_confp)
{
	unconfigure_route(cvip_confp->gateway, NULL);
}
static void configure_nat_realserver_inf(struct cvip_conf *cvip_confp)
{
	if (cvip_confp->gateway[0] == '\0') {
		syslog(LOG_ERR, "gateway not specified in cvip.conf for NAT\n");
		exit(1);
	}
	configure_route(cvip_confp->gateway, NULL);

	/* clear ip_forward for  backup director */
	write_proc("sys.net.ipv4.ip_forward", "0");
}
static void unconfigure_nat_realserver_inf(struct cvip_conf *cvip_confp)
{
	unconfigure_route(cvip_confp->gateway, NULL);
}
void configure_masterdirector_inf(struct cvip_conf *cvip_confp)
{
	switch(lvs_routing) {
		case LVS_DR:
			write_proc("cluster.lvs_routing", "DR");
			configure_dr_masterdirector_inf(cvip_confp);
			break;
		case LVS_NAT:
			write_proc("cluster.lvs_routing", "NAT");
			configure_nat_masterdirector_inf(cvip_confp);
			break;
	}
}
void configure_backupdirector_inf(struct cvip_conf *cvip_confp)
{
	switch(lvs_routing) {
		case LVS_DR:
			write_proc("cluster.lvs_routing", "DR");
			configure_dr_backupdirector_inf(cvip_confp);
			break;
		case LVS_NAT:
			write_proc("cluster.lvs_routing", "NAT");
			configure_nat_backupdirector_inf(cvip_confp);
			break;
	}
}
void configure_realserver_inf(struct cvip_conf *cvip_confp)
{
	switch(lvs_routing) {
		case LVS_DR:
			write_proc("cluster.lvs_routing", "DR");
			configure_dr_realserver_inf(cvip_confp);
			break;
		case LVS_NAT:
			write_proc("cluster.lvs_routing", "NAT");
			configure_nat_realserver_inf(cvip_confp);
			break;
	}
}
void unconfigure_masterdirector_inf(struct cvip_conf *cvip_confp)
{
	switch(lvs_routing) {
		case LVS_DR:
			unconfigure_dr_masterdirector_inf(cvip_confp);
			break;
		case LVS_NAT:
			unconfigure_nat_masterdirector_inf(cvip_confp);
			break;
	}
}
void unconfigure_backupdirector_inf(struct cvip_conf *cvip_confp)
{
	switch(lvs_routing) {
		case LVS_DR:
			unconfigure_dr_backupdirector_inf(cvip_confp);
			break;
		case LVS_NAT:
			unconfigure_nat_backupdirector_inf(cvip_confp);
			break;
	}
}
void unconfigure_realserver_inf(struct cvip_conf *cvip_confp)
{
	switch(lvs_routing) {
		case LVS_DR:
			unconfigure_dr_realserver_inf(cvip_confp);
			break;
		case LVS_NAT:
			unconfigure_nat_realserver_inf(cvip_confp);
			break;
	}
}

/* 
 * This function need to be called by taking a get_cvip_filelock(..)
 * The return value of get_cvip_filelock(..) is passed
 * as the first argument
 */
void configure_masterdirector(FILE * fp, struct cvip_list *clist)
{
	int i;
	for (i = 0; i < clist->cvip_no; i++) {
		configure_masterdirector_inf(get_cvipconf(clist->cvip[i]));
	}

}

/* 
 * This function need to be called by taking a get_cvip_filelock(..)
 * The return value of get_cvip_filelock(..) is passed
 * as the first argument
 */
void configure_backupdirector(FILE * fp, struct cvip_list *clist)
{
	int i;
	for (i = 0; i < clist->cvip_no; i++) {
		clusternode_t masterdnode;

		masterdnode = get_master_from_file(fp, clist->cvip[i]);

		/* 
		 * If we are the master then there is no need in setting up 
		 * the lo interface . Our cvip.conf can put director node also 
		 * as a real server node 
		 */
		if (masterdnode == this_node)
			continue;
		/*
		 * Inform the kernel about the master director node
		 * for the CVIPs this node belong
		 */
		set_mdirector(masterdnode, clist->cvip[i]);

		configure_backupdirector_inf(get_cvipconf(clist->cvip[i]));
	}

}


/* 
 * This function need to be called by taking a get_cvip_filelock(..)
 * The return value of get_cvip_filelock(..) is passed
 * as the first argument
 */
void configure_realserver(FILE * fp, struct cvip_list *clist)
{
	int i,director=0;
	for (i = 0; i < clist->cvip_no; i++) {
		struct cvip_conf *cvip_confp;
		struct director_node *tdnode;

		cvip_confp = get_cvipconf(clist->cvip[i]);

		for (tdnode = cvip_confp->dnode; tdnode != NULL;
				tdnode = tdnode->next) {

			/* 
			 * If we are in both the director list and the realserver list 
			 * then there is no need in setting up 
			 * the lo interface. Our cvip.conf can put director node also 
			 * as a real server node 
			 */
			if (tdnode->node == this_node) {
				director=1;
				break;
			}
		}

		if (!director) {
			/*
			 * Inform the kernel about the master director node
			 * for the CVIPs this node belong
			 * This need to happen if we act only as the 
			 * real server node for this CVIP.
			 */
			clusternode_t masterdnode;

			masterdnode = get_master_from_file(fp, clist->cvip[i]);
			set_mdirector(masterdnode, clist->cvip[i]);

			configure_realserver_inf(cvip_confp);
		}

	}

}
void configure_services(struct cvip_conf *cvips)
{

	struct cvip_conf *cvip_confp;
	struct real_server_node *rnode;
	struct real_server_node *trnode;

	for (cvip_confp = cvips; cvip_confp != NULL;
	     cvip_confp = cvip_confp->next) {
		/* 
		 * First configure the global setting so that they can be 
		 * overwritten by real server specific configuration later
		 */
		if (cvip_confp->svc != NULL ) {

			/* SSI_XXX
			 * we support only clusterwide algorithm as of now
			 * svc->scheduler not taken into account
			 */
			struct service *svc = cvip_confp->svc;
			while (svc != NULL) {
				set_ipvsportweight(svc->sport, svc->eport,
						svc->weight, svc->type,
						svc->scheduler);
				svc = svc->snext;
			}
		}

		rnode = cvip_confp->rnode;
		for (trnode = rnode; trnode != NULL; trnode = trnode->next) {

			if (trnode->node == this_node) {
				if (trnode->svc != NULL) {
					/*
					 * real server specific service 
					 * configuration
					 */
					struct service *svc = trnode->svc;
				while (svc != NULL) {
					set_ipvsportweight(svc->sport, svc->eport,
							svc->weight, svc->type,
							svc->scheduler);
					svc = svc->snext;
				   }
				}
			}
		}
	}
}
void unconfigure_services(struct cvip_conf *cvips)
{

	struct cvip_conf *cvip_confp;
	struct real_server_node *rnode;
	struct real_server_node *trnode;

	for (cvip_confp = cvips; cvip_confp != NULL;
	     cvip_confp = cvip_confp->next) {
		/* 
		 * First configure the global setting so that they can be 
		 * overwritten by real server specific configuration later
		 */
		if (cvip_confp->svc != NULL ) {

			/* SSI_XXX
			 * we support only clusterwide algorithm as of now
			 * svc->scheduler not taken into account
			 */
			struct service *svc = cvip_confp->svc;
			while (svc != NULL) {
				set_ipvsportweight(svc->sport,
						svc->eport, 0, svc->type,
						svc->scheduler);
				svc = svc->snext;
			}
		}

		rnode = cvip_confp->rnode;
		for (trnode = rnode; trnode != NULL; trnode = trnode->next) {

			if (trnode->node == this_node) {
				if (trnode->svc != NULL) {
					/*
					 * real server specific service 
					 * configuration
					 */
					struct service *svc = trnode->svc;
				while (svc != NULL) {
					set_ipvsportweight(svc->sport,
						svc->eport, 0, svc->type,
						svc->scheduler);
					svc = svc->snext;
				   }
				}
			}
		}
	}
}
