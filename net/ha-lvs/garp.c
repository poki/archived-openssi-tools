/*
 * Date   :	Jun 19 2002
 * Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
 *
 *
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License as
 *      published by the Free Software Foundation; either version 2 of
 *      the License, or (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *      or NON INFRINGEMENT.  See the GNU General Public License for more
 *      details.
 *
 *
 */

#include "garp.h"

/* Copied form keepalived project */
static int send_garp(char *ipaddr, char *interface)
{

	int ctlsock;
	struct ifreq req;
	unsigned char *hwaddr;

	char buf[sizeof(struct m_arphdr) + ETHER_HDR_LEN];
	char buflen = sizeof(struct m_arphdr) + ETHER_HDR_LEN;
	struct ether_header *eth = (struct ether_header *)buf;
	struct m_arphdr *arph = (struct m_arphdr *)(buf + ETHER_HDR_LEN);
	int hwlen = ETH_ALEN;
	struct in_addr addr;
	struct sockaddr from;
	int i;

	ctlsock = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
	if (ctlsock < 0) {

		syslog(LOG_ERR,
		       "Error in creating PF_INET socket for sending GARP packets\n");
		return -1;
	}

	memset(&req, 0, sizeof(req));
	strcpy(req.ifr_name, interface);
	if (ioctl(ctlsock, SIOCGIFHWADDR, &req) < 0) {

		syslog(LOG_ERR,
		       "Error in getting the HWaddr for the interface %s\n",
		       interface);
		return -1;
	}
	hwaddr = req.ifr_hwaddr.sa_data;
	close(ctlsock);

	/* 
	 *  hardcoded for ethernet 
	 */
	memset(eth->ether_dhost, 0xFF, ETH_ALEN);
	memcpy(eth->ether_shost, hwaddr, hwlen);
	eth->ether_type = htons(ETHERTYPE_ARP);

	/*
	 * build the arp payload
	 */
	memset(arph, 0, sizeof(*arph));
	arph->ar_hrd = htons(ARPHRD_ETHER);
	arph->ar_pro = htons(ETHERTYPE_IP);
	arph->ar_hln = 6;
	arph->ar_pln = 4;
	arph->ar_op = htons(ARPOP_REQUEST);
	memcpy(arph->__ar_sha, hwaddr, hwlen);
	if (inet_aton(ipaddr, &addr) == 0) {

		syslog(LOG_ERR, "Not a vaild ip address %s\n", ipaddr);
		return -1;
	}
	memcpy(arph->__ar_sip, &(addr.s_addr), sizeof(addr.s_addr));
	memcpy(arph->__ar_tip, &(addr.s_addr), sizeof(addr.s_addr));

	ctlsock = socket(PF_PACKET, SOCK_PACKET, 0x300);	/* 0x300 is magic */
	if (ctlsock < 0) {

		syslog(LOG_ERR,
		       "Error in creating PF_PACKET socket for sending GARP packets\n");
		return -1;
	}
	memset(&from, 0, sizeof(from));
	strcpy(from.sa_data, interface);

	for (i = 0; i < 5; i++) {
		if (sendto(ctlsock, buf, buflen, 0, &from, sizeof(from)) < 0) {

			syslog(LOG_ERR, "Error in sending GARP packets \n");
			close(ctlsock);
			return -1;

		}
	}
	close(ctlsock);
	return 1;

}

void send_clist_garp(struct cvip_list *clist, char *interface)
{
	int i;

	for (i = 0; i < clist->cvip_no; i++)
		send_garp(clist->cvip[i], interface);

}
