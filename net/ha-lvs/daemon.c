/*
 * Date   :	Jun 19 2002
 * Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
 *
 *
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License as
 *      published by the Free Software Foundation; either version 2 of
 *      the License, or (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *      or NON INFRINGEMENT.  See the GNU General Public License for more
 *      details.
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <syslog.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>

#include  "lvs_ha.h"

#define PID_FILE "/var/run/ha-lvs.pid"

static pid_t getpid_fromfile(void)
{
	pid_t pid;
	FILE *fp;
	if ((fp = fopen(PID_FILE, "r")) == NULL) {
		return -1;
	}
	fscanf(fp, "%d", &pid);
	fclose(fp);
	return pid;

}
void remove_pidfile(void)
{
	unlink(PID_FILE);

}

static int writepid_tofile(void)
{
	FILE *fp;
	if ((fp = fopen(PID_FILE, "w")) == NULL) {
		return -1;
	}
	fprintf(fp, "%d", getpid());
	fclose(fp);
	return 1;

}

void redirect_output(void)
{
	int fd;
	/* 
	 * make STDIN/STDOUT and STDERR to /dev/null
	 */
	fd = open("/dev/null", O_RDWR, 0);
	if (fd != -1) {
		dup2(fd, STDIN_FILENO);
		dup2(fd, STDOUT_FILENO);
		dup2(fd, STDERR_FILENO);
	}
	umask(0);
}

void daemonize()
{
	pid_t pid;

	pid = getpid_fromfile();
	if (pid != -1) {
		if (kill(pid, 0) == 0) {
			syslog(LOG_INFO, "HA-LVS already running ");
			syslog(LOG_INFO, "Exiting ........... ");
			exit(1);
		} else {

			if (errno == EPERM) {
				syslog(LOG_INFO, "HA-LVS already running  at \
					 a different  permission level ");
				syslog(LOG_INFO, "Exiting ........... ");
				exit(1);
			}

			remove_pidfile();
		}
	}
	if (fork() == 0) {

		if (setsid() < 0) {
			syslog(LOG_ERR, "Error in daemonizing \n");
			exit(1);
		}
		redirect_output();
		/* 
		 * Write the PID to a file
		 */
		if (writepid_tofile() == -1)
			exit(1);
	} else {
		exit(0);
	}
}
