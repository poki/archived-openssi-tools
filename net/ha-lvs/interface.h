/*
 * Date   :	Jun 24 2002
 * Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
 *
 *
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License as
 *      published by the Free Software Foundation; either version 2 of
 *      the License, or (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *      or NON INFRINGEMENT.  See the GNU General Public License for more
 *      details.
 *
 *
 */

#ifndef _INTERFACE_H_
#define _INTERFACE_H_

#include <stdio.h>
#include "lvs_ha.h"


extern void configure_masterdirector(FILE *, struct cvip_list *);
extern void configure_backupdirector(FILE *, struct cvip_list *);
extern void configure_realserver(FILE *, struct cvip_list *);
extern void unconfigure_masterdirector_inf(struct cvip_conf *);
extern void unconfigure_backupdirector_inf(struct cvip_conf *);
extern void unconfigure_realserver_inf(struct cvip_conf *);
extern void configure_services(struct cvip_conf *);
extern void unconfigure_services(struct cvip_conf *);


#endif				/* _INTERFACE_H_ */
