/*
 *	Copyright 2003 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <linux/cluster.h>
#include <linux/ssisys.h>

int
cluster_set_clustername(char *clustername)
{
	ssisys_iovec_t	iovec;
	int ret;

	/* must have a valid, non-zero string */
	if (clustername == NULL || strlen(clustername) == 0) {
		errno = EINVAL;
		return -1;
	}

	iovec.tio_id.id_cmd = SSISYS_SET_CLUSTERNAME;
	iovec.tio_id.id_ver = SSISYS_CURVER;
	iovec.tio_udatain = (caddr_t)clustername;
	iovec.tio_udatainlen = strlen(clustername) + 1;
	iovec.tio_udataout = (caddr_t)NULL;
	iovec.tio_udataoutlen = 0;

	ret = ssisys((char *)&iovec, sizeof(iovec));
	
	return ret;
}

#define MAXSTR  1024
char buffer[MAXSTR];

int
main(int argc, char *argv[])
{
	int ret;

	/* No arguments print clustername */
	if (argc == 1) {
		ret = cluster_get_clustername(buffer, MAXSTR);
		if (ret == -1) {
			if( errno == ENOENT ) {
				fprintf(stderr,"clustername is not set\n");
			}else { 
				perror("clustername");
			}
			exit(1);
		}
		printf("%s\n", buffer);
		exit(0);
	}
		
	if (argc != 2) {
		fprintf(stderr, "usage clustername {name}\n");
		exit(1);
	}

	ret = cluster_set_clustername(argv[1]);
	if (ret == -1) {
		perror("clustername");
		exit(1);
	}
	exit(0);
}
