#
# Date   :	Jan 13 2004
# Authors:	Brian Watson <Brian.J.Watson@hp.com>
# Authors:	Aneesh Kumar K.V <aneesh.kumar@digital.com>
#
#      This program is free software; you can redistribute it and/or
#      modify it under the terms of the GNU General Public License as
#      published by the Free Software Foundation; either version 2 of
#      the License, or (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
#      or NON INFRINGEMENT.  See the GNU General Public License for more
#      details.

package ssi_utils;
use strict;

# Do NOT use any SSI modules
use Exporter;

our $maxnodes;
our $CLUSTER_DIR = "/cluster";
our $TEMPLATE_DIR = "$CLUSTER_DIR/nodetemplate";
BEGIN {
	sub trim ($) {
		my $string = shift;
		$string =~ s/^\s+//;
		$string =~ s/\s+$//;
		return $string;
	}

	$maxnodes = trim(`cluster -m 2>/dev/null`);
	if ($maxnodes !~ /^\d+$/) {
		# This indicates we are possibly not running
		# in an SSI cluster; set maxnodes to 125
		$maxnodes = 125;
	}

	our @ISA = qw(Exporter);
	our @EXPORT = qw(
			$maxnodes
			$CLUSTER_DIR
			$TEMPLATE_DIR

			trim
			is_ssi

			ip2num
			num2ip
			getnet

			make_string
			make_invstring
			make_ipstring

			mklocalfile
			mkglobalfile

			get_fstabentry
			is_mountpoint

			prompt_yesno
			);
}

sub is_ssi {
	return system("clusternode_num >/dev/null 2>&1") ? 0 : 1;
}

sub ip2num ($) {
	my $ip = shift;
	return unpack("N1", pack("C4", split(/\./, $ip)));
}

sub num2ip ($) {
	my $num = shift;
	return join('.', unpack("C4", pack("N1", $num)));
}

sub getnet ($$) {
	my $ip = shift;
	my $mask = shift;
	return num2ip(ip2num($ip) & ip2num($mask));
}

sub make_string {
	my @list = sort { $a <=> $b } @{ shift() };

	my $string;
	foreach (@list) {
		$string .= "," unless not defined $string;
		$string .= "$_";
	}

	$string =~ /^(\d+)/;
	$string, $1;
}

sub make_invstring {
	my @list = sort { $a <=> $b } @{ shift() };
	my $min = shift;
	my $max = shift;

	my $string;
	my $cur = $min;
	foreach (@list) {
		next unless $cur < $_;
		$string .= "," unless not defined $string;
		$string .= "$cur";
		my $tmp = $_ - 1;
		$string .= "-$tmp" if $cur < $tmp;
	} continue {
		$cur = $_ + 1;
	}
	unless ($cur > $max) {
		$string .= "," unless not defined $string;
		$string .= "$cur";
		$string .= "-$max" if $cur < $max;
	}

	$string =~ /^(\d+)/;
	return ($string, $1);
}

sub make_ipstring {
	my ($string, $default) = make_invstring @_;

	my $ipstring;
	while ($string) {
		$ipstring .= "\t" . num2ip $1 if $string =~ s/^(\d+)//;
		$ipstring .= " - " . num2ip $1 if $string =~ s/^-(\d+)//;
		$ipstring .= "\n" if $string =~ s/^,//;
	}
	return ($ipstring, num2ip($default));
}

sub mklocalfile {
	my $file = shift;
	system("/usr/sbin/mklocalfile $file");
}

sub mkglobalfile {
	my $file = shift;
	if ( -e $file  && ! -l $file ) {
		system("/usr/sbin/mkglobalfile $file");
	}
}

sub get_fstabentry {
	my $fstab_name = shift;
	my $dirname = shift;
	my $input_line;
	my @line_entry;

	open FSTAB, "< $fstab_name" or die;
	while (! eof(FSTAB)) {

		$input_line = <FSTAB>;
		@line_entry = split(/[\t ]+/ ,$input_line);

		if ( $line_entry[0] =~ /^#/ || $line_entry[0] =~ /^[[:space:]]*$/) {
		#Neglect the line that starts with # and also empty lines
			next;
		}

		if ( $line_entry[1] =~ /^$dirname$/ ) { 
			close FSTAB;
			return ($line_entry[0], $line_entry[2], $line_entry[3]);
		} else {
			next;
		}
	}
	close FSTAB;
	return;

}

sub is_mountpoint {
	my $dirname = shift;
	my ($rootdev, $type, $opts) = get_fstabentry("/etc/fstab", $dirname);

	return 0 unless $rootdev;
	return 1;
}

sub prompt_yesno {
	my $display_str = shift;
	my $default = shift;

PROMPT: print "$display_str";

	my $selection = uc trim <STDIN>;
	$selection = $default if $selection eq '';
	return 1 if $selection eq 'Y';
	return 0 if $selection eq 'N';
	goto PROMPT;
}

1;
