#
# Date   :	Jan 12 2004
# Authors:	Brian Watson <Brian.J.Watson@hp.com>
# 		Aneesh Kumar K.V ( aneesh.kumar@digital.com )
#		Scott Hinchley (Scott.Hinchley@hp.com)
#
#
#      This program is free software; you can redistribute it and/or
#      modify it under the terms of the GNU General Public License as
#      published by the Free Software Foundation; either version 2 of
#      the License, or (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
#      or NON INFRINGEMENT.  See the GNU General Public License for more
#      details.
#
#


package ssi_mgmt;
use strict;

use Socket;

use lib "/cluster/lib";
use ssi_arch;
use Clustertab;
use ssi_utils;
use ssi_distro;

BEGIN {
	our @ISA = qw(Exporter);
	our @EXPORT = qw(
			confirm
			doagain

			make_string
			make_invstring
			make_ipstring

			prompt_verbose 
			prompt_node
			prompt_bootprot
			prompt_clustername
			prompt_nodename

			write_clustername
			write_nodename

			fix_fstab

			finishup
			);
}

sub confirm {
	my $record = shift;
	my $noquit = shift;

	print "
The following configuration has been entered:";
	print "
	Node number:		$record->{NODE}";
	print "
	NIC for interconnect:	$record->{IFNAME}"
		if defined $record->{IFNAME};
	print "
	IP address:		$record->{IPADDR}
	Network hardware addr:	$record->{HWADDR}
	Network boot protocol:	";
	print $record->{BOOTPROT} eq 'E' ? "Etherboot" : "PXE";

	print "
	Local boot device:	";
	my $bootdev = $record->{BOOTDEV};
	print defined($bootdev) ? "$bootdev" : 'none';

	print "
	Clustername:		$record->{CLUSTERNAME}"
		if defined $record->{CLUSTERNAME};
	print "
	Nodename:		$record->{NODENAME}"
		if defined $record->{NODENAME};

	if (defined $record->{FAILOVER}) {
		print "
	Root failover:		";
		print $record->{FAILOVER} eq 'Y' ? 'Yes' : 'No';
	}

	print "
	Potential initnode:	";
	print $record->{MASTER} ? 'Yes' : 'No';

	print "
	NFS support:		$record->{NFS}"
		if defined $record->{NFS};
	print "\n\n";

PROMPT:	if ($noquit) {
		print "(W)rite new configuration or (R)econfigure [W]: ";
	} else {
		print "(W)rite new configuration, (R)econfigure, or (Q)uit " .
			"without writing [W]: ";
	}

	my $selection = uc trim <STDIN>;
	return 0 if $selection eq 'R';
	exit(0) if $selection eq 'Q' and not $noquit;
	goto PROMPT unless $selection eq 'W' or $selection eq '';
	return 1;
}

sub doagain {
PROMPT:	print "Do you wish to configure another node (y/n) [n]: ";

	my $selection = uc trim <STDIN>;
	return 1 if $selection eq 'Y';
	goto PROMPT unless $selection eq 'N' or $selection eq '';
	return 0;
}

sub prompt_verbose {
	print "
This configuration tool can either use verbose prompts that 
give you extra information about what it is doing, or it can 
just ask the necessary questions. Experienced OpenSSI users 
might prefer the latter option.

Even if you choose not to use verbose prompts, you can 
select '?' at any prompt to get extra information.

";

PROMPT: print "Do you want verbose prompts (y/n) [y]: ";
	my $selection = uc trim <STDIN>;
	$selection = 'Y' if $selection eq '';
	goto PROMPT unless $selection =~ /^[NY]$/;
	return ($selection eq 'Y') ? 1 : 0;
}

sub help_node {
	print "
Every node in the cluster must have a unique node number. 
The first node is usually 1, although you might want to 
choose another number for a reason such as where the machine 
is physically located.
";
}

sub prompt_node {
	my $records = shift;
	my $verbose = shift;

	my @list;
	my $data = $records->{DATA};
	foreach (@$data) {
		push @list, $_->{NODE};
	}

	help_node if $verbose;
	print "\n";
	my ($string, $default) = make_invstring \@list, 1, $maxnodes;
	die "Sorry, there are no available node numbers to add a new node.\n"
		unless ($string);

PROMPT:	print "Enter a node number ($string) or (?) [$default]: ";

	my $selection = trim <STDIN>;
	do { help_node; print"\n"; goto PROMPT } if $selection eq '?';
	$selection = $default if $selection eq '';
	goto PROMPT unless $selection and validate_node($selection, $records);
	return $selection;
}

sub help_bootprot {
	print "
Select (P)XE or (E)therboot as the network boot protocol for 
this node. PXE is an Intel standard for network booting, and 
many professional grade NICs have a PXE implementation pre-
installed on them. You can probably enable PXE with your BIOS 
configuration tool. If you do not have a NIC with PXE, you can 
make use of open-source project Etherboot, which lets you generate 
a floppy or ROM image for a variety of different NICs.
";
}

# SSI_XXX: we might be able to automatically detect this
sub prompt_bootprot {
	my $default = shift;
	my $verbose = shift;

	help_bootprot if $verbose;
	print "\n";
PROMPT:	print "Select (P)XE, (E)therboot or (?) [$default]: ";

	my $selection = uc trim <STDIN>;
	do { help_bootprot; print"\n"; goto PROMPT } if $selection eq '?';
	$selection = $default unless $selection;
	goto PROMPT unless validate_bootprot($selection);
	return $selection;
}

# The clustername is used for the primary CVIP and HA-LVS
sub help_clustername {
print "
Enter a name for this Cluster.

Cluster name follows the naming convention derived from RFC 952.
It is a text string with at least 2 characters and a maximum
of 24 characters. It must begin with an alpha character, and
end with with an alpha-numeric character, with zero or more
intervening alpha-numeric and '-' (hyphen) characters.

The clustername should resolve to your CVIP address,
either in DNS or the cluster's /etc/hosts file, if you choose 
to configure a CVIP. This is required if you want to run NFS 
server. For more information, please see 
/usr/share/doc/openssi/README.nfs-server.

The current hostname will automatically become the nodename 
for this node.
";
}

sub prompt_clustername {
	my $verbose = shift;

	help_clustername if $verbose;
	print "\n";
PROMPT:	print "Enter a clustername or (?): ";
	my $cluster_name = trim <STDIN>;
	do { help_clustername; print"\n";goto PROMPT } if $cluster_name eq '?';
	goto PROMPT unless validate_name($cluster_name);
	return $cluster_name;
}

sub write_clustername {
	my $cluster_name = shift;

	open(CLUSTER_NAME_FILE,">/etc/clustername") or
		die "Unable to open /etc/clustername for writing: $!\n";
	print CLUSTER_NAME_FILE "$cluster_name\n";
	close CLUSTER_NAME_FILE;
}

sub help_nodename {
	print "
The nodename should be unique in the cluster and it should 
resolve to one of this node's IP addresses, so that client 
NFS can work correctly. The nodename can resolve to either 
the IP address you configured above for the interconnect, 
or to one of external IP addresses that you might configure 
below. The nodename can resolve to the IP address either in 
DNS or in the cluster's /etc/hosts file.

The nodename is stored in /etc/nodename, which is a context-
dependent symlink (CDSL). In this case, the context is node 
number, which means each node you add will have it's own view 
of /etc/nodename containing its own hostname. To learn more 
about CDSLs, please see /usr/share/doc/openssi/cdsl.
";
}

# We set /etc/nodename as the value of HOSTNAME= in /etc/sysconfig/network.
# This is the simplest approach since the machine's configured interface
# should already match the HOSTNAME=.
sub prompt_nodename {
	my $verbose = shift;

	# SSI_XXX: If /etc/nodename already exists for this node,
	#      offer it as the default nodename

	help_nodename if $verbose;
	print "\n";
PROMPT:	print "Enter a nodename or (?): ";
	my $node_name = trim <STDIN>;
	do { help_nodename; print"\n"; goto PROMPT } if $node_name eq '?';
	goto PROMPT unless validate_name($node_name);
	return $node_name;
}

sub write_nodename {
	my $nodenum = shift;
	my $node_name = shift;

	open (NODE_NAME_FILE,">/cluster/node".$nodenum."/etc/nodename") or
		die "Unable to open /etc/nodename for writing: $!\n";
	print NODE_NAME_FILE "$node_name\n";
	close NODE_NAME_FILE;
}

# Add the specified node to the root filesystem line node= option, if it has
# the chard option already
sub fix_fstab 
{
	my($node_num) = shift;
	my($input_line);
	my(@line_entry);
	my($i);
	my($fstab) = "/etc/fstab";
	my($fstab_out) = "/tmp/fstab_out";

	open(INFILE,$fstab) || die ("Unable to open the file $fstab");
	open(OUTFILE,">$fstab_out") || die ("Unable to open the file $fstab_out");

	while(! eof(INFILE) ) {

		$input_line = <INFILE>;
		@line_entry = split(/[\t ]+/ ,$input_line);

		if ($line_entry[0] =~ /^#/ ) {
			print OUTFILE $input_line;
			next ;
		}

		if (@line_entry != 6 ) {

			next;
		}
		$i = 1;

		print OUTFILE  "$line_entry[0]";
		while( $i < @line_entry ) {
			if ( $i == 3 && $line_entry[1] =~ /^\/$/ ) {
				# add $node_num to node list
				my $found;
				my $nodestr = "";
				my $j;
				my @nodes;
				my @opts;

				@opts = split(/,/, $line_entry[$i]);

				$found = "n";
				foreach(@opts) {
					if ( $_ eq "chard" ) {
						$found = "y";
						last;
					}
				}

				goto donothing if ( $found eq "n" );

				# Find existing nodes
				$j = 0;
				foreach(@opts) {
					my $nodestr = "";

					if ($_ =~ /^node=/ ) {

						$nodestr = $_;
						$nodestr =~ s/^node=//;
						@nodes = split(/:/, $nodestr);
						last;
					}
					$j++;
				}

				# See if node already there
				$found = "n";
				foreach(@nodes) {
					if ( $_ == $node_num ) {
						$found = "y";
						last;
					}
				}

				if ( $found eq "n" ) {
					push(@nodes, $node_num);
				}

				# SSI_XXX: Order should be made to match
				# clustertab order which is what CLMS uses
				$opts[$j] = "node=".join(":", @nodes);

				$line_entry[$i] = join(",",@opts);
			}
donothing:				
			print OUTFILE  "\t$line_entry[$i]";
			$i++;
		}
	}

	close(INFILE);
	close(OUTFILE);

	rename($fstab_out,$fstab);
}

sub finishup {
	my $clustertab = shift;
	my $bootloader = shift;
	my $msg = shift;

	print "Rebuilding the boot materials\n";
	my $initrds = read_initrds($bootloader);
	exit(1) unless 
		defined $initrds;
	rebuild_boot_materials($clustertab, $bootloader, $initrds) or
		exit(1);

	print $msg if $msg;
	exit (0);
}

1;
