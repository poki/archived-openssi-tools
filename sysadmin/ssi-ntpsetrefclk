#!/bin/bash

PATH=/bin:/usr/bin:/sbin:/usr/sbin
export PATH

APPNAME=`basename $0`

THIS_NODE=`clusternode_num 2>/dev/null`
if [[ $? -ne 0 ]]; then
    exit 0;
fi

INIT_NODE=`where_pid 1`
if [[ "$THIS_NODE" -ne "$INIT_NODE" ]]; then
    echo "$APPNAME: not running on init node" 1>&2
    exit 1
fi

if ! localview pidof ntpd >/dev/null 2>&1 ; then
    echo "$APPNAME: ntpd is not running; not setting refclk"
    exit 0
fi

if ntpq -p | grep -q "LOCAL(0)"; then
    echo "$APPNAME: ntpd refclk already set"
    exit 0
fi

TMPFILE=""
trap 'rm -f $TMPFILE' EXIT
TMPFILE=`mktemp -q /tmp/$APPNAME.XXXXXX`
if [[ $? -ne 0 ]]; then
    echo "$APPNAME: Cannot create temp file" 1>&2
    exit 1
fi

if [[ ! -r /etc/ntp.conf ]]; then
    echo "$APPNAME: Cannot read /etc/ntp.conf" 1>&2
    exit 1
fi

KEYID=`awk '$1 == "requestkey" { print $2 }' /etc/ntp.conf`
if [[ -z "$KEYID" ]]; then
    echo "$APPNAME: \"requestkey\" entry not found in /etc/ntp.conf" 1>&2
    exit 1
fi

KEYS=`awk '$1 == "keys" { print $2 }' /etc/ntp.conf`
if [[ -z "$KEYS" ]]; then
    echo "$APPNAME: \"keys\" entry not found in /etc/ntp.conf" 1>&2
    exit 1
fi

if [[ ! -r "$KEYS" ]]; then
    echo "$APPNAME: Cannot read keyfile $KEYS" 1>&2
    exit 1
fi

PASS=`awk -vkeyid=$KEYID '$1 == keyid && $2 == "M" { print $3 }' $KEYS`
if [[ -z "$PASS" ]]; then
    echo "$APPNAME: MD5 key $KEYID not found in keyfile $KEYS" 1>&2
    exit 1
fi

cat <<EOF >>$TMPFILE
keyid $KEYID
passwd $PASS
addpeer 127.127.1.0 0 4 4
fudge 127.127.1.0 val1 10
EOF

cat $TMPFILE | ntpdc >/dev/null
if [[ $? -ne 0 ]]; then
    echo "$APPNAME: ntpdc failed to set refclk" 1>&2
    exit 1
fi

exit 0
