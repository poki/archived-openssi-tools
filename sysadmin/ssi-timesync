#!/bin/bash
#
# ssi-timesync
# description: Synchronizes time across the cluster with the initnode
#
#	Date   :	Feb 08 2004
#	Authors:	Brian Watson <Brian.J.Watson@hp.com>
#
#	Copyright 2004 Hewlett-Packard Company
#
#	This program is free software; you can redistribute it and/or
#	modify it under the terms of the GNU General Public License as 
#	published by the Free Software Foundation; either version 2 of 
#	the License, or (at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
#	or NON INFRINGEMENT.  See the GNU General Public License for more
#	details.
#
# 	You should have received a copy of the GNU General Public License
# 	along with this program; if not, write to the Free Software
# 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
#	Questions/Comments/Bugfixes to ssic-linux-users@lists.sf.net
#


PATH=/sbin:/bin:/usr/bin:/usr/sbin

# Do not run if this is not an SSI kernel
clusternode_num >/dev/null 2>&1 || exit 0

# Check that we are root ... so non-root users stop here
[ `id -u` = 0 ] || exit 1

# Determine who the initnode is
export INITNODE=$(where_pid 1)

# Get the date from the initnode
export INITDATE="$(onnode $INITNODE date)"

# Set the system clock.
onall bash -c "\"
   if [ \\\`clusternode_num\\\` -ne $INITNODE ]; then
	date -s '$INITDATE';
   else
	date
   fi
\""
