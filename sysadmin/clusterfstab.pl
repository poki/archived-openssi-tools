#! /usr/bin/perl -w
#
#
# Date   :	MAY 27 2005	
# Authors:	Aneesh Kumar K.V ( aneesh.kumar@gmail.com )
#
# 	Copyright 2005-6 Hewlett-Packard Development Company
#
# Contributors:
#      Roger Tsang		<roger.tsang@gmail.com> 
#
#      This program is free software; you can redistribute it and/or
#      modify it under the terms of the GNU General Public License as
#      published by the Free Software Foundation; either version 2 of
#      the License, or (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
#      or NON INFRINGEMENT.  See the GNU General Public License for more
#      details.
#
#

use strict; 
use Getopt::Long;

use lib "/cluster/lib";
use ssi_utils;

#record layout
# <file system>
# <mount point>
# <type>
# <options>
# <dump>
# <pass>
# <nodenumber>
sub read_fstab {
	my $fstab_file = shift;
	my $records = [];

	return [] if not -f $fstab_file;

	open (FSTAB, "< $fstab_file") or die "Cannot open $fstab_file: $!\n";
	while(! eof(FSTAB) ) {
		my $line = <FSTAB>;
		chop($line);
		if ( $line =~ /^#/ ) {
			# if the line start with '#' skip
			next;
		}
		my @line_entry = split(/[\t ]+/ ,$line);
		if (@line_entry != 6) {
			# Skip the line that we don't understand
			# possibly a format error
			next;
		}

		my %record;
		$record{FILE_SYSTEM} 	= $line_entry[0];
		$record{MOUNT_POINT} 	= $line_entry[1];
		$record{TYPE} 		= $line_entry[2];
		$record{OPTIONS} 	= $line_entry[3];
		$record{DUMP} 		= $line_entry[4];
		$record{PASS} 		= $line_entry[5];
		$record{NODES}		= "";
		# split with , seperator
		my @options = split(/,/ ,$record{OPTIONS});
		my $option;
		foreach $option (@options) {
			if ($option =~ /node=/ ) {
				# 5 is "node="
				$record{NODES}= substr($option, 5);
			}
		}
		push @$records, \%record;
	}
	close(FSTAB);
	return $records;

}
sub retain_commented_entries {

	my($fstab_file)	= shift;
	my($tmp_fstab) = "$fstab_file".".tmp";

	open (FSTAB_TMP, ">$tmp_fstab") or
			 die("cannot create temporary file $tmp_fstab");
	open (FSTAB, "<$fstab_file") or die("Cannot open $fstab_file");
	while (! eof(FSTAB)) {
		my $line = <FSTAB>;
		chop($line);
		if ( $line =~ /^#/ ) {
			print FSTAB_TMP "$line\n";
			next;
		}
	}
	close(FSTAB_TMP);
	close(FSTAB);
	rename("$tmp_fstab", "$fstab_file");
}

sub write_fstab_entry
{

	my $fstab_file 	= shift;
	my $records 	= shift;
	my $record;

	retain_commented_entries($fstab_file);

	# Now append valid records
	open(FSTAB,">>$fstab_file") || die ( "Unable to open $fstab_file");
	foreach $record (@$records) {
		if (($record->{TYPE} !~ /^swap$/ ) &&
			! -d $record->{MOUNT_POINT} ) {
MOUNT_PROMPT: 
			print "Mount point $record->{MOUNT_POINT} not found\n";
			print "Do you want to create the mount point (y/n) [Y]: ";
			my $selection = uc trim <STDIN>;
			$selection = "Y" if $selection eq '';
			goto MOUNT_PROMPT unless $selection =~ /^[NY]$/;
			if ( $selection =~ /^Y$/) {
				mkdir($record->{MOUNT_POINT}, 0755);
			}else {
				print "Mount point  not created !!!!\n";
			}
		}
		print FSTAB	"$record->{FILE_SYSTEM}\t";
		print FSTAB	"$record->{MOUNT_POINT}\t";
		print FSTAB	"$record->{TYPE}\t";
		print FSTAB	"$record->{OPTIONS}\t";
		print FSTAB	"$record->{DUMP}\t";
		print FSTAB	"$record->{PASS}\n";
	}
	close (FSTAB);
}

sub update_node_record {
	my($record) = shift;
	my($node_num) = shift;

	if($record->{NODES} !~ /$node_num/) {
		#The existing field doesn't contain the node number
		if ($record->{OPTIONS} =~ /node=/) {
			$record->{OPTIONS} = 
				"$record->{OPTIONS}".":"."$node_num";
			if ($record->{OPTIONS} !~ /chard/) {
				$record->{OPTIONS} =
					"chard,"."$record->{OPTIONS}";
			}
		} else { 
			#There is no node= entry field in the options
			#add the same
			$record->{OPTIONS} = 
				"$record->{OPTIONS}".",node="."$node_num";
		}
	}
}
sub add_fstab_entry {

	my($node_num, $mountpoint, $uuid, $records) = @_;
	my($partition_name, $type, $options, $dump, $pass);

	if ($uuid == 0) {
		partition_print();
		print "Select a partition :";
		$partition_name = <STDIN>;
		chop($partition_name);
		$partition_name="/dev/"."$partition_name";
	} else {
		$partition_name = "UUID="."$uuid";
	}

	filesystem_print();
	print "Select the file system on ";
	print "the partion $partition_name :";
	$type =<STDIN>;
	chop($type);
	if (!$mountpoint) {
		print "Select the mount point on ";
		print "the partion $partition_name should be mounted:";
		$mountpoint =<STDIN>;
		chop($mountpoint);
	}

FSTAB_PROMPT:
	print "Enter the below fields as it should appear in fstab file\n";
	print "<options> <dump>  <pass>\n";
	my $entry = <STDIN>;
	chop($entry);
	my (@line_entry) = split(/[\t ]+/ ,$entry);
	goto FSTAB_PROMPT unless (@line_entry == 3);

	my %record;
	$record{OPTIONS} 	= $line_entry[0];
	$record{DUMP} 		= $line_entry[1];
	$record{PASS} 		= $line_entry[2];
	$record{FILE_SYSTEM} 	= $partition_name;
	$record{TYPE} 		= $type;
	$record{MOUNT_POINT}	= $mountpoint;
	# If the user didn't add a node= option 
	# then add it ourself
	$record{NODES}		= "";
	# split with , seperator
	my @options = split(/,/ ,$record{OPTIONS});
	my $option;
	foreach $option (@options) {
		if ($option =~ /node=/ ) {
			# 5 is "node="
			$record{NODES}= substr($option, 5);
		}
	}
	update_node_record(\%record, $node_num);

	push @$records, \%record;
}
sub generate_initial_fstab_ssi {

	my($node_num, $fstab_save, $fstab_file, $uuid) = @_;
	my($input_line);
	my(@line_entry);
	my($i);

	open(INFILE,$fstab_save) || die ("Unable to open the file $fstab_save");
	open(OUTFILE,">>$fstab_file") || die ("Unable to open the file $fstab_file");

	while(! eof(INFILE) ) {

		$input_line = <INFILE>;
		@line_entry = split(/[\t ]+/ ,$input_line);

		if ($line_entry[0] =~ /^#/ ) {
			print OUTFILE $input_line;
			next ;
		}

		if (@line_entry != 6 ) {
			# Skip the line that we don't understand
			# possibly a format error
			next;
		}

		# comment out /dev/shm mount; not supported
		if ($line_entry[1] =~ /^\/dev\/shm$/ ) {
			print OUTFILE "#$input_line";
			next ;
		}

		$i = 1;
		# chard mounts always use UUID values instead of dev or label
		if ( ! $uuid =~ /^$/  && $line_entry[1] =~ /^\/$/ ) {
			print OUTFILE "UUID=$uuid";
		} else {
			print OUTFILE  "$line_entry[0]";
		}
		while( $i < @line_entry ) {

			if( $i == 3) {
				print OUTFILE  "\t";

				if ($line_entry[2] =~ /proc/ ||
					$line_entry[2] =~ /devfs/  ||
					$line_entry[2] =~ /devpts/  ||
					$line_entry[2] =~ /sysfs/  ||
					$line_entry[2] =~ /nfs/ ) {
					print OUTFILE  $line_entry[$i].",node=*";
				} else {
					if ($line_entry[1] =~ /mnt\/(cdrom|floppy)/ ||
					    $line_entry[1] =~ /media\/(cdrecorder|floppy)/ ) {
						print OUTFILE  $line_entry[$i].",node=*";
					} else {
						if ( ! $uuid =~ /^$/ && $line_entry[1] =~ /^\/$/ ) {
							print OUTFILE  "chard,".$line_entry[$i].",node=$node_num";
						} else {
							print OUTFILE  $line_entry[$i].",node=$node_num";
						}
					}
				}

			} else {

				print OUTFILE  "\t$line_entry[$i]";
			}
			$i++;
		}
	}

	close(INFILE);
	close(OUTFILE);

}

sub partition_print 
{

	my($input_line);
	open(PROC_PARTITION,"/proc/partitions") || die( "/proc/partitions not found");
	print "\nAvailable partions are:\n";
	while(! eof(PROC_PARTITION) ) {
		$input_line = <PROC_PARTITION>;
		print "$input_line"; 
	}
	close(PROC_PARTITION);

}

sub filesystem_print 
{

	my($input_line);
	my(@line_entry);
	open(PROC_FILESYSTEM,"/proc/filesystems") || die( "/proc/filesystems not found");

	print "\nAvailable filesystems  are:\n";
	print  "swap\n";

	while(! eof(PROC_FILESYSTEM) ) {

		$input_line = <PROC_FILESYSTEM>;
		@line_entry = split(/[\t ]+/ ,$input_line);
		if ($line_entry[0] ne "nodev" ) {
			print  @line_entry;
		}
	}

	close(PROC_FILESYSTEM);

}


sub usage() {
	print STDERR
	"usage: clusterfstab [options]
	--help		(This message)
	--nodenum	(The cluster node number)
	--mountpoint	(The mount point)
	--uuid		(The UUID value)
	";
	exit 1;
}

# add or modify /etc/fstab entry pertaining to the the 
# particular node number
# if /etc/fstab is not present 
# Generate the initial clusterwide fstab from /etc/fstab.ssisave
sub main 
{

	my($node_num, $mountpoint);
	my($uuid, $this_node, $help);
	my($record, $records);
	my($match) = 0;

	my $fstab_file	= "/etc/fstab";

	$node_num = 0;
	$uuid = 0;

	GetOptions("nodenum=i"		=> \$node_num,
		   "mountpoint=s"	=> \$mountpoint,
		   "uuid=s"		=> \$uuid,
		   "help"       	=> \$help ) || usage();

	usage() if ($help);

	if ( ! -e $fstab_file ) {
		if ( -e "$fstab_file".".ssisave" ) {
			if ($node_num == 0) {
				print STDERR "Needs --nodenum argument";
				usage();
			}
			generate_initial_fstab_ssi($node_num, 
				"$fstab_file".".ssisave",
				$fstab_file, $uuid);
			exit 0;
		}
		die("Can't find file $fstab_file.ssisave\n");
	}

	print "Cluster fstab generation script\n";

	$this_node  = `clusternode_num 2>/dev/null`;
	chop($this_node);

	# Anything other than digit 
	if ($this_node =~ /\D/ || $this_node eq "" ) {
	        die(" This Program need to run on a SSI cluster!!!!");
	}
	
	$records = read_fstab($fstab_file);
	if ($mountpoint || $uuid) {
		foreach $record (@$records) {
			if ( $mountpoint && $record->{MOUNT_POINT} =~ /^$mountpoint$/) {
				if ($uuid) {
					# update the partition with uuid field
					# Also add chard to file system options
					$record->{FILE_SYSTEM}="UUID=$uuid";
					if ($record->{OPTIONS} !~ /chard/) {
						$record->{OPTIONS} =
						"chard,$record->{OPTIONS}";
					}
				}
				if ($node_num) {
					update_node_record($record, $node_num);
					$match = 1;
					last;
				}
			} elsif ( $uuid && $record->{FILE_SYSTEM} =~ /^UUID=$uuid$/) {
				if ($node_num) {
					update_node_record($record, $node_num);
					$match = 1;
					last;
				}
			}
		}
	}
	print "Adding new entry to $fstab_file\n";
	if ($node_num == 0) {
		$node_num = $this_node;
	}
	if ($match == 0 ) {
		if ($node_num != $this_node) {
			die("For adding new entry the script should be run on the same node");
		}
		add_fstab_entry($node_num, $mountpoint, $uuid, $records);
	}
	print "\n!!!Do not interrupt the script!!\n\n";
	write_fstab_entry($fstab_file, $records);

}

main;
