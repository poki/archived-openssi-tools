#	Clustertab.pm -- routines for reading and writing CI clustertab
#			 and boottab files
#
#	Date:		May 08 2003
#	Authors:	Brian Watson <Brian.J.Watson@compaq.com>
#			Scott Hinchley (Scott.Hinchley@hp.com)
#
#	Copyright (c) 2001-3 Hewlett-Packard Company
#
#	This program is free software; you can redistribute it and/or
#	modify it under the terms of the GNU General Public License as 
#	published by the Free Software Foundation; either version 2 of 
#	the License, or (at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
#	or NON INFRINGEMENT.  See the GNU General Public License for more
#	details.
#
# 	You should have received a copy of the GNU General Public License
# 	along with this program; if not, write to the Free Software
# 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
#	Questions/Comments/Bugfixes to ssic-linux-devel@lists.sf.net

package Clustertab;
use strict;

# Do NOT use any SSI modules, except ssi_arch and ssi_utils
use lib "/cluster/lib";
use ssi_utils;

BEGIN {
	our @ISA = qw(Exporter);
	our @EXPORT = qw(
			normalize_hwaddr
			normalize_ipaddr
			normalize_bootdev

 			validate_bootdev 
			validate_bootprot
			validate_hwaddr
			validate_ipaddr
			validate_iprange
			validate_master
			validate_name
			validate_node

			read_clustertab
			write_clustertab
			write_boottab
			update_masterlist

			read_ifconfig
			match_ifconfig
			);

	# write_clustertab only backs up the clustertab once per process
	our $backuptab = 1;
}

# should be run _before_ validation
sub normalize_hwaddr {
	my $value = uc shift;
	my @array;
	foreach (split(/:/, $value)) {
		s/^0+//;
		my $len = length;

		my $num;
		if ($len == 0) {
			$num = '00';
		}
		elsif ($len == 1) {
			$num = "0$_";
		}
		else {
			$num = $_;
		}
		push @array, $num;
	}
	$value = join(':', @array);
	return $value;
}

# should be run _after_ validation
sub normalize_ipaddr {
	my $value = shift;
	return $value if $value eq '<DHCP>';
	return num2ip(ip2num($value));
}

sub normalize_bootdev {
	my ($dev, $node) = @_;

	my $onnode = 'onnode';

	unless (defined $node) {
		$node = `clusternode_num`;
		chomp $node;
	}

	$onnode = "" unless $node;

	# First work out clusterwide name for block device:

	if ($dev =~ /^LABEL=/ || $dev =~ /^UUID=/) {
		$dev = `$onnode $node findfs $dev 2>/dev/null`;
		return unless $dev;
		chomp $dev;
	}

	if ($node && $dev =~ /^\/dev\// && $dev !~ /^\/dev\/$node\//) {
		$dev =~ s/\/dev\//\/dev\/$node\//;
	}

	# Is it good?

	return unless -b $dev;

	# Now work out UUID of filesystem, avoids supprises if 
	# SCSI disk names get re-ordered.

	$dev =~ s/^\/dev\/$node\//\/dev\//;

	$dev = `$onnode $node /sbin/blkid -o value -s UUID $dev`;
	chomp $dev;

	return unless length $dev;

	return "UUID=\"$dev\"";
}

sub validate_bootdev {
	my $value = shift;
	my $node = shift;

	return 0 unless defined $value;

	if ($value =~ /^LABEL=/ || $value =~ /^UUID=/ || $value =~ /^\//) {
		return 1 if defined $node && ! `cluster $node 2>/dev/null`;
		return unless normalize_bootdev ($value, $node);
		return 1;
	}

	return 0;
}

sub validate_bootprot {
	my $value = shift;
	return 0 unless defined $value;
	return 0 unless $value =~ /^[EP]$/;
	return 1;
}

sub validate_hwaddr {
	my $value = shift;
	my $records = shift;

	return 0 unless defined $value;
	return 0 unless $value =~ /^([\dA-F]{2}:){5}[\dA-F]{2}$/;
	return 1 unless $records;

	my $data = $records->{DATA};
	foreach (@$data) {
		return 0 if $value eq $_->{HWADDR};
	}
	return 1;
}

sub validate_ipaddr {
	my $value = shift;
	my $records = shift;

	my $count;
	return 0 unless defined $value;
	return 1 if $value eq "<DHCP>";
	return 0 unless $value =~ /^[1-9][.\d]+$/;
	foreach (split(/\./, $value)) {
		return 0 unless $_ =~ /\d/ and $_ >= 0 and $_ <= 255;
		$count++;
	}
	return 0 unless $count == 4;
	return 1 unless $records;

	my $ipaddr = normalize_ipaddr($value);
	my $data = $records->{DATA};
	foreach (@$data) {
		return 0 if $ipaddr eq $_->{IPADDR};
		# SSI_XXX: is the IP address on the same subnet as others?
	}

	#	SSI_XXX: This code should not be duplicated in 
	#		 read_clustertab(). If you wish to differentiate
	#		 among errors detected by this function, use an 
	#		 optional $errstr argument to return an appropriate 
	#		 error string.
	my $ipnum = ip2num($ipaddr);
	my $ipranges = $records->{IPRANGES};
	foreach (@$ipranges) {
		return 0 if $ipnum >= ip2num($_->{STARTIP})
			and $ipnum <= ip2num($_->{ENDIP});
	}
	return 1;
}

# SSI_XXX: It would be better if this leveraged validate_ipaddr() to
# 	   verify STARTIP and ENDIP (including checking that they don't
# 	   conflict with existing static IP addresses and dynamic ranges).
#
# 	   It should also leverage validate_node() to ensure that the node 
# 	   numbers are valid and don't conflict with any node numbers already 
# 	   assigned (including those implicitly assigned by other dynamic 
# 	   IP address ranges).
#
# 	   Also, this code should not be duplicated in read_clustertab().
# 	   Use an optional $errstr argument to return an appropriate error
# 	   string for whatever error is detected by this function.
sub validate_iprange {
	my $range = shift;
	my $records = shift;

	return 0 unless defined $range;
	return 1 unless $records;

	my $startip = ip2num($range->{STARTIP});
	my $endip = ip2num($range->{ENDIP});

	return 0 if $startip > $endip;

	$range->{STARTIP} =~ /(\d*)$/;
	my $startnode = $1;
	$range->{ENDIP} =~ /(\d*)$/;
	my $endnode = $1;

	my $data = $records->{DATA};
	foreach (@$data) {
		next if $_->{IPADDR} eq '<DHCP>';
		return 0 if ip2num($_->{IPADDR}) >= $startip
			and ip2num($_->{IPADDR}) <= $endip;
		return 0 if $_->{NODE} >= $startnode
			and $_->{NODE} <= $endnode;
	}

	my $ipranges = $records->{IPRANGES};
	foreach (@$ipranges) {
		return 0 unless $startip > ip2num($_->{ENDIP})
			or $endip < ip2num($_->{STARTIP});
	}
	return 1;
}

sub validate_master {
	my $value = shift;
	return 0 unless defined $value;
	return 0 unless $value =~ /^[01]$/;
	return 1;
}

sub validate_name {
	my $value = shift;
	return 0 unless defined $value;
	return 0 unless $value =~ 
		/^[[:alpha:]]([-[:alnum:]]{0,22}[[:alnum:]])?$/;
	# SSI_XXX: is the name clusterwide unique?
	# SSI_XXX: does the name match a local interface? (or CVIP for clustername)
	return 1;
}

# $node == 0 is considered valid to support non-ICS interfaces in 
# /etc/clustertab, but a node number of zero may not be appropriate
# for all contexts. Callers of this function may want to test for a 
# zero value.
sub validate_node {
	my $value = shift;
	my $records = shift;

	return 0 unless defined $value;
	return 0 unless $value =~ /^\d+$/;
	return 0 unless $value >= 0 && $value <= $maxnodes;
	return 1 unless $records;

	# duplicate records w/ node 0 are allowed
	return 1 if $value == 0;

	# check for duplicate records
	my $data = $records->{DATA};
	foreach (@$data) {
		return 0 if $value == $_->{NODE};
	}
	# SSI_XXX: use libcluster to find other taken nodenums in cluster?
	return 1;
}

sub read_clustertab {
	my $clustertab = shift;
	return [] if not -f $clustertab;

	my $count;
	my $masterfound;

	my %records;
	$records{TAB} = $clustertab;
	my $data = $records{DATA} = [];
	my $ipranges = $records{IPRANGES} = [];

	open TAB, "< $clustertab" or die "Cannot open $clustertab: $!\n";
	while (<TAB>) {
		$count++;
		next if /^\s*#/ or /^\s*$/;

		s/^\s*//;
		if (/^IPRANGE\s/) {
			my @fields = split /\s+/, $_;
			shift @fields;

			my %range;
			$range{LINE} = $count;
			$range{STARTIP} = my $startip = shift @fields;
			$range{ENDIP} = my $endip = shift @fields;
			die "Start IP address on line $count of " .
				"$clustertab is invalid\n"
				unless validate_ipaddr($startip)
					and $startip ne '<DHCP>';
			die "End IP address on line $count of " .
				"$clustertab is invalid\n"
				unless validate_ipaddr($endip)
					and $endip ne '<DHCP>';

			my $s = ip2num($startip);
			my $e = ip2num($endip);

			die "IP range on line $count of " .
				"$clustertab is invalid\n"
				unless $s <= $e;

			foreach (@$ipranges) {
				die "IP range on line $count of " .
					"$clustertab overlaps with " .
					"line $_->{LINE}\n"
					unless $s > ip2num($_->{ENDIP})
						or $e < ip2num($_->{STARTIP});
			}

			# Check if the range includes a fixed address
			foreach (@$data) {
				next if $_->{IPADDR} eq '<DHCP>';
				die "IP range on line $count of " .
					"$clustertab includes IP address " .
					"on line $_->{LINE}\n"
					if $s <= ip2num($_->{IPADDR})
						and $e >= ip2num($_->{IPADDR});
			}
			push @$ipranges, \%range;
			next;
		}

		my @fields = split /\s+/, $_;

		my %record;
		$record{LINE} = $count;
		$record{NODE} = my $node = shift @fields;
		$record{IPADDR} = my $ip = shift @fields;
		$record{HWADDR} = my $mac = normalize_hwaddr shift @fields;
		$record{BOOTPROT} = my $bootprot = uc shift @fields;
		$record{MASTER} = my $master = shift @fields;
		$record{BOOTDEV} = my $bootdev = shift @fields
			unless not defined $fields[0] or $fields[0] =~ /^#/;

		die "Field 1 (node number) of line $count of " .
			"$clustertab is invalid\n"
			unless validate_node($node);
		die "Field 2 (IP address) of line $count of " .
			"$clustertab is invalid\n"
			unless validate_ipaddr($ip);
		die "Field 3 (hardware address) of line $count of " .
			"$clustertab is invalid\n"
			unless validate_hwaddr($mac);
		die "Field 4 (boot protocol flag) of line $count of " .
			"$clustertab is invalid (E|P)\n"
			unless $node == 0 or validate_bootprot($bootprot);
		die "Field 5 (CLMS master flag) of line $count of " .
			"$clustertab is invalid (0|1)\n"
			unless $node == 0 or validate_master($master);

		$record{IPADDR} = $ip = normalize_ipaddr $ip;

		foreach (@$data) {
			die "Field 1 (node number) of line $count of " .
				"$clustertab matches line $_->{LINE}\n"
				if $node == $_->{NODE} and $node > 0;
			die "Field 2 (IP address) of line $count of " .
				"$clustertab matches line $_->{LINE}\n"
				if $ip eq $_->{IPADDR}
					and $_->{IPADDR} ne '<DHCP>';
			die "Field 3 (hardware address) of line $count of " .
				"$clustertab matches line $_->{LINE}\n"
				if $mac eq $_->{HWADDR};
		}

		# Check if the fixed address is included in a range
		if ($ip ne '<DHCP>') {
			$ip = ip2num($ip);
			foreach (@$ipranges) {
				die "Field 2 (IP address of line $count of " .
					"$clustertab conflicts with " .
					"line $_->{LINE}\n"
					if $ip >= ip2num($_->{STARTIP})
						and $ip <= ip2num($_->{ENDIP});
			}
		}

		if ($bootdev && `cluster $node`) {
			die "Field 6 (boot device) of line $count of " .
				"$clustertab must refer to a block device\n"
				unless validate_bootdev($bootdev, $node);
# SSI_XXX: Until we have two phase installation, we need to be able
# to set the CLMS master flag before a boot device can be set-up.
# Temporarily disable this check
#		} else {
#			die "Field 6 (boot device) of line $count of " .
#				"$clustertab is required for CLMS master\n"
#				unless not $master;
		}

		$masterfound = 1 if $master;
		push @$data, \%record;
	}
	die "Field 5 (CLMS master flag) in $clustertab must be set for at " .
		"least one node\n"
		unless $masterfound;
	close TAB;

	return \%records;
}

sub write_clustertab {
	my $records = shift;

	my $data = $records->{DATA};
	my $ipranges = $records->{IPRANGES};
	my $clustertab = $records->{TAB};

	if ($Clustertab::backuptab and -f $clustertab) {
		my $old = $clustertab;
		my $new = "$old~";
		system("mv $old $new") == 0 or
			die "Error moving $old to $new: $!\n";
	}
	$Clustertab::backuptab = 0;

	open TAB, "> $clustertab" or die "Can't open $clustertab: $!\n";
	print TAB
"# $clustertab
#
# Provide a list of nodes in the cluster. Run mkinitrd (with the --cfs option)
# then ssi-ksync to propagate any changes you make.
#
# Initnodes are nodes that are allowed to run the single init in the cluster.
# They must be listed in order of precedence. This order is used to determine 
# which node starts init during the cluster boot or after failure of the
# active initnode. The order of non-initnodes can be arbitrary.
#
# An initnode must have a local boot device, as well as physical access to 
# the shared root. Non-initnodes may leave the boot device field blank and 
# always use Etherboot or PXE to boot.
#
# The Boot Protocol field indicates the network booting technology used to
# boot a node into the cluster, (P)XE or (E)therboot. Certain network cards 
# and blade computers come pre-installed with PXE support, so it is 
# convenient. The other option is the open-source Etherboot. It requires
# a system administrator to create a boot disk or flash a ROM for each 
# new node, although it is free. This field is mostly irrelevant if a local
# boot device is specified.
#
# WARNING: The ssi-addnode/rmnode/chnode command suite rewrites this file 
# in a vanilla format. Any special comments and formatting are lost. Before 
# rewriting, a backup copy is made at $clustertab~.
#
# Example (node 2 is the primary initnode and node 1 is the secondary):
#
# Node  IP address   Hardware address   Boot Prot  Initnode?   Boot device
# ----  -----------  -----------------  ---------  ---------   -----------
#  2    192.168.0.2  00:11:22:AA:BB:DD	   E          1        /dev/sda1
#  1    192.168.0.1  00:11:22:AA:BB:CC 	   E          1        /dev/sda3
#  3    192.168.0.3  00:11:22:AA:BB:EE     P          0

";

	foreach (@$ipranges) {
		print TAB "IPRANGE\t$_->{STARTIP}\t$_->{ENDIP}\n";
	}

	foreach (@$data) {
		my $record = $_;
		next if not defined $_;
		my $node = $record->{NODE};
		my $ip = $record->{IPADDR};
		my $mac = $record->{HWADDR};
		my $bootprot = $record->{BOOTPROT};
		my $master = $record->{MASTER};
		my $bootdev = $record->{BOOTDEV};

		print TAB "$node\t$ip\t$mac\t$bootprot\t$master";
		print TAB "\t$bootdev" if defined $bootdev;
		print TAB "\n";
	}
	close TAB;
}

sub update_masterlist {
	my $records = shift;

	my $data = $records->{DATA};
	my $commaflag;

	if  (! (-f "/proc/cluster/clms_masterlist") ) {
		return;
	}

	open LIST, "> /proc/cluster/clms_masterlist" or die "Can't open /proc/cluster/clms_masterlist: $!\n";
	foreach (@$data) {
		if ($_->{MASTER}) {
			if ($commaflag) {
				print LIST ",";
			} else {
				$commaflag = 1;
			}
			print LIST "$_->{NODE}:$_->{IPADDR}";
		}
	}
	print LIST "\n";
}

# SSI_XXX: should be sure all IP addrs are on the same subnet
sub write_boottab {
	my $records = shift;
	my $mask = shift;

	my $data = $records->{DATA};
	my $commaflag;

	foreach (@$data) {
		if ($_->{MASTER}) {
			if ($commaflag) {
				print ",";
			} else {
				$commaflag = 1;
			}
			print "$_->{NODE}:$_->{IPADDR}";
		}
	}
	print "\n";

	foreach (@$data) {
		my $bootdev = $_->{BOOTDEV};
		print "$_->{NODE}/$_->{HWADDR}/$_->{IPADDR}:";
		print "$mask" if $_->{IPADDR} ne '<DHCP>';
		print "/$bootdev" if $bootdev;
		print "\n";
	}
}

sub read_ifconfig {
	my @interfaces;

	local $_;

	# should be no need to normalize output from ifconfig command
	open IFCONFIG, "LC_ALL='C' /sbin/ifconfig |" or die;
	while (<IFCONFIG>) {
		my %interface;

		/^(\S+).*HWaddr (([0-9A-F]{2}:){5}[0-9A-F]{2})/ or next;
		$interface{NAME} = $1;
		$interface{HWADDR} = $2;

		$_ = <IFCONFIG>;
		/inet addr:([\d\.]+)\s+Bcast:([\d\.]+)\s+Mask:([\d\.]+)\s+/
			or next;

		unless (validate_ipaddr($1)) {
			warn "configured IP address for " .
				"$interface{NAME} is invalid: $1\n";
			next;
		}
		unless (validate_ipaddr($2)) {
			warn "configured broadcast address for " .
				"$interface{NAME} is invalid: $3\n";
			next;
		}
		unless (validate_ipaddr($3)) {
			warn "configured netmask for " .
				"$interface{NAME} is invalid: $3\n";
			next;
		}

		$interface{IPADDR} = $1;
		$interface{BCAST} = $2;
		$interface{NETMASK} = $3;
		$interface{NETADDR} = getnet($1, $3);

		push @interfaces, \%interface;
	}
	close IFCONFIG;

	return \@interfaces;
}

sub match_ifconfig {
	my $records = shift;

	my $data = $records->{DATA};
	my $clustertab = $records->{TAB};

	my $interfaces = read_ifconfig();
	my $interface;
	my $record;

	foreach $interface (@$interfaces) {
		foreach (@$data) {
			if ($_->{HWADDR} eq $interface->{HWADDR}) {
				$record = $_;
				last;
			}
		}
		next unless $record;

		my $ipaddr = $interface->{IPADDR};
		die "Field 2 (IP address) of line $record->{LINE} of " .
			"$clustertab disagrees with\n" .
			"\tcurrently configured address ($ipaddr)\n"
			unless $record->{IPADDR} eq $ipaddr
				or $record->{IPADDR} eq '<DHCP>';

		my $mask = $interface->{NETMASK};
		my $net = $interface->{NETADDR};

		foreach (@$data) {
			next if $_->{IPADDR} eq '<DHCP>';
			die "Field 2 (IP address) of line $_->{LINE} of " .
				"$clustertab is on a different network\n"
				unless getnet($_->{IPADDR}, $mask) eq $net
					or $_->{NODE} == 0;
		}

		return $interface;
	}
	die "Local hardware address not found in $clustertab\n" unless $record;
}

1;
