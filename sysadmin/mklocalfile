#!/bin/bash 
#
# Authors : Aneesh Kumar K.V <aneesh.kumar@digital.com>
#	    Brian J. Watson <Brian.J.Watson@hp.com>
#
#      This program is free software; you can redistribute it and/or
#      modify it under the terms of the GNU General Public License as
#      published by the Free Software Foundation; either version 2 of
#      the License, or (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
#      or NON INFRINGEMENT.  See the GNU General Public License for more
#      details.

CLUSTER_DIR=/cluster
TEMPLATE_DIR="$CLUSTER_DIR/nodetemplate"
cdsl=0
clusterwide=0
mode="nomode"
directory=2

usage () {
	cat >&2 <<EOF
$0 [ --mode mode --type file|directory ] [ --help ] file
	--help			This message
	--mode			Mode for new file or directory
	--type			Create new 'file' or 'directory'
	file			File to convert
EOF
}

[ $EUID -eq 0 ] || { 
	echo "$0: ERROR: Please run this command as root" >&2; 
	exit 1; 
}

# Getopt portion copied from example file
TEMP=`getopt -o m:,t:,h --long mode:,type:,help  -- "$@"`
echo $0 | grep mklocalfile >/dev/null && cdsl=1 || clusterwide=1

if [ $? != 0 ] ; then 
	echo "$0: ERROR: cannot parse arguments" >&2
	usage
	exit 1
fi

# Note the quotes around `$TEMP': they are essential!
eval set -- "$TEMP"

while true ; do
	case "$1" in
		-h|--help)
			usage
			exit 0
			;;
		-m|--mode) 
			mode=$2
			shift 2;;
		-t|--type) 
			if [ x$2 = xdirectory ];  then
				directory=1
			elif [ x$2 = xfile ];  then
				directory=0
			else
				echo -n "$0: ERROR: " >&2
				echo "--type must be 'file' or 'directory'" >&2
				usage
				exit 1
			fi 
			shift 2;;
		--) shift ; break ;;
		*) 
			echo "$0: ERROR: cannot parse arguments" >&2; 
			usage; 
			exit 1 
			;;
	esac
done

# We support only one file name at a time 
name=$1 
if [ -z "$name" ]; then
	echo "$0: ERROR: a filename is a required argument" >&2
	usage
	exit 1
fi

if [ $cdsl -eq 1 ]; then 
	if [ -h $name ]; then 
		echo -n "$0: WARNING: File $name is a symbolic link. " >&2
		echo "Doing nothing." >&2
		exit 0
	fi
	dir_name=`dirname $name`
	if [  -e  $name ]; then
		cd $dir_name
		# Get the fully qualified name
		dir_name=`pwd`
		name=`basename $name`
		#remove multiple / in the beginning
		name=`echo "$dir_name/$name" | sed 's/\/\/*/\//g'`

		if [ -e $TEMPLATE_DIR/$name ]; then
			echo -n "$0: ERROR: $TEMPLATE_DIR/$name already " >&2
			echo "exists. Cannot make a CDSL." >&2
			exit 1
		fi

		# Now check whether it is a mount point 
		grep -v '^#' /etc/fstab | awk '{print $2 }' | 
			grep ^${name}$ >/dev/null
		if [ $? -eq 0 ]; then
			echo -n "$0: ERROR: $name seems to be mount point." >&2
			echo " Cannot make a CDSL." >&2
			exit 1
		fi

		echo "Converting $name into a CDSL ....."
		mkdir -p $TEMPLATE_DIR/$dir_name
		mv $name $TEMPLATE_DIR/$name
	else 
		if [ x$mode = xnomode ] || [ $directory -eq 2 ]; then 
			echo -n "$0: ERROR: File does not exist. " >&2
			echo "To create one, specify --mode and --type" >&2
			usage
			exit 1
		fi

		if [ -e $TEMPLATE_DIR/$name ]; then
			echo -n "$0: ERROR: $TEMPLATE_DIR/$name already " >&2
			echo "exists. Cannot make a CDSL." >&2
			exit 1
		fi

		if [ $directory -eq 1 ]; then
			echo "Making $name a directory CDSL..."
			mkdir -p $TEMPLATE_DIR/$name
			#now set the permissions
			chmod $mode $TEMPLATE_DIR/$name
		else
			echo "Making $name a file CDSL..."
			mkdir -p $TEMPLATE_DIR/$dir_name
			touch $TEMPLATE_DIR/$name
			#now set the permissions
			chmod $mode $TEMPLATE_DIR/$name
		fi

	fi

	# Now copy the files to all already existing node directories
	for i in $CLUSTER_DIR/node*[0-9]
	do
		if [ ! -d $i ]; then 
			continue
		fi
		#If it doesn't exist create it
		if [ ! -e $i/$name ]; then
			mkdir -p $i/$dir_name
			cp -a $TEMPLATE_DIR/$name $i/$name
		fi
	done

	# Now create the symlink
	ln -sf  $CLUSTER_DIR/node{nodenum}/$name  $name
	
	# Change the owner and mode of link file
	mode=`stat --format %a $TEMPLATE_DIR/$name`
	userid=`stat --format %u $TEMPLATE_DIR/$name`
	groupid=`stat --format %g $TEMPLATE_DIR/$name`
	chown $userid:$groupid $name
fi

if [ $clusterwide -eq 1 ]; then
	if [ -h $name ]; then 
		echo -n "$0: WARNING: File $name is a symbolic link. " >&2
		echo "Doing nothing." >&2
		exit 0
	fi
	if [ -e $CLUSTER_DIR/$name ]; then
		echo -n "$0: WARNING: $CLUSTER_DIR/$name already exists. " >&2
		echo "Doing nothing." >&2
		exit 0
	fi
	if [ ! -e $TEMPLATE_DIR/$name ]; then
		echo "$0: ERROR: cannot find $name in $TEMPLATE_DIR" >&2
		exit 1
	fi
	dir_name=`dirname $name`
	echo "Making $name clusterwide"
	mkdir -p $CLUSTER_DIR/$dir_name
	mv $TEMPLATE_DIR/$name $CLUSTER_DIR/$name

	# Now make a symlink
	ln -sf $CLUSTER_DIR/$name $TEMPLATE_DIR/$name 

	# Change the owner and mode of link file
	mode=`stat --format %a $CLUSTER_DIR/$name`
	userid=`stat --format %u $CLUSTER_DIR/$name`
	groupid=`stat --format %g $CLUSTER_DIR/$name`
	chown $userid:$groupid $TEMPLATE_DIR/$name

	# Now copy the files to all already existing node directories
	for i in $CLUSTER_DIR/node*[0-9]
	do
		if [ ! -d $i/$dir_name ]; then 
			continue
		fi
		if [ -e $i/$name ]; then 
			echo "Saving $i/$name as $i/${name}.ssisave"
			mv $i/$name $i/${name}.ssisave
		fi
		ln -s $CLUSTER_DIR/$name $i/$name 

		# Change the owner and mode of link file
		mode=`stat --format %a $CLUSTER_DIR/$name`
		userid=`stat --format %u $CLUSTER_DIR/$name`
		groupid=`stat --format %g $CLUSTER_DIR/$name`
		chown $userid:$groupid $i/$name
	done
fi
