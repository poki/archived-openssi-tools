#
# Date   :     April 02 2003
# Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
#
#
#
#      This program is free software; you can redistribute it and/or
#      modify it under the terms of the GNU General Public License as
#      published by the Free Software Foundation; either version 2 of
#      the License, or (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
#      or NON INFRINGEMENT.  See the GNU General Public License for more
#      details.
#
#
#

package ssi_arch; 
use strict;

# Do NOT use any SSI modules, except ssi_utils
use lib "/cluster/lib";
use ssi_utils;

our $arch;

BEGIN {
	$arch = "alpha";
	our @ISA = qw(Exporter);
	our @EXPORT = qw(
				$arch

				read_initrds
			);
}

#FIXME!! This will break since ssi-ksync will not be able to handle this
# I guess we may need to rewrite ssi-ksync*
sub read_initrds {
	my $bootloader = shift;

	my ($input_line,@initrds,@this_initrd);
	open(ABOOT_CONF , $bootloader) ||
		die("Unable to open $bootloader");

	while (! eof(ABOOT_CONF)) {
		$input_line = <ABOOT_CONF>;
		if ( $input_line !~ /initrd=(\S+)/ ) {
			next;
		}

		@this_initrd =  $1;
PROMPT:	print "Do you wish to modify this initrd $this_initrd[0] (y/n) [n]: ";
		my $selection = uc trim <STDIN>;
		if( $selection eq 'Y' ) {
			push @initrds, $this_initrd[0];
			next;
		}
		goto PROMPT unless $selection eq 'N' or $selection eq '';
	}
	close ABOOT_CONF;
	return \@initrds;
}

1;
