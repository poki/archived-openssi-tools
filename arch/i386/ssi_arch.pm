#
# Date   :      April 02 2003
# Authors:      Brian Watson <Brian.J.Watson@compaq.com>
#		Aneesh Kumar K.V ( aneesh.kumar@digital.com )
#
#
#
#      This program is free software; you can redistribute it and/or
#      modify it under the terms of the GNU General Public License as
#      published by the Free Software Foundation; either version 2 of
#      the License, or (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
#      or NON INFRINGEMENT.  See the GNU General Public License for more
#      details.
#
#
#
#
#

package ssi_arch; 
use strict;

# Do NOT use any SSI modules, except ssi_utils

our $arch;

BEGIN {
	$arch = "i386";
	our @ISA = qw(Exporter);
	our @EXPORT = qw(
				$arch

				read_initrds
			);
}

sub read_initrds {
	my $bootloader = shift;

	if ( $bootloader =~ /lilo.conf$/ ) {
		print '
Automatic updating of boot images is not supported with the LILO bootloader.
You will have to manually rebuild the SSI initrds and sync them to all
nodes with local boot devices. Also, consider using the GRUB bootloader.

';
		return undef;
	}

	my @initrds;
	if ( -f "/cluster/kernel_version" ) {
		open VERSION_FILE, "< /cluster/kernel_version" 
			or die "Cannot open /cluster/kernel_version \n";
		while (<VERSION_FILE>) {
			# build the initrd with version number present 
			# in the above file
			my $rd="/boot/initrd.img-".$_;
			push @initrds, $rd;
		}
		close VERSION_FILE;

	} else {

		open GRUB, "< $bootloader" or die "Can't open $bootloader: $!\n";
		while (<GRUB>) {
			/^\s+initrd\s+(\S+)/ or next;
			my $rd = $1;
			my @x = split(/\//, $rd);
			$rd = $x[@x-1];
			$rd = "/boot/$rd";
			push @initrds, $rd;
		}
		close GRUB;
	}
	return \@initrds;
}

1;
