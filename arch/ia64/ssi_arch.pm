#
# Date   :     April 02 2003
# Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
#
#
#
#      This program is free software; you can redistribute it and/or
#      modify it under the terms of the GNU General Public License as
#      published by the Free Software Foundation; either version 2 of
#      the License, or (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
#      or NON INFRINGEMENT.  See the GNU General Public License for more
#      details.
#
#
#
#
#

package ssi_arch; 
use strict;

# Do NOT use any SSI modules, except ssi_utils
our $arch;

BEGIN {
	$arch = "ia64";
	our @ISA = qw(Exporter);
	our @EXPORT = qw(
				$arch 

				read_initrds 
			);
}

sub read_initrds {
	my $bootloader = shift;

	print "You need to manually rebuild the SSI initrds and tftp images\n";
	return undef;
}

1;
