.TH SSI-CREATE 8ssi  "06 January 2004"  ""  "OpenSSI Prog Manual"
.SH NAME
ssi-create \-  creates a SSI cluster
.SH SYNOPSIS
.nf
.sp
.BI "ssi-create [" OPTIONS "]"
.fi
.SH DESCRIPTION
\fBssi-create\fP creates a single node SSI cluster.
.TP 
This command does the following :
.TP
Creates the \fI/cluster/node#\fP directory
.TP 
Creates symlinks under \fI/cluster/node#\fP 
.TP 
Creates cdsl's for \fI/etc/sysconfig/network-scripts\fP, \fI/var/run\fP, \fI/var/lock\fP, \fI/var/log\fP, \fI/etc/nodename\fP, \fI/var/lib/nfs/statd\fP, \fI/var/lib/random-seed\fP. In the presence of \fI--nfs\fP option \fI/var/lib/nfs/statd\fP is made a \fBcdsl\fP. 
.TP 
Creates the \fI/cluster/node#/etc/nodename\fP file with node name read from the \fBHOSTNAME\fP entry in \fI/etc/sysconfig/network\fP. The \fBHOSTNAME\fP entry in \fI/etc/sysconfig/network\fP is assigned to the name in \fB/etc/nodename\fP. 
.TP 
Cluster name is read from stdin, and written into \fI/etc/clustername\fP.
.TP 
Enables \fBdhcpd\fP and \fBtftpd\fP.
.TP 
Disables \fBkudzu\fP as it dosen't work well with a global root on RedHat systems.
.TP 
Enables \fBha-lvs\fP so that once \fI/etc/cvip.conf\fP is created it will start.
.TP 
Enables \fBloadlevel\fP so that once \fI/etc/sysconfig/loadlevellist\fP is created it will start. 
.TP 
If node name not found in \fI/cluster/node#/etc/nodename\fP, user is prompted to enter a node name.
.TP 
Adds new node entries to \fI/etc/clustertab\fP.
.TP 
Adds node entries to \fI/etc/fstab\fP file.
.TP 
It asks the user whether or not to enable root filesystem failover. Your root filesystem must be on a shared disk hardware to enable failover.
.TP
After creation of a single node SSI cluster using ssi-create(8ssi), One can add more nodes to the cluster using ssi-addnode(8ssi).

.SH "OPTIONS"
Options can be any of, 
.TP 
.B "--help"
Prints help message 
.TP 
.B "--verbose"
Verbose output 
.TP 
.BI "--nodenum=" nodenum         
Node number of the root node that is created with this command. Default is 1.
.TP 
.BI "--ifname=" value
Interface to cluster interconnect (e.g., eth0) 
.TP 
.BI "--bootprot=" value      
(E)therboot or (P)XE network boot protocol
.TP 
.BI "--nfs=" value     
NFS support. Default is "yes"
.TP 
.BI "--clustertab=" value      
Cluster node table (default: /etc/clustertab)

.SH "ERRORS"
You should be a root user to run this command. 

.SH "FILES"
/etc/clustername

/etc/nodename

.SH "SEE ALSO"
.BR cluster (8ssi),
.BR ssi-create (8ssi),
.BR cluster_maxnodes (3ssi),
.BR cluster_membership (3ssi),
.BR cluster_ssiconfig (3ssi),
.BR clusternode_num (3ssi)
